//// Convert a 1D limit plot into HEP data format (YAML) 
//// J. Haley <joseph.haley@cern.ch>
//// 2017 April 14

// #include <TFile>
// #include <TGraphAsymmErrors>
// #include <string>
// #include <stdio>

using namespace std;

// string lumi = "36.1"; // integrated luminosity
// string cme = "13000"; // center of mass energy [GeV]

void dump_TH2(TH2* h, ofstream& myfile, string limit_type="Observed", TH2* hdn = 0, TH2* hup = 0){

  int nx = h->GetNbinsX();
  int ny = h->GetNbinsY();
  
  myfile << "- header: {name: \'Lower limit on H++ mass at 95% CL\', units: GeV}" << endl;
  myfile << "  qualifiers:" << endl;
  myfile << "  - {name: Limit, value: " << limit_type << "}" << endl;
  myfile << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  myfile << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  myfile << "  values:" << endl;

  for(int i = 0; i < nx; ++i){
    for(int j = 0; j < ny; ++j){
      float val = h->GetBinContent(i+1,j+1);
      if(val>0) {
        myfile << "  - value: " << val << "" << endl;
        if (hdn and hup){
          myfile << "    errors: " << endl;
          myfile << "    - {asymerror: {plus: " << hup->GetBinContent(i+1,j+1)-val << ", minus: " << hdn->GetBinContent(i+1,j+1)-val << "}, label: '1 sigma'}" << endl;
        }
      }
      else {
        myfile << "  - {value: \'-\'}" << endl;
      }
    }
  }
}


void Limit2D_to_YAML(string fn1 = "2D_TTS_BRscan_HEP.root", string folder = "", bool error = false){
  
  TFile *_file1 = TFile::Open(fn1.c_str());
  
  TH2* h_expected = 0;
  TH2* h_observed = 0;

  h_expected    = (TH2*)_file1->Get( (folder+string("/Expected")).c_str() );
  h_expectedup  = (TH2*)_file1->Get( (folder+string("/Expected1up")).c_str() );
  h_expecteddn  = (TH2*)_file1->Get( (folder+string("/Expected1dn")).c_str() );
  h_observed    = (TH2*)_file1->Get( (folder+string("/Observed")).c_str() );
  
  assert(h_expected);
  assert(h_observed);
  assert(h_expectedup);
  assert(h_expecteddn);

  TH2* h = h_expected;

  int nx = h->GetNbinsX();
  int ny = h->GetNbinsY();
  
  ofstream myfile;
  string outfilename = "yaml/"+folder+".yaml";
  myfile.open (outfilename);

  myfile << "independent_variables:" << endl;

  //myfile << "- header: {name: BR(T->Wb)}" << endl;
  myfile << "- header: {name: BR(H -> ee)}" << endl;
  myfile << "  values:" << endl;
  for(int i = 0; i < nx; ++i){
    for(int j = 0; j < ny; ++j){
    myfile << "  - {value: " << float(i)/(nx-1) << "}" << endl;
    }
  }

  //myfile << "- header: {name: BR(T->Ht)}" << endl;
  myfile << "- header: {name: BR(H -> mm)}" << endl;
  myfile << "  values:" << endl;
  for(int i = 0; i < nx; ++i){
    for(int j = 0; j < ny; ++j){
      myfile << "  - {value: " << float(j)/(ny-1) << "}" << endl;
    }
  }
  
  myfile << "dependent_variables:" << endl;

  if (error)
    dump_TH2(h_expected, myfile, "Expected", h_expecteddn, h_expectedup);
  else
    dump_TH2(h_expected, myfile, "Expected"); 
  dump_TH2(h_observed, myfile, "Observed");

  myfile.close();

}