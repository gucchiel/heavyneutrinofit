//// Convert a 1D limit plot into HEP data format (YAML) 
//// J. Haley <joseph.haley@cern.ch>
//// 2017 April 14

// #include <TFile>
// #include <TGraphAsymmErrors>
// #include <string>
// #include <stdio>

using namespace std;

string lumi = "36.1"; // integrated luminosity
string cme = "13000"; // center of mass energy [GeV]

void dump_limit(TGraph* g, ofstream& myfile, string limit_type="Observed",bool type = true){
  int nbins = g->GetN();

  if (type)
    myfile << "- header: {name: \'Cross section upper limit at 95% CL\', units: fb}" << endl;
  else
      myfile << "- header: {name: Mass, units: GeV}" << endl;
  myfile << "  qualifiers:" << endl;
  myfile << "  - {name: Limit, value: " << limit_type << "}" << endl;
  myfile << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  myfile << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  myfile << "  values:" << endl;
  
  for(int i = 0; i < nbins; ++i){
    myfile << "  - {value: " << g->GetY()[i] << "}" << endl;
  }
}

void dump_expected_pm_1and2sigma(TGraphAsymmErrors* g1, TGraphAsymmErrors* g2, ofstream& myfile, bool type = true){
  int nbins = g1->GetN();

  // + error band
  if (type)
    myfile << "- header: {name: \'Cross section upper limit at 95% CL\', units: fb}" << endl;
  else
      myfile << "- header: {name: Mass, units: GeV}" << endl;
  myfile << "  qualifiers:" << endl;
  myfile << "  - {name: Limit, value: Expected}" << endl;
  myfile << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  myfile << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  myfile << "  values:" << endl;

  for(int i = 0; i < nbins; ++i){
    myfile << "  - value: " << g1->GetY()[i] << endl;
    myfile << "    errors: " << endl;
    myfile << "    - {asymerror: {plus: " << g1->GetEYhigh()[i] << ", minus: " << -g1->GetEYlow()[i] << "}, label: '1 sigma'}" << endl;
    myfile << "    - {asymerror: {plus: " << g2->GetEYhigh()[i] << ", minus: " << -g2->GetEYlow()[i] << "}, label: '2 sigma'}" << endl;
  }
}

void Limit1D_to_YAML(string filename, string folder, bool type = true){
  
  
  TFile *_file1 = TFile::Open(filename.c_str());
  
  TGraph*               g_expected = 0;
  TGraphAsymmErrors*    gae_1sigma = 0;
  TGraphAsymmErrors*    gae_2sigma = 0;
  TGraph*               g_observed = 0;

  g_expected    = (TGraph*)_file1->Get( (folder+string("/expected")).c_str() );
  gae_1sigma    = (TGraphAsymmErrors*)_file1->Get( (folder+string("/1sigma" )).c_str() );
  gae_2sigma    = (TGraphAsymmErrors*)_file1->Get( (folder+string("/2sigma" )).c_str() );
  g_observed    = (TGraph*)_file1->Get( (folder+string("/observed")).c_str() );
  
  if(!g_expected){
    // alternative naming
    g_expected    = (TGraph*)_file1->Get("Graph3");
    gae_2sigma    = (TGraphAsymmErrors*)_file1->Get("Graph1");
    gae_1sigma    = (TGraphAsymmErrors*)_file1->Get("Graph2");
    g_observed    = (TGraph*)_file1->Get("Graph4");
  }
  
  assert(g_expected);
  assert(gae_1sigma);
  assert(gae_2sigma);
  assert(g_observed);

  int nbins = g_expected->GetN();

  TGraph* g = g_expected;

  ofstream myfile;
  string outfilename = "yaml/"+folder+".yaml";
  myfile.open (outfilename);
  myfile << "independent_variables:" << endl;
  if (type)
    myfile << "- header: {name: Mass, units: GeV}" << endl;
  else
      myfile << "- header: {name: Branching ratio, units: percent}" << endl;
  myfile << "  values:" << endl;
  for(int i = 0; i < nbins; ++i){
    myfile << "  - {value: " << g->GetX()[i] << "}" << endl;
  }
  
  myfile << "dependent_variables:" << endl;

  dump_limit(g_observed, myfile, "Observed", type);
  dump_expected_pm_1and2sigma(gae_1sigma, gae_2sigma, myfile, type);

  myfile.close();

}