from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy

ROOT.gStyle.SetPaintTextFormat("2.2f");

ROOT.gROOT.SetBatch(True)

# ROOT.gStyle.SetPalette(ROOT.kBeach)

NRGBs = 5
NCont = 255

stops = array('d',[ 0.00, 0.34, 0.61, 0.87, 1.0 ])
red   = array('d',[ 207./255., 207./255., 174./255., 205./255., 1.00 ])
green = array('d',[ 183./255., 174./255., 198./255., 255./255., 1.00 ])
blue  = array('d',[ 174./255., 198./255., 207./255., 210./255., 1.00 ])

minimumValue = 999

tempGrYellow = ROOT.TH1F("yellow","yellow",1,0,1)
tempGrYellow.SetFillColor(5)
tempGrYellow.SetLineColor(1)
tempGrYellow.SetLineWidth(1)

tempGrRed = ROOT.TH1F("red","red",1,0,1)
tempGrRed.SetLineColor(ROOT.kRed)
tempGrRed.SetLineWidth(1)

tempGrGreen = ROOT.TH1F("green","green",1,0,1)
tempGrGreen.SetFillColor(8)
tempGrGreen.SetLineColor(1)
tempGrGreen.SetLineWidth(1)

lumi =  36097.56


basefolder = "HistFitter_HN_combined_036_SSONLY"

backup_ee = "HistFitter_HN_v2_250"

NTUPLEDIR = "/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/same_sign/HeavyNeutrino/EXOT12/"

SSONLY = False

signalSamples = [
[600,50,51.02],
[600,150,49.713],
[600,300,37.33],
[600,450,16.138],
[600,500,8.7848],
[600,900,0.015643],
[600,1200,0.0031095],
[1200,50,3.2448],
[1200,300,3.1065],
[1200,600,2.3153],
[1200,900,0.98898],
[1200,1100,0.17725],
[1200,1800,0.0004383],
[1200,2400,5.162e-05],
[1800,50,0.50828],
[1800,450,0.47505],
[1800,900,0.3507],
[1800,1350,0.14888],
[1800,1700,0.01393],
[1800,2700,2.9008e-05],
[1800,3600,1.9213e-06],
[2400,50,0.11263],
[2400,600,0.10148],
[2400,1200,0.074743],
[2400,1800,0.031435],
[2400,2300,0.001897],
[2400,3600,2.5613e-06],
[2400,4800,9.7783e-08],
[3000,50,0.030543],
[3000,750,0.026238],
[3000,1500,0.018827],
[3000,2250,0.0078485],
[3000,2900,0.00033845],
[3000,4500,2.5493e-07],
[3000,6000,7.3718e-09],
[3500,50,0.011405],
[3500,875,0.0091305],
[3500,1750,0.0064078],
[3500,2625,0.002618],
[3500,3400,9.027e-05],
[3500,5250,3.984e-08],
[3500,7000,1.3538e-09],
[3600,50,0.0094965],
[3600,900,0.0073963],
[3600,1800,0.005172],
[3600,2700,0.0021193],
[3600,3500,7.013e-05],
[3600,5400,2.7605e-08],
[3600,7200,1.0035e-09],
[4200,50,0.0033228],
[4200,1050,0.0022143],
[4200,2100,0.0014893],
[4200,3150,0.00059705],
[4200,4100,1.557e-05],
[4200,6300,3.4015e-09],
[4200,8400,2.1228e-10],
[600,600,0.36365],
[600,700,0.079408],
[1000,700,2.7778],
[1000,800,1.5465],
[1000,1000,0.044133],
[1000,1100,0.015058],
[1200,1200,0.019283],
[1200,1500,0.001777],
[1500,1000,0.5361],
[1500,1100,0.39588],
[1500,1200,0.25818],
[1800,1500,0.079323],
[2400,1900,0.023783],
[2400,2100,0.010368],
[3000,1600,0.017545],
[3000,1800,0.014728],
[3000,30,0.032707],
[3000,150,0.029708],
[3000,300,0.02845],
[4000,400,0.0038988],
[5000,500,0.00068688],
[4500,50,0.0020453],
[4500,1125,0.0012275],
[4500,2250,0.00080206],
[4500,3375,0.00031672],
[4500,4400,7.4606e-06],
[5000,50,0.0010374],
[5000,1250,0.00047349],
[5000,2500,0.00028817],
[5000,3750,0.00011102],
[5000,4900,2.2329e-06],
]

Np = 255
N95MAX = 20

# for channel in ["ee"]:
for channel in ["ee","mm"]:

  print "running for channel: ", channel

  contDN2c = []
  contDNc  = []
  contUPc  = []
  contUP2c = []
  contc    = []
  contObsc = []

  # for neutrino in ["","Dirac"]:
  for neutrino in [""]:

    print "running for neutrino: ", (neutrino if neutrino != "" else "Majorana")

    underscore = "_" if neutrino != "" else ""

    p = subprocess.Popen(['mkdir -p limits_'+basefolder],shell=True,stdout=subprocess.PIPE)
    output, errors = p.communicate()
    # print output


    contour       = ROOT.TGraph2D()
    contour.SetPoint(contour.GetN(),5500,5500,1e6)
    contour.SetPoint(contour.GetN(),-1000,5500,1e6)
    contour.SetPoint(contour.GetN(),5500,-1000,1e6)
    contour.SetPoint(contour.GetN(),-1000,-1000,1e6)

    contourObs       = ROOT.TGraph2D()
    contourObs.SetPoint(contourObs.GetN(),5500,5500,1e6)
    contourObs.SetPoint(contourObs.GetN(),-1000,5500,1e6)
    contourObs.SetPoint(contourObs.GetN(),5500,-1000,1e6)
    contourObs.SetPoint(contourObs.GetN(),-1000,-1000,1e6)

    contourUP       = ROOT.TGraph2D()
    contourUP.SetPoint(contourUP.GetN(),5500,5500,1e6)
    contourUP.SetPoint(contourUP.GetN(),-1000,5500,1e6)
    contourUP.SetPoint(contourUP.GetN(),5500,-1000,1e6)
    contourUP.SetPoint(contourUP.GetN(),-1000,-1000,1e6)

    contourDN       = ROOT.TGraph2D()
    contourDN.SetPoint(contourDN.GetN(),5500,5500,1e6)
    contourDN.SetPoint(contourDN.GetN(),-1000,5500,1e6)
    contourDN.SetPoint(contourDN.GetN(),5500,-1000,1e6)
    contourDN.SetPoint(contourDN.GetN(),-1000,-1000,1e6)

    contourUP2       = ROOT.TGraph2D()
    contourUP2.SetPoint(contourUP2.GetN(),5500,5500,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),-1000,5500,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),5500,-1000,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),-1000,-1000,1e6)

    contourDN2       = ROOT.TGraph2D()
    contourDN2.SetPoint(contourDN2.GetN(),5500,5500,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),-1000,5500,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),5500,-1000,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),-1000,-1000,1e6)

    results2D    = ROOT.TH2D("results2D"+channel+neutrino,"results2D"+channel+neutrino         ,5500,0,5500,6500,-500,6000)
    results2DN   = ROOT.TH2D("results2DN"+channel+neutrino,"results2DN"+channel+neutrino       ,5500,0,5500,6500,-500,6000)
    #EffHisto     = ROOT.TH2D("EffHisto"+channel+neutrino,"EffHisto"+channel+neutrino           ,5500,0,5500,8500,-500,8000)
    contourN     = ROOT.TGraph2D()
    #EfficiencyGr = ROOT.TGraph2D()


    for sample in signalSamples:

      # if channel=="ee" and (sample[0],sample[1]) == (600,300):
      #   continue

      folder = "/afs/cern.ch/work/t/tadej/guilia/FitSSOSComb/HistFitter/"
      string = folder+"results"+"/"

      # print sample[0]," ",sample[1]

      #fileEff = ROOT.TFile(NTUPLEDIR + "HN_v2_052_SYS/nominal/MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s.root"% (sample[0],sample[1]),"READ")
      #SRcutflow = fileEff.Get("cutflow_weighted_%s-SS-Z-SR" % ("electron" if channel == "ee" else "muon"))
      #if not SRcutflow:
       # print "not ",  sample[0]," ",sample[1]
      #eff = 6*SRcutflow.GetBinContent(SRcutflow.GetNbinsX())/fileEff.Get("cutflow").GetBinContent(1)
      # print eff
      #EfficiencyGr.SetPoint(EfficiencyGr.GetN(),sample[0],sample[1],eff)
      #EffHisto.Fill(sample[0],sample[1],eff)

      name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])      
      if not os.path.isfile(string+"SameSignHN-WR%s_NR%s_Sig_%s-excl_Output_upperlimit.root" % (sample[0],sample[1],channel)):
        results2D.Fill(sample[0],sample[1],-1)
        print name," not available file.."
        continue

      file = ROOT.TFile(string+"SameSignHN-WR%s_NR%s_Sig_%s-excl_Output_upperlimit.root" % (sample[0],sample[1],channel),"READ")
      result = file.Get(name)

      # print result
      if result and result.GetExpectedUpperLimit(0) < minimumValue:
        minimumValue = result.GetExpectedUpperLimit(0)

      if result:
        # print "WR: ",sample[0]," NR: ",sample[1]," ratio: ",sample[0]/float(sample[1])," ul: ",result.GetExpectedUpperLimit(0)
        contourObs.SetPoint(contourObs.GetN(),sample[0],sample[1],result.UpperLimit())
        contour.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0))
        contourUP.SetPoint(contourUP.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(1))
        contourDN.SetPoint(contourDN.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-1))
        contourUP2.SetPoint(contourUP2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(2))
        contourDN2.SetPoint(contourDN2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-2))
        results2D.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0))
        results2DN.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*sample[2]/6.)
        # if (result.GetExpectedUpperLimit(0)*lumi*sample[2]/6. <= N95MAX ):
        contourN.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*sample[2]/6.)
      else:
        #print name," not available LIMIT.. EFF: ",eff," ",sample[0]," ",sample[1]
        print name," not available  ",sample[0]," ",sample[1]
        results2D.Fill(sample[0],sample[1],-2)

      file.Close()

    contour.SetNpx( Np )
    contour.SetNpy( Np )
    contour.SetMinimum( minimumValue )
    contour.SetMaximum( 1.0 )


    contourObs.SetNpx( Np )
    contourObs.SetNpy( Np )
    contourObs.SetMinimum( minimumValue )
    contourObs.SetMaximum( 1.0 )

    #EfficiencyGr.SetNpx( Np )
    #EfficiencyGr.SetNpy( Np )
    #EfficiencyGr.SetMinimum( 1./1000 )
    #EfficiencyGr.SetMaximum( 0.5 )

    contourUP.SetNpx( Np )
    contourUP.SetNpy( Np )
    contourUP2.SetNpx( Np )
    contourUP2.SetNpy( Np )

    contourDN.SetNpx( Np )
    contourDN.SetNpy( Np )
    contourDN2.SetNpx( Np )
    contourDN2.SetNpy( Np )

    exclusionLine = contour.GetHistogram().Clone("exclusionLine"+channel+neutrino)
    exclusionLine.SetContour(1)
    exclusionLine.SetContourLevel(0,2 if neutrino == "Dirac" else 1)
    exclusionLine.SetLineColor(1)
    exclusionLine.SetLineStyle(1)
    exclusionLine.SetLineWidth(2)

    exclusionLineObs = contourObs.GetHistogram().Clone("exclusionLineObs"+channel+neutrino)
    exclusionLineObs.SetContour(1)
    exclusionLineObs.SetContourLevel(0,2 if neutrino == "Dirac" else 1)
    exclusionLineObs.SetLineColor(1)
    # exclusionLineObs.SetLineStyle(1)
    exclusionLineObs.SetLineWidth(2)

    exclusionLineUP = contourUP.GetHistogram().Clone("exclusionLineUP"+channel+neutrino)
    exclusionLineUP.SetContour(1)
    exclusionLineUP.SetContourLevel(0,2 if neutrino == "Dirac" else 1)
    exclusionLineUP.SetLineColor(8)
    exclusionLineUP.SetLineWidth(2)

    exclusionLineDN = contourDN.GetHistogram().Clone("exclusionLineDN"+channel+neutrino)
    exclusionLineDN.SetContour(1)
    exclusionLineDN.SetContourLevel(0,2 if neutrino == "Dirac" else 1)
    exclusionLineDN.SetLineColor(8)
    exclusionLineDN.SetLineWidth(2)

    exclusionLineUP2 = contourUP2.GetHistogram().Clone("exclusionLineUP2"+channel+neutrino)
    exclusionLineUP2.SetContour(1)
    exclusionLineUP2.SetContourLevel(0,2 if neutrino == "Dirac" else 1)
    exclusionLineUP2.SetLineColor(8)
    exclusionLineUP2.SetLineWidth(2)

    exclusionLineDN2 = contourDN2.GetHistogram().Clone("exclusionLineDN2"+channel+neutrino)
    exclusionLineDN2.SetContour(1)
    exclusionLineDN2.SetContourLevel(0,2 if neutrino == "Dirac" else 1)
    exclusionLineDN2.SetLineColor(8)
    exclusionLineDN2.SetLineWidth(2)

    leg = ROOT.TLegend(0.50,0.87,0.7,0.91);
    leg.SetBorderSize(0);
    leg.SetFillColor(0);
    leg.SetFillStyle(0);
    leg.SetTextSize(0.045);
    leg.AddEntry(exclusionLine,"#font[42]{Expected 95% CL}","l");
    # leg.AddEntry(exclusionLineUP,"#font[42]{Expected limit #pm 1#sigma}","l");

    # eff
    ROOT.gStyle.SetPalette(ROOT.kBird)
    canvE = ROOT.TCanvas("efficiency","efficiency",800,600)
    canvE.cd()
    canvE.SetRightMargin(0.15)
    #EffHisto.Draw("text");
    #EfficiencyGr.Draw("COLZ same");
    #EffHisto.Draw("text same");
    #EffHisto.SetMarkerSize(1.3)
    #EffHisto.GetZaxis().SetTitle("N_{95%}")
    #EffHisto.GetXaxis().SetTitle("m(W_{R}) [GeV]")
    #EffHisto.GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.49,0.875,1,"efficiency*6 (rescaled)")
    ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    ROOT.myText(0.185,0.725,1,"electron channel (e^{#pm}e^{#pm})" if channel == "ee" else "muon channel (#mu^{#pm}#mu^{#pm})")
    ROOT.gPad.RedrawAxis("g");
    canvE.Print("limits_"+basefolder+"/eff_%s.eps" % (channel+underscore+neutrino))


    # nominal mu_sig
    canv = ROOT.TCanvas("limits"+channel+neutrino,"limits"+channel+neutrino,800,600)
    canv.cd()
    canv.SetRightMargin(0.15)
    results2D.Draw("text");
    line = ROOT.TLine(0, 0, 5500, 5000)
    line.SetLineStyle(2)
    line.SetLineColor(ROOT.kGray)
    line2 = ROOT.TLine(0, 0, 5500, 0)
    line2.SetLineStyle(3)
    line2.SetLineColor(ROOT.kGray)

    contour.Draw("COLZ same")
    exclusionLine.Draw("CONT LIST same")
    canv.Update()
    conts = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    contourObs.Draw("COLZ same")
    exclusionLineObs.Draw("CONT LIST same")
    canv.Update()
    contsObs = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineUP.Draw("CONT LIST same")
    canv.Update()
    contsUP = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineDN.Draw("CONT LIST same")
    canv.Update()
    contsDN = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineUP2.Draw("CONT LIST same")
    canv.Update()
    contsUP2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineDN2.Draw("CONT LIST same")
    canv.Update()
    contsDN2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))

    results2D.Draw("text");
    contour.Draw("COLZ same")
    exclusionLine.Draw("CONT3 same")
    # exclusionLineObs.Draw("CONT3 same")
    exclusionLineUP2.Draw("CONT3 same")
    exclusionLineUP.Draw("CONT3 same")
    exclusionLineDN2.Draw("CONT3 same")
    line .Draw("same")
    line2.Draw("same")
    results2D.Draw("text same")
    ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
    ROOT.gStyle.SetNumberContours(NCont)
    results2D.SetMarkerSize(1.3)
    results2D.GetZaxis().SetTitle("#mu_{95%}")
    results2D.GetXaxis().SetTitle("m(W_{R}) [GeV]")
    results2D.GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    if not SSONLY:
        ROOT.myText(0.185,0.725,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
    else:
        ROOT.myText(0.185,0.725,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
    leg.Draw()
    ROOT.gPad.RedrawAxis("g");
    canv.Print("limits_"+basefolder+"/limits_%s.eps" % (channel+underscore+neutrino))

    cont = conts[0]
    contObs = contsObs[0]
    contUP = contsUP[0]
    contDN = contsDN[0]
    contUP2 = contsUP2[0]
    contDN2 = contsDN2[0]

    canv2 = ROOT.TCanvas("excl","excl",800,600)
    canv2.cd()
    contDN.First().SetFillColor(8)
    contDN2.First().SetFillColor(5)
    contUP.First().SetFillColor(5)
    contUP2.First().SetFillColor(ROOT.kWhite)
    cont.First().SetLineStyle(2)

    leg2 = ROOT.TLegend(0.18,0.61,0.4,0.81);
    leg2.SetBorderSize(0);
    leg2.SetFillColor(0);
    leg2.SetFillStyle(0);
    leg2.SetTextSize(0.045);
    leg2.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg2.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg2.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
    leg2.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

    contDN2.First().Draw("a F")
    contDN.First().Draw("F")
    contUP.First().Draw("same F")
    contUP2.First().Draw("same F")
    cont.First().Draw("l")
    contObs.First().Draw("l")

    contDN2.First().GetYaxis().SetRangeUser(100,4000)
    contDN2.First().GetXaxis().SetRangeUser(700,5000)
    contDN2.First().GetXaxis().SetTitle("m(W_{R}) [GeV]")
    contDN2.First().GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.185,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    line3 = ROOT.TLine(700, 700, 4000, 4000)
    line3.SetLineStyle(2)
    line3.SetLineColor(ROOT.kGray+1)
    line3.Draw()
    # line4 = ROOT.TLine(700, 1050, 2666, 4000)
    # line4.SetLineStyle(2)
    # line4.SetLineColor(ROOT.kGray+1)
    # line4.Draw()
    # line5 = ROOT.TLine(700, 700, 4000, 4000)
    # line5.SetLineStyle(2)
    # line5.SetLineColor(ROOT.kGray+1)
    # line5.Draw()
    if not SSONLY:
        ROOT.myText(0.54,0.875,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
    else:
        ROOT.myText(0.54,0.875,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
    leg2.Draw()
    ROOT.gPad.RedrawAxis("g");
    canv2.Print("limits_"+basefolder+"/exclusion_%s.eps" % (channel+underscore+neutrino))

    contDN2c += [deepcopy(contDN2.First().Clone(contDN2.GetName()+"clone"+channel))]
    contDNc  += [deepcopy(contDN.First() .Clone(contDN .GetName()+"clone"+channel))]
    contUPc  += [deepcopy(contUP.First() .Clone(contUP .GetName()+"clone"+channel))]
    contUP2c += [deepcopy(contUP2.First().Clone(contUP2.GetName()+"clone"+channel))]
    contc    += [deepcopy(cont.First()   .Clone(cont   .GetName()+"clone"+channel))]
    contObsc += [deepcopy(contObs.First().Clone(contObs.GetName()+"clone"+channel))]

    leg3 = ROOT.TLegend(0.2,0.61,0.4,0.81);
    leg3.SetBorderSize(0);
    leg3.SetFillColor(0);
    leg3.SetFillStyle(0);
    leg3.SetTextSize(0.045);
    leg3.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg3.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg3.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
    leg3.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

    leg4 = ROOT.TLegend(0.5,0.60,0.82,0.85);
    leg4.SetBorderSize(0);
    leg4.SetFillColor(0);
    leg4.SetFillStyle(0);
    leg4.SetTextSize(0.045);
    leg4.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg4.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg4.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
    leg4.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");
    leg4.AddEntry(tempGrRed,"#font[42]{#sigma_{KS}}","l");

    
    xaxisTeV = ROOT.TGaxis(ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY1(),ROOT.gPad.GetFrame().GetX2(),ROOT.gPad.GetFrame().GetY1(),XMIN/1000.,XMAX/1000.,10,"")
    xaxisTeV.SetLabelSize(0.045)
    xaxisTeV.SetLabelFont(42)
    xaxisTeV.Draw()

    yaxisTeV = ROOT.TGaxis(ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY1(),ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY2(),YMIN/1000.,YMAX/1000.,10,"")
    yaxisTeV.SetLabelSize(0.045)
    yaxisTeV.SetLabelFont(42)
    yaxisTeV.Draw()

    ROOT.gPad.RedrawAxis("g");
    canvas.Update()

    # for ratio in []:
    for ratio in [2./3,4./3,2.,4.]:

      # print "running 1D limits for ratio: ",ratio

      obs = ROOT.TGraph()
      exp = ROOT.TGraph()
      exp1 = ROOT.TGraphAsymmErrors()
      exp2 = ROOT.TGraphAsymmErrors()
      exp1.SetFillColor(8)
      exp2.SetFillColor(5)
      exp.SetLineStyle(2)
      exp.SetLineWidth(2)
      obs.SetLineWidth(2)

      xsec = ROOT.TGraph()
      obs_xsec = ROOT.TGraph()
      exp_xsec = ROOT.TGraph()
      exp1_xsec = ROOT.TGraphAsymmErrors()
      exp2_xsec = ROOT.TGraphAsymmErrors()
      exp1_xsec.SetFillColor(8)
      exp2_xsec.SetFillColor(5)
      exp_xsec.SetLineStyle(2)
      exp_xsec.SetLineWidth(2)
      obs_xsec.SetLineWidth(2)
      xsec.SetLineWidth(2)
      xsec.SetLineColor(ROOT.kRed)

      Xmin = 10000
      Xmax = 0
      for sample in signalSamples:
        if float(sample[0]) != ratio*sample[1]:
          continue

        file = ROOT.TFile(string+"SameSignHN-WR%s_NR%s_Sig_%s-excl_Output_upperlimit.root" % (sample[0],sample[1],channel),"READ")
        name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])
        result = file.Get(name)

        if not result:
          # print "continue ",sample[0]," ",sample[2]
          continue

        if sample[0] < Xmin:
          Xmin = sample[0]
        if sample[0] > Xmax:
          Xmax = sample[0]

        obs.SetPoint(obs.GetN(),sample[0],result.UpperLimit())
        exp.SetPoint(exp.GetN(),sample[0],result.GetExpectedUpperLimit(0))
        exp1.SetPoint(exp1.GetN(),sample[0],result.GetExpectedUpperLimit(0))
        exp2.SetPoint(exp2.GetN(),sample[0],result.GetExpectedUpperLimit(0))
        exp1.SetPointError(exp1.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-1),result.GetExpectedUpperLimit(1)-result.GetExpectedUpperLimit(0))
        exp2.SetPointError(exp2.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-2),result.GetExpectedUpperLimit(2)-result.GetExpectedUpperLimit(0))

        obs_xsec.SetPoint(obs_xsec.GetN(),sample[0],result.UpperLimit()*sample[2])
        exp_xsec.SetPoint(exp_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
        exp1_xsec.SetPoint(exp1_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
        exp2_xsec.SetPoint(exp2_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
        exp1_xsec.SetPointError(exp1_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*sample[2]-result.GetExpectedUpperLimit(-1)*sample[2],result.GetExpectedUpperLimit(1)*sample[2]-result.GetExpectedUpperLimit(0)*sample[2])
        exp2_xsec.SetPointError(exp2_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*sample[2]-result.GetExpectedUpperLimit(-2)*sample[2],result.GetExpectedUpperLimit(2)*sample[2]-result.GetExpectedUpperLimit(0)*sample[2])
        xsec.SetPoint(xsec.GetN(),sample[0],sample[2])

        file.Close()

      # exp1.SetPoint(exp1.GetN(),1e6,1e6)
      # exp1.SetPointError(exp1.GetN()-1,0,0,0,0)
      # exp2.SetPoint(exp2.GetN(),1e6,1e6)
      # exp2.SetPointError(exp2.GetN()-1,0,0,0,0)

      mg1D = ROOT.TMultiGraph()
      mg1D.Add(exp2,"3")
      mg1D.Add(exp1,"3")
      mg1D.Add(exp,"l")
      mg1D.Add(obs,"l")

      mg1D_xsec = ROOT.TMultiGraph()
      mg1D_xsec.Add(exp2_xsec,"3")
      mg1D_xsec.Add(exp1_xsec,"3")
      mg1D_xsec.Add(exp_xsec,"l")
      mg1D_xsec.Add(obs_xsec,"l")
      mg1D_xsec.Add(xsec,"c")

      # print Xmin," ",Xmax

      canv1D = ROOT.TCanvas("excl1D%1.2f%s"%(ratio,channel),"excl1D%1.2f%s"%(ratio,channel),800,600)
      canv1D.cd()
      canv1D.SetLogy()
      mg1D.Draw("a")
      mg1D.SetMaximum(1e3)
      mg1D.GetXaxis().SetTitle("m(W_{R}) = %1.2f #times m(N_{R})" % ratio)
      mg1D.GetYaxis().SetTitle("#mu_{SIG}^{95%}")
      mg1D.GetXaxis().SetRangeUser(Xmin,Xmax)
      ROOT.ATLASLabel(0.2,0.875,"Internal",1)
      ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
      if not SSONLY:
          ROOT.myText(0.54,0.875,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
      else:
          ROOT.myText(0.54,0.875,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
      leg3.Draw()
      canv1D.Update();
      xmin = ROOT.gPad.GetUxmin()
      xmax = ROOT.gPad.GetUxmax()
      ymin = ROOT.gPad.GetUymin()
      ymax = ROOT.gPad.GetUymax()
      line = ROOT.TLine(xmin,1,xmax,1)
      line.Draw()
      line.SetLineColor(ROOT.kRed)
      ROOT.gPad.RedrawAxis("g");
      canv1D.Print("limits_"+basefolder+"/exclusion_1D_mu_%2.0f_%s.eps" % (float(100*ratio),(channel+underscore+neutrino)))

      canv1D_xsec = ROOT.TCanvas("excl1D%1.2f_xsec%s"%(ratio,channel),"excl1D%1.2f_xsec%s"%(ratio,channel),800,600)
      canv1D_xsec.cd()
      canv1D_xsec.SetLogy()
      mg1D_xsec.Draw("a")
      if ratio < 2:
        mg1D_xsec.SetMaximum(1e3)
        mg1D_xsec.SetMinimum(1e-4)
      else:
        mg1D_xsec.SetMaximum(1e5)
        mg1D_xsec.SetMinimum(1e-4)    
      mg1D_xsec.GetXaxis().SetTitle("m(W_{R}) = %1.2f #times m(N_{R})" % ratio)
      mg1D_xsec.GetYaxis().SetTitle("#sigma [pb]")
      mg1D_xsec.GetXaxis().SetRangeUser(Xmin,Xmax)
      ROOT.ATLASLabel(0.2,0.875,"Internal",1)
      ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
      if not SSONLY:
          ROOT.myText(0.54,0.875,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
      else:
          ROOT.myText(0.54,0.875,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
      leg4.Draw()
      ROOT.gPad.RedrawAxis("g");
      canv1D_xsec.Print("limits_"+basefolder+"/exclusion_1D_%2.0f_%s.eps" % (float(100*ratio),(channel+underscore+neutrino)))

  # canvc = ROOT.TCanvas("canvc"+channel,"canvc"+channel,800,600)
  # canvc.cd()
  # for x in [contDN2c[1],contDNc[1],contUPc[1]]:
  #   x.SetFillColorAlpha(x.GetFillColor(),0.5)

  # contDN2c[0].Draw("a F")
  # contDNc[0].Draw("same F")
  # contUPc[0].Draw("same F")
  # contUP2c[0].Draw("same F")

  # # contDN2c[1].SetFillStyle(3004)
  # # contDNc[1] .SetFillStyle(3005)
  # # contUPc[1] .SetFillStyle(3004)

  # # contDN2c[1].Draw("same F")
  # # contDNc[1] .Draw("same F")
  # # contUPc[1] .Draw("same F")
  # # contUP2c[1].Draw("same F")
  # print contDN2c[1].GetN()
  # print contDNc[1] .GetN()
  # print contUPc[1] .GetN()
  # print contUP2c[1].GetN()

  # contc[0].Draw("l")
  # contObsc[0].Draw("l")
  # contc[1].Draw("l")
  # contObsc[1].Draw("l")

  # contc[1].SetLineColor(ROOT.kBlue)
  # contObsc[1].SetLineColor(ROOT.kBlue)

  # ROOT.gPad.RedrawAxis("g");
  # canvc.Print("limits_"+basefolder+"/limits_combined_%s.pdf" % channel)

# print exclusionLineUP.GetN()
# print exclusionLineDN.GetN()

