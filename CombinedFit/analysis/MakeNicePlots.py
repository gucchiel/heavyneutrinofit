from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

from array import array
from decimal import Decimal

ROOT.gROOT.SetBatch(True)

infile = ROOT.TFile("data/templateHN_v2_003.root","update")

for key in infile.GetListOfKeys():
  if "WR" in key.GetName() and "Nom" in key.GetName():
    print key


## modules
import os
import sys
import re
import subprocess

def getLineWithSys(string,table):
  out = re.findall(string+".*",table)[0].split("&")[1:] if len(re.findall(string+".*",table)) else []
  if out == []:
    return out
  out[-1] = out[-1].replace("\\\\","")
  out = [ re.findall("[ ]*\$([0-9\.]*)[\\\pm\_\{ ]*([0-9\.\-]*)[\^\{\} ]*([0-9\.\+]*)[ \}]*\$",x)[0] for x in out]
  return out

def FillHistogram(histo,string,table):
  line = getLineWithSys(string, table)
  if line == []:
    print "no ",string," in table "
    return histo
  assert histo.GetNbinsX() == len(line)-1, "incompatible length "+str(histo.GetNbinsX())+" "+str(len(line))
  for i in range(len(line)-1):
    histo.SetBinContent(i+1,histo.GetBinContent(i+1) + float(line[i+1][0]) )
  return histo

def FillGraphData(graph,axis,string,table):
  line = getLineWithSys(string, table)
  if line == []:
    return graph
  assert graph.GetN() == len(line)-1 == len(axis)-1, "incompatible length "+str(graph.GetN())+" "+str(len(line))
  for i in range(len(line)-1):
    if float(line[i+1][0]) == 0.:
      continue
    graph.SetPoint(i,(axis[i]+axis[i+1])/2.,float(line[i+1][0]))
    graph.SetPointError(i,0,0,getPoissonInterval(float(line[i+1][0]))[0],getPoissonInterval(float(line[i+1][0]))[0])
  return graph

def FillGraphWithSys(graph,axis,string,table):
  line = getLineWithSys(string, table)
  if line == []:
    return graph
  assert graph.GetN() == len(line)-1 == len(axis)-1, "incompatible length "+str(graph.GetN())+" "+str(len(line))
  for i in range(len(line)-1):
    graph.SetPoint(i,(axis[i]+axis[i+1])/2.,float(line[i+1][0]))
    if line[i+1][2]:
      graph.SetPointError(i,(axis[i]+axis[i+1])/2.-axis[i],axis[i+1]-(axis[i]+axis[i+1])/2.,abs(float(line[i+1][1])),abs(float(line[i+1][2])))
    else:
      graph.SetPointError(i,(axis[i]+axis[i+1])/2.-axis[i],axis[i+1]-(axis[i]+axis[i+1])/2.,abs(float(line[i+1][1])),abs(float(line[i+1][1])))

  return graph


def getPoissonInterval(n, alpha=1.- 0.682689492):
  return (n - ROOT.Math.gamma_quantile( alpha/2, n, 1.) if n!=0. else 0.,\
    ROOT.Math.gamma_quantile_c( alpha/2, n+1, 1) - n)

#____________________________________________________________
def generateLogBins(bins_N,bins_min,bins_max):
  bins = []
  bins += [bins_min]
  bins_factor = pow( Decimal(bins_max)/Decimal(bins_min) , Decimal(1)/Decimal(bins_N) )
  for i in range(1,bins_N+1):
    bins += [bins[i-1]*bins_factor]
  for i in range(bins_N+1):
    bins[i] = round(bins[i],0)
    if i!=0: assert bins[i]!=bins[i-1], "two consetuvie bin edges have the same value due to rounding"
  return bins


regDict = [
("SSZCRee" ,"e^{#pm}e^{#pm}",              generateLogBins(12,10,4000),False,True, "CR (e^{#pm}e^{#pm})",0,150),
("SSZVRee" ,"e^{#pm}e^{#pm}",              generateLogBins(8,150,4000),False,True, "VR (e^{#pm}e^{#pm})",0,30),
("SSZSRee" ,"e^{#pm}e^{#pm}",              generateLogBins(8,400,4000),False,True, "SR (e^{#pm}e^{#pm})",0,10),
("SSZCRmm" ,"#mu^{#pm}#mu^{#pm}",   [10] + generateLogBins(4,110,4000),False,True, "CR (#mu^{#pm}#mu^{#pm})",0,120),
("SSZVRmm" ,"#mu^{#pm}#mu^{#pm}",          generateLogBins(8,150,4000),False,True, "VR (#mu^{#pm}#mu^{#pm})",0,15),
("SSZSRmm" ,"#mu^{#pm}#mu^{#pm}",          generateLogBins(8,400,4000),False,True, "SR (#mu^{#pm}#mu^{#pm})",0,6),
]
for x in regDict:
  print x[0]

regTable = [open('tables/allBins/%s.tex' % x[0],'r').read() for x in regDict]

for fitType in ["Fitted","MC exp."]:

  MCTtotstr = "Fitted bkg events" if fitType == "Fitted" else "MC exp. SM events"
  suffix = "post" if fitType == "Fitted" else "pre"

  for reg,table in zip(regDict,regTable):
    print reg
    binsX = array('d',reg[2])
    histoTop = ROOT.TH1D(reg[0]+suffix+"top",reg[0]+suffix+"top",len(reg[2])-1,binsX)
    histoDY = ROOT.TH1D(reg[0]+suffix+"DY",reg[0]+suffix+"DY",len(reg[2])-1,binsX)
    histoDB = ROOT.TH1D(reg[0]+suffix+"dib",reg[0]+suffix+"dib",len(reg[2])-1,binsX)
    histoFake = ROOT.TH1D(reg[0]+suffix+"fake",reg[0]+suffix+"fake",len(reg[2])-1,binsX)
    histoMCTot = ROOT.TH1D(reg[0]+suffix+"tot",reg[0]+suffix+"tot",len(reg[2])-1,binsX)

    histoMCsig1 = ROOT.TH1D(reg[0]+suffix+"sig1",reg[0]+suffix+"sig1",len(reg[2])-1,binsX)
    histoMCsig2 = ROOT.TH1D(reg[0]+suffix+"sig2",reg[0]+suffix+"sig2",len(reg[2])-1,binsX)
    histoMCsig3 = ROOT.TH1D(reg[0]+suffix+"sig3",reg[0]+suffix+"sig3",len(reg[2])-1,binsX)

    for i in range(1,histoTop.GetNbinsX()+1):
      histoTop.SetBinContent(i,0)
      histoDY.SetBinContent(i,0)
      histoDB.SetBinContent(i,0)
      histoFake.SetBinContent(i,0)
      histoMCTot.SetBinContent(i,0)

    hProxyLabelHisto = ROOT.TH1D(reg[0]+suffix+"proxyHisto",reg[0]+suffix+"proxyHisto",len(reg[2])-1,binsX)


    graphData = ROOT.TGraphAsymmErrors(len(reg[2])-1)
    graphMCTot = ROOT.TGraphAsymmErrors(len(reg[2])-1)
    graphData = FillGraphData(graphData,reg[2],"Observed events", table)
    graphMCTot = FillGraphWithSys(graphMCTot,reg[2],MCTtotstr,table)


    histoMCTot = FillHistogram(histoMCTot, MCTtotstr,table)

    histoTop = FillHistogram(histoTop, "%s top\\\\_physics events " % fitType,table)
    histoFake = FillHistogram(histoFake,"%s fakes events" % fitType,table)
    histoDY = FillHistogram(histoDY,"%s SherpaDY221 events" % fitType,table)
    histoDB = FillHistogram(histoDB, "%s dibosonSherpa events " % fitType,table)

    ROOT.gStyle.SetHatchesSpacing(0.5)

    graphMCTot.SetFillStyle(3354)
    graphMCTot.SetFillColor(ROOT.kBlue+2)

    histoTop.SetFillColor(ROOT.kBlue-1)
    histoTop.SetLineColor(ROOT.kBlue+4)
    histoTop.SetLineWidth(1)

    histoFake.SetFillColor(ROOT.kRed-6)
    histoFake.SetLineColor(ROOT.kRed+2)
    histoFake.SetLineWidth(1)

    histoDB.SetFillColor(ROOT.kGreen-6)
    histoDB.SetLineColor(ROOT.kGreen-2)
    histoDB.SetLineWidth(1)

    histoDY.SetFillColor(ROOT.kYellow-9)
    histoDY.SetLineColor(ROOT.kYellow-6)
    histoDY.SetLineWidth(1)

    histoMCTot.SetLineColor(ROOT.kBlack)
    histoMCTot.SetLineWidth(2)

    graphDataR  = graphData.Clone("dataR"+reg[0])
    graphMCTotR = graphMCTot.Clone("MCR"+reg[0])

    for i in range(graphDataR.GetN()):
      yy = graphMCTot.GetY()[i] if graphMCTot.GetY()[i] != 0 else 1e9
      if graphData.GetY()[i] > 0.:
        graphDataR.SetPoint(i,graphData.GetX()[i],graphData.GetY()[i]/yy)
      else:
        graphDataR.SetPoint(i,graphData.GetX()[i],-999.) 
      graphDataR.GetEYhigh()[i] = graphData.GetEYhigh()[i]/yy
      graphDataR.GetEYlow()[i] = graphData.GetEYlow()[i]/yy

      graphMCTotR.SetPoint(i,graphMCTot.GetX()[i],yy/yy)
      graphMCTotR.GetEYhigh()[i] = graphMCTot.GetEYhigh()[i]/yy
      graphMCTotR.GetEYlow()[i] = graphMCTot.GetEYlow()[i]/yy

    hs = ROOT.THStack("hs","hs")
    hs.Add(histoTop)
    hs.Add(histoFake)
    hs.Add(histoDB)
    hs.Add(histoDY)

    canv = ROOT.TCanvas("c1"+reg[0],"c1"+reg[0],800,600)
    rsplit = 0.3
    pad1TopMarin = 0.07
    pad1BotMarin = 0.04
    pad2TopMarin = 0.03
    pad2BotMarin = 0.45
    ratio = rsplit/(1-rsplit)
    pad1 = ROOT.TPad("pad1","top pad",0.,rsplit,1.,1.)
    pad1.SetTopMargin(pad1TopMarin)
    pad1.SetBottomMargin(pad1BotMarin)
    pad1.Draw()
    pad2 = ROOT.TPad("pad2","bottom pad",0,0,1,rsplit)
    pad2.SetTopMargin(pad2TopMarin)
    pad2.SetBottomMargin(pad2BotMarin)
    pad2.SetTicky()
    pad2.SetTickx()
    pad2.Draw()

    pad1.cd()
    if reg[4]:
      pad1.SetLogx()
    hs.Draw()
    histoMCTot.Draw("same")
    hs.GetYaxis().SetLabelSize(0.16*ratio )
    hs.GetYaxis().SetTitle("Events")
    hs.GetYaxis().SetTitleOffset(0.4/ratio)
    hs.GetYaxis().SetTitleSize(0.16*ratio)
    hs.GetXaxis().SetLabelSize(0)
    graphMCTot.Draw("same e2")
    if reg[3]:
      pad1.SetLogy()
      hs.SetMaximum( 100000*ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()) if 1000*ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()) else 1e2 )
    elif not len(reg)>6:
      if histoDB.GetNbinsX()>1:
        hs.SetMaximum( 2.2*ROOT.TMath.Max(ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()),ROOT.TMath.MaxElement(graphMCTot.GetN(),graphMCTot.GetY())) )
      else:
        hs.SetMaximum( 10. )
    else:
        hs.SetMaximum( reg[7] ) 
    if reg[0] == "SR1P3LEM":
      hs.SetMaximum(12)

    moveLabel = -0.22

    latex = ROOT.TLatex()
    latex.SetTextAlign(12)
    latex.SetTextSize(0.065)
    latex.SetNDC()
    latex.SetTextFont(72)
    latex.SetTextColor(1)
    latex.DrawLatex(0.40+moveLabel,0.875,"ATLAS");
    latex = ROOT.TLatex()
    latex.SetTextSize(0.06)
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextColor(1)
    latex.DrawLatex(0.525+moveLabel,0.855,"Internal");
    latex = ROOT.TLatex()
    latex.SetTextSize(0.06)
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextColor(1)
    latex.DrawLatex(0.40+moveLabel,0.785,"#sqrt{s}=13 TeV, 36.1 fb^{-1}");
    latex = ROOT.TLatex()
    latex.SetTextSize(0.06)
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextColor(1)
    latex.DrawLatex(0.7+moveLabel,0.853,reg[5]);

    if reg[0] not in ["SSZSRmm"]:
      leg = ROOT.TLegend(0.4+moveLabel,0.35,0.625+moveLabel,0.75)
      leg.SetNColumns(1)
    else:
      leg = ROOT.TLegend(0.4+moveLabel,0.55,0.825+moveLabel,0.75)
      leg.SetNColumns(2)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(0.06)
    leg.AddEntry(graphData,"#font[42]{Data}","pe")
    leg.AddEntry(graphMCTot,"#font[42]{Total SM}","l f")
    if histoDY.GetSum():
      leg.AddEntry(histoDY,"#font[42]{Drell-Yan}","f")
    if histoDB.GetSum():
      leg.AddEntry(histoDB,"#font[42]{Diboson}","f")
    if histoFake.GetSum():
      leg.AddEntry(histoFake,"#font[42]{Fakes}","f")
    if histoTop.GetSum():
      leg.AddEntry(histoTop,"#font[42]{Top}","f")

    leg.Draw()


    if "SR" in reg[0]:
      sigHisto1 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR4200_NR2100Nom_SSZSR%s_obs_HT" % ("ee" if "ee" in reg[0] else "mm"))
      sigHisto2 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR3000_NR300Nom_SSZSR%s_obs_HT"  % ("ee" if "ee" in reg[0] else "mm"))
      sigHisto3 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR600_NR1200Nom_SSZSR%s_obs_HT"  % ("ee" if "ee" in reg[0] else "mm"))
      print "hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR4200_NR2100Nom_SSZSR%s_obs_HT" % ("ee" if "ee" in reg[0] else "mm")
      print sigHisto1
      assert sigHisto1.GetNbinsX() == histoMCsig1.GetNbinsX(), "incompatible bin X"
      for i in range(histoMCsig1.GetNbinsX()+1):
        histoMCsig1.SetBinContent(i,sigHisto1.GetBinContent(i))
        histoMCsig2.SetBinContent(i,sigHisto2.GetBinContent(i))
        histoMCsig3.SetBinContent(i,sigHisto3.GetBinContent(i))

      histoMCsig1.Draw("same")
      histoMCsig2.Draw("same")
      histoMCsig3.Draw("same")

      histoMCsig1.SetLineStyle(1)
      histoMCsig1.SetLineColor(ROOT.kBlue)
      histoMCsig1.SetLineWidth(4)
      histoMCsig2.SetLineStyle(1)
      histoMCsig2.SetLineColor(ROOT.kRed)
      histoMCsig2.SetLineWidth(4)
      histoMCsig3.SetLineStyle(1)
      histoMCsig3.SetLineColor(ROOT.kGreen)
      histoMCsig3.SetLineWidth(4)

      leg2 = ROOT.TLegend(0.63,0.3,0.875,0.9185)
      leg2.SetBorderSize(0)
      leg2.SetFillColor(0)
      leg2.SetFillStyle(0)
      leg2.SetTextSize(0.06)
      leg2.AddEntry(histoMCsig3,"#font[42]{#splitline{m(W_{R}) = %4.0f GeV}{m(N_{R}) = %4.0f GeV}}" % (600,1200),"l")
      leg2.AddEntry(histoMCsig2,"#font[42]{#splitline{m(W_{R}) = %4.0f GeV}{m(N_{R}) = %4.0f GeV}}" % (3000,300),"l")
      leg2.AddEntry(histoMCsig1,"#font[42]{#splitline{m(W_{R}) = %4.0f GeV}{m(N_{R}) = %4.0f GeV}}" % (4200,2100),"l")
      leg2.Draw()

    graphData.Draw("same ep")
    ROOT.gPad.RedrawAxis("g");


    pad2.cd()
    if reg[4]:
      pad2.SetLogx()
    hProxyLabelHisto.Draw()
    hProxyLabelHisto.GetXaxis().SetNoExponent()
    hProxyLabelHisto.GetXaxis().SetMoreLogLabels()
    hProxyLabelHisto.GetXaxis().SetLabelSize(0.16)
    hProxyLabelHisto.GetXaxis().SetTitleSize(0.16)
    hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.1)
    hProxyLabelHisto.GetXaxis().SetLabelOffset(0.05)
    hProxyLabelHisto.GetYaxis().SetNdivisions(405)
    hProxyLabelHisto.GetYaxis().SetLabelSize(0.16)
    hProxyLabelHisto.GetYaxis().SetTitle("Data/SM")
    if "CR" in reg[0] :
      hProxyLabelHisto.GetXaxis().SetTitle("m(jj) [GeV]")
    else:
      hProxyLabelHisto.GetXaxis().SetTitle("H_{T}(lljj) [GeV]")
    hProxyLabelHisto.GetXaxis().SetTitleOffset(1.2)
    hProxyLabelHisto.GetYaxis().SetTitleOffset(0.4)
    hProxyLabelHisto.GetYaxis().SetTitleSize(0.16)
    graphMCTotR.Draw("same e2")
    graphDataR.Draw("same pe 0")
    line1 = ROOT.TLine(reg[2][0],1,  reg[2][-1],1);
    line2 = ROOT.TLine(reg[2][0],1.5,reg[2][-1],1.5);
    line3 = ROOT.TLine(reg[2][0],0.5,reg[2][-1],0.5);
    line2.SetLineStyle(3)
    line3.SetLineStyle(3)
    line1.Draw()
    line2.Draw()
    line3.Draw()

    canv.Print("histos/histo_"+reg[0]+"_"+suffix+".eps")
