from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy

ROOT.gStyle.SetPaintTextFormat("2.0f");

ROOT.gROOT.SetBatch(True)

threshold = 0.05

# matrixNorm = ROOT.TFile("/afs/f9.ijs.si/home/miham/HistFitter/results/SameSignHN-WR4200_NR2100_Sig_ee-excl/c_corrMatrix_RooExpandedFitResult_afterFit.root","READ").Get("c_corrMatrix_RooExpandedFitResult_afterFit")
matrix     = ROOT.TFile("/afs/f9.ijs.si/home/miham/HistFitter/results/SameSignHN_WR600_NR900_Sig_ee_excl/c_corrMatrix_RooExpandedFitResult_afterFit.root","READ").Get("c_corrMatrix_RooExpandedFitResult_afterFit")

# for x in matrixNorm.GetListOfPrimitives():
#   if x.GetName() == "h_corr_RooExpandedFitResult_afterFit":
#     histoNorm = x
for x in matrix.GetListOfPrimitives():
  if x.GetName() == "h_corr_RooExpandedFitResult_afterFit":
    histo = x


N = histo.GetNbinsX()

assert histo.GetXaxis().GetBinLabel(2) == histo.GetYaxis().GetBinLabel(N-1)

print "== norm =="

matrixNormSmall = [[-999 for x in range(N+1)] for y in range(N+1)]
matrixSmall = [[-999 for x in range(N+1)] for y in range(N+1)]
dictNormSmall = {}
dictSmall = {}

# counter = 0
# for i in range(1,N+1):
#   xVal = histoNorm.GetXaxis().GetBinLabel(i).replace("alpha_","")
#   if "gamma" in xVal:
#     continue
#   for j in range(N-i,0,-1):
#     yVal = histoNorm.GetYaxis().GetBinLabel(j).replace("alpha_","")
#     if "gamma" in yVal:
#       continue
#     if abs(histoNorm.GetBinContent(i,j)) > threshold:
#       if xVal not in dictNormSmall.keys():
#         counter += 1
#         dictNormSmall.update({xVal:counter})
#       if yVal not in dictNormSmall.keys():
#         counter += 1
#         dictNormSmall.update({yVal:counter})
#       print xVal," ",yVal," ",histoNorm.GetBinContent(i,j)
#       matrixNormSmall[dictNormSmall[xVal]][dictNormSmall[yVal]] = 100.*histoNorm.GetBinContent(i,j)
#       matrixNormSmall[dictNormSmall[yVal]][dictNormSmall[xVal]] = 100.*histoNorm.GetBinContent(i,j)

counter = 0
for i in range(1,N+1):
  xVal = histo.GetXaxis().GetBinLabel(i).replace("alpha_","")
  if "gamma" in xVal:
    continue
  for j in range(N-i,0,-1):
    yVal = histo.GetYaxis().GetBinLabel(j).replace("alpha_","")
    if "gamma" in yVal:
      continue
    if abs(histo.GetBinContent(i,j)) > threshold:
      if xVal not in dictSmall.keys():
        counter += 1
        dictSmall.update({xVal:counter})
      if yVal not in dictSmall.keys():
        counter += 1
        dictSmall.update({yVal:counter})
      print xVal," ",yVal," ",histo.GetBinContent(i,j)
      matrixSmall[dictSmall[xVal]][dictSmall[yVal]] = 100.*histo.GetBinContent(i,j)
      matrixSmall[dictSmall[yVal]][dictSmall[xVal]] = 100.*histo.GetBinContent(i,j)


# print dictNormSmall
# nMaxNorm = max(dictNormSmall.values())
# TH2DNorm = ROOT.TH2D("Norm","Norm",nMaxNorm,0.5,nMaxNorm-0.5,nMaxNorm,0.5,nMaxNorm-0.5)
# for i in range(1,nMaxNorm+1):
#   TH2DNorm.GetXaxis().SetBinLabel(i,dictNormSmall.keys()[dictNormSmall.values().index(i)])
#   TH2DNorm.GetYaxis().SetBinLabel(i,dictNormSmall.keys()[dictNormSmall.values().index(i)])
#   for j in range(1,nMaxNorm+1):
#     if i == j:
#       TH2DNorm.SetBinContent(i,j,100)
#     else:
#       TH2DNorm.SetBinContent(i,j,matrixNormSmall[i][j])

print dictSmall
nMax = max(dictSmall.values())
TH2D = ROOT.TH2D("","",nMax,0.5,nMax-0.5,nMax,0.5,nMax-0.5)
for i in range(1,nMax+1):
  TH2D.GetXaxis().SetBinLabel(i,dictSmall.keys()[dictSmall.values().index(i)])
  TH2D.GetYaxis().SetBinLabel(i,dictSmall.keys()[dictSmall.values().index(i)])
  for j in range(1,nMax+1):
    if i == j:
      TH2D.SetBinContent(i,j,100)
    else:
      TH2D.SetBinContent(i,j,matrixSmall[i][j])

# c1 = ROOT.TCanvas("c1","c1",800,600)
# c1.SetRightMargin(0.15)
# c1.SetLeftMargin(0.3)
# c1.SetBottomMargin(0.3)
# TH2DNorm.Draw("colz text")
# TH2DNorm.SetMinimum(-100)
# TH2DNorm.SetMarkerSize(0.8)
# TH2DNorm.GetZaxis().SetLabelSize(0.04)
# TH2DNorm.GetZaxis().SetTitle("correlation [%]")
# TH2DNorm.GetZaxis().SetRangeUser(-100,100)
# TH2DNorm.GetXaxis().LabelsOption("v")
# TH2DNorm.GetXaxis().SetLabelSize(0.04)
# TH2DNorm.GetYaxis().SetLabelSize(0.04)
# TH2DNorm.GetYaxis().SetTickLength(0)
# TH2DNorm.GetXaxis().SetTickLength(0)
# ROOT.ATLASLabel(0.01,0.18,"Internal",1)
# ROOT.myText(0.01,0.13,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
# ROOT.myText(0.01,0.08,1,"normalized sys.")
# c1.Print("reducedMatrix_norm.eps")

c2 = ROOT.TCanvas("c2","c2",800,600)
c2.SetRightMargin(0.15)
c2.SetLeftMargin(0.3)
c2.SetBottomMargin(0.3)
TH2D.Draw("colz text")
TH2D.SetMinimum(-1)
TH2D.SetMarkerSize(0.8)
TH2D.GetZaxis().SetLabelSize(0.04)
TH2D.GetZaxis().SetTitle("correlation [%]")
TH2D.GetZaxis().SetRangeUser(-100,100)
TH2D.GetXaxis().LabelsOption("v")
TH2D.GetXaxis().SetLabelSize(0.04)
TH2D.GetYaxis().SetLabelSize(0.04)
TH2D.GetYaxis().SetTickLength(0)
TH2D.GetXaxis().SetTickLength(0)
ROOT.ATLASLabel(0.01,0.18,"Internal",1)
ROOT.myText(0.01,0.13,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
ROOT.myText(0.01,0.08,1,"full sys.")
c2.Print("reducedMatrix.eps")


