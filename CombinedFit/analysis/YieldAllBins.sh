mkdir -p tables/allBins

 # python scripts/YieldsTable.py -L "tab:channelYieldSSZCRee" \
 # -t "region SSZCRee" -c SSZCRee -P \
 # -w results/SameSignHN-WR4200_NR2100_Sig_mm-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
 # -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZCRee.tex

 # python scripts/YieldsTable.py -L "tab:channelYieldSSZVRee" \
 # -t "region SSZVRee" -c SSZVRee -P \
 # -w results/SameSignHN-WR4200_NR2100_Sig_mm-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
 # -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZVRee.tex

 # python scripts/YieldsTable.py -L "tab:channelYieldSSZSRee" \
 # -t "region SSZSRee" -c SSZSRee -P \
 # -w results/SameSignHN-WR4200_NR2100_Sig_mm-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
 # -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZSRee.tex

 # python scripts/YieldsTable.py -L "tab:channelYieldSSZCRmm" \
 # -t "region SSZCRmm" -c SSZCRmm -P \
 # -w results/SameSignHN-WR4200_NR2100_Sig_mm-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
 # -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZCRmm.tex

 # python scripts/YieldsTable.py -L "tab:channelYieldSSZVRmm" \
 # -t "region SSZVRmm" -c SSZVRmm -P \
 # -w results/SameSignHN-WR4200_NR2100_Sig_mm-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
 # -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZVRmm.tex

 # python scripts/YieldsTable.py -L "tab:channelYieldSSZSRmm" \
 # -t "region SSZSRmm" -c SSZSRmm -P \
 # -w results/SameSignHN-WR4200_NR2100_Sig_mm-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
 # -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZSRmm.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSR_eeos" \
  -t "region SR_eeos" -c SR_eeos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-excl/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/SR_eeos.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSR_mmos" \
  -t "region SR_mmos" -c SR_mmos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_mm-excl/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/SR_mmos.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSR_emos" \
  -t "region SR_emos" -c SR_emos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/SR_emos.tex

  python scripts/YieldsTable.py -L "tab:channelYieldZCR_eeos" \
  -t "region ZCR_eeos" -c ZCR_eeos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/ZCR_eeos.tex

  python scripts/YieldsTable.py -L "tab:channelYieldZCR_mmos" \
  -t "region ZCR_mmos" -c ZCR_mmos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/ZCR_mmos.tex

  python scripts/YieldsTable.py -L "tab:channelYieldZVR_eeos" \
  -t "region ZVR_eeos" -c ZVR_eeos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/ZVR_eeos.tex

  python scripts/YieldsTable.py -L "tab:channelYieldZVR_mmos" \
  -t "region ZVR_mmos" -c ZVR_mmos -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_ttV,Wjets_Diboson,Zjets -b -o tables/allBins/ZVR_mmos.tex

    python scripts/YieldsTable.py -L "tab:channelYieldSSZVRee" \
  -t "region SSZVRee" -c SSZVRee -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZVRee.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSSZCRee" \
  -t "region SSZCRee" -c SSZCRee -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZCRee.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSSZSRee" \
  -t "region SSZSRee" -c SSZSRee -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-excl/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZSRee.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSSZVRmm" \
  -t "region SSZVRmm" -c SSZVRmm -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZVRmm.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSSZCRmm" \
  -t "region SSZCRmm" -c SSZCRmm -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_ee-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZCRmm.tex

  python scripts/YieldsTable.py -L "tab:channelYieldSSZSRmm" \
  -t "region SSZSRmm" -c SSZSRmm -P \
  -w results/SameSignHN-WR4200_NR2100_Sig_mm-excl/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
  -s top_physics,dibosonSherpa,fakes,SherpaDY221 -b -o tables/allBins/SSZSRmm.tex

# python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
# -c SSZCRee,SSZVRee,SSZSRee,SSZCRmm,SSZVRmm,SSZSRmm \
# -o tables/allBins/AllSysHN.tex

# python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
# -c SSZCRee,SSZVRee,SSZSRee,SSZCRmm,SSZVRmm,SSZSRmm,SR_eeos,SR_mmos,SR_emos,ZCR_eeos,ZCR_mmos,ZVR_eeos,ZVR_mmos \
# -o tables/allBins/AllSysHN.tex

# python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
# -c SSZCRee,SSZVRee,SSZSRee,SSZCRmm,SSZVRmm,SSZSRmm,SR_eeos,SR_mmos,SR_emos,ZCR_eeos,ZCR_mmos,ZVR_eeos,ZVR_mmos -b \
# -o tables/allBins/AllSysHN_before.tex
