massPoint="MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2"
echo $massPoint
mkdir -p logs
mkdir -p limits
cp data/mergedOSSS.root data/backupCacheFile_SameSignHN-LRSM_WR$1_NR$2-bkg.root
HistFitter.py -w -f -F bkg -D before,after analysis/SameSignHN/SameSignHN_bkg.py\
 -c "analysisSuffix='LRSM_WR$1_NR$2-bkg';sigSamples=['MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2']"\
 # -g $massPoint  2>&1 | tee logs/LRSM_WR$1_NR$2-HNbkg.log
 -g $massPoint
mkdir -p pulls
# python scripts/pull_maker.py -i logs/LRSM_WR$1_NR$2-HNbkg.log