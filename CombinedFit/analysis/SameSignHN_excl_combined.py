"""
SS Analysis
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from collections import defaultdict
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from copy import deepcopy

import re
import os 

import logger
from logger import Logger

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
ROOT.SetAtlasStyle()

typicalFakeFactors = {"ee":0.5,"mm":0.9}

#---------------------------------------
# Analysis name
#---------------------------------------

if not analysisSuffix:
    analysisSuffix = "01"

analysis_name="SameSignHN-" + str(analysisSuffix)

print analysis_name

if not 'ALLCR' in locals():
  ALLCR = False

if not 'ONLYSS' in locals():
  ONLYSS = False

if not 'VALIDATION' in locals():
  VALIDATION = False

if not 'fittedBkgSys' in locals():
  fittedBkgSys = "overallNormHistoSys"
# fittedBkgSys = "histoSys"

#---------------------------------------
# Logger
#---------------------------------------

log = Logger(analysis_name)
log.setLevel(logger.INFO) #should have no effect if -L is used
log.warning("example warning from python")
log.error("example error from python")

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat=True # statistics variation of samples 

#-------------------------------
# Config manager basics
#-------------------------------

# Setting the parameters of the hypothesis test
configMgr.doExclusion=True # True=exclusion, False=discovery
#configMgr.doHypoTest=False
#configMgr.nTOYs=5000
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=100

configMgr.writeXML = True

configMgr.analysisName = analysis_name
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

#activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False # enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
configMgr.histBackupCacheFile =  "data/backupCacheFile_" + analysis_name +".root" # histogram templates - the data file of your previous fit, backup cache

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 36.07456 # Luminosity of input TTree after weighting
configMgr.outputLumi = 36.07456 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")
configMgr.removeEmptyBins=True

#generate using Asimov mock data
if "asimov" in locals():
  configMgr.useAsimovSet=True
else:
  configMgr.useAsimovSet=False
  
configMgr.blindSR = False

sig = analysisSuffix.replace("_ee-excl","").replace("_mm-excl","").replace("_ee-bkg","").replace("_mm-bkg","")

temp = re.findall("WR([0-9]*)_NR([0-9]*)",sig)[0]
OSvar = "Mlljj" if float(temp[0]) >= float(temp[1]) else "Mjj"
print float(temp[0])," ",float(temp[1])," ",OSvar

#-------------------------------------
# Now we start to build the data model
#-------------------------------------


# Dictionnary of cuts defining channels/regions (for Tree->hist)
if ALLCR and not ONLYSS:
  configMgr.cutsDict["ZCR_eeos"] = "1."
  configMgr.cutsDict["ZCR_mmos"] = "1."
if channel == "ee":
  if not ONLYSS:
    configMgr.cutsDict["SR_eeos"] = "1."
    configMgr.cutsDict["ZCR_eeos"] = "1."
  if neutrino == "Majorana":
    configMgr.cutsDict["SSZSRee"] = "1."
if channel =="mm":
  if not ONLYSS:
    configMgr.cutsDict["SR_mmos"] = "1."
    configMgr.cutsDict["ZCR_mmos"] = "1."
  if neutrino == "Majorana":
    configMgr.cutsDict["SSZSRmm"] = "1."

if not ONLYSS:
  configMgr.cutsDict["SR_emos"] = "1."
if neutrino == "Majorana":
  configMgr.cutsDict["SSZCRee"] = "1."
  configMgr.cutsDict["SSZCRmm"] = "1."

if VALIDATION:
  configMgr.cutsDict["SSZVRee"] = "1."
  configMgr.cutsDict["SSZVRmm"] = "1."
  if not ONLYSS:
    configMgr.cutsDict["ZVR_eeos"] = "1."
    configMgr.cutsDict["ZVR_mmos"] = "1."
    configMgr.cutsDict["SR_mmos"] = "1."
    configMgr.cutsDict["SR_eeos"] = "1."

sysNamesOScommon = [
'JET_SR1_JET_GroupedNP_1',
'JET_SR1_JET_GroupedNP_2',
'JET_SR1_JET_GroupedNP_3',
'FT_EFF_Eigen_B_0_AntiKt4EMTopoJets',
'FT_EFF_Eigen_B_1_AntiKt4EMTopoJets',
'FT_EFF_Eigen_B_2_AntiKt4EMTopoJets',
'FT_EFF_Eigen_C_0_AntiKt4EMTopoJets',
'FT_EFF_Eigen_C_1_AntiKt4EMTopoJets',
'FT_EFF_Eigen_C_2_AntiKt4EMTopoJets',
'FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets',
'FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets',
'FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets',
'FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets',
'FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets',
'FT_EFF_extrapolation_AntiKt4EMTopoJets',
'FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets',
]
sysNamesOSee = [
'EG_RESOLUTION_ALL_OS',
'EG_SCALE_ALL',
'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
]
sysNamesOSmm = [
'MUON_EFF_STAT',
'MUON_EFF_SYS',
'MUON_TTVA_STAT',
'MUON_TTVA_SYS',
'MUON_ISO_STAT',
'MUON_ISO_SYS',
'MUON_ID_OS',
'MUON_SCALE_OS',
'MUON_EFF_TrigSystUncertainty',
'MUON_EFF_TrigStatUncertainty',
]
oneSidedSysNamesOS = [
"JET_JER_SINGLE_NP",
]
topTheorySysNamesOS = [
"ttbar_HardScatter", 
"ttbar_FragmentationHadronization", 
"ttbar_AdditionalRadiation",
]
ZjetsTheorySysNamesOS = [
"Scale_MUR_MUF_PDF261000",
"AlphaS_PDF261000",
"JET_SR1_JET_EtaIntercalibration_NonClosure",
"PDF_PDF261000",
"PDFVary_PDF",
]
oneSidedZjetsTheorySysNamesOS = [
"MJJRW_1",
] 

sysNamesSScommon = [
'B_SYS',
'C_SYS',
'L_SYS',
'E_SYS',
'EFC_SYS',
'JVT_SYS',
'JET_BJES_Response',
'JET_EffectiveNP_1',
'JET_EffectiveNP_2',
'JET_EffectiveNP_3',
'JET_EffectiveNP_4',
'JET_EffectiveNP_5',
'JET_EffectiveNP_6',
'JET_EffectiveNP_7',
'JET_EffectiveNP_8restTerm',
'JET_EtaIntercalibration_Modelling',
'JET_EtaIntercalibration_NonClosure',
'JET_EtaIntercalibration_TotalStat',
'JET_Flavor_Composition',
'JET_Flavor_Response',
'JET_Pileup_OffsetMu',
'JET_Pileup_OffsetNPV',
'JET_Pileup_PtTerm',
'JET_Pileup_RhoTopology',
'JET_PunchThrough_MC15',
'JET_SingleParticle_HighPt',
'JET_JER_CROSS_CALIB_FORWARD',
'JET_JER_NOISE_FORWARD',
'JET_JER_NP0',
'JET_JER_NP1',
'JET_JER_NP2',
'JET_JER_NP3',
'JET_JER_NP4',
'JET_JER_NP5',
'JET_JER_NP6',
'JET_JER_NP7',
'JET_JER_NP8',
]
sysNamesSSee = [
'ID',
'TRIG',
'RECO',
'ISO',
'EG_RESOLUTION_ALL',
'EG_SCALE_ALLCORR',
'EG_SCALE_E4SCINTILLATOR',
'CF',
]
sysNamesSSmm = [
'RECOSYS',
'RECOSTAT',
'TRIGSYS',
'TRIGSTAT',
'ISOSYS',
'ISOSTAT',
'TTVASYS',
'TTVASTAT',
'MUON_ID',
'MUON_MS',
'MUON_RESBIAS',
'MUON_RHO',
'MUON_SCALE',
]
TheorySysNamesSS = [
'ALPHA_SYS',
'PDF_SYS_ENVELOPE',
'QCD_SCALE_ENVELOPE',
'PDF_COICE_ENVELOPE',
]


# Define weights - dummy here
configMgr.weights = "1."

#--------------------
# List of systematics
#--------------------

elfakesys = Systematic("FF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
mufakesys = Systematic("MUFF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")

customStatErr = Systematic("StatErr","_NoSys","dummy","dummy","tree","shapeStat")

#nominal name of the histograms with systematic variation
configMgr.nomName = "_NoSys"

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

if neutrino == "Majorana":
  topSample = Sample("top_physics",kGreen-9)
  topSample.setNormByTheory() #scales with lumi
  topSample.setStatConfig(useStat)
  for sysName in sysNamesSScommon:
    topSample.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ) )

  zSample = Sample("SherpaDY221",kAzure+1)
  zSample.setNormByTheory()
  zSample.setStatConfig(useStat)
  zSample.setNormFactor("mu_ZSS",1.,0.,5000.)
  zSample.setNormRegions([("SSZCRee","Mjj")])
  for sysName in sysNamesSScommon + TheorySysNamesSS:
    zSample.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )

  dbSample = Sample("dibosonSherpa",kYellow-3)
  dbSample.setNormByTheory()
  dbSample.setStatConfig(useStat)
  dbSample.setNormFactor("mu_DB",1.,0.,5000.)
  dbSample.setNormRegions([("SSZCRmm","Mjj"),("SSZCRee","Mjj")])
  for sysName in sysNamesSScommon + TheorySysNamesSS:
    dbSample.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )

  fakesSample = Sample("fakes",kGray+1) 
  fakesSample.setStatConfig(False)

  histoFile = ROOT.TFile(configMgr.histBackupCacheFile,"READ")
  keys = histoFile.GetListOfKeys()
  for key in keys:
    histoName = key.GetName()
    if "Nom" not in histoName:
      continue
    if "obs" not in histoName:
      continue
    if "fakes" in histoName:
      region = re.findall("hfakesNom_([a-zA-Z]+)_",histoName)[0]
      if region not in configMgr.cutsDict.keys():
        continue
      chann = "ee" if "ee" in histoName else "mm"
      fitVar = "Mjj" if "CR" in region else "HT"
      if fitVar not in histoName:
        continue
      histo = histoFile.Get(histoName)
      print region," ",fitVar," ",chann
      histoYields = []
      statErrors = []
      for i in range(1,histo.GetNbinsX()+1):
        print "bin ",i," yield ",histo.GetBinContent(i)," ",histo.GetBinError(i)
        if histo.GetBinContent(i) == 0:
          histoYields += [1e-3]
          statErrors  += [1.14*2/3*typicalFakeFactors[chann]-1.14/3*typicalFakeFactors[chann]**2]
        else:
          histoYields += [histo.GetBinContent(i)]
          statErrors  += [histo.GetBinError(i)]
      print histoYields
      print statErrors
      fakesSample.buildHisto(histoYields,region,fitVar,0,histo.GetXaxis().GetBinWidth(1))
      fakesSample.buildStatErrors(statErrors,region,fitVar)

  fakesSample.addSystematic( Systematic("StatFakes","dummy","dummy","dummy","tree","shapeStat") )

if not ONLYSS:
  topSampleOS = Sample("top_ttV",kGreen-9)
  topSampleOS.setNormByTheory() #scales with lumi
  topSampleOS.setStatConfig(useStat)
  topSampleOS.setNormFactor("mu_top",1.,0.,5000.)
  topSampleOS.setNormRegions([('SR_emos',OSvar)])
  for sysName in sysNamesOScommon:
    topSampleOS.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
  for oneSidedSysName in oneSidedSysNamesOS:
    topSampleOS.addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )

  zSampleOS = Sample("Zjets",kAzure+1)
  zSampleOS.setNormByTheory() #scales with lumi
  zSampleOS.setStatConfig(useStat)
  zSampleOS.setNormFactor("mu_ZOS",1.,0.,5000.)
  if ALLCR:
    zSampleOS.setNormRegions([('ZCR_eeos','cuts'),('ZCR_mmos','cuts')])
  else:
    if channel == "ee":
      zSampleOS.setNormRegions([('ZCR_eeos','cuts')])
    elif channel == "mm":
      zSampleOS.setNormRegions([('ZCR_mmos','cuts')])
  for sysName in sysNamesOScommon:
    zSampleOS.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
  for oneSidedSysName in oneSidedSysNamesOS:
    zSampleOS.addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )
  for ZjetsTheorySysName in ZjetsTheorySysNamesOS:
    zSampleOS.addSystematic( Systematic( "%s" % ( ZjetsTheorySysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
  for oneSidedZjetsTheorySysName in oneSidedZjetsTheorySysNamesOS:
    zSampleOS.addSystematic( Systematic( "%s" % ( oneSidedZjetsTheorySysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )

  dbSampleOS = Sample("Wjets_Diboson",kYellow-3)
  dbSampleOS.legName = "DB + Wjets"
  dbSampleOS.setNormByTheory() #scales with lumi
  dbSampleOS.setStatConfig(useStat)
  for sysName in sysNamesOScommon:
    dbSampleOS.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ) )
  for oneSidedSysName in oneSidedSysNamesOS:
    dbSampleOS.addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", "histoSys"+"OneSide" ) )

# Signal sample
sigSample = Sample(sig,kViolet)
sigSample.legName = "HN signal"
sigSample.setNormByTheory()
sigSample.setStatConfig(useStat)       
sigSample.setNormFactor("mu_SIG",1.,0.,500.)

#data
dataSample = Sample("Data",kBlack)
dataSample.setData()


#**************
# fit
#**************

## Bkg-only template
commonSamples = [dataSample,sigSample]
if not ONLYSS:
  commonSamples += [zSampleOS,topSampleOS,dbSampleOS]
if neutrino == "Majorana":
  commonSamples += [fakesSample,topSample,dbSample,zSample]

## Parameters of the Measurement
measName = "NormalMeasurement"
measLumi = 1.
measLumiError = 0.021 # 2015+16

bkgOnly = configMgr.addFitConfig("Template_BkgOnly")
if useStat:
    bkgOnly.statErrThreshold=0.05 #values above this will be considered in the fit
else:
    bkgOnly.statErrThreshold=None
bkgOnly.addSamples(commonSamples)
# bkgOnly.addSamples([sigSample])
bkgOnly.setSignalSample(sigSample)
meas = bkgOnly.addMeasurement(measName,measLumi,measLumiError)
meas.addPOI("mu_SIG")

CRs = []
SRs = []
VRs = []

## same-sign
if neutrino == "Majorana":
  channelSSZCRee = bkgOnly.addChannel("Mjj",["SSZCRee"],12,0,12)
  channelSSZCRee.hasB = False
  channelSSZCRee.hasBQCD = False
  channelSSZCRee.useOverflowBin = False
  for sysName in sysNamesSSee:
    channelSSZCRee.getSample("SherpaDY221").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZCRee.getSample("dibosonSherpa").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZCRee.getSample("top_physics").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ))
    # channelSSZCRee.getSample("fakes").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
  channelSSZCRee.getSample("fakes").addSystematic(elfakesys)
  channelSSZCRee.getSample("top_physics").addSystematic( Systematic("tt_THEORY", 1, 1.1, 0.9, "user",'userHistoSys' ))
  # channelSSZCRee.getSample("fakes").addSystematic(customStatErr)
  if not ONLYSS:
    channelSSZCRee.removeSample("top_ttV")
    channelSSZCRee.removeSample("Zjets")
    channelSSZCRee.removeSample("Wjets_Diboson")
  channelSSZCRee.removeSample(sig)
  CRs += [channelSSZCRee]

  channelSSZCRmm = bkgOnly.addChannel("Mjj",["SSZCRmm"],5,0,5)
  channelSSZCRmm.hasB = False
  channelSSZCRmm.hasBQCD = False
  channelSSZCRmm.useOverflowBin = False
  for sysName in sysNamesSSmm:
    channelSSZCRmm.getSample("dibosonSherpa").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZCRmm.getSample("top_physics").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ))
    # channelSSZCRmm.getSample("fakes").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
  channelSSZCRmm.getSample("fakes").addSystematic(mufakesys)
  channelSSZCRmm.getSample("top_physics").addSystematic( Systematic("tt_THEORY", 1, 1.1, 0.9, "user",'userHistoSys' ))
  # channelSSZCRmm.getSample("fakes").addSystematic(customStatErr)
  channelSSZCRmm.removeSample("SherpaDY221")
  if not ONLYSS:
    channelSSZCRmm.removeSample("top_ttV")
    channelSSZCRmm.removeSample("Zjets")
    channelSSZCRmm.removeSample("Wjets_Diboson")
  channelSSZCRmm.removeSample(sig)
  CRs += [channelSSZCRmm]

  if VALIDATION:
    channelSSZVRee = bkgOnly.addChannel("HT",["SSZVRee"],8,0,8)
    channelSSZVRee.hasB = False
    channelSSZVRee.hasBQCD = False
    channelSSZVRee.useOverflowBin = False
    for sysName in sysNamesSSee:
      channelSSZVRee.getSample("SherpaDY221").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZVRee.getSample("dibosonSherpa").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZVRee.getSample("top_physics").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ))
      # channelSSZVRee.getSample("fakes").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZVRee.getSample("fakes").addSystematic(elfakesys)
    channelSSZVRee.getSample("top_physics").addSystematic( Systematic("tt_THEORY", 1, 1.1, 0.9, "user",'userHistoSys' ))
    if not ONLYSS:
      channelSSZVRee.removeSample("top_ttV")
      channelSSZVRee.removeSample("Zjets")
      channelSSZVRee.removeSample("Wjets_Diboson")
    channelSSZVRee.removeSample(sig)
    VRs += [channelSSZVRee]

    channelSSZVRmm = bkgOnly.addChannel("HT",["SSZVRmm"],8,0,8)
    channelSSZVRmm.hasB = False
    channelSSZVRmm.hasBQCD = False
    channelSSZVRmm.useOverflowBin = False
    for sysName in sysNamesSSmm:
      channelSSZVRmm.getSample("dibosonSherpa").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZVRmm.getSample("top_physics").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ))
      # channelSSZVRmm.getSample("fakes").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZVRmm.getSample("fakes").addSystematic(mufakesys)
    channelSSZVRmm.getSample("top_physics").addSystematic( Systematic("tt_THEORY", 1, 1.1, 0.9, "user",'userHistoSys' ))
    channelSSZVRmm.removeSample("SherpaDY221")
    if not ONLYSS:
      channelSSZVRmm.removeSample("top_ttV")
      channelSSZVRmm.removeSample("Zjets")
      channelSSZVRmm.removeSample("Wjets_Diboson")
    channelSSZVRmm.removeSample(sig)
    VRs += [channelSSZVRmm]

  if channel =="ee" or VALIDATION:
    print "======= electron channel SS SR ==========="
    configMgr.cutsDict["SSZSRee"] = "1."
    channelSSZSRee = bkgOnly.addChannel("HT",["SSZSRee"],8,0,8)
    channelSSZSRee.hasB = False
    channelSSZSRee.hasBQCD = False
    channelSSZSRee.useOverflowBin = False
    for sysName in sysNamesSSee:
      channelSSZSRee.getSample("SherpaDY221").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZSRee.getSample("dibosonSherpa").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZSRee.getSample("top_physics").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ))
      # channelSSZSRee.getSample("fakes").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZSRee.getSample("fakes").addSystematic(elfakesys)
    channelSSZSRee.getSample("top_physics").addSystematic( Systematic("tt_THEORY", 1, 1.1, 0.9, "user",'userHistoSys' ))
    # channelSSZSRee.getSample("fakes").addSystematic(customStatErr)
    if not ONLYSS:
      channelSSZSRee.removeSample("top_ttV")
      channelSSZSRee.removeSample("Zjets")
      channelSSZSRee.removeSample("Wjets_Diboson")
    if VALIDATION:
      channelSSZSRee.removeSample(sig)
    if not VALIDATION:
      for sysName in sysNamesSScommon + sysNamesSSee:
        channelSSZSRee.getSample(sig).addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZSRee.getSample(sig).addSystematic( Systematic("SIG_THEORY", 1, 1.1, 0.9, "user",'userNormHistoSys' ))
    SRs += [channelSSZSRee]

  if channel =="mm" or VALIDATION:
    print "======= muon channel SS SR ==========="
    configMgr.cutsDict["SSZSRmm"] = "1."
    channelSSZSRmm = bkgOnly.addChannel("HT",["SSZSRmm"],8,0,8)
    channelSSZSRmm.hasB = False
    channelSSZSRmm.hasBQCD = False
    channelSSZSRmm.useOverflowBin = False
    for sysName in sysNamesSSmm:
      channelSSZSRmm.getSample("dibosonSherpa").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZSRmm.getSample("top_physics").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ))
      # channelSSZSRmm.getSample("fakes").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    channelSSZSRmm.getSample("fakes").addSystematic(mufakesys)
    channelSSZSRmm.getSample("top_physics").addSystematic( Systematic("tt_THEORY", 1, 1.1, 0.9, "user",'userHistoSys' ))
    # channelSSZSRmm.getSample("fakes").addSystematic(customStatErr)
    channelSSZSRmm.removeSample("SherpaDY221")
    if not ONLYSS:
      channelSSZSRmm.removeSample("top_ttV")
      channelSSZSRmm.removeSample("Zjets")
      channelSSZSRmm.removeSample("Wjets_Diboson")
    if VALIDATION:
      channelSSZSRmm.removeSample(sig)
    if not VALIDATION:
      for sysName in sysNamesSScommon + sysNamesSSmm:
        channelSSZSRmm.getSample(sig).addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      channelSSZSRmm.getSample(sig).addSystematic( Systematic("SIG_THEORY", 1, 1.1, 0.9, "user",'userNormHistoSys' ))
    SRs += [channelSSZSRmm]

## opposite-sign

if not ONLYSS:
  if ALLCR or channel =="ee":
    ZCRee = bkgOnly.addChannel( 'cuts', [ 'ZCR_eeos' ], 1, 0.5, 1.5 )
    ZCRee.hasB = False
    ZCRee.hasBQCD = False
    ZCRee.useOverflowBin = False
    for sysName in sysNamesOSee:
      ZCRee.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      ZCRee.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      ZCRee.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    for sysName in topTheorySysNamesOS:
      ZCRee.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys)  )
    if neutrino == "Majorana":
      ZCRee.removeSample("top_physics")
      ZCRee.removeSample("SherpaDY221")
      ZCRee.removeSample("dibosonSherpa")
      ZCRee.removeSample("fakes")
    ZCRee.removeSample(sig)
    CRs += [ZCRee]
    if VALIDATION:
      ZVRee = bkgOnly.addChannel( OSvar, [ 'ZVR_eeos' ], 18, 0, 9000 )
      ZVRee.hasB = False
      ZVRee.hasBQCD = False
      ZVRee.useOverflowBin = False
      for sysName in sysNamesOSee:
        ZVRee.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
        ZVRee.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
        ZVRee.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      for sysName in topTheorySysNamesOS:
        ZVRee.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys))
      ZVRee.removeSample("top_physics")
      ZVRee.removeSample("SherpaDY221")
      ZVRee.removeSample("dibosonSherpa")
      ZVRee.removeSample("fakes")
      ZVRee.removeSample(sig)
      VRs += [ZVRee]
  if ALLCR or channel =="mm":
    ZCRmm = bkgOnly.addChannel( 'cuts', [ 'ZCR_mmos' ], 1, 0.5, 1.5 )
    ZCRmm.hasB = False
    ZCRmm.hasBQCD = False
    ZCRmm.useOverflowBin = False
    for sysName in sysNamesOSmm:
      ZCRmm.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      ZCRmm.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      ZCRmm.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    for sysName in topTheorySysNamesOS:
      ZCRmm.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys)  )
    if neutrino == "Majorana":
      ZCRmm.removeSample("top_physics")
      ZCRmm.removeSample("SherpaDY221")
      ZCRmm.removeSample("dibosonSherpa")
      ZCRmm.removeSample("fakes")
    ZCRmm.removeSample(sig)
    CRs += [ZCRmm]
    if VALIDATION:
      ZVRmm = bkgOnly.addChannel( OSvar, [ 'ZVR_mmos' ], 18, 0, 9000 )
      ZVRmm.hasB = False
      ZVRmm.hasBQCD = False
      ZVRmm.useOverflowBin = False
      for sysName in sysNamesOSmm:
        ZVRmm.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
        ZVRmm.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
        ZVRmm.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      for sysName in topTheorySysNamesOS:
        ZVRmm.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys))
      ZVRmm.removeSample("top_physics")
      ZVRmm.removeSample("SherpaDY221")
      ZVRmm.removeSample("dibosonSherpa")
      ZVRmm.removeSample("fakes")
      ZVRmm.removeSample(sig)
      VRs += [ZVRmm]

  mixedCR = bkgOnly.addChannel( OSvar, [ 'SR_emos' ], 18, 0, 9000 )
  mixedCR.hasB = False
  mixedCR.hasBQCD = False
  mixedCR.useOverflowBin = False
  for sysName in sysNamesOSmm + sysNamesOSee:
    mixedCR.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    mixedCR.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    mixedCR.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
  for sysName in topTheorySysNamesOS:
    mixedCR.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "overallNormHistoSys") )
  if neutrino == "Majorana":
    mixedCR.removeSample("top_physics")
    mixedCR.removeSample("SherpaDY221")
    mixedCR.removeSample("dibosonSherpa")
    mixedCR.removeSample("fakes")
  mixedCR.removeSample(sig)
  CRs += [mixedCR]

  if channel =="ee" or VALIDATION:
    print "======= electron channel OS SR ==========="
    SRee = bkgOnly.addChannel( OSvar, [ 'SR_eeos' ], 18, 0, 9000 )
    SRee.hasB = False
    SRee.hasBQCD = False
    SRee.useOverflowBin = False
    for sysName in sysNamesOSee:
      SRee.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      SRee.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      SRee.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    for sysName in topTheorySysNamesOS:
      SRee.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys) )
    if neutrino == "Majorana":
      SRee.removeSample("top_physics")
      SRee.removeSample("SherpaDY221")
      SRee.removeSample("dibosonSherpa")
      SRee.removeSample("fakes")
    if VALIDATION:
      SRee.removeSample(sig)
    if not VALIDATION:
      for sysName in sysNamesOScommon + sysNamesOSee:
        SRee.getSample(sig).addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
      for oneSidedSysName in oneSidedSysNamesOS:
        SRee.getSample(sig).addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ))
      SRee.getSample(sig).addSystematic( Systematic("SIG_THEORY", 1, 1.1, 0.9, "user", 'userNormHistoSys' ))
    SRs += [SRee]

  if channel =="mm" or VALIDATION:
    print "======= muon channel OS SR ==========="
    SRmm = bkgOnly.addChannel( OSvar, [ 'SR_mmos' ], 18, 0, 9000 )
    SRmm.hasB = False
    SRmm.hasBQCD = False
    SRmm.useOverflowBin = False
    for sysName in sysNamesOSmm:
      SRmm.getSample("Zjets").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      SRmm.getSample("top_ttV").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
      SRmm.getSample("Wjets_Diboson").addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ))
    for sysName in topTheorySysNamesOS:
      SRmm.getSample("top_ttV").addSystematic(Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys)  )
    if neutrino == "Majorana":
      SRmm.removeSample("top_physics")
      SRmm.removeSample("SherpaDY221")
      SRmm.removeSample("dibosonSherpa")
      SRmm.removeSample("fakes")
    if VALIDATION:
      SRmm.removeSample(sig)
    if not VALIDATION:
      for sysName in sysNamesOScommon + sysNamesOSmm:
        SRmm.getSample(sig).addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
      for oneSidedSysName in oneSidedSysNamesOS:
        SRmm.getSample(sig).addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )
      SRmm.getSample(sig).addSystematic( Systematic("SIG_THEORY", 1, 1.1, 0.9, "user", 'userNormHistoSys' ))
    SRs += [SRmm]


## CRs:
bkgOnly.addBkgConstrainChannels(CRs)
## SRs:
bkgOnly.addSignalChannels(SRs)
## VRS:
if VALIDATION:
  bkgOnly.addValidationChannels(VRs)

