from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy

ROOT.gStyle.SetPaintTextFormat("2.2f");

ROOT.gROOT.SetBatch(True)

basefolder = "/ceph/grid/home/atlas/miham/AnalysisCode/HN_v2_CR_test/nominal/"

lumi =  36097.56

fileEE = ROOT.TFile("/afs/f9.ijs.si/home/miham/AnalysisCode/run/HN_v2_CR_test/hists_Mjj5_electron-SS-Z-CR_SSZCRee.root","READ")
fileMM = ROOT.TFile("/afs/f9.ijs.si/home/miham/AnalysisCode/run/HN_v2_CR_test/hists_Mjj6_muon-SS-Z-CR_SSZCRmm.root","READ")

folder = "/afs/f9.ijs.si/home/miham/AnalysisCode/run/HN_v2_CR_test_02/"

signalSamples = [
[600,50,51.02],
[600,150,49.713],
[600,300,37.33],
[600,450,16.138],
[600,500,8.7848],
[600,900,0.015643],
[600,1200,0.0031095],
[1200,50,3.2448],
[1200,300,3.1065],
[1200,600,2.3153],
[1200,900,0.98898],
[1200,1100,0.17725],
[1200,1800,0.0004383],
[1200,2400,5.162e-05],
[1800,50,0.50828],
[1800,450,0.47505],
[1800,900,0.3507],
[1800,1350,0.14888],
[1800,1700,0.01393],
[1800,2700,2.9008e-05],
[1800,3600,1.9213e-06],
[2400,50,0.11263],
[2400,600,0.10148],
[2400,1200,0.074743],
[2400,1800,0.031435],
[2400,2300,0.001897],
[2400,3600,2.5613e-06],
[2400,4800,9.7783e-08],
[3000,50,0.030543],
[3000,750,0.026238],
[3000,1500,0.018827],
[3000,2250,0.0078485],
[3000,2900,0.00033845],
[3000,4500,2.5493e-07],
[3000,6000,7.3718e-09],
[3500,50,0.011405],
[3500,875,0.0091305],
[3500,1750,0.0064078],
[3500,2625,0.002618],
[3500,3400,9.027e-05],
[3500,5250,3.984e-08],
[3500,7000,1.3538e-09],
[3600,50,0.0094965],
[3600,900,0.0073963],
[3600,1800,0.005172],
[3600,2700,0.0021193],
[3600,3500,7.013e-05],
[3600,5400,2.7605e-08],
[3600,7200,1.0035e-09],
[4200,50,0.0033228],
[4200,1050,0.0022143],
[4200,2100,0.0014893],
[4200,3150,0.00059705],
[4200,4100,1.557e-05],
[4200,6300,3.4015e-09],
[4200,8400,2.1228e-10],
[600,600,0.36365],
[600,700,0.079408],
[1000,700,2.7778],
[1000,800,1.5465],
[1000,1000,0.044133],
[1000,1100,0.015058],
[1200,1200,0.019283],
[1200,1500,0.001777],
[1500,1000,0.5361],
[1500,1100,0.39588],
[1500,1200,0.25818],
[1800,1500,0.079323],
[2400,1900,0.023783],
[2400,2100,0.010368],
[3000,1600,0.017545],
[3000,1800,0.014728],
[3000,30,0.032707],
[3000,150,0.029708],
[3000,300,0.02845],
[4000,400,0.0038988],
[5000,500,0.00068688],
[4500,50,0.0020453],
[4500,1125,0.0012275],
[4500,2250,0.00080206],
[4500,3375,0.00031672],
[4500,4400,7.4606e-06],
[5000,50,0.0010374],
[5000,1250,0.00047349],
[5000,2500,0.00028817],
[5000,3750,0.00011102],
[5000,4900,2.2329e-06],
]

Np = 255
N95MAX = 20

ROOT.gStyle.SetPalette(ROOT.kRainBow)

for channel in ["ee","mm"]:

    MIN = 1e6
    MAX = 1e-11

    contour       = ROOT.TGraph2D()

    for sample in signalSamples:

        if channel == "ee":
            string = "hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%sNom_SSZCReeNorm" % (sample[0],sample[1])
            histo = fileEE.Get(string)
            ratio = histo.GetBinContent(1)/370.
        else:
            string = "hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%sNom_SSZCRmmNorm" % (sample[0],sample[1])
            histo = fileMM.Get(string)
            ratio = histo.GetBinContent(1)/130.

        if ratio != 0 and ratio < MIN:
            MIN = ratio
        if ratio > MAX:
            MAX = ratio       
        contour.SetPoint(contour.GetN(),sample[0],sample[1],ratio)
        print channel," ",sample[0]," ",sample[1]," ",histo.GetBinContent(1)

    print MIN," ",MAX

    contour.SetNpx( Np )
    contour.SetNpy( Np )
    contour.SetMinimum( 1e-11 )
    contour.SetMaximum( 1e3 )

    exclusionLine4 = contour.GetHistogram().Clone("exclusionLine4"+channel)
    exclusionLine4.SetContour(1)
    exclusionLine4.SetContourLevel(0,1e-4)
    exclusionLine4.SetLineWidth(2)
    exclusionLine4.SetLineColor(ROOT.kBlue)

    exclusionLine3 = contour.GetHistogram().Clone("exclusionLine3"+channel)
    exclusionLine3.SetContour(1)
    exclusionLine3.SetContourLevel(0,1e-3)
    exclusionLine3.SetLineWidth(2)
    exclusionLine3.SetLineColor(ROOT.kCyan)

    exclusionLine2 = contour.GetHistogram().Clone("exclusionLine2"+channel)
    exclusionLine2.SetContour(1)
    exclusionLine2.SetContourLevel(0,1e-2)
    exclusionLine2.SetLineWidth(2)
    exclusionLine2.SetLineColor(ROOT.kRed)

    leg = ROOT.TLegend(0.185,0.55,0.4,0.7);
    leg.SetBorderSize(0);
    leg.SetFillColor(0);
    leg.SetFillStyle(0);
    leg.SetTextSize(0.045);
    leg.AddEntry(exclusionLine2,"#font[42]{10^{-2}}","l");
    leg.AddEntry(exclusionLine3,"#font[42]{10^{-3}}","l");
    leg.AddEntry(exclusionLine4,"#font[42]{10^{-4}}","l");

    canv = ROOT.TCanvas("contamination_"+channel,"contamination_"+channel,800,600)
    canv.cd()
    canv.SetLogz()
    canv.SetRightMargin(0.18)
    contour.Draw("COLZ")
    contour.GetXaxis().SetNdivisions(505)
    exclusionLine4.Draw("CONT3 SAME")
    exclusionLine3.Draw("CONT3 SAME")
    exclusionLine2.Draw("CONT3 SAME")
    contour.GetZaxis().SetTitleOffset(1.2)
    contour.GetZaxis().SetTitle("N(SIG)/N(SM)")
    contour.GetXaxis().SetTitle("m(W_{R}) [GeV]")
    contour.GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    ROOT.myText(0.185,0.73,1,"SS CR %s" % ("ee" if channel == "ee" else "#mu#mu"))
    leg.Draw()
    ROOT.gPad.RedrawAxis("g")

    canv.Update()
    xmax = ROOT.gPad.GetUxmax()
    xmin = ROOT.gPad.GetUxmin()
    ymax = ROOT.gPad.GetUymax()
    ymin = ROOT.gPad.GetUymin()


    line1 = ROOT.TLine(xmin, xmin+1000, 1400, 1400+1000)
    line1.SetLineWidth(2)
    line1.SetLineColor(ROOT.kBlack)
    line1.Draw("same")

    line2 = ROOT.TLine(1400, 1400+1000, 3000, 1400+1000)
    line2.SetLineWidth(2)
    line2.SetLineColor(ROOT.kBlack)
    line2.Draw("same")

    line3 = ROOT.TLine(3000, 1400+1000, 3000, ymin)
    line3.SetLineWidth(2)
    line3.SetLineColor(ROOT.kBlack)
    line3.Draw("same")

    canv.Print("signalContamination/contamination_%s.eps" % channel)

signals = [
[600,500  ,3],
[600,900  ,4],
[1000,800,6],
[1000,1100,7],
[1200,900,8],
[1200,1500,9],
]

print "test"

for channel in ["electron","muon"]:
    print channel
    for var in zip(["invMassSignalHN", "HTlljj", "Mjj0", "Mlljj0"],["m(ll)","HT(lljj)","m(jj)","m(lljj)"],[2000,2000,1000,5000],["mll","HTlljj","mjj","mlljj"]):
        hs = deepcopy(ROOT.THStack(channel+var[0],channel+var[0]))
        maxVal = 0
        leg = ROOT.TLegend(0.5,0.88-0.035*len(signals),0.85,0.88)
        leg.SetBorderSize(0)
        leg.SetFillColor(0)
        leg.SetFillStyle(0)
        leg.SetTextSize(0.045)
        for WR,NR,color in signals:
            print WR,NR,color
            file = ROOT.TFile(folder+"hists_%s_%s-SS-Z-nomll-CR_SSZCR%s.root" % (var[0], channel, ("ee" if channel=="electron" else "mm")))
            histo = deepcopy(file.Get("h_%s-SS-Z-nomll-CR_nominal_MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s" % (channel,WR,NR) ))
            print histo
            histo.SetLineColor(color)
            histo.SetMarkerColor(color)
            integral = histo.GetSum()
            leg.AddEntry(histo,"#font[42]{%s, %s, sum: %1.1E}" % (format(float(WR/1000.),".2f"),format(float(NR/1000.),".2f"),integral),"l")
            histo.Scale(1./histo.GetSum())
            if histo.GetMaximum() > maxVal:
                maxVal = histo.GetMaximum()
            hs.Add(histo)
        print "canvas"
        c = ROOT.TCanvas(channel+var[0]+"c",channel+var[0]+"c",800,600)
        c.cd()
        print "draw"
        hs.Draw("nostack hist")
        title = var[1] + " [GeV]"
        if channel == "electron":
            title = title.replace("ll","ee")
        else:
            title = title.replace("ll","#mu#mu")
        hs.GetXaxis().SetTitle(title)
        hs.GetXaxis().SetLimits(0,var[2])
        hs.GetXaxis().SetNdivisions(505)
        hs.GetYaxis().SetTitle("Entries / (25 GeV)")
        hs.SetMaximum(maxVal*1.4)
        leg.Draw()
        ROOT.ATLASLabel(0.185,0.875,"Internal",1)
        ROOT.myText(0.185,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
        ROOT.myText(0.185,0.78,1,"%s channel" % channel)
        ROOT.myText(0.51,0.9,1,"mass W_{R} N_{R} [TeV]")
        c.Print("signalContamination/signalDistribution_%s_%s.pdf" % (channel,var[3]))

