from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

from array import array
from decimal import Decimal

import math

ROOT.gROOT.SetBatch(True)

infile = ROOT.TFile("data/mergedOSSS_29_05_FF30.root","update")

for key in infile.GetListOfKeys():
  if "CR" in key.GetName() and "eeos" in key.GetName() and "Zjets" in key.GetName() and "Nom" in key.GetName():
    print key


## modules
import os
import sys
import re
import subprocess

def getLineWithSys(string,table):
  out = re.findall(string+".*",table)[0].split("&")[1:] if len(re.findall(string+".*",table)) else []
  if out == []:
    return out
  out[-1] = out[-1].replace("\\\\","")
  out = [ re.findall("[ ]*\$([0-9\.]*)[\\\pm\_\{ ]*([0-9\.\-]*)[\^\{\} ]*([0-9\.\+]*)[ \}]*\$",x)[0] for x in out]
  return out

def FillHistogram(histo,string,table):
  line = getLineWithSys(string, table)
  if line == []:
    print "no ",string," in table "
    return histo
  assert histo.GetNbinsX() == len(line)-1, "incompatible length "+str(histo.GetNbinsX())+" "+str(len(line))
  for i in range(len(line)-1):
    histo.SetBinContent(i+1,histo.GetBinContent(i+1) + float(line[i+1][0]) )
  return histo

def FillGraphData(graph,axis,string,table):
  line = getLineWithSys(string, table)
  if line == []:
    return graph
  assert graph.GetN() == len(line)-1 == len(axis)-1, "incompatible length "+str(graph.GetN())+" "+str(len(line))
  for i in range(len(line)-1):
    if float(line[i+1][0]) == 0.:
      continue
    graph.SetPoint(i,(axis[i]+axis[i+1])/2.,float(line[i+1][0]))
    graph.SetPointError(i,0,0,getPoissonInterval(float(line[i+1][0]))[0],getPoissonInterval(float(line[i+1][0]))[0])
  return graph

def FillGraphWithSys(graph,axis,string,table):
  line = getLineWithSys(string, table)
  if line == []:
    return graph
  assert graph.GetN() == len(line)-1 == len(axis)-1, "incompatible length "+str(graph.GetN())+" "+str(len(line))
  for i in range(len(line)-1):
    graph.SetPoint(i,(axis[i]+axis[i+1])/2.,float(line[i+1][0]))
    if line[i+1][2]:
      graph.SetPointError(i,(axis[i]+axis[i+1])/2.-axis[i],axis[i+1]-(axis[i]+axis[i+1])/2.,abs(float(line[i+1][1])),abs(float(line[i+1][2])))
    else:
      graph.SetPointError(i,(axis[i]+axis[i+1])/2.-axis[i],axis[i+1]-(axis[i]+axis[i+1])/2.,abs(float(line[i+1][1])),abs(float(line[i+1][1])))

  return graph

def getTotal(string,table):
  line = getLineWithSys(string, table)
  if line == []:
    return 0,0
  first = float(line[0][0])
  if line[0][2]:
    second = (abs(float(line[0][1]))+abs(float(line[0][2])))/2.
  else:
    second = abs(float(line[0][1]))

  return first,second


def getPoissonInterval(n, alpha=1.- 0.682689492):
  return (n - ROOT.Math.gamma_quantile( alpha/2, n, 1.) if n!=0. else 0.,\
    ROOT.Math.gamma_quantile_c( alpha/2, n+1, 1) - n)

#____________________________________________________________
def generateLogBins(bins_N,bins_min,bins_max,scale = 1.):
  bins = []
  bins += [bins_min]
  bins_factor = pow( Decimal(bins_max)/Decimal(bins_min) , Decimal(1)/Decimal(bins_N) )
  for i in range(1,bins_N+1):
    bins += [bins[i-1]*bins_factor]
  for i in range(bins_N+1):
    bins[i] = round(bins[i],0)
    if i!=0: assert bins[i]!=bins[i-1], "two consetuvie bin edges have the same value due to rounding"
  for i in range(0,len(bins)):
    bins[i] = bins[i]/scale
  return bins

#____________________________________________________________
def generateLinBins(bins_N,bins_min,bins_max,scale = 1.):
  bins = []
  bins += [bins_min]
  bins_factor = (bins_max-bins_min)/bins_N
  for i in range(1,bins_N+1):
    bins += [bins[i-1] + bins_factor]
  for i in range(0,len(bins)):
    bins[i] = bins[i]/scale
  return bins

SSONLY = False

regDict = []

fillColor =ROOT.TColor(9999,    85/255., 98/255., 144/255.)
lineColor =ROOT.TColor(9998,    66/255., 99/255., 112/255.)

if not SSONLY:
  regDict += [
  ("ZCR_eeos","e^{#pm}e^{#mp}",              generateLinBins(1,0.5,1.5) ,False,False, "CR (e^{#pm}e^{#mp})"),
  ("SR_emos" ,"e^{#pm}#mu^{#mp}",            generateLinBins(18,0,9000,1000.) ,True,False, "CR (e^{#pm}#mu^{#mp})"),
  ("ZCR_mmos","#mu^{#pm}#mu^{#mp}",          generateLinBins(1,0.5,1.5) ,False,False, "CR (#mu^{#pm}#mu^{#mp})"),
  ]
regDict += [
("SSZCRee" ,"e^{#pm}e^{#pm}",              generateLogBins(8,30,900),False,True, "CR (e^{#pm}e^{#pm})",0,170),
("SSZCRmm" ,"#mu^{#pm}#mu^{#pm}",          [10.] + generateLogBins(4,110,4000)[:-1],False,True, "CR (#mu^{#pm}#mu^{#pm})",0,120),
]
if not SSONLY:
  regDict += [
  ("ZVR_eeos","e^{#pm}e^{#mp}",              generateLinBins(18,0,9000,1000.) ,True,False, "VR (e^{#pm}e^{#mp})"),
  ("ZVR_mmos","#mu^{#pm}#mu^{#mp}",          generateLinBins(18,0,9000,1000.) ,True,False, "VR (#mu^{#pm}#mu^{#mp})"),
  ]
regDict += [
("SSZVRee" ,"e^{#pm}e^{#pm}",              generateLogBins(8,150,4000,1000.),False,True, "VR (e^{#pm}e^{#pm})",0,30),
("SSZVRmm" ,"#mu^{#pm}#mu^{#pm}",          generateLogBins(8,150,4000,1000.),False,True, "VR (#mu^{#pm}#mu^{#pm})",0,13),
]
if not SSONLY:
  regDict += [
  ("SR_eeos" ,"e^{#pm}e^{#mp}",              generateLinBins(18,0,9000,1000.) ,True,False, "SR (e^{#pm}e^{#mp})"),
  ("SR_mmos" ,"#mu^{#pm}#mu^{#mp}",          generateLinBins(18,0,9000,1000.) ,True,False, "SR (#mu^{#pm}#mu^{#mp})"),
  ]
regDict += [
("SSZSRee" ,"e^{#pm}e^{#pm}",              generateLogBins(8,400,4000,1000.),False,True, "SR (e^{#pm}e^{#pm})",0,12),
("SSZSRmm" ,"#mu^{#pm}#mu^{#pm}",          generateLogBins(8,400,4000,1000.),False,True, "SR (#mu^{#pm}#mu^{#pm})",0,6.5),
]
if not SSONLY:
  regDict += [
  ("ZVR_eeos_mjj","e^{#pm}e^{#mp}",  [0.11] + generateLinBins(17,500,9000,1000.) ,True,False, "VR (e^{#pm}e^{#mp})"),
  ("ZVR_mmos_mjj","#mu^{#pm}#mu^{#mp}", [0.11]+ generateLinBins(17,500,9000,1000.) ,True,False, "VR (#mu^{#pm}#mu^{#mp})"),
  ("SR_eeos_mjj" ,"e^{#pm}e^{#mp}", [0.11] + generateLinBins(17,500,9000,1000.),True,False, "SR (e^{#pm}e^{#mp})"),
  ("SR_emos_mjj" ,"e^{#pm}#mu^{#mp}", [0.11] + generateLinBins(17,500,9000,1000.) ,True,False, "CR (e^{#pm}#mu^{#mp})"),
  ("SR_mmos_mjj" ,"#mu^{#pm}#mu^{#mp}", [0.11] + generateLinBins(17,500,9000,1000.) ,True,False, "SR (#mu^{#pm}#mu^{#mp})"),
  ]

print "=== ",len(regDict)
for x in regDict:
  print x[0]

nRegDraw = len(regDict) - 5 if not SSONLY else len(regDict)

regTable = [open('tables/allBins/%s.tex' % x[0],'r').read() for x in regDict]

hAllRegData       = ROOT.TGraphAsymmErrors(nRegDraw)
hAllRegDataRatio  = ROOT.TGraphAsymmErrors(nRegDraw)
hAllRegAllBkgGr   = ROOT.TGraphErrors(nRegDraw)
hAllRegAllBkgGrRa = ROOT.TGraphErrors(nRegDraw)
hAllRegAllBkg     = ROOT.TH1D("hAllRegAllBkg","hAllRegAllBkg",nRegDraw,-0.5,nRegDraw-0.5)
hAllRegDiboson    = ROOT.TH1D("hAllRegDiboson","hAllRegDiboson",nRegDraw,-0.5,nRegDraw-0.5)
hAllRegDrellYan   = ROOT.TH1D("hAllRegDrellYan","hAllRegDrellYan",nRegDraw,-0.5,nRegDraw-0.5)
hAllRegTopPhysics = ROOT.TH1D("hAllRegTopPhysics","hAllRegTopPhysics",nRegDraw,-0.5,nRegDraw-0.5)
hAllRegFakes      = ROOT.TH1D("hAllRegFakes","hAllRegFakes",nRegDraw,-0.5,nRegDraw-0.5)
hProxyLabelHistoAll  = ROOT.TH1D("hProxyLabelHistoAll","hProxyLabelHistoAll",nRegDraw,-0.5,nRegDraw-0.5)
ROOT.gStyle.SetHatchesSpacing(0.5)
hAllRegAllBkgGr.SetFillStyle(3354)
hAllRegAllBkgGr.SetFillColor(ROOT.kBlue+2)
hAllRegAllBkgGrRa.SetFillStyle(3354)
hAllRegAllBkgGrRa.SetFillColor(ROOT.kBlue+2)

hAllRegAllBkgGrPre   = ROOT.TGraphErrors(nRegDraw)
hAllRegAllBkgGrRaPre = ROOT.TGraphErrors(nRegDraw)
hAllRegAllBkgPre     = ROOT.TH1D("hAllRegAllBkgPre","hAllRegAllBkgPre",nRegDraw,-0.5,nRegDraw-0.5)
hAllRegAllBkgPre.SetLineWidth(2)

hAllRegAllBkgGrPre.SetFillStyle(3354)
hAllRegAllBkgGrPre.SetFillColor(ROOT.kRed+2)
hAllRegAllBkgGrRaPre.SetFillStyle(3354)
hAllRegAllBkgGrRaPre.SetFillColor(ROOT.kRed+2)



for fitType in ["Fitted","MC exp."]:

  print fitType

  nReg = -1

  MCTtotstr = "Fitted bkg events" if fitType == "Fitted" else "MC exp. SM events"
  suffix = "post" if fitType == "Fitted" else "pre"


  for reg,table in zip(regDict,regTable):
    print reg
    binsX = array('d',reg[2])
    histoTop = ROOT.TH1D(reg[0]+suffix+"top",reg[0]+suffix+"top",len(reg[2])-1,binsX)
    histoDY = ROOT.TH1D(reg[0]+suffix+"DY",reg[0]+suffix+"DY",len(reg[2])-1,binsX)
    histoDB = ROOT.TH1D(reg[0]+suffix+"dib",reg[0]+suffix+"dib",len(reg[2])-1,binsX)
    histoFake = ROOT.TH1D(reg[0]+suffix+"fake",reg[0]+suffix+"fake",len(reg[2])-1,binsX)
    histoMCTot = ROOT.TH1D(reg[0]+suffix+"tot",reg[0]+suffix+"tot",len(reg[2])-1,binsX)

    histoMCsig1 = ROOT.TH1D(reg[0]+suffix+"sig1",reg[0]+suffix+"sig1",len(reg[2])-1,binsX)
    histoMCsig2 = ROOT.TH1D(reg[0]+suffix+"sig2",reg[0]+suffix+"sig2",len(reg[2])-1,binsX)
    histoMCsig3 = ROOT.TH1D(reg[0]+suffix+"sig3",reg[0]+suffix+"sig3",len(reg[2])-1,binsX)

    for i in range(1,histoTop.GetNbinsX()+1):
      histoTop.SetBinContent(i,0)
      histoDY.SetBinContent(i,0)
      histoDB.SetBinContent(i,0)
      histoFake.SetBinContent(i,0)
      histoMCTot.SetBinContent(i,0)

    hProxyLabelHisto = ROOT.TH1D(reg[0]+suffix+"proxyHisto",reg[0]+suffix+"proxyHisto",len(reg[2])-1,binsX)


    graphData = ROOT.TGraphAsymmErrors(len(reg[2])-1)
    graphMCTot = ROOT.TGraphAsymmErrors(len(reg[2])-1)
    graphData = FillGraphData(graphData,reg[2],"Observed events", table)
    graphMCTot = FillGraphWithSys(graphMCTot,reg[2],MCTtotstr,table)

    print "asdasd ",graphData.GetN()


    histoMCTot = FillHistogram(histoMCTot, MCTtotstr,table)

    histoTop = FillHistogram(histoTop, "%s top\\\\_physics events " % fitType,table)
    histoTop = FillHistogram(histoTop, "%s top\\\\_ttV events " % fitType,table)
    histoFake = FillHistogram(histoFake,"%s fakes events" % fitType,table)
    histoDY = FillHistogram(histoDY,"%s SherpaDY221 events" % fitType,table)
    histoDY = FillHistogram(histoDY,"%s Zjets events" % fitType,table)
    histoDB = FillHistogram(histoDB, "%s dibosonSherpa events " % fitType,table)
    histoDB = FillHistogram(histoDB, "%s Wjets\\\\_Diboson events " % fitType,table)

    ROOT.gStyle.SetHatchesSpacing(0.5)

    graphMCTot.SetFillStyle(3354)
    graphMCTot.SetFillColor(ROOT.kBlue+2)

    histoTop.SetFillColor(9999)
    histoTop.SetLineColor(9998)
    histoTop.SetLineWidth(1)

    histoFake.SetFillColor(ROOT.kRed-6)
    histoFake.SetLineColor(ROOT.kRed+2)
    histoFake.SetLineWidth(1)

    histoDB.SetFillColor(ROOT.kGreen-6)
    histoDB.SetLineColor(ROOT.kGreen-2)
    histoDB.SetLineWidth(1)

    histoDY.SetFillColor(ROOT.kYellow-9)
    histoDY.SetLineColor(ROOT.kYellow-6)
    histoDY.SetLineWidth(1)

    histoMCTot.SetLineColor(ROOT.kBlack)
    histoMCTot.SetLineWidth(2)

    graphDataR  = graphData.Clone("dataR"+reg[0])
    graphMCTotR = graphMCTot.Clone("MCR"+reg[0])

    for i in range(graphDataR.GetN()):
      yy = graphMCTot.GetY()[i] if graphMCTot.GetY()[i] != 0 else 1e9
      if graphData.GetY()[i] > 0.:
        graphDataR.SetPoint(i,graphData.GetX()[i],graphData.GetY()[i]/yy)
      else:
        graphDataR.SetPoint(i,graphData.GetX()[i],-999.) 
      graphDataR.GetEYhigh()[i] = graphData.GetEYhigh()[i]/yy
      graphDataR.GetEYlow()[i] = graphData.GetEYlow()[i]/yy

      binCenter = graphMCTot.GetX()[i]
      binLeftEdge = histoMCTot.GetBinLowEdge(i+1)
      binWidth = histoMCTot.GetBinWidth(i+1)
      binRightEdge = binLeftEdge + binWidth
      if reg[4]:
        binCenter = 10**((math.log10(binLeftEdge)+math.log10(binRightEdge))/2.)

      if graphData.GetY()[i] > 0.:
        graphData.SetPoint(i,binCenter,graphData.GetY()[i])
        graphDataR.SetPoint(i,binCenter,graphDataR.GetY()[i])
      graphMCTotR.SetPoint(i,binCenter,yy/yy)
      graphMCTotR.GetEYhigh()[i] = graphMCTot.GetEYhigh()[i]/yy
      graphMCTotR.GetEYlow()[i] = graphMCTot.GetEYlow()[i]/yy
      graphMCTotR.GetEXhigh()[i] = graphMCTot.GetEYhigh()[i]/yy
      graphMCTotR.GetEXlow()[i] = binCenter - binLeftEdge
      graphMCTotR.GetEXhigh()[i] = binRightEdge - binCenter

    hs = ROOT.THStack("hs","hs")
    hs.Add(histoTop)
    hs.Add(histoFake)
    hs.Add(histoDB)
    hs.Add(histoDY)

    canv = ROOT.TCanvas("c1"+reg[0],"c1"+reg[0],800,600)
    rsplit = 0.3
    pad1TopMarin = 0.07
    pad1BotMarin = 0.04
    pad2TopMarin = 0.03
    pad2BotMarin = 0.45
    ratio = rsplit/(1-rsplit)
    pad1 = ROOT.TPad("pad1","top pad",0.,rsplit,1.,1.)
    pad1.SetTopMargin(pad1TopMarin)
    pad1.SetBottomMargin(pad1BotMarin)
    pad1.Draw()
    pad2 = ROOT.TPad("pad2","bottom pad",0,0,1,rsplit)
    pad2.SetTopMargin(pad2TopMarin)
    pad2.SetBottomMargin(pad2BotMarin)
    pad2.SetTicky()
    pad2.SetTickx()
    pad2.Draw()

    pad1.cd()
    if reg[4]:
      pad1.SetLogx()
    hs.Draw()
    if "mjj" in reg[0]:
      hs.GetXaxis().SetRange(1,8)
    elif reg[0] in ["SR_emos","SR_eeos","SR_mmos"]:
      hs.GetXaxis().SetRange(1,12)
    elif reg[0] in ["ZVR_eeos","ZVR_mmos"] and "mjj" not in reg[0]:
      hs.GetXaxis().SetRange(1,13)
    histoMCTot.Draw("same")
    hs.GetYaxis().SetLabelSize(0.16*ratio )
    hs.GetYaxis().SetTitle("Events")
    hs.GetYaxis().SetTitleOffset(0.4/ratio)
    hs.GetYaxis().SetTitleSize(0.16*ratio)
    hs.GetXaxis().SetLabelSize(0)
    graphMCTot.Draw("same e2")
    if reg[3]:
      hs.SetMinimum( 1e-1 )
      #hs.GetYaxis().SetRangeUser(1e-1,20)
      hs.SetMaximum(10)
#      pad1.SetLogy()
#      hs.SetMaximum( 2.2*ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()) if ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()) else 1e2 )
      #hs.SetMaximum( 2.2*ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()) if ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()) else 1e2 )
    elif not len(reg)>6:
      hs.SetMaximum(10)
      #hs.GetYaxis().SetRangeUser(1e-1,20)
      #hs.SetMaximum( 2.2*ROOT.TMath.Max(ROOT.TMath.MaxElement(graphData.GetN(),graphData.GetY()),ROOT.TMath.MaxElement(graphMCTot.GetN(),graphMCTot.GetY())) )
    else:
      hs.SetMaximum(10)
      #hs.GetYaxis().SetRangeUser(1e-1,20)
      #hs.SetMaximum( reg[7] )

    moveLabel = -0.23

    latex = ROOT.TLatex()
    latex.SetTextAlign(12)
    latex.SetTextSize(0.065)
    latex.SetNDC()
    latex.SetTextFont(72)
    latex.SetTextColor(1)
    latex.DrawLatex(0.40+moveLabel,0.875,"ATLAS");
    latex = ROOT.TLatex()
    latex.SetTextSize(0.06)
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextColor(1)
    latex.DrawLatex(0.525+moveLabel,0.855,"Internal");
    latex = ROOT.TLatex()
    latex.SetTextSize(0.06)
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextColor(1)
    latex.DrawLatex(0.40+moveLabel,0.785,"#sqrt{s}=13 TeV, 36.1 fb^{-1}      #font[42]{post-fit}");
    latex = ROOT.TLatex()
    latex.SetTextSize(0.06)
    latex.SetNDC()
    latex.SetTextFont(42)
    latex.SetTextColor(1)
    if fitType == "MC exp.":
      latex.DrawLatex(0.7+moveLabel,0.853,reg[5] + " pre-fit");
    else:
      latex.DrawLatex(0.7+moveLabel,0.853,reg[5] );

    leg = ROOT.TLegend(0.4+moveLabel,0.55,0.865+moveLabel,0.75)
    leg.SetNColumns(2)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(0.06)
    leg.AddEntry(graphData,"#font[42]{Data}","pe")
    leg.AddEntry(graphMCTot,"#font[42]{Total Pred}","l f")
    if histoDY.GetSum():
      leg.AddEntry(histoDY,"#font[42]{Z+jet(s)}","f")
    if histoDB.GetSum():
      if "SS" in reg[0]:
        leg.AddEntry(histoDB,"#font[42]{Diboson}","f")
      else:
        leg.AddEntry(histoDB,"#font[42]{Diboson,W+jet(s)}","f")        
    if histoFake.GetSum():
      leg.AddEntry(histoFake,"#font[42]{Fakes}","f")
    if histoTop.GetSum():
      leg.AddEntry(histoTop,"#font[42]{t#bar{t},single-t,t#bar{t}V}","f")

    leg.Draw()


    if "SSZSR" in reg[0]:
      sigHisto1 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR4200_NR3150Nom_SSZSR%s_obs_HT" % ("ee" if "ee" in reg[0] else "mm"))
      sigHisto2 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR4200_NR2100Nom_SSZSR%s_obs_HT"  % ("ee" if "ee" in reg[0] else "mm"))
      sigHisto3 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR4500_NR2250Nom_SSZSR%s_obs_HT"  % ("ee" if "ee" in reg[0] else "mm"))
      #sigHisto1 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR600_NR900Nom_SSZSR%s_obs_HT" % ("ee" if "ee" in reg[0] else "mm"))
      #sigHisto2 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR600_NR700Nom_SSZSR%s_obs_HT"  % ("ee" if "ee" in reg[0] else "mm"))
      #sigHisto3 = infile.Get("hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR1000_NR1100Nom_SSZSR%s_obs_HT"  % ("ee" if "ee" in reg[0] else "mm"))
      print "hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR4200_NR2100Nom_SSZSR%s_obs_HT" % ("ee" if "ee" in reg[0] else "mm")
      print sigHisto1
      print sigHisto1.GetNbinsX()
      print histoMCsig1.GetNbinsX()
      assert sigHisto1.GetNbinsX() == histoMCsig1.GetNbinsX(), "incompatible bin X"
      for i in range(histoMCsig1.GetNbinsX()+1):
        histoMCsig1.SetBinContent(i,sigHisto1.GetBinContent(i))
        histoMCsig2.SetBinContent(i,sigHisto2.GetBinContent(i))
        histoMCsig3.SetBinContent(i,sigHisto3.GetBinContent(i))

      histoMCsig1.Draw("same")
      histoMCsig2.Draw("same")
      histoMCsig3.Draw("same")

      histoMCsig1.SetLineStyle(9)
      histoMCsig1.SetLineColor(ROOT.kBlue)
      histoMCsig1.SetLineWidth(4)
      histoMCsig2.SetLineStyle(9)
      histoMCsig2.SetLineColor(ROOT.kRed)
      histoMCsig2.SetLineWidth(4)
      histoMCsig3.SetLineStyle(9)
      histoMCsig3.SetLineColor(ROOT.kGreen)
      histoMCsig3.SetLineWidth(4)

      leg2 = ROOT.TLegend(0.67,0.3,0.877,0.9185)
      leg2.SetBorderSize(0)
      leg2.SetFillColor(0)
      leg2.SetFillStyle(0)
      leg2.SetTextSize(0.06)
      leg2.AddEntry(histoMCsig3,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (4500/1000.,2250/1000.),"l")
      leg2.AddEntry(histoMCsig2,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (4200/1000.,2100/1000.),"l")
      leg2.AddEntry(histoMCsig1,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (4200/1000.,3150/1000.),"l")
      #leg2.AddEntry(histoMCsig3,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (1000/1000.,1100/1000.),"l")
      #leg2.AddEntry(histoMCsig2,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (600/1000.,700/1000.),"l")
      #leg2.AddEntry(histoMCsig1,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (600/1000.,900/1000.),"l")
      leg2.Draw()
    elif "SR_eeos" in reg[0] or "SR_mmos" in reg[0]:
      if "mjj" in reg[0]:
        sigHisto1 = infile.Get("hWR600_NR900_SigNom_SR_%sos_obs_Mjj"   % ("ee" if "ee" in reg[0] else "mm"))
        sigHisto2 = infile.Get("hWR600_NR700_SigNom_SR_%sos_obs_Mjj"  % ("ee" if "ee" in reg[0] else "mm"))
        sigHisto3 = infile.Get("hWR1000_NR1100_SigNom_SR_%sos_obs_Mjj"  % ("ee" if "ee" in reg[0] else "mm"))
      else:
        sigHisto1 = infile.Get("hWR4200_NR3150_SigNom_SR_%sos_obs_Mlljj"   % ("ee" if "ee" in reg[0] else "mm"))
        sigHisto2 = infile.Get("hWR4200_NR2100_SigNom_SR_%sos_obs_Mlljj"  % ("ee" if "ee" in reg[0] else "mm"))
        sigHisto3 = infile.Get("hWR4500_NR2250_SigNom_SR_%sos_obs_Mlljj"  % ("ee" if "ee" in reg[0] else "mm"))
      print sigHisto1
      print "hWR4200_NR2100_SigNom_SR_%sos_obs_Mlljj" % ("ee" if "ee" in reg[0] else "mm")
      print sigHisto1.GetNbinsX()
      print histoMCsig1.GetNbinsX()
      assert sigHisto1.GetNbinsX() == histoMCsig1.GetNbinsX(), "incompatible bin X"
      for i in range(histoMCsig1.GetNbinsX()+1):
        histoMCsig1.SetBinContent(i,sigHisto1.GetBinContent(i))
        histoMCsig2.SetBinContent(i,sigHisto2.GetBinContent(i))
        histoMCsig3.SetBinContent(i,sigHisto3.GetBinContent(i))

      histoMCsig1.Draw("same")
      histoMCsig2.Draw("same")
      histoMCsig3.Draw("same")

      histoMCsig1.SetLineStyle(9)
      histoMCsig1.SetLineColor(ROOT.kBlue)
      histoMCsig1.SetLineWidth(4)
      histoMCsig2.SetLineStyle(9)
      histoMCsig2.SetLineColor(ROOT.kRed)
      histoMCsig2.SetLineWidth(4)
      histoMCsig3.SetLineStyle(9)
      histoMCsig3.SetLineColor(ROOT.kGreen)
      histoMCsig3.SetLineWidth(4)

      leg2 = ROOT.TLegend(0.67,0.3,0.877,0.9185)
      leg2.SetBorderSize(0)
      leg2.SetFillColor(0)
      leg2.SetFillStyle(0)
      leg2.SetTextSize(0.06)
      if "mjj" in reg[0]:
        leg2.AddEntry(histoMCsig1,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (600/1000.,900/1000.),"l")
        leg2.AddEntry(histoMCsig2,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (600/1000.,700/1000.),"l")
        leg2.AddEntry(histoMCsig3,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (1000/1000.,1100/1000.),"l")
      else:
        leg2.AddEntry(histoMCsig1,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (4500/1000.,2250/1000.),"l")
        leg2.AddEntry(histoMCsig2,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (4200/1000.,2100/1000.),"l")
        leg2.AddEntry(histoMCsig3,"#font[42]{#splitline{m_{W_{R}} = %1.1f TeV}{m_{N_{R}} = %1.1f TeV}}" % (4200/1000.,3150/1000.),"l")
      leg2.Draw()

    graphData.Draw("same ep")
    ROOT.gPad.RedrawAxis("g");


    pad2.cd()
    pad2.SetGridy()
    if reg[4]:
      pad2.SetLogx()
    hProxyLabelHisto.Draw()
    if "mjj" in reg[0]:
      hProxyLabelHisto.GetXaxis().SetRange(1,8)
    elif reg[0] in ["SR_emos","SR_eeos","SR_mmos"]:
      hProxyLabelHisto.GetXaxis().SetRange(1,12)
    elif reg[0] in ["ZVR_eeos","ZVR_mmos"] and "mjj" not in reg[0]:
      hProxyLabelHisto.GetXaxis().SetRange(1,13)
    hProxyLabelHisto.GetXaxis().SetNoExponent()
    hProxyLabelHisto.GetXaxis().SetMoreLogLabels()
    hProxyLabelHisto.GetXaxis().SetLabelSize(0.16)
    hProxyLabelHisto.GetXaxis().SetTitleSize(0.16)
    if "SR_emos" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,3.2)
    elif "SR_ee" and "mlljj" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,6.5)
    elif "ZSRee" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    elif "ZSRmm" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    elif "ZCRee" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    elif "ZCRmm" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    elif "ZVRee" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    elif "ZVRmm" in reg[0]:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    else:
      hProxyLabelHisto.GetYaxis().SetRangeUser(0,2.2)
    hProxyLabelHisto.GetXaxis().SetLabelOffset(0.05)
    hProxyLabelHisto.GetYaxis().SetNdivisions(405)
    hProxyLabelHisto.GetYaxis().SetLabelSize(0.16)
    hProxyLabelHisto.GetYaxis().SetTitle("Data/Pred")
    if "SSZCRee" in reg[0] or "SSZCRmm" in reg[0]:
      hProxyLabelHisto.GetXaxis().SetTitle("m_{jj} [GeV]")
    elif "mjj" in reg[0]:
      hProxyLabelHisto.GetXaxis().SetTitle("m_{jj} [TeV]")
    elif "SSZSR" in reg[0] or "SSZVR" in reg[0]:
      hProxyLabelHisto.GetXaxis().SetTitle("H_{T} [TeV]")
    else:
      hProxyLabelHisto.GetXaxis().SetTitle("m_{lljj} [TeV]")
    hProxyLabelHisto.GetXaxis().SetTitleOffset(1.2)
    hProxyLabelHisto.GetYaxis().SetTitleOffset(0.4)
    hProxyLabelHisto.GetYaxis().SetTitleSize(0.16)
    graphMCTotR.Draw("same e2")
    graphDataR.Draw("same pe 0")
    pad2.Update()
    xmin = ROOT.gPad.GetUxmin()
    xmax = ROOT.gPad.GetUxmax()
    ymin = ROOT.gPad.GetUymin()
    ymax = ROOT.gPad.GetUymax()
    if reg[4]:
      line1 = ROOT.TLine(10**xmin,1,  10**xmax,1);
      line2 = ROOT.TLine(10**xmin,1.5,10**xmax,1.5);
      line3 = ROOT.TLine(10**xmin,0.5,10**xmax,0.5);
    else:
      line1 = ROOT.TLine(xmin,1,  xmax,1);
      line2 = ROOT.TLine(xmin,1.5,xmax,1.5);
      line3 = ROOT.TLine(xmin,0.5,xmax,0.5);      
    # line2.SetLineStyle(3)
    # line3.SetLineStyle(3)
    line1.Draw()
    # line2.Draw()
    # line3.Draw()

    canv.Print("histos/histo_"+reg[0]+"_"+suffix+".eps")

    if fitType=="Fitted" and "mjj" not in reg[0]:

      nReg += 1
      Ndata = 0
      NMC = 0
      totMCErr = getTotal(MCTtotstr,table)[1]
      for point in range(graphData.GetN()):
        Ndata += graphData.GetY()[point]
      for point in range(graphMCTot.GetN()):
        NMC += graphMCTot.GetY()[point]
      print "---------- ",totMCErr

      hAllRegData      .SetPoint(nReg,float(nReg),Ndata)
      hAllRegData      .SetPointError(nReg,0,0,getPoissonInterval(Ndata)[0],getPoissonInterval(Ndata)[1])
      hAllRegDataRatio .SetPoint(nReg,float(nReg),Ndata/NMC)
      hAllRegDataRatio .SetPointError(nReg,0,0,getPoissonInterval(Ndata)[0]/NMC,getPoissonInterval(Ndata)[1]/NMC)
      hAllRegAllBkgGr  .SetPoint(nReg,float(nReg),NMC)
      hAllRegAllBkgGr  .SetPointError(nReg,0.5,float(totMCErr))
      hAllRegAllBkgGrRa.SetPoint(nReg,float(nReg),1)
      hAllRegAllBkgGrRa.SetPointError(nReg,0.5,float(totMCErr)/NMC)

      hProxyLabelHistoAll.SetBinContent(nReg+1,-999.)
      hProxyLabelHistoAll.GetXaxis().SetBinLabel(nReg+1,reg[5])

      hAllRegAllBkg    .SetBinContent(nReg+1,float(NMC))
      hAllRegDiboson   .SetBinContent(nReg+1,float(histoDB.GetSum()))
      hAllRegDrellYan  .SetBinContent(nReg+1,float(histoDY.GetSum()))
      hAllRegTopPhysics.SetBinContent(nReg+1,float(histoTop.GetSum()))
      hAllRegFakes     .SetBinContent(nReg+1,float(histoFake.GetSum()))
    elif fitType!="Fitted" and "mjj" not in reg[0]:
      nReg += 1
      Ndata = 0
      NMC = 0
      totMCErr = getTotal(MCTtotstr,table)[1]
      for point in range(graphData.GetN()):
        Ndata += graphData.GetY()[point]
      for point in range(graphMCTot.GetN()):
        NMC += graphMCTot.GetY()[point]
      print "---------- ",totMCErr

      hAllRegAllBkgGrPre  .SetPoint(nReg,float(nReg),NMC)
      hAllRegAllBkgGrPre  .SetPointError(nReg,0.5,float(totMCErr))
      hAllRegAllBkgGrRaPre.SetPoint(nReg,float(nReg),1)
      hAllRegAllBkgGrRaPre.SetPointError(nReg,0.5,float(totMCErr)/NMC)

      hProxyLabelHistoAll.SetBinContent(nReg+1,-999.)
      hProxyLabelHistoAll.GetXaxis().SetBinLabel(nReg+1,reg[5])

      hAllRegAllBkgPre    .SetBinContent(nReg+1,float(NMC))

preOverPost = ROOT.TGraphAsymmErrors()
for i in range(hAllRegAllBkgGrPre.GetN()):
  xx = hAllRegAllBkgGrPre.GetX()[i]
  yy = hAllRegData.GetY()[i]/hAllRegAllBkgGrPre.GetY()[i]
  # yy = hAllRegAllBkgGrPre.GetY()[i] / hAllRegAllBkgGr.GetY()[i]
  yylow  = 0 if yy > 1 else 1-yy
  yyhigh = 0 if yy < 1 else yy-1
  preOverPost.SetPoint(preOverPost.GetN(),xx,yy)
  preOverPost.SetPointError(preOverPost.GetN()-1,0,0,yyhigh,yylow)
preOverPost.Draw("same p")
# preOverPost.SetMarkerSize(0)
preOverPost.SetMarkerStyle(24)
preOverPost.SetMarkerColor(ROOT.kRed)
# preOverPost.SetLineColor(ROOT.kRed)
# preOverPost.SetLineWidth(10)


mg = ROOT.TMultiGraph()
mg.Add(hAllRegAllBkgGrRa,"e2")
mg.Add(preOverPost,"p")
mg.Add(hAllRegDataRatio,"0 pe")


leg2 = ROOT.TLegend(0.125,0.83,0.2,0.95)
leg2.SetBorderSize(0)
leg2.SetFillColor(0)
leg2.SetFillStyle(0)
leg2.SetTextSize(0.10)
leg2.AddEntry(preOverPost,"#font[42]{pre-fit ratio}","p")


hAllRegDrellYan.SetFillColor(ROOT.kYellow-9)
hAllRegDrellYan.SetLineColor(ROOT.kYellow-6)
hAllRegDrellYan.SetLineWidth(1)
hAllRegTopPhysics.SetFillColor(9999)
hAllRegTopPhysics.SetLineColor(9998)
hAllRegTopPhysics.SetLineWidth(1)
hAllRegFakes.SetFillColor(ROOT.kRed-6)
hAllRegFakes.SetLineColor(ROOT.kRed+2)
hAllRegFakes.SetLineWidth(1)
hAllRegDiboson.SetFillColor(ROOT.kGreen-6)
hAllRegDiboson.SetLineColor(ROOT.kGreen-2)
hAllRegDiboson.SetLineWidth(1)

hAllRegAllBkg.SetLineWidth(2)

hs = ROOT.THStack("hs","hs")
hs.Add(hAllRegTopPhysics)
hs.Add(hAllRegFakes)
hs.Add(hAllRegDiboson)
hs.Add(hAllRegDrellYan)

canv = ROOT.TCanvas("c1","c1",1400,600)
rsplit = 0.45
pad1TopMarin = 0.07
pad1BotMarin = 0.04
pad2TopMarin = 0.03
pad2BotMarin = 0.55
pad2BotMarin = 0.55
LeftMarin = 0.1
ratio = rsplit/(1-rsplit)
pad1 = ROOT.TPad("pad1","top pad",0.,rsplit,1.,1.)
pad1.SetTopMargin(pad1TopMarin)
pad1.SetBottomMargin(pad1BotMarin)
pad1.SetLeftMargin(LeftMarin)
pad1.Draw()
pad2 = ROOT.TPad("pad2","bottom pad",0,0,1,rsplit)
pad2.SetTopMargin(pad2TopMarin)
pad2.SetBottomMargin(pad2BotMarin)
pad2.SetLeftMargin(LeftMarin)
pad2.SetTicky()
pad2.SetTickx()
pad2.Draw()

pad1.cd()
#pad1.SetLogy()
hs.Draw()
#hs.GetYaxis().SetRangeUser(0,100)
hs.GetYaxis().SetLabelSize(0.11*ratio )
hs.GetYaxis().SetTitle("Events")
hs.GetYaxis().SetTitleOffset(0.4/ratio)
hs.GetYaxis().SetTitleSize(0.11*ratio)
hs.GetXaxis().SetLabelSize(0)
hAllRegAllBkg.Draw("same")
hAllRegAllBkgGr.Draw("same e2")
hAllRegData.Draw("same 0 pe")

# pad1.Update();
# print ROOT.gPad.GetUymax()
# print ROOT.gPad.GetUymin()
# lineY1 = ROOT.TLine(4.5,10**ROOT.gPad.GetUymin(),4.5,10**ROOT.gPad.GetUymax());
# lineY1.Draw()
# lineY2 = ROOT.TLine(12.5,10**ROOT.gPad.GetUymin(),12.5,10**ROOT.gPad.GetUymax());
# lineY2.Draw()
# latex = ROOT.TLatex()
# latex.SetTextSize(0.10*ratio) 
# latex.SetNDC()
# latex.SetTextFont(42)
# latex.SetTextColor(ROOT.kWhite)
# latex.DrawLatex(3/19.,0.1,"CRs");
# latex = ROOT.TLatex()
# latex.SetTextSize(0.10*ratio) 
# latex.SetNDC()
# latex.SetTextFont(42)
# latex.SetTextColor(ROOT.kWhite)
# latex.DrawLatex(8./19.,0.1,"VRs");
# latex = ROOT.TLatex()
# latex.SetTextSize(0.10*ratio) 
# latex.SetNDC()
# latex.SetTextFont(42)
# latex.SetTextColor(ROOT.kWhite)
# latex.DrawLatex(14/19.,0.1,"SRs");


latex = ROOT.TLatex()
latex.SetTextAlign(12)
latex.SetTextSize(0.11*ratio) 
latex.SetNDC()
latex.SetTextFont(72)
latex.SetTextColor(1)
latex.DrawLatex(0.35,0.85,"ATLAS");
latex = ROOT.TLatex()
latex.SetTextSize(0.10*ratio) 
latex.SetNDC()
latex.SetTextFont(42)
latex.SetTextColor(1)
latex.DrawLatex(0.45,0.83,"Internal");
latex = ROOT.TLatex()
latex.SetTextSize(0.10*ratio) 
latex.SetNDC()
latex.SetTextFont(42)
latex.SetTextColor(1)
latex.DrawLatex(0.35,0.73,"#sqrt{s}=13 TeV, 36.1 fb^{-1}");
latex = ROOT.TLatex()
latex.SetTextSize(0.10*ratio) 
latex.SetNDC()
latex.SetTextFont(42)
latex.SetTextColor(1)
latex.DrawLatex(0.35,0.63,"#font[42]{post-fit}");

leg = ROOT.TLegend(0.57,0.6,0.925,0.9)
leg.SetBorderSize(0)
leg.SetNColumns(2)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg.SetTextSize(0.08)
leg.AddEntry(hAllRegData,"#font[42]{Data}","pe")
leg.AddEntry(hAllRegAllBkgGrRa,"#font[42]{Total Pred}","l f")
leg.AddEntry(hAllRegDrellYan,"#font[42]{Z+jet(s)}","f")
if not SSONLY:
  leg.AddEntry(hAllRegDiboson,"#font[42]{Diboson (and W+jet(s) in OS)}","f")
else:
  leg.AddEntry(hAllRegDiboson,"#font[42]{Diboson}","f") 
leg.AddEntry(hAllRegFakes,"#font[42]{Fakes}","f")
leg.AddEntry(hAllRegTopPhysics,"#font[42]{t#bar{t},single-t,t#bar{t}V}","f")
leg.Draw()

pad2.cd()
hProxyLabelHistoAll.Draw()
line1 = ROOT.TLine(-0.5,1,nRegDraw-0.5,1);
line2 = ROOT.TLine(-0.5,1.2,nRegDraw-0.5,1.2);
line3 = ROOT.TLine(-0.5,0.8,nRegDraw-0.5,0.8);
line2.SetLineStyle(3)
line3.SetLineStyle(3)
line1.Draw()
line2.Draw()
line3.Draw()
hProxyLabelHistoAll.GetXaxis().SetLabelSize(0.17)
hProxyLabelHistoAll.GetXaxis().LabelsOption("v")
hProxyLabelHistoAll.GetYaxis().SetRangeUser(0.7,1.35)
hProxyLabelHistoAll.GetXaxis().SetLabelOffset(0.05)
hProxyLabelHistoAll.GetYaxis().SetNdivisions(405)
hProxyLabelHistoAll.GetYaxis().SetLabelSize(0.11)
hProxyLabelHistoAll.GetYaxis().SetTitle("Data/Pred")
hProxyLabelHistoAll.GetYaxis().SetTitleOffset(0.4)
hProxyLabelHistoAll.GetYaxis().SetTitleSize(0.11)
mg.Draw()
leg2.Draw()


# pad2.Update();
# print ROOT.gPad.GetUymax()
# print ROOT.gPad.GetUymin()
# lineY1d = ROOT.TLine(4.5,ROOT.gPad.GetUymin(),4.5,ROOT.gPad.GetUymax());
# lineY1d.Draw()
# lineY2d = ROOT.TLine(8.5,ROOT.gPad.GetUymin(),8.5,ROOT.gPad.GetUymax());
# lineY2d.Draw()

canv.Print("histos/allRegions.eps")
canv.Print("histos/allRegions.png")
canv.Print("histos/allRegions.pdf")
