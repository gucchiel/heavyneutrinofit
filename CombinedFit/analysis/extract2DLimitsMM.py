from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy


folder = "HistFitter_HN_v2_250_mm"

string = "/ceph/grid/home/atlas/miham/AnalysisCode/"+folder+"/"

NTUPLEDIR = "/ceph/grid/home/atlas/miham/AnalysisCode/"

p = subprocess.Popen(['mkdir -p limits_'+folder],shell=True,stdout=subprocess.PIPE)
output, errors = p.communicate()
print output

ROOT.gStyle.SetPaintTextFormat("2.2f");

ROOT.gROOT.SetBatch(True)

# ROOT.gStyle.SetPalette(ROOT.kBeach)

NRGBs = 5
NCont = 255

stops = array('d',[ 0.00, 0.34, 0.61, 0.87, 1.0 ])
red   = array('d',[ 207./255., 207./255., 174./255., 205./255., 1.00 ])
green = array('d',[ 183./255., 174./255., 198./255., 255./255., 1.00 ])
blue  = array('d',[ 174./255., 198./255., 207./255., 210./255., 1.00 ])

minimumValue = 999

tempGrYellow = ROOT.TH1F("yellow","yellow",1,0,1)
tempGrYellow.SetFillColor(5)
tempGrYellow.SetLineColor(1)
tempGrYellow.SetLineWidth(1)

tempGrRed = ROOT.TH1F("red","red",1,0,1)
tempGrRed.SetLineColor(ROOT.kRed)
tempGrRed.SetLineWidth(1)

tempGrGreen = ROOT.TH1F("green","green",1,0,1)
tempGrGreen.SetFillColor(8)
tempGrGreen.SetLineColor(1)
tempGrGreen.SetLineWidth(1)

lumi =  36097.56

signalSamples = [
[600,1200 ,0.0031095 ],
[600,150  ,48.75],
[600,300  ,35.76],
[600,450  ,14.68],
[600,500  ,50.84],
[600,50   ,8.498],
[600,900  ,0.0149],
[1200,1100,0.1771],
[1200,1800,0.0004182],
[1200,2400,0.00005126],
[1200,300 ,3.29],
[1200,50  ,3.209],
[1200,600 ,2.423],
[1200,900 ,1.046],
[1800,1350,0.1447],
[1800,1700,0.01313],
[1800,2700,0.0000284],
[1800,3600,0.000001971],
[1800,450 ,0.4795],
[1800,50  ,0.4433],
[1800,900 ,0.3794],
[2400,1200,0.07335],
[2400,1800,0.03117],
[2400,2300,0.001689],
[2400,3600,0.00000271],
[2400,4800,1.027E-07],
[2400,50  ,0.1067],
[2400,600 ,0.0973],
[3000,1500,0.01874],
[3000,150 ,0.0099158 ],
[3000,2250,0.007684],
[3000,2900,0.0003066],
[3000,300 ,0.011018  ],
[3000,30  ,0.0095138  ],
[3000,4500,2.635E-07],
[3000,50  ,0.02785],
[3000,6000,7.117E-09],
[3000,750 ,0.02683],
[3500,1750,0.006285],
[3500,2625,0.002501],
[3500,3400,0.00008502],
[3500,50  ,0.01054],
[3500,5250,4.146E-08],
[3500,7000,1.263E-09],
[3500,875 ,0.009024],
[3600,1800,0.005128],
[3600,2700,0.002099],
[3600,3500,0.00007153],
[3600,50  ,0.009091],
[3600,5400,2.691E-08],
[3600,7200,1.068E-09],
[3600,900 ,0.007562],
[4000,400 ,0.0013074 ],
[4200,1050,0.002262],
[4200,2100,0.001544],
[4200,3150,0.0005505],
[4200,4100,0.00001576],
[4200,50  ,0.003327],
[4200,6300,3.45E-09],
[4200,8400,2.186E-10],
[4500,1125,0.0012275],
[4500,2250,0.00080206],
[4500,3375,0.00031672],
[4500,4400,7.4606e-06],
[4500,50  ,0.0020453 ],
[5000,1250,0.00047349],
[5000,2500,0.00028817],
[5000,3750,0.00011102],
[5000,4900,2.2329e-06],
[5000,500 ,0.0010374 ],
[5000,50  ,0.00068688 ],
]

Np = 100
N95MAX = 20

contour       = ROOT.TGraph2D()
contour.SetPoint(contour.GetN(),5500,5500,1e6)
contour.SetPoint(contour.GetN(),-1000,5500,1e6)
contour.SetPoint(contour.GetN(),5500,-1000,1e6)
contour.SetPoint(contour.GetN(),-1000,-1000,1e6)

contourObs       = ROOT.TGraph2D()
contourObs.SetPoint(contourObs.GetN(),5500,5500,1e6)
contourObs.SetPoint(contourObs.GetN(),-1000,5500,1e6)
contourObs.SetPoint(contourObs.GetN(),5500,-1000,1e6)
contourObs.SetPoint(contourObs.GetN(),-1000,-1000,1e6)

contourUP       = ROOT.TGraph2D()
contourUP.SetPoint(contourUP.GetN(),5500,5500,1e6)
contourUP.SetPoint(contourUP.GetN(),-1000,5500,1e6)
contourUP.SetPoint(contourUP.GetN(),5500,-1000,1e6)
contourUP.SetPoint(contourUP.GetN(),-1000,-1000,1e6)

contourDN       = ROOT.TGraph2D()
contourDN.SetPoint(contourDN.GetN(),5500,5500,1e6)
contourDN.SetPoint(contourDN.GetN(),-1000,5500,1e6)
contourDN.SetPoint(contourDN.GetN(),5500,-1000,1e6)
contourDN.SetPoint(contourDN.GetN(),-1000,-1000,1e6)

contourUP2       = ROOT.TGraph2D()
contourUP2.SetPoint(contourUP2.GetN(),5500,5500,1e6)
contourUP2.SetPoint(contourUP2.GetN(),-1000,5500,1e6)
contourUP2.SetPoint(contourUP2.GetN(),5500,-1000,1e6)
contourUP2.SetPoint(contourUP2.GetN(),-1000,-1000,1e6)

contourDN2       = ROOT.TGraph2D()
contourDN2.SetPoint(contourDN2.GetN(),5500,5500,1e6)
contourDN2.SetPoint(contourDN2.GetN(),-1000,5500,1e6)
contourDN2.SetPoint(contourDN2.GetN(),5500,-1000,1e6)
contourDN2.SetPoint(contourDN2.GetN(),-1000,-1000,1e6)

results2D    = ROOT.TH2D("results2D","results2D"            ,5500,0,5500,6500,-500,6000)
results2DN   = ROOT.TH2D("results2DN","results2DN"          ,5500,0,5500,6500,-500,6000)
contourN     = ROOT.TGraph2D()
EfficiencyGr = ROOT.TGraph2D()
EffHisto     = ROOT.TH2D("EffHisto","EffHisto"            ,5500,0,5500,8500,-500,8000)


for sample in signalSamples:
  file = ROOT.TFile(string+"SameSignHN-LRSM_WR%s_NR%s-excl-mm_Output_upperlimit.root" % (sample[0],sample[1]),"READ")
  fileEff = ROOT.TFile(NTUPLEDIR + "HN_v2_050_SYS/nominal/MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s.root"% (sample[0],sample[1]),"READ")
  name = "hypo_MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s" % (sample[0],sample[1])
  result = file.Get(name)

  SRcutflow = fileEff.Get("cutflow_weighted_muon-SS-Z-SR")
  eff = 6*SRcutflow.GetBinContent(SRcutflow.GetNbinsX())/fileEff.Get("cutflow").GetBinContent(1)

  print "WR: ",sample[0]," NR: ",sample[1]," eff: ",eff
  EfficiencyGr.SetPoint(EfficiencyGr.GetN(),sample[0],sample[1],eff)
  EffHisto.Fill(sample[0],sample[1],eff)

  # if not result:
  #   "skipping ",sample[0]," NR: ",sample[1]
  #   continue

  if result and result.GetExpectedUpperLimit(0) < minimumValue:
    minimumValue = result.GetExpectedUpperLimit(0)

  if result:
    print "WR: ",sample[0]," NR: ",sample[1]," ul: ",result.GetExpectedUpperLimit(0)
    contourObs.SetPoint(contourObs.GetN(),sample[0],sample[1],result.UpperLimit())
    contour.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0))
    contourUP.SetPoint(contourUP.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(1))
    contourDN.SetPoint(contourDN.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-1))
    contourUP2.SetPoint(contourUP2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(2))
    contourDN2.SetPoint(contourDN2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-2))
    results2D.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0))
    results2DN.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*sample[2]/6.)
    # if (result.GetExpectedUpperLimit(0)*lumi*sample[2]/6. <= N95MAX ):
    contourN.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*sample[2]/6.)

contour.SetNpx( Np )
contour.SetNpy( Np )
contour.SetMinimum( minimumValue )
contour.SetMaximum( 1.0 )


contourObs.SetNpx( Np )
contourObs.SetNpy( Np )
contourObs.SetMinimum( minimumValue )
contourObs.SetMaximum( 1.0 )

EfficiencyGr.SetNpx( Np )
EfficiencyGr.SetNpy( Np )
EfficiencyGr.SetMinimum( 1./1000 )
EfficiencyGr.SetMaximum( 0.5 )

contourN.SetNpx( Np )
contourN.SetNpy( Np )
contourN.SetMinimum( 3 )
# contourN.SetMaximum( N95MAX )

contourUP.SetNpx( Np )
contourUP.SetNpy( Np )
contourUP2.SetNpx( 50 )
contourUP2.SetNpy( 50 )

contourDN.SetNpx( Np )
contourDN.SetNpy( Np )
contourDN2.SetNpx( Np )
contourDN2.SetNpy( Np )

exclusionLine = contour.GetHistogram().Clone("exclusionLine")
exclusionLine.SetContour(1)
exclusionLine.SetContourLevel(0,1.)
exclusionLine.SetLineColor(1)
exclusionLine.SetLineStyle(1)
exclusionLine.SetLineWidth(2)

exclusionLineObs = contourObs.GetHistogram().Clone("exclusionLineObs")
exclusionLineObs.SetContour(1)
exclusionLineObs.SetContourLevel(0,1.)
exclusionLineObs.SetLineColor(1)
# exclusionLineObs.SetLineStyle(1)
exclusionLineObs.SetLineWidth(2)

exclusionLineUP = contourUP.GetHistogram().Clone("exclusionLineUP")
exclusionLineUP.SetContour(1)
exclusionLineUP.SetContourLevel(0,1.)
exclusionLineUP.SetLineColor(8)
exclusionLineUP.SetLineWidth(2)

exclusionLineDN = contourDN.GetHistogram().Clone("exclusionLineDN")
exclusionLineDN.SetContour(1)
exclusionLineDN.SetContourLevel(0,1.)
exclusionLineDN.SetLineColor(8)
exclusionLineDN.SetLineWidth(2)

exclusionLineUP2 = contourUP2.GetHistogram().Clone("exclusionLineUP2")
exclusionLineUP2.SetContour(1)
exclusionLineUP2.SetContourLevel(0,1.)
exclusionLineUP2.SetLineColor(8)
exclusionLineUP2.SetLineWidth(2)

exclusionLineDN2 = contourDN2.GetHistogram().Clone("exclusionLineDN2")
exclusionLineDN2.SetContour(1)
exclusionLineDN2.SetContourLevel(0,1.)
exclusionLineDN2.SetLineColor(8)
exclusionLineDN2.SetLineWidth(2)

leg = ROOT.TLegend(0.50,0.87,0.7,0.91);
leg.SetBorderSize(0);
leg.SetFillColor(0);
leg.SetFillStyle(0);
leg.SetTextSize(0.045);
leg.AddEntry(exclusionLine,"#font[42]{Expected 95% CL}","l");
# leg.AddEntry(exclusionLineUP,"#font[42]{Expected limit #pm 1#sigma}","l");

# N95
canv1 = ROOT.TCanvas("N95","N95",800,600)
canv1.cd()
canv1.SetRightMargin(0.15)
canv1.SetLogz()
results2DN.Draw("text")
contourN.Draw("COLZ same")
results2DN.Draw("text same");
results2DN.SetMarkerSize(1.3)
results2DN.GetZaxis().SetTitle("N_{95%}")
results2DN.GetXaxis().SetTitle("m(W_{R}) [GeV]")
results2DN.GetYaxis().SetTitle("m(N_{R}) [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.49,0.875,1,"N_{95}/6 (rescaled)")
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
ROOT.myText(0.185,0.725,1,"muon channel (#mu^{#pm}#mu^{#pm})")
ROOT.gPad.RedrawAxis("g");
canv1.Print("limits_"+folder+"/N95_mm.eps")

# eff
canvE = ROOT.TCanvas("efficiency","efficiency",800,600)
canvE.cd()
canvE.SetRightMargin(0.15)
EffHisto.Draw("text");
EfficiencyGr.Draw("COLZ same");
EffHisto.Draw("text same");
EffHisto.SetMarkerSize(1.3)
EffHisto.GetZaxis().SetTitle("N_{95%}")
EffHisto.GetXaxis().SetTitle("m(W_{R}) [GeV]")
EffHisto.GetYaxis().SetTitle("m(N_{R}) [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.49,0.875,1,"efficiency*6 (rescaled)")
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
ROOT.myText(0.185,0.725,1,"muon channel (#mu^{#pm}#mu^{#pm})")
ROOT.gPad.RedrawAxis("g");
canvE.Print("limits_"+folder+"/eff_mm.eps")

# nominal mu_sig
canv = ROOT.TCanvas("limits","limits",800,600)
canv.cd()
canv.SetRightMargin(0.15)
results2D.Draw("text");
line = ROOT.TLine(0, 0, 5500, 5000)
line.SetLineStyle(2)
line.SetLineColor(ROOT.kGray)
line2 = ROOT.TLine(0, 0, 5500, 0)
line2.SetLineStyle(3)
line2.SetLineColor(ROOT.kGray)

contour.Draw("COLZ same")
exclusionLine.Draw("CONT LIST same")
canv.Update()
conts = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
contourObs.Draw("COLZ same")
exclusionLineObs.Draw("CONT LIST same")
canv.Update()
contsObs = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineUP.Draw("CONT LIST same")
canv.Update()
contsUP = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineDN.Draw("CONT LIST same")
canv.Update()
contsDN = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineUP2.Draw("CONT LIST same")
canv.Update()
contsUP2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineDN2.Draw("CONT LIST same")
canv.Update()
contsDN2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))

results2D.Draw("text");
contour.Draw("COLZ same")
exclusionLine.Draw("CONT3 same")
exclusionLineObs.Draw("CONT3 same")
# exclusionLineUP2.Draw("CONT3 same")
line .Draw("same")
line2.Draw("same")
results2D.Draw("text same")
ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
ROOT.gStyle.SetNumberContours(NCont)
results2D.SetMarkerSize(1.3)
results2D.GetZaxis().SetTitle("#mu_{95%}")
results2D.GetXaxis().SetTitle("m(W_{R}) [GeV]")
results2D.GetYaxis().SetTitle("m(N_{R}) [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
ROOT.myText(0.185,0.725,1,"muon channel (#mu^{#pm}#mu^{#pm})")
leg.Draw()
ROOT.gPad.RedrawAxis("g");
canv.Print("limits_"+folder+"/limits_mm.eps")

print conts.GetSize()
print contsUP.GetSize()
print contsDN.GetSize()

cont = conts[0]
contObs = contsObs[0]
contUP = contsUP[0]
contDN = contsDN[0]
contUP2 = contsUP2[0]
contDN2 = contsDN2[0]

canv2 = ROOT.TCanvas("excl","excl",800,600)
canv2.cd()
contDN.First().SetFillColor(8)
contDN2.First().SetFillColor(5)
contUP.First().SetFillColor(5)
contUP2.First().SetFillColor(ROOT.kWhite)
cont.First().SetLineStyle(2)

leg2 = ROOT.TLegend(0.18,0.61,0.4,0.81);
leg2.SetBorderSize(0);
leg2.SetFillColor(0);
leg2.SetFillStyle(0);
leg2.SetTextSize(0.045);
leg2.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
leg2.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
leg2.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
leg2.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

contDN2.First().Draw("a F")
contDN.First().Draw("F")
contUP.First().Draw("same F")
contUP2.First().Draw("same F")
cont.First().Draw("l")
contObs.First().Draw("l")

contDN2.First().GetYaxis().SetRangeUser(50,4000)
contDN2.First().GetXaxis().SetRangeUser(700,5000)
contDN2.First().GetXaxis().SetTitle("m(W_{R}) [GeV]")
contDN2.First().GetYaxis().SetTitle("m(N_{R}) [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.185,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
ROOT.myText(0.60,0.875,1,"muon channel (#mu^{#pm}#mu^{#pm})")
leg2.Draw()

line3 = ROOT.TLine(600, 50, 5000, 4000)
line3.SetLineStyle(2)
line3.SetLineColor(ROOT.kGray+1)
# line3.Draw()
ROOT.gPad.RedrawAxis("g");

canv2.Print("limits_"+folder+"/exclusion_mm.eps")

leg3 = ROOT.TLegend(0.2,0.61,0.4,0.81);
leg3.SetBorderSize(0);
leg3.SetFillColor(0);
leg3.SetFillStyle(0);
leg3.SetTextSize(0.045);
leg3.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
leg3.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
leg3.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
leg3.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

leg4 = ROOT.TLegend(0.5,0.60,0.82,0.85);
leg4.SetBorderSize(0);
leg4.SetFillColor(0);
leg4.SetFillStyle(0);
leg4.SetTextSize(0.045);
leg4.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
leg4.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
leg4.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
leg4.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");
leg4.AddEntry(tempGrRed,"#font[42]{#sigma_{KS}}","l");

for ratio in [2./3,4./3,2.,4.]:
  obs = ROOT.TGraph()
  exp = ROOT.TGraph()
  exp1 = ROOT.TGraphAsymmErrors()
  exp2 = ROOT.TGraphAsymmErrors()
  exp1.SetFillColor(8)
  exp2.SetFillColor(5)
  exp.SetLineStyle(2)
  exp.SetLineWidth(2)
  obs.SetLineWidth(2)

  xsec = ROOT.TGraph()
  obs_xsec = ROOT.TGraph()
  exp_xsec = ROOT.TGraph()
  exp1_xsec = ROOT.TGraphAsymmErrors()
  exp2_xsec = ROOT.TGraphAsymmErrors()
  exp1_xsec.SetFillColor(8)
  exp2_xsec.SetFillColor(5)
  exp_xsec.SetLineStyle(2)
  exp_xsec.SetLineWidth(2)
  obs_xsec.SetLineWidth(2)
  xsec.SetLineWidth(2)
  xsec.SetLineColor(ROOT.kRed)

  Xmin = 10000
  Xmax = 0
  for sample in signalSamples:
    if float(sample[0]) != ratio*sample[1]:
      continue

    file = ROOT.TFile(string+"SameSignHN-LRSM_WR%s_NR%s-excl-mm_Output_upperlimit.root" % (sample[0],sample[1]),"READ")
    fileEff = ROOT.TFile(NTUPLEDIR + "HN_v2_050_SYS/nominal/MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s.root"% (sample[0],sample[1]),"READ")
    name = "hypo_MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s" % (sample[0],sample[1])
    result = file.Get(name)

    print sample[0]," ",sample[2]
    if not result:
      print "continue"
      continue

    if sample[0] < Xmin:
      Xmin = sample[0]
    if sample[0] > Xmax:
      Xmax = sample[0]

    print result.UpperLimit()
    print result.GetExpectedUpperLimit(0)
    print result.GetExpectedUpperLimit(1)
    print result.GetExpectedUpperLimit(2)
    print result.GetExpectedUpperLimit(-1)
    print result.GetExpectedUpperLimit(-2)

    obs.SetPoint(obs.GetN(),sample[0],result.UpperLimit())
    exp.SetPoint(exp.GetN(),sample[0],result.GetExpectedUpperLimit(0))
    exp1.SetPoint(exp1.GetN(),sample[0],result.GetExpectedUpperLimit(0))
    exp2.SetPoint(exp2.GetN(),sample[0],result.GetExpectedUpperLimit(0))
    exp1.SetPointError(exp1.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-1),result.GetExpectedUpperLimit(1)-result.GetExpectedUpperLimit(0))
    exp2.SetPointError(exp2.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-2),result.GetExpectedUpperLimit(2)-result.GetExpectedUpperLimit(0))

    obs_xsec.SetPoint(obs_xsec.GetN(),sample[0],result.UpperLimit()*sample[2])
    exp_xsec.SetPoint(exp_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
    exp1_xsec.SetPoint(exp1_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
    exp2_xsec.SetPoint(exp2_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
    exp1_xsec.SetPointError(exp1_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*sample[2]-result.GetExpectedUpperLimit(-1)*sample[2],result.GetExpectedUpperLimit(1)*sample[2]-result.GetExpectedUpperLimit(0)*sample[2])
    exp2_xsec.SetPointError(exp2_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*sample[2]-result.GetExpectedUpperLimit(-2)*sample[2],result.GetExpectedUpperLimit(2)*sample[2]-result.GetExpectedUpperLimit(0)*sample[2])
    xsec.SetPoint(xsec.GetN(),sample[0],sample[2])

  # exp1.SetPoint(exp1.GetN(),1e6,1e6)
  # exp1.SetPointError(exp1.GetN()-1,0,0,0,0)
  # exp2.SetPoint(exp2.GetN(),1e6,1e6)
  # exp2.SetPointError(exp2.GetN()-1,0,0,0,0)

  mg1D = ROOT.TMultiGraph()
  mg1D.Add(exp2,"3")
  mg1D.Add(exp1,"3")
  mg1D.Add(exp,"l")
  mg1D.Add(obs,"l")

  mg1D_xsec = ROOT.TMultiGraph()
  mg1D_xsec.Add(exp2_xsec,"3")
  mg1D_xsec.Add(exp1_xsec,"3")
  mg1D_xsec.Add(exp_xsec,"l")
  mg1D_xsec.Add(obs_xsec,"l")
  mg1D_xsec.Add(xsec,"c")

  print Xmin," ",Xmax

  canv1D = ROOT.TCanvas("excl1D%1.2f"%ratio,"excl1D%1.2f"%ratio,800,600)
  canv1D.cd()
  canv1D.SetLogy()
  mg1D.Draw("a")
  mg1D.SetMaximum(1e3)
  mg1D.GetXaxis().SetTitle("m(W_{R}) = %1.2f #times m(N_{R})" % ratio)
  mg1D.GetYaxis().SetTitle("#mu_{SIG}^{95%}")
  mg1D.GetXaxis().SetRangeUser(Xmin,Xmax)
  ROOT.ATLASLabel(0.2,0.875,"Internal",1)
  ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
  ROOT.myText(0.58,0.875,1,"muon channel (#mu^{#pm}#mu^{#pm})")
  leg3.Draw()
  canv1D.Update();
  xmin = ROOT.gPad.GetUxmin()
  xmax = ROOT.gPad.GetUxmax()
  ymin = ROOT.gPad.GetUymin()
  ymax = ROOT.gPad.GetUymax()
  line = ROOT.TLine(xmin,1,xmax,1)
  line.Draw()
  line.SetLineColor(ROOT.kRed)
  ROOT.gPad.RedrawAxis("g");
  canv1D.Print("limits_"+folder+"/exclusion_mm_1D_mu_%2.0f.eps" % float(100*ratio))

  canv1D_xsec = ROOT.TCanvas("excl1D%1.2f_xsec"%ratio,"excl1D%1.2f_xsec"%ratio,800,600)
  canv1D_xsec.cd()
  canv1D_xsec.SetLogy()
  mg1D_xsec.Draw("a")
  if ratio < 2:
    mg1D_xsec.SetMaximum(1e3)
    mg1D_xsec.SetMinimum(1e-4)
  else:
    mg1D_xsec.SetMaximum(1e5)
    mg1D_xsec.SetMinimum(1e-4)    
  mg1D_xsec.GetXaxis().SetTitle("m(W_{R}) = %1.2f #times m(N_{R})" % ratio)
  mg1D_xsec.GetYaxis().SetTitle("#sigma [pb]")
  mg1D_xsec.GetXaxis().SetRangeUser(Xmin,Xmax)
  ROOT.ATLASLabel(0.2,0.875,"Internal",1)
  ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
  ROOT.myText(0.52,0.875,1,"muon channel (#mu^{#pm}#mu^{#pm})")
  leg4.Draw()
  ROOT.gPad.RedrawAxis("g");
  canv1D_xsec.Print("limits_"+folder+"/exclusion_mm_1D_%2.0f.eps" % float(100*ratio))

# print exclusionLineUP.GetN()
# print exclusionLineDN.GetN()

