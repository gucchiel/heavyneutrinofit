from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

def myText_2(x,y,color,text):
  l = ROOT.TLatex()
  l.SetTextColor(color)
  l.DrawLatex(x,y,text)
 
def ATLASLabel_2(x,y,text,color):
  l = ROOT.TLatex()
  l.SetTextFont(72)
  l.SetTextColor(color)

  xmax = ROOT.gPad.GetUxmax()
  xmin = ROOT.gPad.GetUxmin()
  delx = (xmax-xmin)*(0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()))

  l.DrawLatex(x,y,"ATLAS")
  if text:
    p = ROOT.TLatex()
    p.SetTextFont(42)
    p.SetTextColor(color)
    p.DrawLatex(x+1.5*delx,y,text)



## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy

ROOT.gStyle.SetPaintTextFormat("2.2f");

ROOT.gROOT.SetBatch(True)

# ROOT.gStyle.SetPalette(ROOT.kBeach)

NRGBs = 5
NCont = 255

stops = array('d',[ 0.00, 0.34, 0.61, 0.87, 1.0 ])
red   = array('d',[ 207./255., 207./255., 174./255., 205./255., 1.00 ])
green = array('d',[ 183./255., 174./255., 198./255., 255./255., 1.00 ])
blue  = array('d',[ 174./255., 198./255., 207./255., 210./255., 1.00 ])

tempGrYellow = ROOT.TH1F("yellow","yellow",1,0,1)
tempGrYellow.SetFillColor(5)
tempGrYellow.SetLineColor(1)
tempGrYellow.SetLineWidth(1)

tempGrRed = ROOT.TH1F("red","red",1,0,1)
tempGrRed.SetLineColor(ROOT.kRed)
tempGrRed.SetLineWidth(1)

tempGrGreen = ROOT.TH1F("green","green",1,0,1)
tempGrGreen.SetFillColor(8)
tempGrGreen.SetLineColor(1)
tempGrGreen.SetLineWidth(1)

lumi =  36097.56 #(pb^-1)

basefolder = "results_mu_scan_FF_10/"
#basefolder = "HistFitter_HN_combined_076_exclusion"

outputFolder = "/afs/cern.ch/work/g/gucchiel/private/HistFitterComb/HistFitter/HistFitter/HistFitter"
#outputFolder = "/ceph/grid/home/atlas/miham/AnalysisCode"

NTUPLEDIR = "/ceph/grid/home/atlas/miham/AnalysisCode/"

SSONLY = False

#cross-section in pb
signalSamples = [
#[ 600 , 50 , 51.02 ],
[ 600 , 150 , 49.713 ],
[ 600 , 300 , 37.33 ],
[ 600 , 450 , 16.138 ],
[ 600 , 500 , 8.7848 ],
[ 600 , 600 , 0.36365 ],
[ 600 , 700 , 0.079408 ],
[ 600 , 900 , 0.015643 ],
[ 600 , 1200 , 0.0031095 ],
[ 1000 , 700 , 2.7778 ],
[ 1000 , 800 , 1.5465 ],
[ 1000 , 1000 , 0.044133 ],
[ 1000 , 1100 , 0.015058 ],
[ 1000 , 1400 , 2.0290E-06*1E3 ],
#[ 1200 , 50 , 3.2448 ],
#[ 1200 , 300 , 3.1065 ],
[ 1200 , 600 , 2.3153 ],
[ 1200 , 900 , 0.98898 ],
[ 1200 , 1100 , 0.17725 ],
[ 1200 , 1200 , 0.019283 ],
[ 1200 , 1500 , 0.001777 ],
[ 1200 , 1800 , 0.0004383 ],
[ 1200 , 2400 , 5.162e-05 ],
[ 1500 , 1000 , 0.5361 ],
[ 1500 , 1100 , 0.39588 ],
[ 1500 , 1200 , 0.25818 ],
[ 1500 , 1600 , 3.2349E-06*1E3 ],
[ 1500 , 1800 , 7.2710E-07*1E3 ],
#[ 1800 , 50 , 0.50828 ],
#[ 1800 , 450 , 0.47505 ],
[ 1800 , 900 , 0.3507 ],
[ 1800 , 1350 , 0.14888 ],
[ 1800 , 1500 , 0.079323 ],
[ 1800 , 1600 , 0.04133 ],
[ 1800 , 1700 , 0.01393 ],
[ 1800 , 2700 , 2.9008e-05 ],
[ 1800 , 3600 , 1.9213e-06 ],
[ 2100 , 1500, 8.1807E-05*1E3 ],
[ 2100 , 1800 , 2.7776E-05*1E3 ],
[ 2100 , 2200 , 7.2120E-07*1E3 ],
#[ 2400 , 50 , 0.11263 ],
#[ 2400 , 600 , 0.10148 ],
[ 2400 , 1200 , 0.074743 ],
[ 2400 , 1800 , 0.031435 ],
[ 2400 , 1900 , 0.023783 ],
[ 2400 , 2100 , 0.010368 ],
[ 2400 , 2300 , 0.001897 ],
[ 2400 , 3600 , 2.5613e-06 ],
[ 2400 , 4800 , 9.7783e-08 ],
[ 2700 , 300 , 5.4544E-05*1E3 ],
[ 3000 , 30 , 0.032707 ],
#[ 3000 , 50 , 0.030543 ],
#[ 3000 , 150 , 0.029708 ],
[ 3000 , 300 , 0.02845 ],
[ 3000 , 750 , 0.026238 ],
[ 3000 , 1500 , 0.018827 ],
[ 3000 , 1600 , 0.017545 ],
[ 3000 , 1800 , 0.014728 ],
[ 3000 , 2250 , 0.0078485 ],
[ 3000 , 2900 , 0.00033845 ],
[ 3000 , 4500 , 2.5493e-07 ],
[ 3000 , 6000 , 7.3718e-09 ],
#[ 3500 , 50 , 0.011405 ],
#[ 3500 , 400, 9.9448E-06*1E3 ],
[ 3500 , 875 , 0.0091305 ],
[ 3500 , 1750 , 0.0064078 ],
[ 3500 , 2625 , 0.002618 ],
[ 3500 , 3400 , 9.027e-05 ],
[ 3500 , 5250 , 3.984e-08 ],
[ 3500 , 7000 , 1.3538e-09 ],
#[ 3600 , 50 , 0.0094965 ],
[ 3600 , 900 , 0.0073963 ],
[ 3600 , 1800 , 0.005172 ],
[ 3600 , 2700 , 0.0021193 ],
[ 3600 , 3500 , 7.013e-05 ],
[ 3600 , 5400 , 2.7605e-08 ],
[ 3600 , 7200 , 1.0035e-09 ],
[ 3800, 3000, 1.0520E-06*1E3 ],
[ 4000 , 400 , 0.0038988 ],
#[ 4200 , 50 , 0.0033228 ],
#[ 4200 , 200, 3.0004E-06*1E3 ],
[ 4200 , 400, 2.6964E-06*1E3 ],
[ 4200 , 1050 , 0.0022143 ],
[ 4200 , 2100 , 0.0014893 ],
[ 4200 , 3150 , 0.00059705 ],
[ 4200 , 4100 , 1.557e-05 ],
[ 4200 , 6300 , 3.4015e-09 ],
[ 4200 , 8400 , 2.1228e-10 ],
#[ 4500 , 50 , 0.0020453 ],
[ 4500 , 800, 1.4201E-06*1E3 ],
[ 4500 , 1125 , 0.0012275 ],
[ 4500 , 2250 , 0.00080206 ],
[ 4500 , 3375 , 0.00031672 ],
[ 4500 , 4400 , 7.4606e-06 ],
[ 4750 , 3000, 3.3310E-07*1E3 ],
#[ 5000 , 50 , 0.0010374 ],
#[ 5000 , 500 , 0.00068688 ],
[ 5000 , 1250 , 0.00047349 ],
[ 5000 , 2500 , 0.00028817 ],
[ 5000 , 3750 , 0.00011102 ],
[ 5000 , 4900 , 2.2329e-06 ],
[ 5200 , 1000 , 3.8360E-07*1E3 ],
[ 5200 , 2000 , 2.5260E-07*1E3 ],
[ 5200 , 3000 , 1.5610E-07*1E3 ],
#[ 5800 , 500  , 2.2190E-07*1E3 ],
[ 5800 , 1000, 1.5420E-07*1E3 ],
[ 5800 , 2000 , 8.5500E-08*1E3 ],
[ 5800 , 3000 , 5.3700E-08*1E3 ],
]

Np = 255
N95MAX = 20

XMIN = 650.
XMAX = 5000.
YMIN = 100.
YMAX = 4790.
results2DLimitProxy  = ROOT.TH2F("results2DLimitProxy","results2DLimitProxy",10,XMIN,XMAX,10,YMIN,YMAX)


#for channel in ["ee"]:
for channel in ["ee","mm"]:



  print "running for channel: ", channel

  contDN2c = []
  contDNc  = []
  contUPc  = []
  contUP2c = []
  contc    = []
  contObsc = []

  # for neutrino in ["","Dirac"]:
  for neutrino in [""]:

    minimumValue = 999
    minimumValue2 = 1e6
    maximumValue2 = 0

    print "running for neutrino: ", (neutrino if neutrino != "" else "Majorana")

    underscore = "_" if neutrino != "" else ""

    p = subprocess.Popen(['mkdir -p limits_'+basefolder],shell=True,stdout=subprocess.PIPE)
    output, errors = p.communicate()
    # print output


    contour       = ROOT.TGraph2D()
    contour.SetPoint(contour.GetN(),6000,6000,1e6)
    contour.SetPoint(contour.GetN(),-2000,6000,1e6)
    contour.SetPoint(contour.GetN(),6000,-2000,1e6)
    contour.SetPoint(contour.GetN(),-2000,-2000,1e6)
    contour.SetPoint(contour.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contour.SetPoint(contour.GetN(),600,100,1e-2)

    contourObs       = ROOT.TGraph2D()
    contourObs.SetPoint(contourObs.GetN(),6000,6000,1e6)
    contourObs.SetPoint(contourObs.GetN(),-2000,6000,1e6)
    contourObs.SetPoint(contourObs.GetN(),6000,-2000,1e6)
    contourObs.SetPoint(contourObs.GetN(),-2000,-2000,1e6)
    contourObs.SetPoint(contourObs.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourObs.SetPoint(contourObs.GetN(),600,100,1e-2)

    contourObsUp       = ROOT.TGraph2D()
    contourObsUp.SetPoint(contourObsUp.GetN(),6000,6000,1e6)
    contourObsUp.SetPoint(contourObsUp.GetN(),-2000,6000,1e6)
    contourObsUp.SetPoint(contourObsUp.GetN(),6000,-2000,1e6)
    contourObsUp.SetPoint(contourObsUp.GetN(),-2000,-2000,1e6)
    contourObsUp.SetPoint(contourObsUp.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourObsUp.SetPoint(contour.GetN(),600,100,1e-2*0.9)

    contourObsDn       = ROOT.TGraph2D()
    contourObsDn.SetPoint(contourObsDn.GetN(),6000,6000,1e6)
    contourObsDn.SetPoint(contourObsDn.GetN(),-2000,6000,1e6)
    contourObsDn.SetPoint(contourObsDn.GetN(),6000,-2000,1e6)
    contourObsDn.SetPoint(contourObsDn.GetN(),-2000,-2000,1e6)
    contourObsDn.SetPoint(contourObsDn.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourObsDn.SetPoint(contourObsDn.GetN(),600,100,1e-2*1.1)


    contourUP       = ROOT.TGraph2D()
    contourUP.SetPoint(contourUP.GetN(),6000,6000,1e6)
    contourUP.SetPoint(contourUP.GetN(),-2000,6000,1e6)
    contourUP.SetPoint(contourUP.GetN(),6000,-2000,1e6)
    contourUP.SetPoint(contourUP.GetN(),-2000,-2000,1e6)
    contourUP.SetPoint(contourUP.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourUP.SetPoint(contourUP.GetN(),600,100,1e-2*2)


    contourDN       = ROOT.TGraph2D()
    contourDN.SetPoint(contourDN.GetN(),6000,6000,1e6)
    contourDN.SetPoint(contourDN.GetN(),-2000,6000,1e6)
    contourDN.SetPoint(contourDN.GetN(),6000,-2000,1e6)
    contourDN.SetPoint(contourDN.GetN(),-2000,-2000,1e6)
    contourDN.SetPoint(contourDN.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourDN.SetPoint(contourDN.GetN(),600,100,1e-2/2)


    contourUP2       = ROOT.TGraph2D()
    contourUP2.SetPoint(contourUP2.GetN(),6000,6000,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),-2000,6000,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),6000,-2000,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),-2000,-2000,1e6)
    contourUP2.SetPoint(contourUP2.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourUP2.SetPoint(contourUP2.GetN(),600,100,1e-2*6)


    contourDN2       = ROOT.TGraph2D()
    contourDN2.SetPoint(contourDN2.GetN(),6000,6000,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),-2000,6000,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),6000,-2000,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),-2000,-2000,1e6)
    contourDN2.SetPoint(contourDN2.GetN(),0,0,1e-4)
    # if channel == "ee":
    #     contourDN2.SetPoint(contourDN2.GetN(),600,100,1e-2/6)


    results2DLimit       = ROOT.TGraph2D()

    results2D    = ROOT.TH2D("results2D"+channel+neutrino,"results2D"+channel+neutrino         ,6000,0,6000,6500,-500,6000)
    results2DN   = ROOT.TH2D("results2DN"+channel+neutrino,"results2DN"+channel+neutrino       ,6000,0,6000,6500,-500,6000)
    #EffHisto     = ROOT.TH2D("EffHisto"+channel+neutrino,"EffHisto"+channel+neutrino           ,6000,0,6000,8500,-500,8000)
    contourN     = ROOT.TGraph2D()
    #EfficiencyGr = ROOT.TGraph2D()


    for sample in signalSamples:

      # if channel=="mm":
      #   if (sample[0],sample[1]) in [(600,50),(600,300),(600,450),(1200,900),(1500,1200),(1800,900),(1800,1350)]:
      #       continue

      #string = outputFolder+"/"+basefolder+"_"+channel+"/"
      string = outputFolder+"/"+basefolder+"/"

      # print sample[0]," ",sample[1]

      #fileEff = ROOT.TFile(NTUPLEDIR + "HN_v2_052_SYS/nominal/MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s.root"% (sample[0],sample[1]),"READ")
      #SRcutflow = fileEff.Get("cutflow_weighted_%s-SS-Z-SR" % ("electron" if channel == "ee" else "muon"))
      #if not SRcutflow:
       # print "not ",  sample[0]," ",sample[1]
      #eff = 6*SRcutflow.GetBinContent(SRcutflow.GetNbinsX())/fileEff.Get("cutflow").GetBinContent(1)
      # print eff
      #EfficiencyGr.SetPoint(EfficiencyGr.GetN(),sample[0],sample[1],eff)
      #EffHisto.Fill(sample[0],sample[1],eff)

      name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])      
      if not os.path.isfile(string+"SameSignHN-WR%s_NR%s_Sig_%s-excl_Output_upperlimit.root" % (sample[0],sample[1],channel)):
        results2D.Fill(sample[0],sample[1],-1)
        print name," not available file.."
        continue

      file = ROOT.TFile(string+"SameSignHN-WR%s_NR%s_Sig_%s-excl_Output_upperlimit.root" % (sample[0],sample[1],channel),"READ")
      result = file.Get(name)

      # print result
      if result and result.GetExpectedUpperLimit(0) < minimumValue:
        minimumValue = result.GetExpectedUpperLimit(0)

      if (sample[0] <= XMAX) and (sample[1] <= ( YMIN + ( 0.725 * (YMAX-100) ) ) ):
          if result and result.UpperLimit()*sample[2] < minimumValue2:
            minimumValue2 = result.UpperLimit()*sample[2]
          if result and result.UpperLimit()*sample[2] > maximumValue2:
            maximumValue2 = result.UpperLimit()*sample[2]

      if result:
        # print "WR: ",sample[0]," NR: ",sample[1]," ratio: ",sample[0]/float(sample[1])," ul: ",result.GetExpectedUpperLimit(0)
        contourObs.SetPoint(contourObs.GetN(),sample[0],sample[1],result.UpperLimit())
        contourObsUp.SetPoint(contourObsUp.GetN(),sample[0],sample[1],result.UpperLimit())
        contourObsDn.SetPoint(contourObsDn.GetN(),sample[0],sample[1],result.UpperLimit())
        contour.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0))
        contourUP.SetPoint(contourUP.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(1))
        contourDN.SetPoint(contourDN.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-1))
        contourUP2.SetPoint(contourUP2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(2))
        contourDN2.SetPoint(contourDN2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-2))
        results2D.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0))
        results2DLimit.SetPoint(results2DLimit.GetN(),sample[0],sample[1],result.UpperLimit()*sample[2])
        results2DN.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*sample[2]/6.)
        # if (result.GetExpectedUpperLimit(0)*lumi*sample[2]/6. <= N95MAX ):
        contourN.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*sample[2]/6.)

      else:
        #print name," not available LIMIT.. EFF: ",eff," ",sample[0]," ",sample[1]
        print name," not available  ",sample[0]," ",sample[1]
        results2D.Fill(sample[0],sample[1],-2)

      file.Close()

    contour.SetNpx( Np )
    contour.SetNpy( Np )
    contour.SetMinimum( minimumValue )
    contour.SetMaximum( 1.0 )


    contourObs.SetNpx( Np )
    contourObs.SetNpy( Np )
    contourObs.SetMinimum( minimumValue )
    contourObs.SetMaximum( 1.0 )

    contourObsUp.SetNpx( Np )
    contourObsUp.SetNpy( Np )
    contourObsUp.SetMinimum( minimumValue )
    contourObsUp.SetMaximum( 0.9 )

    contourObsDn.SetNpx( Np )
    contourObsDn.SetNpy( Np )
    contourObsDn.SetMinimum( minimumValue )
    contourObsDn.SetMaximum( 1.1 )

    #EfficiencyGr.SetNpx( Np )
    #EfficiencyGr.SetNpy( Np )
    #EfficiencyGr.SetMinimum( 1./1000 )
    #EfficiencyGr.SetMaximum( 0.5 )

    contourUP.SetNpx( Np )
    contourUP.SetNpy( Np )
    contourUP2.SetNpx( Np )
    contourUP2.SetNpy( Np )

    contourDN.SetNpx( Np )
    contourDN.SetNpy( Np )
    contourDN2.SetNpx( Np )
    contourDN2.SetNpy( Np )

    #results2DLimit.SetMinimum(0)
    #results2DLimit.SetMaximum(maximumValue2)
    #results2DLimit.SetMaximum(4)
    results2DLimit.SetNpy( 150 )
    results2DLimit.SetNpx( 150 )

    print "minimumValue2 ",minimumValue2
    print "maximumValue2 ",maximumValue2

    exclusionLine = contour.GetHistogram().Clone("exclusionLine"+channel+neutrino)
    exclusionLine.SetContour(1)
    exclusionLine.SetContourLevel(0,2 if neutrino == "Dirac" else 1)

    exclusionLineObs = contourObs.GetHistogram().Clone("exclusionLineObs"+channel+neutrino)
    exclusionLineObs.SetContour(1)
    exclusionLineObs.SetContourLevel(0,2 if neutrino == "Dirac" else 1)

    exclusionLineObsUp = contourObsUp.GetHistogram().Clone("exclusionLineObs"+channel+neutrino)
    exclusionLineObsUp.SetContour(1)
    exclusionLineObsUp.SetContourLevel(0,1.8 if neutrino == "Dirac" else 0.9)

    exclusionLineObsDn = contourObsDn.GetHistogram().Clone("exclusionLineObs"+channel+neutrino)
    exclusionLineObsDn.SetContour(1)
    exclusionLineObsDn.SetContourLevel(0,2.2 if neutrino == "Dirac" else 1.1)

    exclusionLineUP = contourUP.GetHistogram().Clone("exclusionLineUP"+channel+neutrino)
    exclusionLineUP.SetContour(1)
    exclusionLineUP.SetContourLevel(0,2 if neutrino == "Dirac" else 1)

    exclusionLineDN = contourDN.GetHistogram().Clone("exclusionLineDN"+channel+neutrino)
    exclusionLineDN.SetContour(1)
    exclusionLineDN.SetContourLevel(0,2 if neutrino == "Dirac" else 1)

    exclusionLineUP2 = contourUP2.GetHistogram().Clone("exclusionLineUP2"+channel+neutrino)
    exclusionLineUP2.SetContour(1)
    exclusionLineUP2.SetContourLevel(0,2 if neutrino == "Dirac" else 1)

    exclusionLineDN2 = contourDN2.GetHistogram().Clone("exclusionLineDN2"+channel+neutrino)
    exclusionLineDN2.SetContour(1)
    exclusionLineDN2.SetContourLevel(0,2 if neutrino == "Dirac" else 1)

    leg = ROOT.TLegend(0.50,0.87,0.7,0.91);
    leg.SetBorderSize(0);
    leg.SetFillColor(0);
    leg.SetFillStyle(0);
    leg.SetTextSize(0.045);
    leg.AddEntry(exclusionLine,"#font[42]{Expected 95% CL}","l");

    # eff
    ROOT.gStyle.SetPalette(ROOT.kBird)
    canvE = ROOT.TCanvas("efficiency","efficiency",800,600)
    canvE.cd()
    canvE.SetRightMargin(0.15)
    #EffHisto.Draw("text");
    #EfficiencyGr.Draw("COLZ same");
    #EffHisto.Draw("text same");
    #EffHisto.SetMarkerSize(1.3)
    #EffHisto.GetZaxis().SetTitle("N_{95%}")
    #EffHisto.GetXaxis().SetTitle("m(W_{R}) [GeV]")
    #EffHisto.GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.49,0.875,1,"efficiency*6 (rescaled)")
    ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    ROOT.myText(0.185,0.725,1,"electron channel (e^{#pm}e^{#pm})")
    ROOT.gPad.RedrawAxis("g");
    canvE.Print("limits_"+basefolder+"/eff_%s.eps" % (channel+underscore+neutrino))


    # nominal mu_sig
    canv = ROOT.TCanvas("limits"+channel+neutrino,"limits"+channel+neutrino,800,600)
    canv.cd()
    canv.SetRightMargin(0.15)
    results2D.Draw("text");
    line = ROOT.TLine(0, 0, 6000, 5000)
    line.SetLineStyle(2)
    line.SetLineColor(ROOT.kGray)
    line2 = ROOT.TLine(0, 0, 6000, 0)
    line2.SetLineStyle(3)
    line2.SetLineColor(ROOT.kGray)

    contour.Draw("COLZ same")
    exclusionLine.Draw("CONT LIST same")
    exclusionLine.SetLineStyle(10)
    exclusionLine.SetLineColor(ROOT.kBlack)
    canv.Update()
    conts = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    contourObs.Draw("COLZ same")
    exclusionLineObs.Draw("CONT LIST same")
    exclusionLineObs.SetLineStyle(1)
    exclusionLineObs.SetLineColor(ROOT.kBlack)
    canv.Update()
    contsObs = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineUP.Draw("CONT LIST same")
    exclusionLineUP.SetLineStyle(10)
    exclusionLineUP.SetLineColor(ROOT.kRed+2)
    canv.Update()
    contsUP = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineDN.Draw("CONT LIST same")
    exclusionLineDN.SetLineStyle(10)
    exclusionLineDN.SetLineColor(ROOT.kRed+4)
    canv.Update()
    contsDN = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineUP2.Draw("CONT LIST same")
    canv.Update()
    contsUP2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineDN2.Draw("CONT LIST same")
    canv.Update()
    contsDN2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))

    exclusionLineObsDn.Draw("CONT LIST same")
    canv.Update()
    contsObsDn = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
    exclusionLineObsUp.Draw("CONT LIST same")
    canv.Update()
    contsObsUp = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))


    results2D.Draw("text");
    contour.Draw("COLZ same")
    exclusionLine.Draw("CONT2 same")
    # exclusionLineObs.Draw("CONT3 same")
    exclusionLineUP2.Draw("CONT2 same")
    exclusionLineUP.Draw("CONT2 same")
    exclusionLineDN2.Draw("CONT2 same")
    line .Draw("same")
    line2.Draw("same")
    results2D.Draw("text same")
    ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
    ROOT.gStyle.SetNumberContours(NCont)
    results2D.SetMarkerSize(1.3)
    results2D.GetZaxis().SetTitle("#mu_{95%}")
    results2D.GetXaxis().SetTitle("m(W_{R}) [GeV]")
    results2D.GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    if not SSONLY:
        ROOT.myText(0.185,0.725,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
    else:
        ROOT.myText(0.185,0.725,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
    leg.Draw()
    ROOT.gPad.RedrawAxis("g");
    canv.Print("limits_"+basefolder+"/limits_%s.eps" % (channel+underscore+neutrino))

    cont = conts[0]
    contObs = contsObs[0]
    contObsDn = contsObsDn[0]
    contObsUp = contsObsUp[0]
    contUP = contsUP[0]
    contDN = contsDN[0]
    contUP2 = contsUP2[0]
    contDN2 = contsDN2[0]

    cont.First().SetLineStyle(2)
    cont.First().SetLineWidth(3)
    cont.First().SetLineColor(ROOT.kRed)
    contUP.First().SetLineStyle(2)
    contUP.First().SetLineColor(ROOT.kRed)
    contUP.First().SetLineWidth(1)
    contDN.First().SetLineStyle(4)
    contDN.First().SetLineColor(ROOT.kRed)
    contDN.First().SetLineWidth(1)

    contObsDn.First().SetLineWidth(1)
    contObsUp.First().SetLineWidth(1)
    contObs.First().SetLineWidth(2)

    leg1 = ROOT.TLegend(0.50,0.70,0.64,0.92);
    leg1.SetBorderSize(0);
    leg1.SetFillColor(0);
    leg1.SetFillStyle(0);
    leg1.SetTextSize(0.04);
    leg1.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg1.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg1.AddEntry(contUP.First(),"#font[42]{Expected limit + 1#sigma}","l");
    leg1.AddEntry(contDN.First(),"#font[42]{Expected limit - 1#sigma}","l");

    # corss-section limit
    ROOT.gStyle.SetPalette(ROOT.kThermometer)
    canv1 = ROOT.TCanvas("crossSectionLimit"+channel+neutrino,"crossSectionLimit"+channel+neutrino,800,600)
    canv1.SetLogz()
    canv1.cd()
    canv1.SetRightMargin(0.18)
    canvWidth  = 800.*(1 - canv1.GetRightMargin() - canv1.GetLeftMargin())
    canvHeight = 600.*(1 - canv1.GetTopMargin() - canv1.GetBottomMargin())
    canvRatio = canvWidth/(1.0*canvHeight)
    results2DLimitProxy.Fill(1000,4200,1)
    results2DLimitProxy.Draw("COLZ")
    results2DLimit.Draw("same COLZ")
    results2DLimit.SetPoint(results2DLimit.GetN(),5000,6000,1e-6)
    #cont.First().Draw("c same")
    #contUP.First().Draw("c same")
    #contDN.First().Draw("c same")
    #contObsDn.First().Draw("c same")
    #contObsUp.First().Draw("c same")
    #contObs.First().Draw("c same")
    canv1.Update()
    xmax = ROOT.gPad.GetUxmax()
    xmin = ROOT.gPad.GetUxmin()
    ymax = ROOT.gPad.GetUymax()
    ymin = ROOT.gPad.GetUymin()
    print xmin," ",xmax," ",ymin," ",ymax
    print "ymin+0.725*(ymax-ymin) ",ymin+0.725*(ymax-ymin)
    line3 = ROOT.TLine(xmin, xmin, 0.9*xmax, 0.9*xmax)
    line3.SetLineStyle(3)
    line3.SetLineColor(ROOT.kGray)
    line3.Draw("same")
    tt = ROOT.TLatex()
    tt.SetTextAngle(38)
    # tt.SetTextAngle(360/(2*ROOT.TMath.Pi())*ROOT.TMath.ATan( canvRatio ))
    tt.SetTextColor(ROOT.kGray)
    tt.SetTextSize(0.03)
    tt.DrawLatex(xmin+0.39*(xmax-xmin),ymin+0.52*(ymax-ymin),"m(W_{R}) = m(N_{R})")
    tt2 = ROOT.TLatex()
    tt2.SetTextAngle(56.4)
    # tt2.SetTextAngle(360/(2*ROOT.TMath.Pi())*ROOT.TMath.ATan( 2*canvRatio ))
    tt2.SetTextColor(ROOT.kGray)
    tt2.SetTextSize(0.03)
    tt2.DrawLatex(xmin+0.14*(xmax-xmin),ymin+0.43*(ymax-ymin),"m(W_{R}) = 2 #times m(N_{R})")
    line4 = ROOT.TLine(xmin, 2*xmin, xmax, 2*xmax)
    line4.SetLineWidth(8)
    line4.SetLineColor(ROOT.kWhite)
    line4.Draw("same")
    results2DLimitProxy.GetZaxis().SetTitle("95% CL upper limit on cross-section [pb]")
    results2DLimitProxy.GetZaxis().SetTitleOffset(1.2)
    results2DLimitProxy.GetZaxis().SetNdivisions(0)
#    results2DLimitProxy.GetZaxis().SetRangeUser(0.0000001,0.01)
    results2DLimitProxy.GetXaxis().SetTitle("m(W_{R}) [GeV]")
    results2DLimitProxy.GetXaxis().SetNdivisions(505)
    results2DLimitProxy.GetYaxis().SetTitle("m(N_{R}) [GeV]")

    results2DLimitProxy.GetXaxis().SetLabelOffset(999)
    results2DLimitProxy.GetXaxis().SetLabelSize(0)
    results2DLimitProxy.GetYaxis().SetLabelOffset(999)
    results2DLimitProxy.GetYaxis().SetLabelSize(0)

    ROOT.gPad.RedrawAxis("g");

    box = ROOT.TBox(xmin,ymin+0.725*(ymax-ymin),xmax,ymax)
    box.SetFillColor(ROOT.kWhite)
    box.SetFillStyle(1001)
    box.SetLineColor(ROOT.kBlack)
    box.Draw("l")

    # custom legend
    LineLeft  = xmin + 0.51*(xmax-xmin)
    LineRight = xmin + 0.55*(xmax-xmin)
    LineY1    = ymin + 0.85*(ymax-ymin)
    LineY2    = ymin + 0.92*(ymax-ymin)
    ATLASLabel_2(xmin + 0.02*(xmax-xmin),LineY2,"Internal",1)
    myText_2(xmin + 0.02*(xmax-xmin),LineY1,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    if not SSONLY:
        myText_2(xmin + 0.02*(xmax-xmin),0.80*(ymax-ymin),1,"%s N_{R}, %s channel, g_{R} = g_{L}" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
    else:
        myText_2(xmin + 0.02*(xmax-xmin),0.80*(ymax-ymin),1,"same-sign, %s channel, g_{R} = g_{L}" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
    print "ymin + 0.85*(ymax-ymin) ",ymin + 0.85*(ymax-ymin)
    #lineExp   = ROOT.TLine(LineLeft, LineY1+0.02*(ymax-ymin), LineRight, LineY1+0.02*(ymax-ymin))
    #lineExp.SetLineStyle(2)
    #lineExp.SetLineWidth(3)
    #lineExp.SetLineColor(ROOT.kRed)
    #lineExp.Draw()
    #lineExp1   = ROOT.TLine(LineLeft, LineY1+0.01*(ymax-ymin), LineRight, LineY1+0.01*(ymax-ymin))
    #lineExp1.SetLineStyle(2)
    #lineExp1.SetLineWidth(1)
    #lineExp1.SetLineColor(ROOT.kRed)
    #lineExp1.Draw()
    #lineExp2   = ROOT.TLine(LineLeft, LineY1+0.03*(ymax-ymin), LineRight, LineY1+0.03*(ymax-ymin))
    #lineExp2.SetLineStyle(2)
    #lineExp2.SetLineWidth(1)
    #lineExp2.SetLineColor(ROOT.kRed)
    #lineExp2.Draw()
    #myText_2(LineRight+0.01*(xmax-xmin), LineY1, ROOT.kBlack, "Expected #pm 1 #sigma")
    #lineObs   = ROOT.TLine(LineLeft, LineY2+0.02*(ymax-ymin), LineRight, LineY2+0.02*(ymax-ymin))
    #lineObs.SetLineStyle(1)
    #lineObs.SetLineWidth(3)
    #lineObs.SetLineColor(ROOT.kBlack)
    #lineObs.Draw()
    #lineObs1   = ROOT.TLine(LineLeft, LineY2+0.01*(ymax-ymin), LineRight, LineY2+0.01*(ymax-ymin))
    #lineObs1.SetLineStyle(1)
    #lineObs1.SetLineWidth(1)
    #lineObs1.SetLineColor(ROOT.kBlack)
    #lineObs1.Draw()
    #lineObs2   = ROOT.TLine(LineLeft, LineY2+0.03*(ymax-ymin), LineRight, LineY2+0.03*(ymax-ymin))
    #lineObs2.SetLineStyle(1)
    #lineObs2.SetLineWidth(1)
    #lineObs2.SetLineColor(ROOT.kBlack)
    #lineObs2.Draw()
    #myText_2(LineRight+0.01*(xmax-xmin), LineY2, ROOT.kBlack, "Observed #pm ^{10% sig}_{x-sec}")

    xaxisTeV = ROOT.TGaxis(ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY1(),ROOT.gPad.GetFrame().GetX2(),ROOT.gPad.GetFrame().GetY1(),XMIN/1000.,XMAX/1000.,10,"")
    xaxisTeV.SetLabelSize(0.045)
    xaxisTeV.SetLabelFont(42)
    xaxisTeV.Draw()

    yaxisTeV = ROOT.TGaxis(ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY1(),ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY2(),YMIN/1000.,YMAX/1000.,10,"")
    yaxisTeV.SetLabelSize(0.045)
    yaxisTeV.SetLabelFont(42)
    yaxisTeV.Draw()

    canv1.Update()

    canv1.Print("limits_"+basefolder+"/crossSectionLimit_%s.eps" % (channel+underscore+neutrino))


    cont.First().SetLineStyle(2)
    cont.First().SetLineWidth(3)
    cont.First().SetLineColor(ROOT.kBlack)


    canv2 = ROOT.TCanvas("excl","excl",800,600)
    canv2.cd()
    contDN.First().SetFillColor(8)
    contDN2.First().SetFillColor(5)
    contUP.First().SetFillColor(5)
    contUP2.First().SetFillColor(ROOT.kWhite)
    cont.First().SetLineStyle(2)

    print  "contDN2.First().GetN() ",  contDN2.First().GetN()

    leg2 = ROOT.TLegend(0.18,0.61,0.4,0.81);
    leg2.SetBorderSize(0);
    leg2.SetFillColor(0);
    leg2.SetFillStyle(0);
    leg2.SetTextSize(0.045);
    leg2.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg2.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg2.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
    leg2.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

    contDN2.First().Draw("a F")
    contDN.First().Draw("F")
    contUP.First().Draw("same F")
    contUP2.First().Draw("same F")
    cont.First().Draw("l")
    contObs.First().Draw("l")

    contDN2.First().GetYaxis().SetRangeUser(100,4000)
    contDN2.First().GetXaxis().SetRangeUser(700,5500)
    contDN2.First().GetXaxis().SetTitle("m(W_{R}) [GeV]")
    contDN2.First().GetYaxis().SetTitle("m(N_{R}) [GeV]")
    ROOT.ATLASLabel(0.185,0.875,"Internal",1)
    ROOT.myText(0.185,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
    line3 = ROOT.TLine(700, 700, 4000, 4000)
    line3.SetLineStyle(2)
    line3.SetLineColor(ROOT.kGray+1)
    line3.Draw()
    # line4 = ROOT.TLine(700, 1050, 2666, 4000)
    # line4.SetLineStyle(2)
    # line4.SetLineColor(ROOT.kGray+1)
    # line4.Draw()
    # line5 = ROOT.TLine(700, 700, 4000, 4000)
    # line5.SetLineStyle(2)
    # line5.SetLineColor(ROOT.kGray+1)
    # line5.Draw()
    if not SSONLY:
        ROOT.myText(0.54,0.875,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
    else:
        ROOT.myText(0.54,0.875,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
    ROOT.myText(0.54,0.82,1,"g_{R} = g_{L}")
    leg2.Draw()
    ROOT.gPad.RedrawAxis("g");
    canv2.Print("limits_"+basefolder+"/exclusion_%s.eps" % (channel+underscore+neutrino))

    contDN2c += [deepcopy(contDN2.First().Clone(contDN2.GetName()+"clone"+channel))]
    contDNc  += [deepcopy(contDN.First() .Clone(contDN .GetName()+"clone"+channel))]
    contUPc  += [deepcopy(contUP.First() .Clone(contUP .GetName()+"clone"+channel))]
    contUP2c += [deepcopy(contUP2.First().Clone(contUP2.GetName()+"clone"+channel))]
    contc    += [deepcopy(cont.First()   .Clone(cont   .GetName()+"clone"+channel))]
    contObsc += [deepcopy(contObs.First().Clone(contObs.GetName()+"clone"+channel))]

    leg3 = ROOT.TLegend(0.2,0.61,0.4,0.81);
    leg3.SetBorderSize(0);
    leg3.SetFillColor(0);
    leg3.SetFillStyle(0);
    leg3.SetTextSize(0.045);
    leg3.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg3.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg3.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
    leg3.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

    leg4 = ROOT.TLegend(0.5,0.60,0.82,0.85);
    leg4.SetBorderSize(0);
    leg4.SetFillColor(0);
    leg4.SetFillStyle(0);
    leg4.SetTextSize(0.045);
    leg4.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
    leg4.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
    leg4.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
    leg4.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");
    leg4.AddEntry(tempGrRed,"#font[42]{#sigma_{KS}}","l");

    # for ratio in []:
    for ratio in [2./3,4./3,2.,4.]:

      # print "running 1D limits for ratio: ",ratio

      obs = ROOT.TGraph()
      exp = ROOT.TGraph()
      exp1 = ROOT.TGraphAsymmErrors()
      exp2 = ROOT.TGraphAsymmErrors()
      exp1.SetFillColor(8)
      exp2.SetFillColor(5)
      exp.SetLineStyle(2)
      exp.SetLineWidth(2)
      obs.SetLineWidth(2)

      xsec = ROOT.TGraph()
      obs_xsec = ROOT.TGraph()
      exp_xsec = ROOT.TGraph()
      exp1_xsec = ROOT.TGraphAsymmErrors()
      exp2_xsec = ROOT.TGraphAsymmErrors()
      exp1_xsec.SetFillColor(8)
      exp2_xsec.SetFillColor(5)
      exp_xsec.SetLineStyle(2)
      exp_xsec.SetLineWidth(2)
      obs_xsec.SetLineWidth(2)
      xsec.SetLineWidth(2)
      xsec.SetLineColor(ROOT.kRed)

      Xmin = 10000
      Xmax = 0
      for sample in signalSamples:
        if float(sample[0]) != ratio*sample[1]:
          continue

        file = ROOT.TFile(string+"SameSignHN-WR%s_NR%s_Sig_%s-excl_Output_upperlimit.root" % (sample[0],sample[1],channel),"READ")
        name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])
        result = file.Get(name)

        if not result:
          # print "continue ",sample[0]," ",sample[2]
          continue

        if sample[0] < Xmin:
          Xmin = sample[0]
        if sample[0] > Xmax:
          Xmax = sample[0]

        obs.SetPoint(obs.GetN(),sample[0],result.UpperLimit())
        exp.SetPoint(exp.GetN(),sample[0],result.GetExpectedUpperLimit(0))
        exp1.SetPoint(exp1.GetN(),sample[0],result.GetExpectedUpperLimit(0))
        exp2.SetPoint(exp2.GetN(),sample[0],result.GetExpectedUpperLimit(0))
        exp1.SetPointError(exp1.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-1),result.GetExpectedUpperLimit(1)-result.GetExpectedUpperLimit(0))
        exp2.SetPointError(exp2.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-2),result.GetExpectedUpperLimit(2)-result.GetExpectedUpperLimit(0))

        obs_xsec.SetPoint(obs_xsec.GetN(),sample[0],result.UpperLimit()*sample[2])
        exp_xsec.SetPoint(exp_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
        exp1_xsec.SetPoint(exp1_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
        exp2_xsec.SetPoint(exp2_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*sample[2])
        exp1_xsec.SetPointError(exp1_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*sample[2]-result.GetExpectedUpperLimit(-1)*sample[2],result.GetExpectedUpperLimit(1)*sample[2]-result.GetExpectedUpperLimit(0)*sample[2])
        exp2_xsec.SetPointError(exp2_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*sample[2]-result.GetExpectedUpperLimit(-2)*sample[2],result.GetExpectedUpperLimit(2)*sample[2]-result.GetExpectedUpperLimit(0)*sample[2])
        xsec.SetPoint(xsec.GetN(),sample[0],sample[2])

        file.Close()

      # exp1.SetPoint(exp1.GetN(),1e6,1e6)
      # exp1.SetPointError(exp1.GetN()-1,0,0,0,0)
      # exp2.SetPoint(exp2.GetN(),1e6,1e6)
      # exp2.SetPointError(exp2.GetN()-1,0,0,0,0)

      mg1D = ROOT.TMultiGraph()
      mg1D.Add(exp2,"3")
      mg1D.Add(exp1,"3")
      mg1D.Add(exp,"l")
      mg1D.Add(obs,"l")

      mg1D_xsec = ROOT.TMultiGraph()
      mg1D_xsec.Add(exp2_xsec,"3")
      mg1D_xsec.Add(exp1_xsec,"3")
      mg1D_xsec.Add(exp_xsec,"l")
      mg1D_xsec.Add(obs_xsec,"l")
      mg1D_xsec.Add(xsec,"c")

      # print Xmin," ",Xmax

      canv1D = ROOT.TCanvas("excl1D%1.2f%s"%(ratio,channel),"excl1D%1.2f%s"%(ratio,channel),800,600)
      canv1D.cd()
      canv1D.SetLogy()
      mg1D.Draw("a")
      mg1D.SetMaximum(1e3)
      mg1D.GetXaxis().SetTitle("m(W_{R}) = %1.2f #times m(N_{R})" % ratio)
      mg1D.GetYaxis().SetTitle("#mu_{SIG}^{95%}")
      mg1D.GetXaxis().SetRangeUser(Xmin,Xmax)
      ROOT.ATLASLabel(0.2,0.875,"Internal",1)
      ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
      if not SSONLY:
          ROOT.myText(0.54,0.875,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
      else:
          ROOT.myText(0.54,0.875,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
      leg3.Draw()
      canv1D.Update();
      xmin = ROOT.gPad.GetUxmin()
      xmax = ROOT.gPad.GetUxmax()
      ymin = ROOT.gPad.GetUymin()
      ymax = ROOT.gPad.GetUymax()
      line = ROOT.TLine(xmin,1,xmax,1)
      line.Draw()
      line.SetLineColor(ROOT.kRed)
      ROOT.gPad.RedrawAxis("g");
      canv1D.Print("limits_"+basefolder+"/exclusion_1D_mu_%2.0f_%s.eps" % (float(100*ratio),(channel+underscore+neutrino)))

      canv1D_xsec = ROOT.TCanvas("excl1D%1.2f_xsec%s"%(ratio,channel),"excl1D%1.2f_xsec%s"%(ratio,channel),800,600)
      canv1D_xsec.cd()
      canv1D_xsec.SetLogy()
      mg1D_xsec.Draw("a")
      if ratio < 2:
        mg1D_xsec.SetMaximum(1e3)
        mg1D_xsec.SetMinimum(1e-4)
      else:
        mg1D_xsec.SetMaximum(1e5)
        mg1D_xsec.SetMinimum(1e-4)    
      mg1D_xsec.GetXaxis().SetTitle("m(W_{R}) = %1.2f #times m(N_{R})" % ratio)
      mg1D_xsec.GetYaxis().SetTitle("#sigma [pb]")
      mg1D_xsec.GetXaxis().SetRangeUser(Xmin,Xmax)
      ROOT.ATLASLabel(0.2,0.875,"Internal",1)
      ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
      if not SSONLY:
          ROOT.myText(0.54,0.875,1,"%s N_{R}, %s channel" % ((neutrino if neutrino == "Dirac" else "Majorana"),("#it{ee}" if channel == "ee" else "#mu#mu")) )
      else:
          ROOT.myText(0.54,0.875,1,"same-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu") )
      ROOT.myText(0.2,0.77,1,"g_{R} = g_{L}")
      leg4.Draw()
      ROOT.gPad.RedrawAxis("g");
      canv1D_xsec.Print("limits_"+basefolder+"/exclusion_1D_%2.0f_%s.eps" % (float(100*ratio),(channel+underscore+neutrino)))

  # canvc = ROOT.TCanvas("canvc"+channel,"canvc"+channel,800,600)
  # canvc.cd()
  # for x in [contDN2c[1],contDNc[1],contUPc[1]]:
  #   x.SetFillColorAlpha(x.GetFillColor(),0.5)

  # contDN2c[0].Draw("a F")
  # contDNc[0].Draw("same F")
  # contUPc[0].Draw("same F")
  # contUP2c[0].Draw("same F")

  # # contDN2c[1].SetFillStyle(3004)
  # # contDNc[1] .SetFillStyle(3005)
  # # contUPc[1] .SetFillStyle(3004)

  # # contDN2c[1].Draw("same F")
  # # contDNc[1] .Draw("same F")
  # # contUPc[1] .Draw("same F")
  # # contUP2c[1].Draw("same F")
  # print contDN2c[1].GetN()
  # print contDNc[1] .GetN()
  # print contUPc[1] .GetN()
  # print contUP2c[1].GetN()

  # contc[0].Draw("l")
  # contObsc[0].Draw("l")
  # contc[1].Draw("l")
  # contObsc[1].Draw("l")

  # contc[1].SetLineColor(ROOT.kBlue)
  # contObsc[1].SetLineColor(ROOT.kBlue)

  # ROOT.gPad.RedrawAxis("g");
  # canvc.Print("limits_"+basefolder+"/limits_combined_%s.pdf" % channel)

# print exclusionLineUP.GetN()
# print exclusionLineDN.GetN()

