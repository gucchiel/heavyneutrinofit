from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

ROOT.gROOT.SetBatch(True)


## modules
import os
import sys
import re
import subprocess

file2 = ROOT.TFile("interpolation2/interpFile.root","READ")
file4 = ROOT.TFile("interpolation4/interpFile.root","READ")

print file2
print file4

for i in range(1,13):
  histName = "obs_x_SSZCRee_MjjSherpaDY221_SSZCRee_Mjj_Hist_alphaalpha_JET_Flavor_Composition%s" % i
  print histName

  hist2 = file2.Get(histName)
  hist4 = file4.Get(histName)

  print hist2
  print hist4

  hist2.SetLineColor(ROOT.kBlue)
  hist4.SetLineColor(ROOT.kRed)

  canv = ROOT.TCanvas(histName,histName,600,600)
  hist2.Draw()
  hist4.Draw("same")

  hist2.GetXaxis().SetRangeUser(-3,3)
  hist4.GetXaxis().SetRangeUser(-3,3)

  canv.Update()

  ymax = ROOT.gPad.GetUymax()
  ymin = ROOT.gPad.GetUymin()

  xmax = ROOT.gPad.GetUxmax()
  xmin = ROOT.gPad.GetUxmin()

  line1 = ROOT.TLine(-1,ymin,-1,ymax)
  line2 = ROOT.TLine(1,ymin,1,ymax)

  line3 = ROOT.TLine(xmin,hist2.GetBinContent(hist2.FindBin(0)),xmax,hist2.GetBinContent(hist2.FindBin(0)))

  line1.Draw()
  line2.Draw()
  line3.Draw()

  hist2.Draw("same")
  hist4.Draw("same")


  leg = ROOT.TLegend(0.185,0.75,0.3,0.85)
  leg.SetBorderSize(0)
  leg.SetFillColor(ROOT.kWhite)
  leg.SetFillStyle(1001)
  leg.SetTextSize(0.045)

  leg.AddEntry(hist2,"#font[42]{InterpCode 2}","l")
  leg.AddEntry(hist4,"#font[42]{InterpCode 4}","l")
  leg.Draw()

  canv.Print("interpolationAnal/JET_Flavor_Composition%s.pdf" % i )


