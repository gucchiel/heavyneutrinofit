massPoint="MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2"
echo $massPoint
mkdir -p logs
mkdir -p limits
echo "cp data/templateHN_v2_003_mjj.root data/backupCacheFile_SameSignHN-LRSM_WR$1_NR$2-bkg-$3.root"
cp data/templateHN_v2_003_mjj.root data/backupCacheFile_SameSignHN-LRSM_WR$1_NR$2-bkg-$3.root
HistFitter.py -w -f -F bkg -D before,after analysis/SameSignHN/SameSignHN_bkg_mjj.py\
 -c "analysisSuffix='LRSM_WR$1_NR$2-bkg';sigSamples=['MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2'];mjj=$3"\
 -g $massPoint  2>&1 | tee logs/LRSM_WR$1_NR$2-HNbkg-$3.log
mkdir -p pulls
python scripts/pull_maker.py -i logs/LRSM_WR$1_NR$2-HNbkg-$3.log -o pulls/LRSM_WR$1_NR$2-HNbkg-$3-pull