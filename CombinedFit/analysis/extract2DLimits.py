from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy


folder = "HistFitter_HN_v2_004"

string = "/ceph/grid/home/atlas/miham/AnalysisCode/"+folder+"/"

p = subprocess.Popen(['mkdir -p limits_'+folder],shell=True,stdout=subprocess.PIPE)
output, errors = p.communicate()
print output

ROOT.gStyle.SetPaintTextFormat("2.2f");

ROOT.gROOT.SetBatch(True)

# ROOT.gStyle.SetPalette(ROOT.kBeach)

NRGBs = 5
NCont = 255

stops = array('d',[ 0.00, 0.34, 0.61, 0.87, 1.0 ])
red   = array('d',[ 207./255., 207./255., 174./255., 205./255., 1.00 ])
green = array('d',[ 183./255., 174./255., 198./255., 255./255., 1.00 ])
blue  = array('d',[ 174./255., 198./255., 207./255., 210./255., 1.00 ])
ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
ROOT.gStyle.SetNumberContours(NCont)

minimumValue = 999

tempGrYellow = ROOT.TH1F("yellow","yellow",1,0,1)
tempGrYellow.SetFillColor(5)
tempGrYellow.SetLineColor(1)
tempGrYellow.SetLineWidth(1)

tempGrGreen = ROOT.TH1F("green","green",1,0,1)
tempGrGreen.SetFillColor(8)
tempGrGreen.SetLineColor(1)
tempGrGreen.SetLineWidth(1)


signalSamples = [
[1200,1100],
[1200,1800],
[1200,2400],
[1200,300],
[1200,50],
[1200,600],
[1200,900],
[1800,1350],
[1800,1700],
[1800,2700],
[1800,3600],
[1800,450],
[1800,50],
[1800,900],
[2400,1200],
[2400,1800],
[2400,2300],
[2400,3600],
[2400,4800],
[2400,50],
[2400,600],
[3000,30],
[3000,150],
[3000,300],
[3000,1500],
[3000,2250],
[3000,2900],
[3000,4500],
[3000,50],
[3000,6000],
[3000,750],
[3500,1750],
[3500,2625],
[3500,3400],
[3500,50],
[3500,5250],
[3500,7000],
[3500,875],
[3600,1800],
[3600,2700],
[3600,3500],
[3600,50],
[3600,5400],
[3600,7200],
[3600,900],
[4000,400],
[4200,1050],
[4200,2100],
[4200,3150],
[4200,4100],
[4200,50],
[4200,6300],
[4200,8400],
[4500,1125],
[4500,2250],
[4500,3375],
[4500,4400],
[4500,50],
[5000,1250],
[5000,2500],
[5000,3750],
[5000,4900],
[5000,50],
[5000,500],
[600,1200],
[600,150],
[600,300],
[600,450],
[600,50],
[600,500],
[600,900],
]

contour       = ROOT.TGraph2D()
contour.SetPoint(contour.GetN(),5500,5500,1e6)
contour.SetPoint(contour.GetN(),0,5500,1e6)
contour.SetPoint(contour.GetN(),5500,0,1e6)
contour.SetPoint(contour.GetN(),0,0,0)

contourUP       = ROOT.TGraph2D()
contourUP.SetPoint(contourUP.GetN(),5500,5500,1e6)
contourUP.SetPoint(contourUP.GetN(),0,5500,1e6)
contourUP.SetPoint(contourUP.GetN(),5500,0,1e6)
contourUP.SetPoint(contourUP.GetN(),0,0,0)

contourDN       = ROOT.TGraph2D()
contourDN.SetPoint(contourDN.GetN(),5500,5500,1e6)
contourDN.SetPoint(contourDN.GetN(),0,5500,1e6)
contourDN.SetPoint(contourDN.GetN(),5500,0,1e6)
contourDN.SetPoint(contourDN.GetN(),0,0,0)

contourUP2       = ROOT.TGraph2D()
contourUP2.SetPoint(contourUP2.GetN(),5500,5500,1e6)
contourUP2.SetPoint(contourUP2.GetN(),0,5500,1e6)
contourUP2.SetPoint(contourUP2.GetN(),5500,0,1e6)
contourUP2.SetPoint(contourUP2.GetN(),0,0,0)

contourDN2       = ROOT.TGraph2D()
contourDN2.SetPoint(contourDN2.GetN(),5500,5500,1e6)
contourDN2.SetPoint(contourDN2.GetN(),0,5500,1e6)
contourDN2.SetPoint(contourDN2.GetN(),5500,0,1e6)
contourDN2.SetPoint(contourDN2.GetN(),0,0,0)

results2D     = ROOT.TH2D("nominal","nominal"            ,5500,0,5500,6000,-500,5500)
results2D_1up = ROOT.TH2D("results2D_1up","results2D_1up",5500,0,5500,6000,-500,5500)
results2D_1dn = ROOT.TH2D("results2D_1dn","results2D_1dn",5500,0,5500,6000,-500,5500)

for sample in signalSamples:
  file = ROOT.TFile(string+"SameSignHN-LRSM_WR%s_NR%s-excl_Output_upperlimit.root" % (sample[0],sample[1]),"OPEN")
  name = "hypo_MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR%s_NR%s" % (sample[0],sample[1])
  result = file.Get(name)
  print result
  if not result:
    "skipping ",sample[0]," NR: ",sample[0]
    continue
  print "WR: ",sample[0]," NR: ",sample[0]," ul: ",result.GetExpectedUpperLimit(0)

  if result.GetExpectedUpperLimit(0) < minimumValue:
    minimumValue = result.GetExpectedUpperLimit(0)

  contour.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0))
  contourUP.SetPoint(contourUP.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(1))
  contourDN.SetPoint(contourDN.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-1))
  contourUP2.SetPoint(contourUP2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(2))
  contourDN2.SetPoint(contourDN2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-2))
  results2D.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0))

contour.SetNpx( 250 )
contour.SetNpy( 250 )
contour.SetMinimum( minimumValue )
contour.SetMaximum( 1.0 )

contourUP.SetNpx( 250 )
contourUP.SetNpy( 250 )
contourUP2.SetNpx( 250 )
contourUP2.SetNpy( 250 )

contourDN.SetNpx( 250 )
contourDN.SetNpy( 250 )
contourDN2.SetNpx( 250 )
contourDN2.SetNpy( 250 )

exclusionLine = contour.GetHistogram().Clone("exclusionLine")
exclusionLine.SetContour(1)
exclusionLine.SetContourLevel(0,1.)
exclusionLine.SetLineColor(1)
exclusionLine.SetLineStyle(1)
exclusionLine.SetLineWidth(2)

exclusionLineUP = contourUP.GetHistogram().Clone("exclusionLineUP")
exclusionLineUP.SetContour(1)
exclusionLineUP.SetContourLevel(0,1.)
exclusionLineUP.SetLineColor(8)
exclusionLineUP.SetLineWidth(2)

exclusionLineDN = contourDN.GetHistogram().Clone("exclusionLineDN")
exclusionLineDN.SetContour(1)
exclusionLineDN.SetContourLevel(0,1.)
exclusionLineDN.SetLineColor(8)
exclusionLineDN.SetLineWidth(2)

exclusionLineUP2 = contourUP2.GetHistogram().Clone("exclusionLineUP2")
exclusionLineUP2.SetContour(1)
exclusionLineUP2.SetContourLevel(0,1.)
exclusionLineUP2.SetLineColor(8)
exclusionLineUP2.SetLineWidth(2)

exclusionLineDN2 = contourDN2.GetHistogram().Clone("exclusionLineDN2")
exclusionLineDN2.SetContour(1)
exclusionLineDN2.SetContourLevel(0,1.)
exclusionLineDN2.SetLineColor(8)
exclusionLineDN2.SetLineWidth(2)

leg = ROOT.TLegend(0.50,0.87,0.7,0.91);
leg.SetBorderSize(0);
leg.SetFillColor(0);
leg.SetFillStyle(0);
leg.SetTextSize(0.045);
leg.AddEntry(exclusionLine,"#font[42]{Expected 95% CL}","l");
# leg.AddEntry(exclusionLineUP,"#font[42]{Expected limit #pm 1#sigma}","l");

# exclusionLine.DrawCopy("CONT LIST goff")


canv = ROOT.TCanvas("limits","limits",800,600)
canv.cd()
canv.SetRightMargin(0.15)
results2D.Draw("text");

line = ROOT.TLine(0, 0, 5500, 5000)
line.SetLineStyle(2)
line.SetLineColor(ROOT.kGray)
line2 = ROOT.TLine(0, 0, 5500, 0)
line2.SetLineStyle(3)
line2.SetLineColor(ROOT.kGray)


contour.Draw("COLZ same")
exclusionLine.Draw("CONT LIST same")
canv.Update()
conts = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineUP.Draw("CONT LIST same")
canv.Update()
contsUP = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineDN.Draw("CONT LIST same")
canv.Update()
contsDN = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineUP2.Draw("CONT LIST same")
canv.Update()
contsUP2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineDN2.Draw("CONT LIST same")
canv.Update()
contsDN2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))


results2D.Draw("text");
contour.Draw("COLZ same")
exclusionLine.Draw("CONT3 same")
line .Draw("same")
line2.Draw("same")
results2D.Draw("text same")

results2D.SetMarkerSize(1.3)
# results2D.GetXaxis().SetNdivisions(1005)
results2D.GetZaxis().SetTitle("#mu_{95%}")
results2D.GetXaxis().SetTitle("m(W_{R}) [GeV]")
results2D.GetYaxis().SetTitle("m(N_{R}) [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
leg.Draw()
ROOT.gPad.RedrawAxis("g");
canv.Print("limits_"+folder+"/limits.eps")

print conts.GetSize()
print contsUP.GetSize()
print contsDN.GetSize()

cont = conts[0]
contUP = contsUP[0]
contDN = contsDN[0]
contUP2 = contsUP2[0]
contDN2 = contsDN2[0]

canv2 = ROOT.TCanvas("excl","excl",800,600)
canv2.cd()
contDN.First().SetFillColor(8)
contDN2.First().SetFillColor(5)
contUP.First().SetFillColor(5)
contUP2.First().SetFillColor(ROOT.kWhite)
cont.First().SetLineStyle(2)

leg2 = ROOT.TLegend(0.18,0.61,0.4,0.79);
leg2.SetBorderSize(0);
leg2.SetFillColor(0);
leg2.SetFillStyle(0);
leg2.SetTextSize(0.045);
leg2.AddEntry(cont.First(),"#font[42]{Expected 95% CL}","l");
leg2.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
leg2.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

contDN2.First().Draw("a F")
contDN.First().Draw("F")
contUP.First().Draw("same F")
contUP2.First().Draw("same F")
cont.First().Draw("l")

contDN2.First().GetYaxis().SetRangeUser(50,4000)
contDN2.First().GetXaxis().SetRangeUser(600,5000)
contDN2.First().GetXaxis().SetTitle("m(W_{R}) [GeV]")
contDN2.First().GetYaxis().SetTitle("m(N_{R}) [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
leg2.Draw()

line3 = ROOT.TLine(600, 50, 5000, 4000)
line3.SetLineStyle(2)
line3.SetLineColor(ROOT.kGray+1)
# line3.Draw()
ROOT.gPad.RedrawAxis("g");

canv2.Print("limits_"+folder+"/exclusion.eps")


# print exclusionLineUP.GetN()
# print exclusionLineDN.GetN()

