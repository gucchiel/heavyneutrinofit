from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
from copy import deepcopy
import argparse

ROOT.gStyle.SetPaintTextFormat("2.2f");
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser(description="Plot #it{B}azil Band plots from output from HistFitter")
parser.add_argument('-p', '--prefix', action='store', help='results file prefix', type=str, required=True)
parser.add_argument('-o', '--outputFolder', action='store', help='output folder name', type=str)
parser.add_argument('-m', '--mu_Sig', action='store', help='mu_sig level', type=int, required=True)
parser.add_argument('-1D', '--do1D', action='store_true', help='do 1D plots')
parser.add_argument('-x', '--extra', action='store_true', help='plot extra mass grid')
parser.add_argument('-s', '--smooth', action='store_true', help='smooth contours')
parser.add_argument('-rm', '--remove', action='store_true', help='remove points')
parser.add_argument('-channel','--channel', action='store',help='setting channel flavour')
args=parser.parse_args()
print args

channel=args.channel

if not args.outputFolder:
	args.outputFolder = args.prefix

try:
	os.mkdir( args.outputFolder )
except:
	pass

#channel = args.prefix.split('_')[-1].replace('os','')

def myText_2(x,y,color,text):
  l = ROOT.TLatex()
  l.SetTextColor(color)
  l.DrawLatex(x,y,text)
 
def ATLASLabel_2(x,y,text,color):
  l = ROOT.TLatex()
  l.SetTextFont(72)
  l.SetTextColor(color)

  xmax = ROOT.gPad.GetUxmax()
  xmin = ROOT.gPad.GetUxmin()
  delx = (xmax-xmin)*(0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw()))

  l.DrawLatex(x,y,"ATLAS")
  if text:
    p = ROOT.TLatex()
    p.SetTextFont(42)
    p.SetTextColor(color)
    p.DrawLatex(x+1.5*delx,y,text)

NRGBs = 5
NCont = 255

stops = array('d',[ 0.00, 0.34, 0.61, 0.87, 1.0 ])
red	 = array('d',[ 207./255., 207./255., 174./255., 205./255., 1.00 ])
green = array('d',[ 183./255., 174./255., 198./255., 255./255., 1.00 ])
blue	= array('d',[ 174./255., 198./255., 207./255., 210./255., 1.00 ])

minimumValue = 999
minimumValue2 = 1e6
maximumValue2 = 0

XMIN = 650.
XMAX = 5000.
YMIN = 100.
YMAX = 4790.
results2DLimitProxy  = ROOT.TH2F("results2DLimitProxy","results2DLimitProxy",10,XMIN,XMAX,10,YMIN,YMAX)


tempGrYellow = ROOT.TH1F("yellow","yellow",1,0,1)
tempGrYellow.SetFillColor(5)
tempGrYellow.SetLineColor(1)
tempGrYellow.SetLineWidth(1)

tempGrRed = ROOT.TH1F("red","red",1,0,1)
tempGrRed.SetLineColor(ROOT.kRed)
tempGrRed.SetLineWidth(1)

tempGrGreen = ROOT.TH1F("green","green",1,0,1)
tempGrGreen.SetFillColor(8)
tempGrGreen.SetLineColor(1)
tempGrGreen.SetLineWidth(1)

lumi =	36097.56
if args.smooth:
	Np = 100
	nametail = "_smooth.pdf"
else:
	Np = 255
	nametail = ".eps"

if args.remove:
	nametail = "_remove"+nametail

N95MAX = 20

contour			 = ROOT.TGraph2D()
contourObs			 = ROOT.TGraph2D()
contourUP			 = ROOT.TGraph2D()
contourDN			 = ROOT.TGraph2D()
contourUP2			 = ROOT.TGraph2D()
contourDN2			 = ROOT.TGraph2D()

#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisMetadata
# mWR, mNR, xsec [pb]
signalSamples = [
[2100 , 1700 , 4.335e-05*1E3 ],
[2400 , 200 , 1.076e-04*1E3],
[2400 , 400 , 9.782e-05*1E3],
[3000 , 2100 , 1.046e-05*1E3],
[3000 , 2500 , 4.147e-06*1E3],
[3500 , 200 , 1.028e-05*1E3],
[3500 , 300 , 1.008e-05*1E3],
[ 600 , 50 , 51.02 ],
[ 600 , 150 , 49.713 ],
[ 600 , 300 , 37.33 ],
[ 600 , 450 , 16.138 ],
[ 600 , 500 , 8.7848 ],
[ 600 , 600 , 0.36365 ],
[ 600 , 700 , 0.079408 ],
[ 600 , 900 , 0.015643 ],
[ 600 , 1200 , 0.0031095 ],
[ 1000 , 700 , 2.7778 ],
[ 1000 , 800 , 1.5465 ],
[ 1000 , 1000 , 0.044133 ],
[ 1000 , 1100 , 0.015058 ],
[ 1000 , 1400 , 2.0290E-06*1E3 ],
[ 1200 , 50 , 3.2448 ],
[ 1200 , 300 , 3.1065 ],
[ 1200 , 600 , 2.3153 ],
[ 1200 , 900 , 0.98898 ],
[ 1200 , 1100 , 0.17725 ],
[ 1200 , 1200 , 0.019283 ],
[ 1200 , 1500 , 0.001777 ],
[ 1200 , 1800 , 0.0004383 ],
[ 1200 , 2400 , 5.162e-05 ],
[ 1500 , 1000 , 0.5361 ],
[ 1500 , 1100 , 0.39588 ],
[ 1500 , 1200 , 0.25818 ],
[ 1500 , 1600 , 3.2349E-06*1E3 ],
[ 1500 , 1800 , 7.2710E-07*1E3 ],
[ 1800 , 50 , 0.50828 ],
[ 1800 , 450 , 0.47505 ],
[ 1800 , 900 , 0.3507 ],
[ 1800 , 1350 , 0.14888 ],
[ 1800 , 1500 , 0.079323 ],
[ 1800 , 1600 , 0.04133 ],
[ 1800 , 1700 , 0.01393 ],
[ 1800 , 2700 , 2.9008e-05 ],
[ 1800 , 3600 , 1.9213e-06 ],
[ 2100 , 1500, 8.1807E-05*1E3 ],
[ 2100 , 1800 , 2.7776E-05*1E3 ],
[ 2100 , 2200 , 7.2120E-07*1E3 ],
[ 2400 , 50 , 0.11263 ],
[ 2400 , 600 , 0.10148 ],
[ 2400 , 1200 , 0.074743 ],
[ 2400 , 1800 , 0.031435 ],
[ 2400 , 1900 , 0.023783 ],
[ 2400 , 2100 , 0.010368 ],
[ 2400 , 2300 , 0.001897 ],
[ 2400 , 3600 , 2.5613e-06 ],
[ 2400 , 4800 , 9.7783e-08 ],
[ 2700 , 300 , 5.4544E-05*1E3 ],
[ 3000 , 30 , 0.032707 ],
[ 3000 , 50 , 0.030543 ],
[ 3000 , 150 , 0.029708 ],
[ 3000 , 300 , 0.02845 ],
[ 3000 , 750 , 0.026238 ],
[ 3000 , 1500 , 0.018827 ],
[ 3000 , 1600 , 0.017545 ],
[ 3000 , 1800 , 0.014728 ],
[ 3000 , 2250 , 0.0078485 ],
[ 3000 , 2900 , 0.00033845 ],
[ 3000 , 4500 , 2.5493e-07 ],
[ 3000 , 6000 , 7.3718e-09 ],
[ 3500 , 50 , 0.011405 ],
[ 3500 , 400, 9.9448E-06*1E3 ],
[ 3500 , 875 , 0.0091305 ],
[ 3500 , 1750 , 0.0064078 ],
[ 3500 , 2625 , 0.002618 ],
[ 3500 , 3400 , 9.027e-05 ],
[ 3500 , 5250 , 3.984e-08 ],
[ 3500 , 7000 , 1.3538e-09 ],
[ 3600 , 50 , 0.0094965 ],
[ 3600 , 900 , 0.0073963 ],
[ 3600 , 1800 , 0.005172 ],
[ 3600 , 2700 , 0.0021193 ],
[ 3600 , 3500 , 7.013e-05 ],
[ 3600 , 5400 , 2.7605e-08 ],
[ 3600 , 7200 , 1.0035e-09 ],
[ 3800, 3000, 1.0520E-06*1E3 ],
[ 4000 , 400 , 0.0038988 ],
[ 4200 , 50 , 0.0033228 ],
[ 4200 , 200, 3.0004E-06*1E3 ],
[ 4200 , 400, 2.6964E-06*1E3 ],
[ 4200 , 1050 , 0.0022143 ],
[ 4200 , 2100 , 0.0014893 ],
[ 4200 , 3150 , 0.00059705 ],
[ 4200 , 4100 , 1.557e-05 ],
[ 4200 , 6300 , 3.4015e-09 ],
[ 4200 , 8400 , 2.1228e-10 ],
[ 4500 , 50 , 0.0020453 ],
[ 4500 , 800, 1.4201E-06*1E3 ],
[ 4500 , 1125 , 0.0012275 ],
[ 4500 , 2250 , 0.00080206 ],
[ 4500 , 3375 , 0.00031672 ],
[ 4500 , 4400 , 7.4606e-06 ],
[ 4750 , 3000, 3.3310E-07*1E3 ],
[ 5000 , 50 , 0.0010374 ],
[ 5000 , 500 , 0.00068688 ],
[ 5000 , 1250 , 0.00047349 ],
[ 5000 , 2500 , 0.00028817 ],
[ 5000 , 3750 , 0.00011102 ],
[ 5000 , 4900 , 2.2329e-06 ],
[ 5200 , 1000 , 3.8360E-07*1E3 ],
[ 5200 , 2000 , 2.5260E-07*1E3 ],
[ 5200 , 3000 , 1.5610E-07*1E3 ],
[ 5800 , 500  , 2.2190E-07*1E3 ],
[ 5800 , 1000, 1.5420E-07*1E3 ],
[ 5800 , 2000 , 8.5500E-08*1E3 ],
[ 5800 , 3000 , 5.3700E-08*1E3 ],
]

if args.remove:
	signalSamples.remove( [ 4500 , 50 , 0.0020453 ] )

if channel == "mm":
	# remove bottom right suprious contour
	if not args.smooth:
		exx = 5500
		exy = -1000
		exval = 1e6
		contour.SetPoint(contour.GetN(),exx,exy,exval)
		contourObs.SetPoint(contourObs.GetN(),exx,exy,exval)
		contourUP.SetPoint(contourUP.GetN(),exx,exy,exval)
		contourDN.SetPoint(contourDN.GetN(),exx,exy,exval)
		contourUP2.SetPoint(contourUP2.GetN(),exx,exy,exval)
		contourDN2.SetPoint(contourDN2.GetN(),exx,exy,exval)

results2D    = ROOT.TH2D("results2D"+channel,"results2D"+channel         ,6500,0,6500,6500,-500,6000)
results2DN   = ROOT.TH2D("results2DN"+channel,"results2DN"+channel       ,6500,0,6500,6500,-500,6000)
EffHisto     = ROOT.TH2D("EffHisto"+channel,"EffHisto"+channel           ,5500,0,5500,8500,-500,8000)
#contourN     = ROOT.TGraph2D()
EfficiencyGr = ROOT.TGraph2D()
results2DLimit = ROOT.TGraph2D()

mWR = array('d')
mNR = array('d')
ULobs = array('d')

for sample in signalSamples:
	var = 'Mlljj'
	if sample[1] > sample[0]:
		var = 'Mjj'
	resultsfile = ROOT.TFile( 'selected_mass_samples/'+args.prefix+'WR'+str(sample[0])+'_NR'+str(sample[1])+'_Sig_'+channel+'-excl_Output_upperlimit.root', "READ" )

	name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])
	result = resultsfile.Get(name)
	#print name,result

	if result and result.GetExpectedUpperLimit(0) < minimumValue:
		minimumValue = result.GetExpectedUpperLimit(0)

	if (sample[0] <= XMAX) and (sample[1] <= ( YMIN + ( 0.725 * (YMAX-100) ) ) ):
	 	if result and result.UpperLimit()*sample[2] < minimumValue2:
			minimumValue2 = result.UpperLimit()*sample[2]
	  	if result and result.UpperLimit()*sample[2] > maximumValue2:
			maximumValue2 = result.UpperLimit()*sample[2]
	print "minimumValue2 ", minimumValue2		
	print "maxmimumValue2 ", maximumValue2		

	if result:
		print "WR:",sample[0],"NR:",sample[1],"ratio:",sample[0]/float(sample[1]),"ul:",result.GetExpectedUpperLimit(0),"ul-2:",result.GetExpectedUpperLimit(-2),"ul2:",result.GetExpectedUpperLimit(2)
		contourObs.SetPoint(contourObs.GetN(),sample[0],sample[1],result.UpperLimit())
		contour.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0))
		contourUP.SetPoint(contourUP.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(1))
		contourDN.SetPoint(contourDN.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-1))
		contourUP2.SetPoint(contourUP2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(2))
		contourDN2.SetPoint(contourDN2.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(-2))
		#results2D.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0))
		#results2DN.Fill(sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*args.mu_Sig*sample[2]/6.)
		results2D.Fill(sample[0],sample[1],result.UpperLimit())
		results2DN.Fill(sample[0],sample[1],result.UpperLimit()*lumi*args.mu_Sig*sample[2]/6.)
		# if (result.GetExpectedUpperLimit(0)*lumi*args.mu_Sig*sample[2]/6. <= N95MAX ):
		#contourN.SetPoint(contour.GetN(),sample[0],sample[1],result.GetExpectedUpperLimit(0)*lumi*args.mu_Sig*sample[2]/6.)
		results2DLimit.SetPoint(results2DLimit.GetN(),sample[0],sample[1],result.UpperLimit()*args.mu_Sig*sample[2])


		mWR.append(sample[0])
		mNR.append(sample[1])
		ULobs.append(result.UpperLimit())

	resultsfile.Close()

	#mWR.append(sample[0])
	#mNR.append(sample[1])

contour.SetNpx( Np )
contour.SetNpy( Np )
contour.SetMinimum( minimumValue )
contour.SetMaximum( 1.0*args.mu_Sig )

contourObs.SetNpx( Np )
contourObs.SetNpy( Np )
contourObs.SetMinimum( minimumValue )
contourObs.SetMaximum( 1.0*args.mu_Sig )

contourUP.SetNpx( Np )
contourUP.SetNpy( Np )
contourUP.SetMaximum( 1.0*args.mu_Sig )
#contourUP2.SetNpx( 50 )
#contourUP2.SetNpy( 50 )
contourUP2.SetNpx( Np )
contourUP2.SetNpy( Np )
contourUP2.SetMaximum( 1.0*args.mu_Sig )

contourDN.SetNpx( Np )
contourDN.SetNpy( Np )
contourDN.SetMaximum( 1.0*args.mu_Sig )
contourDN2.SetNpx( Np )
contourDN2.SetNpy( Np )
contourDN2.SetMaximum( 1.0*args.mu_Sig )


results2DLimit.SetNpy( Np )
results2DLimit.SetNpx( Np )
results2DLimit.SetMinimum( 0.0001 )
results2DLimit.SetMaximum( 3 )


exclusionLine = contour.GetHistogram().Clone("exclusionLine"+channel)
exclusionLine.SetContour(1)
exclusionLine.SetContourLevel(0,args.mu_Sig)
exclusionLine.SetLineColor(1)
exclusionLine.SetLineStyle(1)
exclusionLine.SetLineWidth(2)

exclusionLineObs = contourObs.GetHistogram().Clone("exclusionLineObs"+channel)
exclusionLineObs.SetContour(1)
exclusionLineObs.SetContourLevel(0,args.mu_Sig)
exclusionLineObs.SetLineColor(1)
# exclusionLineObs.SetLineStyle(1)
exclusionLineObs.SetLineWidth(2)

exclusionLineUP = contourUP.GetHistogram().Clone("exclusionLineUP"+channel)
exclusionLineUP.SetContour(1)
exclusionLineUP.SetContourLevel(0,args.mu_Sig)
exclusionLineUP.SetLineColor(8)
exclusionLineUP.SetLineWidth(2)

exclusionLineDN = contourDN.GetHistogram().Clone("exclusionLineDN"+channel)
exclusionLineDN.SetContour(1)
exclusionLineDN.SetContourLevel(0,args.mu_Sig)
exclusionLineDN.SetLineColor(8)
exclusionLineDN.SetLineWidth(2)

exclusionLineUP2 = contourUP2.GetHistogram().Clone("exclusionLineUP2"+channel)
exclusionLineUP2.SetContour(1)
exclusionLineUP2.SetContourLevel(0,args.mu_Sig)
exclusionLineUP2.SetLineColor(8)
exclusionLineUP2.SetLineWidth(2)

exclusionLineDN2 = contourDN2.GetHistogram().Clone("exclusionLineDN2"+channel)
exclusionLineDN2.SetContour(1)
exclusionLineDN2.SetContourLevel(0,args.mu_Sig)
exclusionLineDN2.SetLineColor(8)
exclusionLineDN2.SetLineWidth(2)

# cross-section limit
ROOT.gStyle.SetPalette(ROOT.kThermometer)
canv1 = ROOT.TCanvas("crossSectionLimit","crossSectionLimit",800,600)
canv1.SetLogz()
canv1.cd()
canv1.SetRightMargin(0.18)
canvWidth  = 800.*(1 - canv1.GetRightMargin() - canv1.GetLeftMargin())
canvHeight = 600.*(1 - canv1.GetTopMargin() - canv1.GetBottomMargin())
canvRatio = canvWidth/(1.0*canvHeight)
results2DLimitProxy.Fill(1000,4200,1)
results2DLimitProxy.Draw("COLZ")
results2DLimit.Draw("same COLZ")
results2DLimit.SetPoint(results2DLimit.GetN(),5000,6000,1e-6)
"""cont.First().Draw("c same")
contUP.First().Draw("c same")
contDN.First().Draw("c same")
contObsDn.First().Draw("c same")
contObsUp.First().Draw("c same")
contObs.First().Draw("c same")"""
canv1.Update()
xmax = ROOT.gPad.GetUxmax()
xmin = ROOT.gPad.GetUxmin()
ymax = ROOT.gPad.GetUymax()
ymin = ROOT.gPad.GetUymin()
print xmin," ",xmax," ",ymin," ",ymax
print "ymin+0.725*(ymax-ymin) ",ymin+0.725*(ymax-ymin)
line3 = ROOT.TLine(xmin, xmin, 0.9*xmax, 0.9*xmax)
line3.SetLineStyle(3)
line3.SetLineColor(ROOT.kGray)
line3.Draw("same")
tt = ROOT.TLatex()
tt.SetTextAngle(38)
# tt.SetTextAngle(360/(2*ROOT.TMath.Pi())*ROOT.TMath.ATan( canvRatio ))
tt.SetTextColor(ROOT.kGray)
tt.SetTextSize(0.03)
tt.DrawLatex(xmin+0.39*(xmax-xmin),ymin+0.52*(ymax-ymin),"#it{m}_{#it{W}_{R}} = #it{m}_{#it{N}_{R}}")
tt2 = ROOT.TLatex()
tt2.SetTextAngle(56.4)
# tt2.SetTextAngle(360/(2*ROOT.TMath.Pi())*ROOT.TMath.ATan( 2*canvRatio ))
tt2.SetTextColor(ROOT.kGray)
tt2.SetTextSize(0.03)
tt2.DrawLatex(xmin+0.14*(xmax-xmin),ymin+0.43*(ymax-ymin),"#it{m}_{#it{W}_{R}} = #frac{1}{2} #times #it{m}_{#it{N}_{R}}")
line4 = ROOT.TLine(xmin, 2*xmin, xmax, 2*xmax)
line4.SetLineWidth(8)
line4.SetLineColor(ROOT.kWhite)
line4.Draw("same")
if channel=="ee":
   results2DLimitProxy.GetZaxis().SetTitle("95% CL UL on #sigma #times #it{B}(#it{W}_{R},#it{N}_{R} #rightarrow #it{ee})[pb]")
if channel=="mm":
   results2DLimitProxy.GetZaxis().SetTitle("95% CL UL on #sigma #times #it{B}(#it{W}_{R},#it{N}_{R} #rightarrow #it{#mu#mu})[pb]")
results2DLimitProxy.GetZaxis().SetTitleOffset(1.2)
results2DLimitProxy.GetZaxis().SetNdivisions(0)
results2DLimitProxy.GetXaxis().SetTitle("#it{m}_{#it{W}_{R}} [TeV]")
results2DLimitProxy.GetXaxis().SetNdivisions(505)
results2DLimitProxy.GetYaxis().SetTitle("#it{m}_{#it{N}_{R}} [TeV]")

ROOT.gPad.RedrawAxis("g");

box = ROOT.TBox(xmin,ymin+0.725*(ymax-ymin),xmax,ymax)
box.SetFillColor(ROOT.kWhite)
box.SetFillStyle(1001)
box.SetLineColor(ROOT.kBlack)
box.Draw("l")

# custom legend
LineLeft  = xmin + 0.51*(xmax-xmin)
LineRight = xmin + 0.55*(xmax-xmin)
LineY1    = ymin + 0.85*(ymax-ymin)
LineY2    = ymin + 0.92*(ymax-ymin)
ATLASLabel_2(xmin + 0.02*(xmax-xmin),LineY2,"Internal",1)
myText_2(xmin + 0.02*(xmax-xmin),LineY1,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
if args.mu_Sig == 2:
    myText_2(xmin + 0.02*(xmax-xmin),0.80*(ymax-ymin),1,"Dirac #it{N}_{R}, %s channel, #it{g}_{R} = #it{g}_{L}" % ("#it{ee}" if channel == "ee" else "#it{#mu#mu}") )
else:
    myText_2(xmin + 0.02*(xmax-xmin),0.80*(ymax-ymin),1,"Majorana #it{N}_{R}, %s channel, #it{g}_{R} = #it{g}_{L}" % ("#it{ee}" if channel == "ee" else "#it{#mu#mu}") )

print "ymin + 0.85*(ymax-ymin) ",ymin + 0.85*(ymax-ymin)
"""lineExp   = ROOT.TLine(LineLeft, LineY1+0.02*(ymax-ymin), LineRight, LineY1+0.02*(ymax-ymin))
lineExp.SetLineStyle(2)
lineExp.SetLineWidth(3)
lineExp.SetLineColor(ROOT.kRed)
lineExp.Draw()
lineExp1   = ROOT.TLine(LineLeft, LineY1+0.01*(ymax-ymin), LineRight, LineY1+0.01*(ymax-ymin))
lineExp1.SetLineStyle(2)
lineExp1.SetLineWidth(1)
lineExp1.SetLineColor(ROOT.kRed)
lineExp1.Draw()
lineExp2   = ROOT.TLine(LineLeft, LineY1+0.03*(ymax-ymin), LineRight, LineY1+0.03*(ymax-ymin))
lineExp2.SetLineStyle(2)
lineExp2.SetLineWidth(1)
lineExp2.SetLineColor(ROOT.kRed)
lineExp2.Draw()
myText_2(LineRight+0.01*(xmax-xmin), LineY1, ROOT.kBlack, "Expected #pm 1 #sigma")
lineObs   = ROOT.TLine(LineLeft, LineY2+0.02*(ymax-ymin), LineRight, LineY2+0.02*(ymax-ymin))
lineObs.SetLineStyle(1)
lineObs.SetLineWidth(3)
lineObs.SetLineColor(ROOT.kBlack)
lineObs.Draw()
lineObs1   = ROOT.TLine(LineLeft, LineY2+0.01*(ymax-ymin), LineRight, LineY2+0.01*(ymax-ymin))
lineObs1.SetLineStyle(1)
lineObs1.SetLineWidth(1)
lineObs1.SetLineColor(ROOT.kBlack)
lineObs1.Draw()
lineObs2   = ROOT.TLine(LineLeft, LineY2+0.03*(ymax-ymin), LineRight, LineY2+0.03*(ymax-ymin))
lineObs2.SetLineStyle(1)
lineObs2.SetLineWidth(1)
lineObs2.SetLineColor(ROOT.kBlack)
lineObs2.Draw()
myText_2(LineRight+0.01*(xmax-xmin), LineY2, ROOT.kBlack, "Observed #pm ^{10% sig}_{x-sec}")"""


#draw new axis
xaxisTeV = ROOT.TGaxis(ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY1(),ROOT.gPad.GetFrame().GetX2(),ROOT.gPad.GetFrame().GetY1(),XMIN/1000.,XMAX/1000.,10,"")
xaxisTeV.SetLabelSize(0.045)
xaxisTeV.SetLabelFont(42)
xaxisTeV.Draw()

yaxisTeV = ROOT.TGaxis(ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY1(),ROOT.gPad.GetFrame().GetX1(),ROOT.gPad.GetFrame().GetY2(),YMIN/1000.,YMAX/1000.,10,"")
yaxisTeV.SetLabelSize(0.045)
yaxisTeV.SetLabelFont(42)
yaxisTeV.Draw()

# get rid of the old the axes
results2DLimitProxy.GetXaxis().SetLabelOffset(999)
results2DLimitProxy.GetXaxis().SetLabelSize(0)
results2DLimitProxy.GetYaxis().SetLabelOffset(999)
results2DLimitProxy.GetYaxis().SetLabelSize(0)

canv1.Print(args.outputFolder+"/crossSectionLimit_%s_mu_sig%s%s" % (channel,str(args.mu_Sig),nametail))

#sys.exit()

if args.smooth:
	exclusionLine.Smooth()
	exclusionLineObs.Smooth()
	exclusionLineUP.Smooth()
	exclusionLineUP2.Smooth()
	exclusionLineDN.Smooth()
	exclusionLineDN2.Smooth()

leg = ROOT.TLegend(0.50,0.87,0.7,0.91);
leg.SetBorderSize(0);
leg.SetFillColor(0);
leg.SetFillStyle(0);
leg.SetTextSize(0.045);
leg.AddEntry(exclusionLine,"#font[42]{Observed 95% CL}","l");
# leg.AddEntry(exclusionLineUP,"#font[42]{Expected limit #pm 1#sigma}","l");

canv = ROOT.TCanvas("limits"+channel,"limits"+channel,800,600)
canv.cd()
canv.SetRightMargin(0.15)
results2D.Draw("text");
line = ROOT.TLine(0, 0, 5500, 5000)
line.SetLineStyle(2)
line.SetLineColor(ROOT.kGray)
line2 = ROOT.TLine(0, 0, 5500, 0)
line2.SetLineStyle(3)
line2.SetLineColor(ROOT.kGray)

contour.Draw("COLZ same")
exclusionLine.Draw("CONT LIST same")
canv.Update()
conts = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
contourObs.Draw("COLZ same")
exclusionLineObs.Draw("CONT LIST same")
canv.Update()
contsObs = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineUP.Draw("CONT LIST same")
canv.Update()
contsUP = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineDN.Draw("CONT LIST same")
canv.Update()
contsDN = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineUP2.Draw("CONT LIST same")
canv.Update()
contsUP2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))
exclusionLineDN2.Draw("CONT LIST same")
canv.Update()
contsDN2 = deepcopy(ROOT.gROOT.GetListOfSpecials().FindObject("contours"))

results2D.Draw("text");
contour.Draw("COLZ same")
#exclusionLineObs.Draw("CONT3 same")
#contourUP2.Draw("COLZ same")
#exclusionLineUP2.Draw("CONT3 same")
exclusionLine.Draw("CONT3 same")
# exclusionLineUP2.Draw("CONT3 same")
line .Draw("same")
line2.Draw("same")
results2D.Draw("text same")
ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
ROOT.gStyle.SetNumberContours(NCont)
results2D.SetMarkerSize(1.3)
results2D.GetZaxis().SetTitle("#mu_{95%}")
results2D.GetXaxis().SetTitle("#it{m}_{#it{W}_{R}} [GeV]")
results2D.GetYaxis().SetTitle("#it{m}_{#it{N}_{R}} [GeV]")
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
if args.mu_Sig == 2:
	ROOT.myText(0.185,0.725,1,"Dirac #it{N}_{R}, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))
else:
	ROOT.myText(0.185,0.725,1,"opposite-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))

leg.Draw()
ROOT.gPad.RedrawAxis("g");
canv.Print( args.outputFolder+"/limits_%s_mu_sig%s.pdf" % (channel,str(args.mu_Sig)))

cont = conts[0]
contObs = contsObs[0]
contUP = contsUP[0]
contDN = contsDN[0]
contUP2 = contsUP2[0]
contDN2 = contsDN2[0]

canv2 = ROOT.TCanvas("excl","excl",800,600)
canv2.cd()
contDN2.First().SetFillColor(5)
contDN.First().SetFillColor(8)
contUP.First().SetFillColor(5)
contUP2.First().SetFillColor(ROOT.kWhite)
cont.First().SetLineStyle(2)

if channel == "mm":
	contDN2.At(1).SetFillColor(5)
	contDN2.At(1).Draw("same F")
contDN2.First().Draw("a F")
if channel == "mm":
	contDN.At(1).SetFillColor(8)
	contDN.At(1).Draw("same F")
contDN.First().Draw("same F")
if channel == "mm":
	contUP.At(1).SetFillColor(5)
	contUP.At(1).Draw("same F")
contUP.First().Draw("same F")
if channel == "ee":
	#print type(contUP2),type(contUP2.First())
	#need to draw the second contour too, because the ul2 of 600/50 is high
	contUP2.At(1).SetFillColor(ROOT.kWhite)
	contUP2.At(1).Draw("same F")
	contUP2.First().SetFillColor(5)
contUP2.First().Draw("same F")
if channel == "mm":
	cont.At(1).SetLineStyle(2)
	cont.At(1).Draw("l")
cont.First().Draw("l")
if channel == "mm" and args.mu_Sig == 1:
	contObs.At(1).Draw("l")
contObs.First().Draw("l")

#contDN2.First().Draw("a F")
#contDN.First().Draw("same l *")
#contUP.First().Draw("same F")
#contUP2.First().Draw("same F")
#cont.First().Draw("l")
#contObs.First().Draw("l")


if args.extra:
	contDN2.First().GetYaxis().SetRangeUser(0,4000)
	contDN2.First().GetXaxis().SetRangeUser(0,5000)
else:
	contDN2.First().GetYaxis().SetRangeUser(100,4000)
	contDN2.First().GetXaxis().SetRangeUser(700,5500)

contDN2.First().GetXaxis().SetTitle("#it{m}_{#it{W}_{R}} [GeV]")
contDN2.First().GetYaxis().SetTitle("#it{m}_{#it{N}_{R}} [GeV]")

if args.extra:
	line3 = ROOT.TLine(0, 0, 8000, 8000)
else:
	line3 = ROOT.TLine(700, 700, 4000, 4000)
line3.SetLineStyle(2)
line3.SetLineColor(ROOT.kGray+1)
line3.Draw()

if args.mu_Sig == 2:
	ROOT.myText(0.54,0.875,1,"Dirac #it{N}_{R}, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))
else:
	ROOT.myText(0.54,0.875,1,"opposite-sign, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))
	"""if channel == "ee":
		ROOT.myText(0.54,0.875,1,"electron channel (e^{#pm}e^{#mp})")
	else:
		ROOT.myText(0.54,0.875,1,"muon channel (#mu^{#pm}#mu^{#mp})")"""

leg2 = ROOT.TLegend(0.18,0.61,0.4,0.81);
leg2.SetBorderSize(0);
leg2.SetFillColor(0);
leg2.SetFillStyle(0);
leg2.SetTextSize(0.045);

if not args.extra:
	leg2.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l")
	leg2.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l")
	leg2.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f")
	leg2.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f")
	ROOT.ATLASLabel(0.185,0.875,"Internal",1)
	ROOT.myText(0.185,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
	ROOT.myText(0.185,0.78,1,"#it{g}_{R} = #it{g}_{L}")
	leg2.Draw()
else:
	leg2.AddEntry("","#font[42]{Observed UL on #mu}","").SetTextColor(ROOT.kGray)
	leg2.AddEntry("","#font[42]{Fit with large NP errors}","").SetTextColor(ROOT.kRed)
	#leg2.AddEntry("","#font[42]{#Delta#mu > 100}","").SetTextColor(ROOT.kRed)
	#leg2.AddEntry("","#font[42]{100 > #Delta#mu > 10}","").SetTextColor(ROOT.kMagenta)
	leg2.Draw()
ROOT.gPad.RedrawAxis("g")

if args.extra:
	#mWRextraOrig = array('d', [ 2400, 2800, 2800, 3150, 3150, 3150, 3500, 3500, 3500, 3800, 3800, 3800, 3800, 3800, 3800, 3800, 3800, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4200, 4200, 4200, 4200, 4200, 4500, 4500, 4500, 4500, 4500, 4750, 4750, 4750, 4750, 4750, 4750, 4750, 4750, 5000, 5000, 5000, 5000, 5200, 5200, 5200, 5200, 5200, 5200, 5200, 5200, 5200, 5200  ])
	#mNRextraOrig = array('d', [ 300, 300,  600, 200, 400, 600,      200, 400,  600, 200, 400, 	600, 	1000, 1500, 2000, 2500, 3000, 200, 700, 1000, 1500, 2000, 2600, 3150, 200, 400, 	700, 1500, 2600, 200, 400, 800, 1500, 2700, 200, 400, 700, 1000, 1500, 2000, 2500, 3000, 200, 800, 1600, 2000,    200, 400, 700, 1000, 1500, 2000, 1000, 1500, 2000, 2500 ])

	#mWRextra = array('d', [ 3500, 3800, 4200, 4500, 4750, 5200, 5200  ])
	#mNRextra = array('d', [ 400, 3000, 200, 400, 3000, 1000, 2000 ])

	#mWRextraOther = array('d', [ 2400, 2800, 2800, 3150, 3150, 3150, 3500, 3500, 3800, 3800, 3800, 3800, 3800, 3800, 3800, 		4000, 4000, 4000, 4000, 4000, 4000, 4000, 4200, 4200, 4200, 4200, 4500, 4500, 4500, 4500, 4750, 4750, 4750, 4750, 4750, 4750, 4750, 5000, 5000, 5000, 5200, 5200, 5200  ])
	#mNRextraOther = array('d', [ 300, 300,  600, 	200, 400,	 600, 200, 600, 200,   400, 	600, 	1000, 1500, 2000, 2500, 200, 700, 1000, 1500, 2000, 2600, 3150, 400, 	700, 1500, 2600, 200, 800, 1500, 2700, 	200, 400, 700, 1000, 1500, 2000, 2500, 800, 1600, 2000,   700, 1500, 2500 ])

	#mWRextra = array('d', [ 1000, 1500, 1500, 2100, 2100, 2100, 2700,3500, 3800, 4200, 4500, 4500, 4750, 5200, 5200, 5200, 5800, 5800, 5800, 5800  ])
	#mNRextra = array('d', [ 1400, 1600, 1800, 1500, 1800, 2200, 300, 400, 3000, 200, 400, 800, 3000, 1000, 2000, 3000, 500, 1000, 2000, 3000 ])

	"""print "Priority"
	for x in range(len(mWRextra)):
		print "mWR=",int(mWRextra[x]), "mNR=",int(mNRextra[x])

	print "Other"
	for x in range(len(mWRextraOther)):
		print "mWR=",int(mWRextraOther[x]), "mNR=",int(mNRextraOther[x])


	print len( mWRextra + mWRextraOther )
	print len( mNRextra + mNRextraOther )		"""

	"""
	if channel == "ee":
		#> 100 deltaMu
		mWRextra = array('d', [ 1200, 1800, 1800, 2400, 2400, 3000, 3000, 3500, 3500, 3600, 3600, 4200, 4200, 4200, 4500, 4500, 5000, 5000 ])
		mNRextra = array('d', [ 2400, 2700, 3600, 3600, 4800, 4500, 6000, 5250, 7000, 5400, 7200, 4100, 6300, 8400, 4400, 50, 50, 4900 ])

		#10 > deltaMu > 100
		mWRextra2 = array('d', [ 1200, 3000, 3500, 3500, 3600, 4200 ])
		mNRextra2 = array('d', [ 1800, 30, 50, 3400, 3500, 50 ])
	else:
		#> 100 deltaMu
		mWRextra = array('d', [ 1200, 1800, 1800, 2400, 2400, 3000, 3000, 3500, 3500, 3600, 3600, 4200, 4200, 4200, 4500, 5000, 5000 ])
		mNRextra = array('d', [ 2400, 2700, 3600, 3600, 4800, 4500, 6000, 5250, 7000, 5400, 7200, 4100, 6300, 8400, 4400, 50, 4900 ])

		#10 > deltaMu > 100
		mWRextra2 = array('d', [ 1200, 3500, 3600 ])
		mNRextra2 = array('d', [ 1800, 3400, 3500 ])

	extraGrid = ROOT.TGraph( len(mWRextra), mWRextra, mNRextra )
	extraGrid.Draw( "P same" )
	extraGrid.SetMarkerStyle( ROOT.kFullDotMedium )
	extraGrid.SetMarkerColor( ROOT.kRed )
	extraGrid2 = ROOT.TGraph( len(mWRextra2), mWRextra2, mNRextra2 )
	extraGrid2.Draw( "P same" )
	extraGrid2.SetMarkerStyle( ROOT.kFullDotMedium )
	extraGrid2.SetMarkerColor( ROOT.kMagenta )
	"""

	"""if channel == "ee":
		#bad NP errors
		mWRextra = array('d', [ 1800, 3500, 3500, 3600, 4200 ])
		mNRextra = array('d', [ 450, 875, 3400, 1800, 1050 ])
	else:
		#bad NP errors
		mWRextra = array('d', [ 600, 600 ])
		mNRextra = array('d', [ 450, 500 ])
		pass"""

	massGrid = ROOT.TGraph( len(mWR), mWR, mNR )
	massGrid.Draw( "P same" )
	massGrid.SetMarkerStyle( ROOT.kFullDotMedium )
	massGrid.SetMarkerColor( ROOT.kBlack )

	if channel == "ee":
		#bad NP errors
		mWRextra = array('d', [ 1200 ])
		mNRextra = array('d', [ 1100 ])
		mWRextra2 = array('d', [ 600, 600, 1200, 1500, 3000, 3500 ])
		mNRextra2 = array('d', [ 150, 450, 600, 1100, 1800, 50 ])
		extraGrid2 = ROOT.TGraph( len(mWRextra2), mWRextra2, mNRextra2 )
		extraGrid2.Draw( "P same" )
		extraGrid2.SetMarkerStyle( ROOT.kFullDotMedium )
		extraGrid2.SetMarkerColor( ROOT.kOrange )
	else:
		#bad NP errors
		mWRextra = array('d', [ 600, 1000, 1200, 1800, 1800 ])
		mNRextra = array('d', [ 150, 800, 600, 50, 1350 ])
		pass

	text = ROOT.TLatex()
	text.SetTextSize(0.018)
	text.SetTextColor(ROOT.kGray)
	text.SetTextAlign(ROOT.kHAlignRight)
	for x in range(len(mWR)):
		#print mWR[x],mNR[x],ULexpUp[x],ULexp[x],ULexpDown[x]
		text.DrawLatex( mWR[x], mNR[x], str(round(ULobs[x],2)) )


	extraGrid = ROOT.TGraph( len(mWRextra), mWRextra, mNRextra )
	extraGrid.Draw( "P same" )
	extraGrid.SetMarkerStyle( ROOT.kFullDotMedium )
	extraGrid.SetMarkerColor( ROOT.kRed )
	canv2.Update()
	canv2.Print( args.outputFolder+"/exclusion_%s_mu_sig%s_badNPerrors%s" % (channel,str(args.mu_Sig),nametail))

else:
	canv2.Update()
	#canv2.Print( args.outputFolder+"/exclusion_%s_mu_sig%s_rm.pdf" % (channel,str(args.mu_Sig)))
	canv2.Print( args.outputFolder+"/exclusion_%s_mu_sig%s%s" % (channel,str(args.mu_Sig),nametail))
	#canv2.Print( args.outputFolder+"/exclusion_%s_mu_sig%s.root" % (channel,str(args.mu_Sig)))

if not args.do1D:
	sys.exit()

leg3 = ROOT.TLegend(0.2,0.61,0.4,0.81);
leg3.SetBorderSize(0);
leg3.SetFillColor(0);
leg3.SetFillStyle(0);
leg3.SetTextSize(0.045);
leg3.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
leg3.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
leg3.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
leg3.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");

leg4 = ROOT.TLegend(0.5,0.60,0.82,0.85);
leg4.SetBorderSize(0);
leg4.SetFillColor(0);
leg4.SetFillStyle(0);
leg4.SetTextSize(0.045);
leg4.AddEntry(contObs.First(),"#font[42]{Observed 95% CL limit}","l");
leg4.AddEntry(cont.First(),"#font[42]{Expected 95% CL limit}","l");
leg4.AddEntry(tempGrGreen,"#font[42]{Expected limit #pm 1#sigma}","f");
leg4.AddEntry(tempGrYellow,"#font[42]{Expected limit #pm 2#sigma}","f");
leg4.AddEntry(tempGrRed,"#font[42]{#sigma_{KS} #times #it{B}(#it{W}_{R},#it{N}_{R} #rightarrow %s)}" % ("#it{ee}" if channel == "ee" else "#it{#mu#mu}"),"l") 

for ratio in [2./3,4./3,2.,4.]:
	obs = ROOT.TGraph()
	exp = ROOT.TGraph()
	exp1 = ROOT.TGraphAsymmErrors()
	exp2 = ROOT.TGraphAsymmErrors()
	exp1.SetFillColor(8)
	exp2.SetFillColor(5)
	exp.SetLineStyle(2)
	exp.SetLineWidth(2)
	obs.SetLineWidth(2)

	xsec = ROOT.TGraph()
	obs_xsec = ROOT.TGraph()
	exp_xsec = ROOT.TGraph()
	exp1_xsec = ROOT.TGraphAsymmErrors()
	exp2_xsec = ROOT.TGraphAsymmErrors()
	exp1_xsec.SetFillColor(8)
	exp2_xsec.SetFillColor(5)
	exp_xsec.SetLineStyle(2)
	exp_xsec.SetLineWidth(2)
	obs_xsec.SetLineWidth(2)
	xsec.SetLineWidth(2)
	xsec.SetLineColor(ROOT.kRed)

	Xmin = 10000
	Xmax = 0
	for sample in signalSamples:
		if float(sample[0]) != ratio*sample[1]:
			continue
		if sample[1] > 5000:
			continue
		if sample[1] > sample[0] and sample[0] > 3000:
			continue

		var = 'Mjj'
		if sample[1] < sample[0]:
			var = 'Mlljj'
		resultsfile = ROOT.TFile( 'selected_mass_samples/'+args.prefix+'WR'+str(sample[0])+'_NR'+str(sample[1])+'_Sig_'+channel+'-excl_Output_upperlimit.root', "READ" )
		
		name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])
		result = resultsfile.Get(name)

		#print sample[0]," ",args.mu_Sig*sample[2]
		if not result:
			print name,"continue"
			continue

		if sample[0] < Xmin:
			Xmin = sample[0]
		if sample[0] > Xmax:
			Xmax = sample[0]

		"""print result.UpperLimit()
		print result.GetExpectedUpperLimit(0)
		print result.GetExpectedUpperLimit(1)
		print result.GetExpectedUpperLimit(2)
		print result.GetExpectedUpperLimit(-1)
		print result.GetExpectedUpperLimit(-2)"""

		obs.SetPoint(obs.GetN(),sample[0],result.UpperLimit())
		exp.SetPoint(exp.GetN(),sample[0],result.GetExpectedUpperLimit(0))
		exp1.SetPoint(exp1.GetN(),sample[0],result.GetExpectedUpperLimit(0))
		exp2.SetPoint(exp2.GetN(),sample[0],result.GetExpectedUpperLimit(0))
		exp1.SetPointError(exp1.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-1),result.GetExpectedUpperLimit(1)-result.GetExpectedUpperLimit(0))
		exp2.SetPointError(exp2.GetN()-1,0,0,result.GetExpectedUpperLimit(0)-result.GetExpectedUpperLimit(-2),result.GetExpectedUpperLimit(2)-result.GetExpectedUpperLimit(0))

		obs_xsec.SetPoint(obs_xsec.GetN(),sample[0],result.UpperLimit()*args.mu_Sig*sample[2])
		exp_xsec.SetPoint(exp_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2])
		exp1_xsec.SetPoint(exp1_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2])
		exp2_xsec.SetPoint(exp2_xsec.GetN(),sample[0],result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2])
		exp1_xsec.SetPointError(exp1_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]-result.GetExpectedUpperLimit(-1)*args.mu_Sig*sample[2],result.GetExpectedUpperLimit(1)*args.mu_Sig*sample[2]-result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2])
		exp2_xsec.SetPointError(exp2_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]-result.GetExpectedUpperLimit(-2)*args.mu_Sig*sample[2],result.GetExpectedUpperLimit(2)*args.mu_Sig*sample[2]-result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2])
		xsec.SetPoint(xsec.GetN(),sample[0],args.mu_Sig*sample[2])

		resultsfile.Close()

	# exp1.SetPoint(exp1.GetN(),1e6,1e6)
	# exp1.SetPointError(exp1.GetN()-1,0,0,0,0)
	# exp2.SetPoint(exp2.GetN(),1e6,1e6)
	# exp2.SetPointError(exp2.GetN()-1,0,0,0,0)

	mg1D = ROOT.TMultiGraph()
	mg1D.Add(exp2,"3")
	mg1D.Add(exp1,"3")
	mg1D.Add(exp,"l")
	mg1D.Add(obs,"l")

	mg1D_xsec = ROOT.TMultiGraph()
	mg1D_xsec.Add(exp2_xsec,"3")
	mg1D_xsec.Add(exp1_xsec,"3")
	mg1D_xsec.Add(exp_xsec,"l")
	mg1D_xsec.Add(obs_xsec,"l")
	mg1D_xsec.Add(xsec,"c")

	#print Xmin," ",Xmax

	canv1D = ROOT.TCanvas("excl1D%1.2f%s"%(ratio,channel),"excl1D%1.2f%s"%(ratio,channel),800,600)
	canv1D.cd()
	canv1D.SetLogy()
	mg1D.Draw("a")
	mg1D.SetMaximum(1e3)
	mg1D.GetXaxis().SetTitle("#it{m}_{#it{W}_{R}} = %1.2f #times #it{m}_{#it{N}_{R}}" % ratio)
	mg1D.GetYaxis().SetTitle("#mu_{SIG}^{95%}")
	mg1D.GetXaxis().SetRangeUser(Xmin,Xmax)
	ROOT.ATLASLabel(0.2,0.875,"Internal",1)
	ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
	ROOT.myText(0.2,0.80,"#it{g}_R = #it{g}_L ")
	if args.mu_Sig == 2:
		ROOT.myText(0.54,0.875,1,"Dirac #it{N}_{R}, %s channel" % ("#it{ee}" if channel == "ee" else "#it{#mu#mu}"))
	else:
		if channel == "ee":
			ROOT.myText(0.54,0.875,1,"Majorana #it{N}_{R}, #it{ee} channel")
		else:
			ROOT.myText(0.54,0.875,1,"Majorana #it{N}_{R}, #it{#mu#mu} channel")
	leg3.Draw()
	canv1D.Update();
	xmin = ROOT.gPad.GetUxmin()
	xmax = ROOT.gPad.GetUxmax()
	ymin = ROOT.gPad.GetUymin()
	ymax = ROOT.gPad.GetUymax()
	line = ROOT.TLine(xmin,args.mu_Sig,xmax,args.mu_Sig)
	line.Draw()
	line.SetLineColor(ROOT.kRed)
	ROOT.gPad.RedrawAxis("g");
	canv1D.Print( args.outputFolder+"/exclusion_1D_mu_%2.0f_%s_mu_sig%s.pdf" % (float(100*ratio),channel,str(args.mu_Sig)))

	canv1D_xsec = ROOT.TCanvas("excl1D%1.2f_xsec%s"%(ratio,channel),"excl1D%1.2f_xsec%s"%(ratio,channel),800,600)
	canv1D_xsec.cd()
	canv1D_xsec.SetLogy()
	mg1D_xsec.Draw("a")
	if ratio < 2:
		mg1D_xsec.SetMaximum(1e3)
		mg1D_xsec.SetMinimum(1e-4)
	else:
		mg1D_xsec.SetMaximum(1e5)
		mg1D_xsec.SetMinimum(1e-4)
	mg1D_xsec.GetXaxis().SetTitle("#it{m}_{#it{W}_{R}} = %1.2f #times #it{m}_{#it{N}_{R}}" % ratio)
	mg1D_xsec.GetYaxis().SetTitle("#sigma #times #it{B}(#it{W}_{R},#it{N}_{R} #rightarrow %s) [pb]" % ("#it{ee}" if channel == "ee" else "#it{#mu#mu}"))
	mg1D_xsec.GetXaxis().SetRangeUser(Xmin,Xmax)
	ROOT.ATLASLabel(0.2,0.875,"Internal",1)
	ROOT.myText(0.2,0.82,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
	ROOT.myText(0.54,0.82,1,"#it{g}_{R} = #it{g}_{L}")
	if args.mu_Sig == 2:
		ROOT.myText(0.54,0.875,1,"Dirac #it{N}_{R}, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))
	else:
		if channel == "ee":
			ROOT.myText(0.54,0.875,1,"electron channel (e^{#pm}e^{#mp})")
		else:
			ROOT.myText(0.54,0.875,1,"muon channel (#mu^{#pm}#mu^{#mp})")
	leg4.Draw()
	ROOT.gPad.RedrawAxis("g");
	canv1D_xsec.Print( args.outputFolder+"/exclusion_1D_%2.0f_%s_mu_sig%s.pdf" % (float(100*ratio),channel,str(args.mu_Sig)))


for mWR in [3000,3500]:
	obs = ROOT.TGraph()
	exp = ROOT.TGraph()
	exp1 = ROOT.TGraphAsymmErrors()
	exp2 = ROOT.TGraphAsymmErrors()
	exp1.SetFillColor(8)
	exp2.SetFillColor(5)
	exp.SetLineStyle(2)
	exp.SetLineWidth(2)
	obs.SetLineWidth(2)

	xsec = ROOT.TGraph()
	obs_xsec = ROOT.TGraph()
	exp_xsec = ROOT.TGraph()
	exp1_xsec = ROOT.TGraphAsymmErrors()
	exp2_xsec = ROOT.TGraphAsymmErrors()
	exp1_xsec.SetFillColor(8)
	exp2_xsec.SetFillColor(5)
	exp_xsec.SetLineStyle(2)
	exp_xsec.SetLineWidth(2)
	obs_xsec.SetLineWidth(2)
	xsec.SetLineWidth(2)
	xsec.SetLineColor(ROOT.kRed)

	Xmin = 10000
	Xmax = 0
	for sample in signalSamples:
		if sample[0] != mWR:
			continue
		"""if sample[1] > 5000:
			continue
		if sample[1] > sample[0] and sample[0] > 3000:
			continue"""

		var = 'Mjj'
		if sample[1] < sample[0]:
			var = 'Mlljj'
		resultsfile = ROOT.TFile( 'selected_mass_samples/'+args.prefix+'WR'+str(sample[0])+'_NR'+str(sample[1])+'_Sig_'+channel+'-excl_Output_upperlimit.root', "READ" )

		name = "hypo_WR%s_NR%s_Sig" % (sample[0],sample[1])
		result = resultsfile.Get(name)

		#print sample[0]," ",args.mu_Sig*sample[2]
		if not result:
			print name,"continue"
			continue

		if sample[1] < Xmin:
			Xmin = sample[1]
		if sample[1] > Xmax:
			Xmax = sample[1]

		"""print result.UpperLimit()
		print result.GetExpectedUpperLimit(0)
		print result.GetExpectedUpperLimit(1)
		print result.GetExpectedUpperLimit(2)
		print result.GetExpectedUpperLimit(-1)
		print result.GetExpectedUpperLimit(-2)"""

		print "WR:",sample[0],"NR:",sample[1],"ul:",result.GetExpectedUpperLimit(0),"ul-2:",result.GetExpectedUpperLimit(-2),"ul2:",result.GetExpectedUpperLimit(2)

		obs.SetPoint(obs.GetN(),sample[1],result.UpperLimit()*0.333)
		exp.SetPoint(exp.GetN(),sample[1],result.GetExpectedUpperLimit(0)*0.333)
		exp1.SetPoint(exp1.GetN(),sample[1],result.GetExpectedUpperLimit(0)*0.333)
		exp2.SetPoint(exp2.GetN(),sample[1],result.GetExpectedUpperLimit(0)*0.333)
		exp1.SetPointError(exp1.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*0.333-result.GetExpectedUpperLimit(-1)*0.333,result.GetExpectedUpperLimit(1)*.333-result.GetExpectedUpperLimit(0)*0.333)
		exp2.SetPointError(exp2.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*0.333-result.GetExpectedUpperLimit(-2)*0.333,result.GetExpectedUpperLimit(2)*0.333-result.GetExpectedUpperLimit(0)*0.333)

		print "cross sec", args.mu_Sig*sample[2]
		print "1/3 cross sec", args.mu_Sig*sample[2]*1/3
		obs_xsec.SetPoint(obs_xsec.GetN(),sample[1],result.UpperLimit()*args.mu_Sig*sample[2]*1/3)
		exp_xsec.SetPoint(exp_xsec.GetN(),sample[1],result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3)
		exp1_xsec.SetPoint(exp1_xsec.GetN(),sample[1],result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3)
		exp2_xsec.SetPoint(exp2_xsec.GetN(),sample[1],result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3)
		exp1_xsec.SetPointError(exp1_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3-result.GetExpectedUpperLimit(-1)*args.mu_Sig*sample[2]*1/3,result.GetExpectedUpperLimit(1)*args.mu_Sig*sample[2]*1/3-result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3)
		exp2_xsec.SetPointError(exp2_xsec.GetN()-1,0,0,result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3-result.GetExpectedUpperLimit(-2)*args.mu_Sig*sample[2]*1/3,result.GetExpectedUpperLimit(2)*args.mu_Sig*sample[2]*1/3-result.GetExpectedUpperLimit(0)*args.mu_Sig*sample[2]*1/3)
		xsec.SetPoint(xsec.GetN(),sample[1],args.mu_Sig*sample[2]*1/3)

		resultsfile.Close()

	# exp1.SetPoint(exp1.GetN(),1e6,1e6)
	# exp1.SetPointError(exp1.GetN()-1,0,0,0,0)
	# exp2.SetPoint(exp2.GetN(),1e6,1e6)
	# exp2.SetPointError(exp2.GetN()-1,0,0,0,0)

	mg1D = ROOT.TMultiGraph()
	mg1D.Add(exp2,"3")
	mg1D.Add(exp1,"3")
	mg1D.Add(exp,"l")
	mg1D.Add(obs,"l")

	mg1D_xsec = ROOT.TMultiGraph()
	mg1D_xsec.Add(exp2_xsec,"3")
	mg1D_xsec.Add(exp1_xsec,"3")
	mg1D_xsec.Add(exp_xsec,"l")
	mg1D_xsec.Add(obs_xsec,"l")
	mg1D_xsec.Add(xsec,"c")

	#print Xmin," ",Xmax

	canv1D = ROOT.TCanvas("excl1D%d%s"%(mWR,channel),"excl1D%d%s"%(mWR,channel),800,600)
	canv1D.cd()
	canv1D.SetLogy()
	mg1D.Draw("a")
	mg1D.SetMaximum(1e3)
	mg1D.GetXaxis().SetTitle("#it{m}_{#it{N}_{R}} for #it{m}_{#it{W}_{R}} = %d GeV" % mWR) # = %1.2f #times m(#it{N}_{R})" % ratio)
	mg1D.GetYaxis().SetTitle("#mu_{SIG}^{95%}")
	mg1D.GetXaxis().SetRangeUser(Xmin,Xmax)
	ROOT.ATLASLabel(0.2,0.875,"Internal",1)
	ROOT.myText(0.2,0.82,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
	if args.mu_Sig == 2:
		ROOT.myText(0.54,0.875,1,"Dirac #it{N}_{R}, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))
	else:
		if channel == "ee":
			ROOT.myText(0.54,0.875,1,"electron channel (e^{#pm}e^{#mp})")
		else:
			ROOT.myText(0.54,0.875,1,"muon channel (#mu^{#pm}#mu^{#mp})")
	leg3.Draw()
	canv1D.Update();
	xmin = ROOT.gPad.GetUxmin()
	xmax = ROOT.gPad.GetUxmax()
	ymin = ROOT.gPad.GetUymin()
	ymax = ROOT.gPad.GetUymax()
	line = ROOT.TLine(xmin,args.mu_Sig,xmax,args.mu_Sig)
	line.Draw()
	line.SetLineColor(ROOT.kRed)
	ROOT.gPad.RedrawAxis("g");
	canv1D.Print( args.outputFolder+"/exclusion_1D_mu_mWR%d_%s_mu_sig%s.pdf" % (mWR,channel,str(args.mu_Sig)))

	canv1D_xsec = ROOT.TCanvas("excl1D%d_xsec%s"%(mWR,channel),"excl1D%d_xsec%s"%(mWR,channel),800,600)
	canv1D_xsec.cd()
	canv1D_xsec.SetLogy()
	mg1D_xsec.Draw("a")
	mg1D_xsec.SetMaximum(1e3)
	mg1D_xsec.SetMinimum(1e-4)
	mg1D_xsec.GetXaxis().SetTitle("#it{m}_{#it{N}_{R}} for #it{m}_{#it{W}_{R}} = %d GeV" % mWR) # = %1.2f #times m(#it{N}_{R})" % ratio)
	mg1D_xsec.GetYaxis().SetTitle("#sigma #times #it{B}(#it{W}_{R},#it{N}_{R} #rightarrow %s) [pb]" % ("#it{ee}" if channel == "ee" else "#it{#mu#mu}"))
	mg1D_xsec.GetXaxis().SetRangeUser(Xmin,Xmax)
	ROOT.ATLASLabel(0.2,0.875,"Internal",1)
	ROOT.myText(0.2,0.82,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")
	if args.mu_Sig == 2:
		ROOT.myText(0.54,0.875,1,"Dirac #it{N}_{R}, %s channel" % ("#it{ee}" if channel == "ee" else "#mu#mu"))
	else:
		if channel == "ee":
			ROOT.myText(0.54,0.875,1,"electron channel (e^{#pm}e^{#mp})")
		else:
			ROOT.myText(0.54,0.875,1,"muon channel (#mu^{#pm}#mu^{#mp})")
	leg4.Draw()
	ROOT.gPad.RedrawAxis("g");
	canv1D_xsec.Print( args.outputFolder+"/exclusion_1D_mWR%d_%s_mu_sig%s.pdf" % (mWR,channel,str(args.mu_Sig)))
