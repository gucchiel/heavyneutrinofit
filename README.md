# HeavyNeutrinoFit

This repository contains the configuration files used for HistFitter to perform the statistical analysis of the Heavy Neutrino search. The repository is divided into two folders, one used for the SS-only fit and one used for the combined (OS+SS) fit.