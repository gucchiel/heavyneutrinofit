"""
 * Project : HistFitter - A ROOT-based package for statistical data analysis      *
 * Package : HistFitter                                                           *
 * Script  : SysTableTex.py                                                       *
 *                                                                                *
 * Description:                                                                   *
 *      Script for producing LaTeX-files derived from systematics tables          *
 *      produced  by SysTable.py script                                           *
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group                                                          *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
"""
import math

MAXSYS = 999999

def tablefragment(m,signalRegions,skiplist,chanStr,showPercent):
  """ 
  main function to transfer the set of numbers/names (=m provided by SysTable) into a LaTeX table

  @param m Set of numbers/names provided by SysTable
  @param signalRegions List of channels/regions used
  @param skiplist List of parameters/members of 'm' to be skipped (such as 'sqrtnobsa') when showing per-systematic errors
  @param chanStr String of all channels used, to be used in label of table
  @param showPercent Boolean deciding whether to show percentage for each systematic
  """

  remap = {}
  translator = {}
  remap["CF"] = "Charge-Flip"
  remap["FF"] = "Fake-Factor ele"
  remap["MUFF"] = "Fake-Factor muon"
  remap["ID"] = "el. ID (TOTAL model)"
  remap["TRIG"] = "el. trig (TOTAL model)"
  remap["RECO"] = "el. reco (TOTAL model)"
  remap["ISO"] = "el. iso (TOTAL model)"
  remap["EG\_RESOLUTION\_ALL"] = "EG resolution (ALLCORR)"
  remap["EG\_SCALE\_ALLCORR"] = "EG scale (ALLCORR)"
  remap["EG\_SCALE\_E4SCINTILLATOR"] = "EG E4SCINTILLATOR"
  remap["BEAM"] = "DY BEAM"
  remap["PI"] = "DY PI"
  remap["PDF"] = "DY PDF ERR"
  remap["CHOICE"] = "DY PDF CHOICE"
  remap["SCALE\_Z"] = "DY SCALE"
  remap["DBGen"] = "diboson theory"
  remap["DBGenEE"] = "diboson theory ($ee$)"
  remap["DBGenEM"] = "diboson theory ($e\\mu$)"
  remap["DBGenMM"] = "diboson theory ($\\mu\\mu$)"

  remap["RECOSYS"]  = "mu reco (SYS)"
  remap["RECOSTAT"] = "mu reco (STAT)"
  remap["TRIGSYS"]  = "mu trig (SYS)"
  remap["TRIGSTAT"] = "mu trig (STAT)"
  remap["ISOSYS"]   = "mu iso  (SYS)"
  remap["ISOSTAT"]  = "mu iso  (STAT)"
  remap["TTVASYS"]  = "mu TTVA (SYS)"
  remap["TTVASTAT"] = "mu TTVA (STAT)"
  remap["MUON_ID"]  = "MUON ID"
  remap["MUON_MS"]  = "MUON MS"
  remap["MUON_RESBIAS"] = "MUON SAGITTA RESBIAS"
  remap["MUON_RHO"] = "MUON SAGITTA RHO"
  remap["MUON_SCALE"] = "MUON SCALE"

  # translator["SSZCRee"]   = "CR ($e^{\\pm}e^{\\pm}$)"
  # translator["SSZCRmm"]   = "CR ($\\mu^{\\pm}\\mu^{\\pm}$)"
  # translator["SSZVRee"]   = "VR ($e^{\\pm}e^{\\pm}$)"
  # translator["SSZVRmm"]   = "VR ($\\mu^{\\pm}\\mu^{\\pm}$)"
  # translator["SSZSRee"]   = "SR ($e^{\\pm}e^{\\pm}$)"
  # translator["SSZSRmm"]   = "SR ($\\mu^{\\pm}\\mu^{\\pm}$)"

  # translator["ZCR\\_eeos"]   = "CR ($e^{\\pm}e^{\\mp}$)"
  # translator["ZCR\\_mmos"]   = "CR ($\\mu^{\\pm}\\mu^{\\mp}$)"
  # translator["ZVR\\_eeos"]   = "VR ($e^{\\pm}e^{\\mp}$)"
  # translator["ZVR\\_mmos"]   = "VR ($\\mu^{\\pm}\\mu^{\\mp}$)"
  # translator["SR\\_eeos"]   = "SR ($e^{\\pm}e^{\\mp}$)"
  # translator["SR\\_mmos"]   = "SR ($\\mu^{\\pm}\\mu^{\\mp}$)"

  
  tableline = ''

  tableline += '''
\\begin{table}
\\begin{center}
\\setlength{\\tabcolsep}{0.0pc}
\\begin{tabular*}{\\textwidth}{@{\\extracolsep{\\fill}}l'''

  """
  print the region names
  """ 
  for region in signalRegions:
    tableline += "c"   
  tableline += '''}
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
{\\textbf{Uncertainty of channel}}                                   ''' 


  """
  print the total fitted (after fit) number of events
  """   
  for region in signalRegions:
    if region.replace('_','\_') in translator.keys():
      string = translator[region.replace('_','\_')]
    else:
      string = region.replace('_','\_')
    tableline += " & " + string + "           "   

  tableline += ''' \\\\
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
%%'''

  tableline += '''
Total background expectation            '''
  for region in signalRegions:
    tableline += " &  $" + str(("%.2f" %m[region]['nfitted'])) + "$       "
  tableline += '''\\\\
%%'''


  tableline += ''' \\\\
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
%%'''


  """
  print sqrt(N_obs) - for comparison with total systematic
  """   
#   tableline += '''
# Total statistical $(\\sqrt{N_{\\rm exp}})$             '''
#   for region in signalRegions:
#     tableline += " & $\\pm " + str(("%.2f" %m[region]['sqrtnfitted'])) + "$       "
#   tableline += '''\\\\
# %%'''

  """
  print total systematic uncertainty
  """   
  tableline += '''
Total background systematic              '''

  for region in signalRegions:
    percentage = m[region]['totsyserr']/m[region]['nfitted'] * 100.0    
    tableline += " & $\\pm " + str(("%.2f" %m[region]['totsyserr'])) + "\ [" + str(("%.2f" %percentage)) + "\\%] $       "

  tableline += '''      \\\\
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
%%''' 


  """
  print systematic uncertainty per floated parameter (or set of parameters, if requested)
  """   
  d = m[signalRegions[len(signalRegions)-1]] 
  m_listofkeys = sorted(d.iterkeys(), key=lambda k: d[k], reverse=True)

  print skiplist

  m_listofkeys += ["total stat. error"]
  for region in signalRegions:
    m[region]["total stat. error"] = 0
  for name in m_listofkeys:
    # print name
    if name not in skiplist:
      printname = name
      printname = printname.replace('syserr_','')
      printname = printname.replace('_','\_')
      for index,region in enumerate(signalRegions):
        if "gamma" in name:
          m[region]["total stat. error"] += m[region][name]*m[region][name]
    # else:
    #   print "SKIIIP: ",name
  for region in signalRegions:
    m[region]["total stat. error"] = math.sqrt(m[region]["total stat. error"])

  d = m[signalRegions[len(signalRegions)-1]] 
  m_listofkeys = sorted(d.iterkeys(), key=lambda k: d[k], reverse=True)

  for nSys,name in enumerate(m_listofkeys):
    if nSys >= MAXSYS:
      break
    if "gamma" in name:
      continue
    if name not in skiplist:
      printname = name
      printname = printname.replace('syserr_','')
      printname = printname.replace('alpha_','')
      printname = printname.replace('gamma_','')
      printname = printname.replace('_','\_')
      for index,region in enumerate(signalRegions):
          # continue
        if index == 0:
          # if printname in remap:
          #   printname = remap[printname]
          tableline += "\n" + printname + "      "
          
        if not showPercent:
          tableline += "   & $\\pm " + str(("%.6f" %m[region][name])) + "$       "
        else:
          percentage = m[region][name]/m[region]['nfitted'] * 100.0
          if percentage <1:
            tableline += "   & $\\pm " + str(("%.2f" %m[region][name])) + "\ [" + str(("%.2f" %percentage)) + "\\%] $       "
          else:
            tableline += "   & $\\pm " + str(("%.2f" %m[region][name])) + "\ [" + str(("%.1f" %percentage)) + "\\%] $       "
                    
          
        if index == len(signalRegions)-1:
          tableline += '''\\\\
%%'''
    # else:
    #   print "SKIIIIIIIIP2: ",name

  """
  print table end with default Caption and Label
  """   

  tableline += '''
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
\\end{tabular*}
\\end{center}
\\caption[Breakdown of uncertainty on background estimates]{
Breakdown of the dominant uncertainties on background estimates. The given total statistical error is
a quadratic sum of individual statistical errors of each bin in the region. Note that the individual
uncertainties can be correlated, and do not necessarily add up quadratically to the total background uncertainty.
Uncertainties are ordered from the largest to smallest in total (last column).
\\label{table.results.bkgestimate.uncertainties.%s}}
\\end{table}
%%''' % (chanStr) 
    
  return tableline

