#!/bin/env python
# Adapted from pull_maker.py

import ROOT
ROOT.SetSignalPolicy( ROOT.kSignalFast )
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)

from array import array
import sys,getopt,os,glob
from math import ceil,fabs

def main(argv):
	"""
	main function call

	@param argv Arguments from user to define input stub name (-i <inputstub>) and output file name (-o <outputName>)
	"""
	inputstub = ''
	outputfile = 'test_out'
	channel = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:c:",["ifile=","ofile=","chan="])
	except getopt.GetoptError:
		print 'pull_checker.py -i <inputstub> -o <outputfile> -c <channel>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'pull_checker.py -i <inputstub> -o <outputfile>'
			sys.exit()
		elif opt in ("-i", "--ifolder"):
			inputstub = arg
		elif opt in ("-o", "--ofile"):
			outputfile = arg
		elif opt in ("-c", "--chan"):
			channel = arg
		#print 'Input file is "', inputstub
		#print 'Output file is "', outputfile

	if not inputstub:
		print "No input stub name defined, please define with '-i <inputstub>'"
		print "format: pull_checker.py -i data/${anaName}/${logFileName}"
		sys.exit(2)
	if outputfile == 'test_out':
		outputfile = inputstub.split('/')[-1]

	# loop over logs that match input stub
	#data/${anaName}/${logFileName}_{}_log.txt
	inputfiles = glob.glob( inputstub+'*'+channel+'*.log' )
	print inputstub
	print inputfiles
	# sort by WR mass then NR mass
	try:
		inputfiles.sort( key=lambda x:( int(x.split('WR')[-1].split('_')[0]), int(x.split('NR')[-1].split('_')[0]) ) ) 
	except:
		pass	

	createHist = True
		
	for filen,inputfile in enumerate(inputfiles):

		log = open(inputfile,'r')
		print inputfile

		uncRes = [[],[],[],[]]
		normFactorDB = [[],[]]
		normFactorZSS = [[],[]]
		switchOn = False
		muIDX = -1
		for aline in log.readlines():
			if 'FinalValue' in aline:
				switchOn = True
				#print "Switching on!"
				uncRes = [[],[],[],[]]
				muIDX = -1
				continue
			if '------------' in aline: continue
			if switchOn:
				if 'gamma_stat' in aline: continue
				if 'QCDNorm' in aline: continue
				#if 'mu_' in aline and muIDX<0:
				#	muIDX=len(uncRes[0])
				if len(aline.strip())<3:
					switchOn=False
					continue
								  
				if ( 'alpha_' in aline or 'Lumi' in aline ) and ( ' +/- ' in aline ):
					#parameter name and central value				
					uncRes[0] += [ aline.split()[0].replace('alpha_','') ]
					uncRes[1] += [ float(aline.split()[2]) ]
					#errors
					uncRes[2] += [ float(aline.split()[4]) ]
					uncRes[3] += [ float(aline.split()[4]) ]
				elif ('mu_DB' in aline) and (' +/- ' in aline ):
				        #parameter and error
				        normFactorDB[0] += [ float(aline.split()[2])]
				        normFactorDB[1] += [ float(aline.split()[4])]
					print normFactorDB[0] 
					print normFactorDB[1]
				elif ('mu_ZSS' in aline) and (' +/- ' in aline ):
				        #parameter and error
				        normFactorZSS[0] += [ float(aline.split()[2])]
				        normFactorZSS[1] += [ float(aline.split()[4])]
				elif ( 'alpha_' in aline or 'Lumi' in aline ) and ( '(+' in aline ) and ( ',-' in aline ):
					uncRes[0] += [ aline.split()[0].replace('alpha_','') ]
					uncRes[1] += [ float(aline.split()[2]) ]
					#minos error
					if len(aline.strip())<2: continue
					brief = (aline.split()[3].split('(')[1].split(')')[0]).replace('--','-')
					uncRes[2] += [ abs(float(brief.split(',')[0])) ]
					uncRes[3] += [ abs(float(brief.split(',')[1])) ]
					if uncRes[2][-1]<0.00001: uncRes[2][-1]=uncRes[3][-1]
					if uncRes[3][-1]<0.00001: uncRes[3][-1]=uncRes[2][-1]
			
 
		if createHist:
			# setup output histogram 
			# how many NP errors are >1 
			# vs. how many >1 NP errors each mass point has 
			# y axis = NPs, x axis = mass points
	
			#print uncRes[0]
			#sys.exit()

			#allNPs = ['Lumi', 'AlphaS_PDF261000', 'EG_RESOLUTION_ALL_OS', 'EG_SCALE_ALL', 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'FT_EFF_Eigen_B_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_B_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_B_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets', 'FT_EFF_extrapolation_AntiKt4EMTopoJets', 'FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets', 'JET_JER_SINGLE_NP', 'JET_SR1_JET_EtaIntercalibration_NonClosure', 'JET_SR1_JET_GroupedNP_1', 'JET_SR1_JET_GroupedNP_2', 'JET_SR1_JET_GroupedNP_3', 'MJJRW_1', 'MUON_EFF_STAT', 'MUON_EFF_SYS', 'MUON_EFF_TrigStatUncertainty', 'MUON_EFF_TrigSystUncertainty', 'MUON_ID_OS', 'MUON_ISO_STAT', 'MUON_ISO_SYS', 'MUON_SCALE_OS', 'MUON_TTVA_STAT', 'MUON_TTVA_SYS', 'PDFVary_PDF', 'PDF_PDF261000', 'SIG_THEORY', 'Scale_MUR_MUF_PDF261000', 'ttbar_AdditionalRadiation', 'ttbar_FragmentationHadronization', 'ttbar_HardScatter']

			#allNPs = ['Lumi', 'ALPHA_SYS', 'AlphaS_PDF261000', 'B_SYS', 'C_SYS', 'EFC_SYS', 'EG_RESOLUTION_ALL', 'EG_RESOLUTION_ALL_OS', 'EG_SCALE_ALL', 'EG_SCALE_ALLCORR', 'EG_SCALE_E4SCINTILLATOR', 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'E_SYS', 'FF', 'CF', 'FT_EFF_Eigen_B_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_B_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_B_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets', 'FT_EFF_extrapolation_AntiKt4EMTopoJets', 'FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets', 'ID', 'ISO', 'ISOSTAT', 'ISOSYS', 'JET_BJES_Response', 'JET_EffectiveNP_1', 'JET_EffectiveNP_2', 'JET_EffectiveNP_3', 'JET_EffectiveNP_4', 'JET_EffectiveNP_5', 'JET_EffectiveNP_6', 'JET_EffectiveNP_7', 'JET_EffectiveNP_8restTerm', 'JET_EtaIntercalibration_Modelling', 'JET_EtaIntercalibration_NonClosure', 'JET_EtaIntercalibration_TotalStat', 'JET_Flavor_Composition', 'JET_Flavor_Response', 'JET_JER_CROSS_CALIB_FORWARD', 'JET_JER_NOISE_FORWARD', 'JET_JER_NP0', 'JET_JER_NP1', 'JET_JER_NP2', 'JET_JER_NP3', 'JET_JER_NP4', 'JET_JER_NP5', 'JET_JER_NP6', 'JET_JER_NP7', 'JET_JER_NP8', 'JET_JER_SINGLE_NP', 'JET_Pileup_OffsetMu', 'JET_Pileup_OffsetNPV', 'JET_Pileup_PtTerm', 'JET_Pileup_RhoTopology', 'JET_PunchThrough_MC15', 'JET_SR1_JET_EtaIntercalibration_NonClosure', 'JET_SR1_JET_GroupedNP_1', 'JET_SR1_JET_GroupedNP_2', 'JET_SR1_JET_GroupedNP_3', 'JET_SingleParticle_HighPt', 'JVT_SYS', 'L_SYS', 'MJJRW_1', 'MUFF', 'MUON_EFF_STAT', 'MUON_EFF_SYS', 'MUON_EFF_TrigStatUncertainty', 'MUON_EFF_TrigSystUncertainty', 'MUON_ID', 'MUON_ID_OS', 'MUON_ISO_STAT', 'MUON_ISO_SYS', 'MUON_MS', 'MUON_RESBIAS', 'MUON_RHO', 'MUON_SCALE', 'MUON_SCALE_OS', 'MUON_TTVA_STAT', 'MUON_TTVA_SYS', 'PDFVary_PDF', 'PDF_COICE_ENVELOPE', 'PDF_PDF261000', 'PDF_SYS_ENVELOPE', 'QCD_SCALE_ENVELOPE', 'RECO', 'RECOSTAT', 'RECOSYS', 'SIG_THEORY', 'Scale_MUR_MUF_PDF261000', 'TRIG', 'TRIGSTAT', 'TRIGSYS', 'TTVASTAT', 'TTVASYS', 'tt_THEORY', 'ttbar_AdditionalRadiation', 'ttbar_FragmentationHadronization', 'ttbar_HardScatter']
			allNPs = ['Lumi', 'ALPHA_SYS', 'B_SYS', 'C_SYS', 'EFC_SYS', 'EG_RESOLUTION_ALL', 'EG_SCALE_ALLCORR', 'EG_SCALE_E4SCINTILLATOR', 'E_SYS', 'FF', 'CF', 'ID', 'ISO', 'ISOSTAT', 'ISOSYS', 'JET_BJES_Response', 'JET_EffectiveNP_1', 'JET_EffectiveNP_2', 'JET_EffectiveNP_3', 'JET_EffectiveNP_4', 'JET_EffectiveNP_5', 'JET_EffectiveNP_6', 'JET_EffectiveNP_7', 'JET_EffectiveNP_8restTerm', 'JET_EtaIntercalibration_Modelling', 'JET_EtaIntercalibration_NonClosure', 'JET_EtaIntercalibration_TotalStat', 'JET_Flavor_Composition', 'JET_Flavor_Response', 'JET_JER_CROSS_CALIB_FORWARD', 'JET_JER_NOISE_FORWARD', 'JET_JER_NP0', 'JET_JER_NP1', 'JET_JER_NP2', 'JET_JER_NP3', 'JET_JER_NP4', 'JET_JER_NP5', 'JET_JER_NP6', 'JET_JER_NP7', 'JET_JER_NP8', 'JET_Pileup_OffsetMu', 'JET_Pileup_OffsetNPV', 'JET_Pileup_PtTerm', 'JET_Pileup_RhoTopology', 'JET_PunchThrough_MC15', 'JET_SingleParticle_HighPt', 'JVT_SYS', 'L_SYS', 'MUFF', 'MUON_ID', 'MUON_MS', 'MUON_RESBIAS', 'MUON_RHO', 'MUON_SCALE', 'PDF_COICE_ENVELOPE', 'PDF_SYS_ENVELOPE', 'QCD_SCALE_ENVELOPE', 'RECO', 'RECOSTAT', 'RECOSYS', 'SIG_THEORY', 'TRIG', 'TRIGSTAT', 'TRIGSYS', 'TTVASTAT', 'TTVASYS', 'tt_THEORY']

			constraintHist = ROOT.TH2D( "", "", len(inputfiles)+1, 0, len(inputfiles)+1, len(allNPs)+1, 0, len(allNPs)+1, )
			pullHist = ROOT.TH2D( "", "", len(inputfiles)+1, 0, len(inputfiles)+1, len(allNPs)+1, 0, len(allNPs)+1, )
			normFactorHistDB = ROOT.TH1D( "", "", len(inputfiles)+1, 0, len(inputfiles)+1)
			normFactorHistZSS = ROOT.TH1D( "", "", len(inputfiles)+1, 0, len(inputfiles)+1)

			# set bin names
			for abin in xrange(len(allNPs)):
				"""if ( 'minos_eeos' in inputstub and uncRes[0][abin] in ['Scale_MUR_MUF_PDF261000','JET_JER_SINGLE_NP','JET_SR1_JET_GroupedNP_2','JET_SR1_JET_GroupedNP_3','EG_SCALE_ALL','EG_RESOLUTION_ALL_OS'] ) or ( 'minos_mmos' in inputstub and uncRes[0][abin] in ['PDFVary_PDF','MUON_ID_OS','JET_SR1_JET_EtaIntercalibration_NonClosure','JET_SR1_JET_GroupedNP_1','JET_SR1_JET_GroupedNP_2','JET_SR1_JET_GroupedNP_3','EG_SCALE_ALL'] ): #HACk
					constraintHist.GetYaxis().SetBinLabel( abin+2, '#color[2]{'+uncRes[0][abin]+'}' )				
			        else:"""
				constraintHist.GetYaxis().SetBinLabel( abin+2, allNPs[abin] )
				pullHist.GetYaxis().SetBinLabel( abin+2, allNPs[abin] )
				#print abin+1, uncRes[0][abin]
			constraintHist.GetYaxis().SetBinLabel( 1, "Total >1 per MP" )
			constraintHist.GetYaxis().LabelsOption('v')
			constraintHist.GetYaxis().SetLabelFont(43)
			constraintHist.GetYaxis().SetLabelSize(15)
			pullHist.GetYaxis().SetBinLabel( 1, "Total >|0.5| per MP" )
			pullHist.GetYaxis().LabelsOption('v')
			pullHist.GetYaxis().SetLabelFont(43)
			pullHist.GetYaxis().SetLabelSize(15)

			for abin in xrange(len(inputfiles)):
				constraintHist.GetXaxis().SetBinLabel( abin+2, inputfiles[abin].split(inputstub)[-1].split('_log.txt')[0][1:] )
				pullHist.GetXaxis().SetBinLabel( abin+2, inputfiles[abin].split(inputstub)[-1].split('_log.txt')[0][1:] )
				#print abin+1, inputfiles[abin].split(inputstub)[-1].split('_log.txt')[0][1:]
			constraintHist.GetXaxis().SetBinLabel( 1, "Total >1 per NP" )
			constraintHist.GetXaxis().SetLabelFont(43)
			constraintHist.GetXaxis().SetLabelSize(15)
			pullHist.GetXaxis().SetBinLabel( 1, "Total >|0.5| per NP" )
			pullHist.GetXaxis().SetLabelFont(43)
			pullHist.GetXaxis().SetLabelSize(15)

			constraintHist.GetZaxis().SetLabelFont(43)
			constraintHist.GetZaxis().SetLabelSize(25)
			pullHist.GetZaxis().SetLabelFont(43)
			pullHist.GetZaxis().SetLabelSize(25)

			constraintHist.GetZaxis().SetTitleFont(43)
			constraintHist.GetZaxis().SetTitleSize(30)
			#constraintHist.GetZaxis().SetTitleOffset(1)
			constraintHist.GetZaxis().SetTitle('Fullsize of NP error')
			pullHist.GetZaxis().SetTitleFont(43)
			pullHist.GetZaxis().SetTitleSize(30)
			#pullHist.GetZaxis().SetTitleOffset(1)
			pullHist.GetZaxis().SetTitle('|NP central value|')

			normFactorHistDB.GetYaxis().SetTitle('NormFactors')
			normFactorHistDB.GetYaxis().SetLabelFont(43)
			normFactorHistDB.GetYaxis().SetLabelSize(15)

			normFactorHistZSS.GetYaxis().SetTitle('NormFactors')
			normFactorHistZSS.GetYaxis().SetLabelFont(43)
			normFactorHistZSS.GetYaxis().SetLabelSize(15)
			
			for abin in xrange(len(inputfiles)):
				normFactorHistDB.GetXaxis().SetBinLabel( abin+2, inputfiles[abin].split(inputstub)[-1].split('_log.txt')[0][1:] )
				normFactorHistZSS.GetXaxis().SetBinLabel( abin+2, inputfiles[abin].split(inputstub)[-1].split('_log.txt')[0][1:] )

			normFactorHistDB.GetXaxis().SetLabelFont(43)
			normFactorHistDB.GetXaxis().SetLabelSize(15)				
			normFactorHistZSS.GetXaxis().SetLabelFont(43)
			normFactorHistZSS.GetXaxis().SetLabelSize(15)				

			# keep track of the number of large NP errors 
			largeerrsperNP = [0 for x in range(len(allNPs))]
			largeerrsperMP = [0 for x in range(len(inputfiles))]

			# keep track of the number of large pulls 
			largepullsperNP = [0 for x in range(len(allNPs))]
			largepullsperMP = [0 for x in range(len(inputfiles))]

			text = ROOT.TLatex()
			text.SetTextAlign(ROOT.kHAlignCenter+ROOT.kVAlignCenter)
			text.SetTextFont(43)
			text.SetTextSize(12)

			createHist = False


		if len(uncRes[1])==0:
			print 'Set inputfile=',inputfile,' failed - no errors found. Moving to next log.'
			continue

		# fill this row of the 2D constraint hist with the fullsize of the NP error
		# fill this row of the 2D pull hist with the size of the pull (difference in central value from 0)
		for abin in xrange(len(normFactorDB[0])):
			normFactorHistDB.SetBinContent(filen+2,normFactorDB[0][0])
			normFactorHistDB.SetBinError(filen+2,normFactorDB[1][1])
			normFactorHistZSS.SetBinContent(filen+2,normFactorZSS[0][0])
			normFactorHistZSS.SetBinError(filen+2,normFactorZSS[1][1])

		for abin in xrange(len(uncRes[1])):	
			# TODO this might not work with minos errors
			errsize = uncRes[2][abin]+uncRes[3][abin]
			#uncRes[1][abin] is the NP central value
			print uncRes[0][abin],uncRes[1][abin],errsize
			
			# keep track of large NP errors
			if errsize > 2:
				largeerrsperMP[filen] += 1
				largeerrsperNP[allNPs.index(uncRes[0][abin])] += 1
			constraintHist.Fill( filen+1, allNPs.index(uncRes[0][abin])+1, errsize )

			# keep track of large pulls
			if fabs(uncRes[1][abin]) > 0.5:
				largepullsperMP[filen] += 1
				largepullsperNP[allNPs.index(uncRes[0][abin])] += 1
			pullHist.Fill( filen+1, allNPs.index(uncRes[0][abin])+1, fabs(uncRes[1][abin]) )

		print ""

	#draw the norm canvas	
	e = ROOT.TCanvas('NormFactors'+outputfile,'',2000,1200)	
	e.SetBottomMargin(0.2)
	e.SetTopMargin(0.012)
	e.SetLeftMargin(0.2)
	normFactorHistDB.SetMarkerColor(4)
	normFactorHistDB.SetMarkerStyle(20)
	normFactorHistDB.SetLineColor(4)
	normFactorHistDB.Draw('p')
	normFactorHistZSS.SetMarkerStyle(20)
	normFactorHistZSS.SetMarkerColor(2)
	normFactorHistZSS.SetLineColor(2)
	normFactorHistZSS.Draw('p,same')

	# draw the constraint plot
	c = ROOT.TCanvas('pullcheck_'+outputfile,'',2000,1200)
	c.SetBottomMargin(0.2)
	c.SetTopMargin(0.012)
	c.SetLeftMargin(0.2)
	c.SetGrid()
	ROOT.gStyle.SetGridStyle(0)
	ROOT.gStyle.SetPalette(ROOT.kRainBow)

	#constraintHist.SetMaximum(ceil(constraintHist.GetMaximum()))
	constraintHist.SetMaximum(4)
	constraintHist.SetContour(4*int(constraintHist.GetMaximum()))
	#constraintHist.SetContour(999)
	constraintHist.Draw( 'COLZ' )
	
	# add the large NP errors per NP line
	for abin in xrange(len(allNPs)):		
		#constraintHist.Fill( 0, abin+1, largeerrsperNP[abin] )
		text.DrawLatex( 0.5, abin+1.5, str(largeerrsperNP[abin]) )
		print allNPs[abin], largeerrsperNP[abin]

	# add the large NP errors per MP line
	for filen in xrange(len(inputfiles)):		
		#constraintHist.Fill( filen+1, 0, largeerrsperMP[filen] )
		text.DrawLatex( filen+1.5, 0.5, str(largeerrsperMP[filen]) )

        #outdir = 'pulls/'+inputstub.split(outputfile)[0].split('data/')[-1] 
        outdir= "checks/"
	if not os.path.exists(outdir):
   		os.makedirs(outdir)
	print ""
	c.SaveAs(outdir+'constraintCheck_'+channel+'.pdf')
	c.SaveAs(outdir+'constraintCheck_'+channel+'.root')
	e.SaveAs(outdir+'NormFactors_'+channel+'.pdf')
	e.SaveAs(outdir+'NormFactors_'+channel+'.root')
	print ""

	#pullHist.SetMaximum(ceil(constraintHist.GetMaximum()))
	#pullHist.SetMaximum(4)
	#pullHist.SetContour(2*int(constraintHist.GetMaximum()))
	#pullHist.SetContour(999)
	pullHist.Draw( 'COLZ' )

	# add the large pulls per NP line
	for abin in xrange(len(uncRes[1])):		
		#constraintHist.Fill( 0, abin+1, largepullsperNP[abin] )
		text.DrawLatex( 0.5, abin+1.5, str(largepullsperNP[abin]) )
		print uncRes[0][abin], largepullsperNP[abin]

	# add the large pulls per MP line
	for filen in xrange(len(inputfiles)):		
		#constraintHist.Fill( filen+1, 0, largepullsperMP[filen] )
		text.DrawLatex( filen+1.5, 0.5, str(largepullsperMP[filen]) )

	print ""
	c.SaveAs(outdir+'pullCheck_'+channel+'.pdf')
	c.SaveAs(outdir+'pullCheck_'+channel+'.root')

if __name__ == "__main__":
	main(sys.argv[1:])

