
\begin{table}
\begin{center}
\setlength{\tabcolsep}{0.0pc}
\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lccc}
\noalign{\smallskip}\hline\noalign{\smallskip}
{\textbf{Uncertainty of channel}}                                    & SSZCRee            & SSZVRee            & SSZSRee            \\
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
Total background expectation             &  $305.40$        &  $30.99$        &  $12.10$       \\
%% \\
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
Total background systematic               & $\pm 17.48\ [5.72\%] $        & $\pm 5.49\ [17.71\%] $        & $\pm 2.42\ [20.02\%] $             \\
\noalign{\smallskip}\hline\noalign{\smallskip}
\noalign{\smallskip}\hline\noalign{\smallskip}
%%
total stat. error         & $\pm 18.01\ [5.9\%] $          & $\pm 4.19\ [13.5\%] $          & $\pm 2.13\ [17.6\%] $       \\
%%
QCD\_SCALE\_ENVELOPE         & $\pm 48.76\ [16.0\%] $          & $\pm 3.20\ [10.3\%] $          & $\pm 1.57\ [13.0\%] $       \\
%%
mu\_ZSS         & $\pm 57.70\ [18.9\%] $          & $\pm 2.51\ [8.1\%] $          & $\pm 1.35\ [11.1\%] $       \\
%%
FF         & $\pm 18.41\ [6.0\%] $          & $\pm 2.43\ [7.9\%] $          & $\pm 1.05\ [8.7\%] $       \\
%%
mu\_DB         & $\pm 22.48\ [7.4\%] $          & $\pm 2.69\ [8.7\%] $          & $\pm 1.02\ [8.4\%] $       \\
%%
CF         & $\pm 11.82\ [3.9\%] $          & $\pm 1.53\ [4.9\%] $          & $\pm 0.52\ [4.3\%] $       \\
%%
JET\_JER\_NP2         & $\pm 17.73\ [5.8\%] $          & $\pm 1.95\ [6.3\%] $          & $\pm 0.38\ [3.1\%] $       \\
%%
JET\_JER\_NP0         & $\pm 19.55\ [6.4\%] $          & $\pm 2.61\ [8.4\%] $          & $\pm 0.33\ [2.7\%] $       \\
%%
EG\_SCALE\_ALLCORR         & $\pm 2.49\ [0.82\%] $          & $\pm 0.90\ [2.9\%] $          & $\pm 0.25\ [2.1\%] $       \\
%%
JET\_Pileup\_RhoTopology         & $\pm 17.41\ [5.7\%] $          & $\pm 1.17\ [3.8\%] $          & $\pm 0.24\ [2.0\%] $       \\
%%
JET\_Pileup\_OffsetNPV         & $\pm 5.40\ [1.8\%] $          & $\pm 0.37\ [1.2\%] $          & $\pm 0.21\ [1.8\%] $       \\
%%
ISO         & $\pm 1.24\ [0.41\%] $          & $\pm 0.30\ [0.98\%] $          & $\pm 0.20\ [1.6\%] $       \\
%%
JET\_JER\_NP4         & $\pm 13.26\ [4.3\%] $          & $\pm 1.94\ [6.3\%] $          & $\pm 0.19\ [1.6\%] $       \\
%%
JET\_JER\_NP1         & $\pm 14.32\ [4.7\%] $          & $\pm 0.65\ [2.1\%] $          & $\pm 0.19\ [1.6\%] $       \\
%%
JET\_JER\_NP3         & $\pm 11.88\ [3.9\%] $          & $\pm 0.92\ [3.0\%] $          & $\pm 0.15\ [1.3\%] $       \\
%%
ID         & $\pm 3.19\ [1.0\%] $          & $\pm 0.36\ [1.2\%] $          & $\pm 0.15\ [1.2\%] $       \\
%%
PDF\_COICE\_ENVELOPE         & $\pm 3.88\ [1.3\%] $          & $\pm 0.37\ [1.2\%] $          & $\pm 0.14\ [1.2\%] $       \\
%%
JET\_Flavor\_Composition         & $\pm 24.68\ [8.1\%] $          & $\pm 1.41\ [4.5\%] $          & $\pm 0.14\ [1.2\%] $       \\
%%
JET\_JER\_NP6         & $\pm 11.75\ [3.8\%] $          & $\pm 0.70\ [2.3\%] $          & $\pm 0.13\ [1.1\%] $       \\
%%
JET\_JER\_NP8         & $\pm 10.85\ [3.6\%] $          & $\pm 0.49\ [1.6\%] $          & $\pm 0.13\ [1.1\%] $       \\
%%
B\_SYS         & $\pm 3.22\ [1.1\%] $          & $\pm 0.58\ [1.9\%] $          & $\pm 0.13\ [1.1\%] $       \\
%%
Lumi         & $\pm 3.92\ [1.3\%] $          & $\pm 0.35\ [1.1\%] $          & $\pm 0.13\ [1.0\%] $       \\
%%
JET\_JER\_NP7         & $\pm 18.21\ [6.0\%] $          & $\pm 0.35\ [1.1\%] $          & $\pm 0.12\ [1.00\%] $       \\
%%
JET\_JER\_NP5         & $\pm 10.44\ [3.4\%] $          & $\pm 0.38\ [1.2\%] $          & $\pm 0.12\ [0.97\%] $       \\
%%
L\_SYS         & $\pm 2.10\ [0.69\%] $          & $\pm 0.24\ [0.79\%] $          & $\pm 0.11\ [0.89\%] $       \\
%%
ALPHA\_SYS         & $\pm 2.62\ [0.86\%] $          & $\pm 0.22\ [0.70\%] $          & $\pm 0.09\ [0.78\%] $       \\
%%
tt\_THEORY         & $\pm 2.32\ [0.76\%] $          & $\pm 0.47\ [1.5\%] $          & $\pm 0.09\ [0.74\%] $       \\
%%
JET\_BJES\_Response         & $\pm 0.08\ [0.03\%] $          & $\pm 0.03\ [0.08\%] $          & $\pm 0.07\ [0.56\%] $       \\
%%
JET\_Flavor\_Response         & $\pm 2.19\ [0.72\%] $          & $\pm 0.37\ [1.2\%] $          & $\pm 0.04\ [0.36\%] $       \\
%%
JET\_EtaIntercalibration\_Modelling         & $\pm 0.68\ [0.22\%] $          & $\pm 0.27\ [0.88\%] $          & $\pm 0.04\ [0.33\%] $       \\
%%
JET\_EffectiveNP\_4         & $\pm 0.10\ [0.03\%] $          & $\pm 0.06\ [0.19\%] $          & $\pm 0.04\ [0.29\%] $       \\
%%
C\_SYS         & $\pm 0.56\ [0.18\%] $          & $\pm 0.07\ [0.23\%] $          & $\pm 0.03\ [0.27\%] $       \\
%%
JET\_EffectiveNP\_3         & $\pm 0.16\ [0.05\%] $          & $\pm 0.02\ [0.08\%] $          & $\pm 0.03\ [0.26\%] $       \\
%%
JET\_EtaIntercalibration\_TotalStat         & $\pm 0.06\ [0.02\%] $          & $\pm 0.06\ [0.20\%] $          & $\pm 0.03\ [0.26\%] $       \\
%%
JET\_EffectiveNP\_1         & $\pm 0.11\ [0.04\%] $          & $\pm 0.53\ [1.7\%] $          & $\pm 0.03\ [0.24\%] $       \\
%%
JET\_EffectiveNP\_2         & $\pm 0.20\ [0.07\%] $          & $\pm 0.00\ [0.01\%] $          & $\pm 0.03\ [0.24\%] $       \\
%%
JET\_Pileup\_PtTerm         & $\pm 0.71\ [0.23\%] $          & $\pm 0.03\ [0.09\%] $          & $\pm 0.02\ [0.21\%] $       \\
%%
RECO         & $\pm 0.60\ [0.20\%] $          & $\pm 0.07\ [0.23\%] $          & $\pm 0.02\ [0.20\%] $       \\
%%
JET\_EtaIntercalibration\_NonClosure         & $\pm 0.09\ [0.03\%] $          & $\pm 0.24\ [0.78\%] $          & $\pm 0.02\ [0.20\%] $       \\
%%
TRIG         & $\pm 0.64\ [0.21\%] $          & $\pm 0.06\ [0.20\%] $          & $\pm 0.02\ [0.17\%] $       \\
%%
EG\_RESOLUTION\_ALL         & $\pm 0.05\ [0.02\%] $          & $\pm 0.15\ [0.48\%] $          & $\pm 0.01\ [0.12\%] $       \\
%%
E\_SYS         & $\pm 0.08\ [0.03\%] $          & $\pm 0.03\ [0.09\%] $          & $\pm 0.01\ [0.07\%] $       \\
%%
PDF\_SYS\_ENVELOPE         & $\pm 0.24\ [0.08\%] $          & $\pm 0.01\ [0.04\%] $          & $\pm 0.01\ [0.06\%] $       \\
%%
JVT\_SYS         & $\pm 0.05\ [0.02\%] $          & $\pm 0.01\ [0.03\%] $          & $\pm 0.00\ [0.02\%] $       \\
%%
JET\_Pileup\_OffsetMu         & $\pm 0.19\ [0.06\%] $          & $\pm 0.00\ [0.01\%] $          & $\pm 0.00\ [0.02\%] $       \\
%%
EFC\_SYS         & $\pm 0.05\ [0.02\%] $          & $\pm 0.00\ [0.01\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_EffectiveNP\_7         & $\pm 0.06\ [0.02\%] $          & $\pm 0.02\ [0.07\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_EffectiveNP\_8restTerm         & $\pm 0.12\ [0.04\%] $          & $\pm 0.02\ [0.05\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_JER\_NOISE\_FORWARD         & $\pm 0.05\ [0.02\%] $          & $\pm 0.00\ [0.01\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_JER\_CROSS\_CALIB\_FORWARD         & $\pm 0.05\ [0.02\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_EffectiveNP\_5         & $\pm 0.06\ [0.02\%] $          & $\pm 0.06\ [0.18\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_EffectiveNP\_6         & $\pm 0.12\ [0.04\%] $          & $\pm 0.01\ [0.04\%] $          & $\pm 0.00\ [0.01\%] $       \\
%%
JET\_PunchThrough\_MC15         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
TRIGSTAT         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
TTVASTAT         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
JET\_SingleParticle\_HighPt         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
RECOSYS         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
ISOSYS         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
MUON\_ID         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
EG\_SCALE\_E4SCINTILLATOR         & $\pm 0.08\ [0.03\%] $          & $\pm 0.02\ [0.06\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
TTVASYS         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
MUFF         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
TRIGSYS         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
MUON\_SCALE         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
MUON\_MS         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
MUON\_RESBIAS         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
RECOSTAT         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
MUON\_RHO         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
ISOSTAT         & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $          & $\pm 0.00\ [0.00\%] $       \\
%%
\noalign{\smallskip}\hline\noalign{\smallskip}
\end{tabular*}
\end{center}
\caption[Breakdown of uncertainty on background estimates]{
Breakdown of the dominant uncertainties on background estimates. The given total statistical error is
a quadratic sum of individual statistical errors of each bin in the region. Note that the individual
uncertainties can be correlated, and do not necessarily add up quadratically to the total background uncertainty.
Uncertainties are ordered from the largest to smallest in total (last column).
\label{table:eleSys.SSZCRee_SSZVRee_SSZSRee}}
\end{table}
%