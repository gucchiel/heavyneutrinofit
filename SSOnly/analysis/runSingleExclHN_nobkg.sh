mkdir -p logs
mkdir -p limits
cp data/mergedSS_fixedFakes.root data/backupCacheFile_SameSignHN-WR$1_NR$2_Sig_$3-$5.root
#HistFitter.py -w -f -F $5 -D before,after,corrMatrix analysis/SameSignHN/SameSignHN_excl_combined_symm_SigContamination.py\
#HistFitter.py -w -p -f -F $5 -l analysis/SameSignHN/SameSignHN_excl_combined_nobkg.py
HistFitter.py -w -f -F $5 -D before,after analysis/SameSignHN/SameSignHN_excl_combined_nobkg.py\
 -c "analysisSuffix='WR$1_NR$2_Sig_$3-$5';channel='$3';neutrino='$4';ALLCR=False;ONLYSS=True;VALIDATION=$6;fittedBkgSys='$7'"\
 2>&1 | tee logs/WR$1_NR$2_Sig_$3-HN$5.log

python scripts/pull_maker.py -i logs/WR$1_NR$2_Sig_$3-HN$5.log

# -m ALL
# source analysis/SameSignHN/runSingleExclHN_combined.sh 1000 800 mm Majorana excl False histoSys
