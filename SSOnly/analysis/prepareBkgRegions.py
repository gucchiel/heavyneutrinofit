from collections import defaultdict
from copy import deepcopy
import re
import os 

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("/afs/f9.ijs.si/home/miham/HistFitter/macros/AtlasStyle.C")
import ROOT
ROOT.SetAtlasStyle()

from optparse import OptionParser

#-----------------
# input
#-----------------
parser = OptionParser()
parser.add_option('-i', '--input', dest='infile',
                  help='input directory',metavar='INFILE',default=None)

(options, args) = parser.parse_args()

typicalFakeFactors = {"ee":0.5,"mm":0.9}

def main():
  #---------------------------------------
  # Analysis name
  #---------------------------------------

  infile = ROOT.TFile(options.infile,"update")
  keys = deepcopy(infile.GetListOfKeys())

  print infile

  for key in keys:
    histoName = key.GetName()
    if "SSZCRmmSSZCRee" in histoName:
      break
    if "dibosonSherpa" in histoName and "Norm" in histoName and "SSZCRmm" in histoName:
      print histoName
      temp1 = infile.Get(histoName)
      temp2 = infile.Get(histoName.replace("SSZCRmm","SSZCRee"))
      print temp1," ",temp2
      outhist = temp1.Clone(histoName.replace("SSZCRmm","SSZCRmmSSZCRee"))
      outhist.Add(temp2)
      outhist.Write()
    # if "dibosonSherpa" in histoName and "Norm" in histoName and "SSZ60CRmm" in histoName:
    #   print histoName
    #   temp1 = infile.Get(histoName)
    #   temp2 = infile.Get(histoName.replace("SSZ60CRmm","SSZ60CRee"))
    #   print temp1," ",temp2
    #   outhist = temp1.Clone(histoName.replace("SSZ60CRmm","SSZ60CRmmSSZ60CRee"))
    #   outhist.Add(temp2)
    #   outhist.Write()
    # if "dibosonSherpa" in histoName and "Norm" in histoName and "SSZ70CRmm" in histoName:
    #   print histoName
    #   temp1 = infile.Get(histoName)
    #   temp2 = infile.Get(histoName.replace("SSZ70CRmm","SSZ70CRee"))
    #   print temp1," ",temp2
    #   outhist = temp1.Clone(histoName.replace("SSZ70CRmm","SSZ70CRmmSSZ70CRee"))
    #   outhist.Add(temp2)
    #   outhist.Write()
    # if "dibosonSherpa" in histoName and "Norm" in histoName and "SSZ80CRmm" in histoName:
    #   print histoName
    #   temp1 = infile.Get(histoName)
    #   temp2 = infile.Get(histoName.replace("SSZ80CRmm","SSZ80CRee"))
    #   print temp1," ",temp2
    #   outhist = temp1.Clone(histoName.replace("SSZ80CRmm","SSZ80CRmmSSZ80CRee"))
    #   outhist.Add(temp2)
    #   outhist.Write()
    # if "dibosonSherpa" in histoName and "Norm" in histoName and "SSZ90CRmm" in histoName:
    #   print histoName
    #   temp1 = infile.Get(histoName)
    #   temp2 = infile.Get(histoName.replace("SSZ90CRmm","SSZ90CRee"))
    #   print temp1," ",temp2
    #   outhist = temp1.Clone(histoName.replace("SSZ90CRmm","SSZ90CRmmSSZ90CRee"))
    #   outhist.Add(temp2)
    #   outhist.Write()
    # if "dibosonSherpa" in histoName and "Norm" in histoName and "SSZ100CRmm" in histoName:
    #   print histoName
    #   temp1 = infile.Get(histoName)
    #   temp2 = infile.Get(histoName.replace("SSZ100CRmm","SSZ100CRee"))
    #   print temp1," ",temp2
    #   outhist = temp1.Clone(histoName.replace("SSZ100CRmm","SSZ100CRmmSSZ100CRee"))
    #   outhist.Add(temp2)
    #   outhist.Write()
    if "dataNom" in histoName:
      print histoName
      temp1 = infile.Get(histoName)
      outhist1 = temp1.Clone(histoName.replace("dataNom","Data"))
      outhist1.Write()
    # if ("fakesNom" in histoName) and ("obs" in histoName):
    #   channel = "ee" if "ee" in histoName else "mm"
    #   print histoName, " channel: ", channel
    #   temp1 = infile.Get(histoName)
    #   outhist1 = temp1.Clone(histoName.replace("fakesNom","fakesStatErrHigh"))
    #   outhist2 = temp1.Clone(histoName.replace("fakesNom","fakesStatErrLow"))
    #   outhist1Norm = temp1.Clone(re.sub("(hfakes.*)(_obs_[a-zA-Z0-9]*)","\\1Norm",histoName.replace("fakesNom","fakesStatErrHigh")))
    #   outhist2Norm = temp1.Clone(re.sub("(hfakes.*)(_obs_[a-zA-Z0-9]*)","\\1Norm",histoName.replace("fakesNom","fakesStatErrLow")))
    #   for i in range(1,temp1.GetNbinsX()+1):
    #     if temp1.GetBinContent(i) == 0:
    #       outhist1.SetBinContent(i,1.4*2/3*typicalFakeFactors[channel]-1.4/3*typicalFakeFactors[channel]**2)
    #       outhist2.SetBinContent(i,0.)
    #     else:
    #       outhist1.SetBinContent(i,temp1.GetBinContent(i)+temp1.GetBinError(i))
    #       outhist2.SetBinContent(i,max(temp1.GetBinContent(i)-temp1.GetBinError(i),0.))
    #   outhist1Norm.Rebin(outhist1Norm.GetNbinsX())
    #   outhist2Norm.Rebin(outhist2Norm.GetNbinsX())
    #   outhist1Norm.SetBinContent(1,outhist1.GetSum())
    #   outhist2Norm.SetBinContent(1,outhist2.GetSum())
    #   outhist1.Write()
    #   outhist2.Write()
    #   outhist1Norm.Write()
    #   outhist2Norm.Write()
    if "A14NNPDF23LO_LRSM" in histoName:
      temp1 = infile.Get(histoName)
      newstring = histoName.replace("MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_","")
      temp = re.findall("WR([0-9]*)_NR([0-9]*)",newstring)[0]
      outhist1 = temp1.Clone(newstring.replace("WR%s_NR%s" % (temp[0],temp[1]), "WR%s_NR%s_Sig" % (temp[0],temp[1])))
      outhist1.Write()
      if "Nom" in histoName:
        print histoName
        print newstring.replace("WR%s_NR%s" % (temp[0],temp[1]), "WR%s_NR%s_Sig" % (temp[0],temp[1]))

  infile.Close()


def nested_dict(n, type):
    if n == 1:
        return defaultdict(type)
    else:
        return defaultdict(lambda: nested_dict(n-1, type))

if __name__=='__main__': main()