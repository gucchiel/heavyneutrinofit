mkdir -p tables
python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
-c SSZCRee,SSZVRee,SSZSRee \
-o tables/HN/ele_sys.tex -%

sed -i 's/table\.results\.bkgestimate\.uncertainties/table:eleSys/g' tables/HN/ele_sys.tex

python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
-c SSZCRmm,SSZVRmm,SSZSRmm \
-o tables/HN/mu_sys.tex -%

sed -i 's/table\.results\.bkgestimate\.uncertainties/table:muSys/g' tables/HN/mu_sys.tex

python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
-c SSZCRee,SSZVRee,SSZSRee,SSZCRmm,SSZVRmm,SSZSRmm \
-o tables/allBins/AllSysHN.tex

# python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-excl/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
# -c SR_eeos,ZVR_eeos,ZCR_eeos -b \
# -o tables/HN/ele_sys_os.tex -%

# sed -i 's/Breakdown of the dominant uncertainties on background estimates/Breakdown of the dominant uncertainties on background estimates. The given total statistical error is a quadratic sum of individual statistical errors of each bin in the region. Note that the individual uncertainties can be correlated, and do not necessarily add up quadratically to the total background uncertainty./g' tables/HN/ele_sys.tex
# sed -i 's/table\.results\.bkgestimate\.uncertainties/table:eleSys/g' tables/HN/ele_sys.tex

# python scripts/SysTable.py -w results/SameSignHN-WR$1_NR$2_Sig_$3-excl/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
# -c SR_mmos,ZVR_mmos,ZCR_mmos -b \
# -o tables/HN/mu_sys_os.tex -%

# sed -i 's/Breakdown of the dominant uncertainties on background estimates/Breakdown of the dominant uncertainties on background estimates. The given total statistical error is a quadratic sum of individual statistical errors of each bin in the region. Note that the individual uncertainties can be correlated, and do not necessarily add up quadratically to the total background uncertainty./g' tables/HN/mu_sys.tex
# sed -i 's/table\.results\.bkgestimate\.uncertainties/table:muSys/g' tables/HN/mu_sys.tex
