"""
SS Analysis
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from collections import defaultdict
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from copy import deepcopy

import re
import os 

import logger
from logger import Logger

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
ROOT.SetAtlasStyle()

def nested_dict(n, type):
    if n == 1:
        return defaultdict(type)
    else:
        return defaultdict(lambda: nested_dict(n-1, type))

#---------------------------------------
# Analysis name
#---------------------------------------

if not analysisSuffix:
    analysisSuffix = "01"

analysis_name="SameSignHN-" + str(analysisSuffix)

print analysis_name

#---------------------------------------
# Logger
#---------------------------------------

log = Logger(analysis_name)
log.setLevel(logger.INFO) #should have no effect if -L is used
log.warning("example warning from python")
log.error("example error from python")

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat=True # statistics variation of samples 

#---------------------------------------------------
# Specify the default signal point
# Others to be given via option -g via command line
#---------------------------------------------------

if not 'sigSamples' in dir():
    sigSamples=["Pythia8EvtGen_A14NNPDF23LO_DCH500"+DCHFilter]

#-------------------------------
# Config manager basics
#-------------------------------

# Setting the parameters of the hypothesis test
configMgr.doExclusion=True # True=exclusion, False=discovery
#configMgr.doHypoTest=False
#configMgr.nTOYs=5000
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=200

configMgr.writeXML = True

configMgr.analysisName = analysis_name
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

#activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False # enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
configMgr.histBackupCacheFile =  "data/backupCacheFile_" + analysis_name + ".root" # histogram templates - the data file of your previous fit, backup cache

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 36.07456 # Luminosity of input TTree after weighting
configMgr.outputLumi = 36.07456 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")
configMgr.removeEmptyBins=True

#generate using Asimov mock data
configMgr.useAsimovSet=False
configMgr.blindSR = False


# sys.exit()

#-------------------------------------
# Now we start to build the data model
#-------------------------------------


# Dictionnary of cuts defining channels/regions (for Tree->hist)
configMgr.cutsDict["SSZCRee"] = "1."
# configMgr.cutsDict["SSZSRee"] = "1."
configMgr.cutsDict["SSZCRmm"] = "1."
configMgr.cutsDict["SSZSRmm"] = "1."

# Define weights - dummy here
configMgr.weights = "1."

#--------------------
# List of systematics
#--------------------


#Caution: Care must be taken when applying Norm type systematics, as only samples that carry a normalization factor mu_... in the fit can be used. Otherwise the uncertainty on the total event count or normalization is not taken into account for this sample, which can lead to underestimation of the sample uncertainty in the signal region.

#-->normalized sys for samples with mu_XX scaling (see above)
#electron ID eff systematics
eleff = Systematic("ID","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #electron Trig eff systematics
eltrig = Systematic("TRIG","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #electron reco eff systematics
elreco = Systematic("RECO","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #electron iso eff systematics
eliso = Systematic("ISO","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #electron resolution  systematics
elreso = Systematic("EG_RESOLUTION_ALL","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #electron energy scale systematics
elescale = Systematic("EG_SCALE_ALLCORR","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #electron energy scale systematics
elscintillator = Systematic("EG_SCALE_E4SCINTILLATOR","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#chflip systematics 
elchflip = Systematic("CF","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#BEAM error sys - DYZ
elDYbeam = Systematic("BEAM","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#PDF CHOICE error sys - DYZ
elDYchoice = Systematic("CHOICE","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#PDF error sys - DYZ
elDYpdf = Systematic("PDF","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#PI error sys - DYZ
elDYpi = Systematic("PI","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#SCALE error sys - DYZ
elDYscale = Systematic("SCALE_Z","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#fake correction systematics 
elfakesys = Systematic("FF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
#model uncertainty for diboson
eldbmod   = Systematic("DBGen",  "_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
neldbmod  = Systematic("DBGen",  "_NoSys","_elEffup","_ElEffdown","tree","histoSys")
eldbmodEE = Systematic("DBGenEE","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
eldbmodEM = Systematic("DBGenEM","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
eldbmodMM = Systematic("DBGenMM","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
#model uncertainty for diboson
# elttGen = Systematic("TTGen","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #model uncertainty for diboson
# elttRad = Systematic("TTRad","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #model uncertainty for diboson
# elttCF = Systematic("TTCF","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# #model uncertainty for diboson
# elttHadron = Systematic("TTHadron","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")

#-->non-normalized sys for samples without mu_XX scaling (see above)
#electron ID eff systematics
neleff = Systematic("ID","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron Trig eff systematics
neltrig = Systematic("TRIG","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron reco eff systematics
nelreco = Systematic("RECO","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron iso eff systematics
neliso = Systematic("ISO","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron resolution  systematics
nelreso = Systematic("EG_RESOLUTION_ALL","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron energy scale systematics
nelescale = Systematic("EG_SCALE_ALLCORR","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron energy scale systematics
nelscintillator = Systematic("EG_SCALE_E4SCINTILLATOR","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
#chflip systematics 
nelchflip = Systematic("CF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")


### muon exp
mueff1  = Systematic("RECOSYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
mueff2  = Systematic("RECOSTAT","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
mutrig1 = Systematic("TRIGSYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
mutrig2 = Systematic("TRIGSTAT","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muiso1  = Systematic("ISOSYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muiso2  = Systematic("ISOSTAT","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muttva1 = Systematic("TTVASYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muttva2 = Systematic("TTVASTAT","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muTreeID = Systematic("MUON_ID","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muTreeMS = Systematic("MUON_MS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muTreeRES = Systematic("MUON_RESBIAS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muTreeRHO = Systematic("MUON_RHO","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
muTreeSCALE = Systematic("MUON_SCALE","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
### muon exp n
mufakesys = Systematic("MUFF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmueff1  = Systematic("RECOSYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmueff2  = Systematic("RECOSTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmutrig1 = Systematic("TRIGSYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmutrig2 = Systematic("TRIGSTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuiso1  = Systematic("ISOSYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuiso2  = Systematic("ISOSTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuttva1 = Systematic("TTVASYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuttva2 = Systematic("TTVASTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeID    = Systematic("MUON_ID","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeMS    = Systematic("MUON_MS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeRES   = Systematic("MUON_RESBIAS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeRHO   = Systematic("MUON_RHO","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeSCALE = Systematic("MUON_SCALE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")



### jet systematics overallNormHistoSys
B_SYS = Systematic("B_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                             
C_SYS = Systematic("C_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                             
L_SYS = Systematic("L_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                             
E_SYS = Systematic("E_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                             
EFC_SYS = Systematic("EFC_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                           
JVT_SYS = Systematic("JVT_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                           
JET_BJES_Response = Systematic("JET_BJES_Response","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_1 = Systematic("JET_EffectiveNP_1","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_2 = Systematic("JET_EffectiveNP_2","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_3 = Systematic("JET_EffectiveNP_3","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_4 = Systematic("JET_EffectiveNP_4","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_5 = Systematic("JET_EffectiveNP_5","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_6 = Systematic("JET_EffectiveNP_6","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_7 = Systematic("JET_EffectiveNP_7","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_EffectiveNP_8restTerm = Systematic("JET_EffectiveNP_8restTerm","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")         
JET_EtaIntercalibration_Modelling = Systematic("JET_EtaIntercalibration_Modelling","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys") 
JET_EtaIntercalibration_NonClosure = Systematic("JET_EtaIntercalibration_NonClosure","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
JET_EtaIntercalibration_TotalStat = Systematic("JET_EtaIntercalibration_TotalStat","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys") 
JET_Flavor_Composition = Systematic("JET_Flavor_Composition","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")            
JET_Flavor_Response = Systematic("JET_Flavor_Response","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")               
JET_Pileup_OffsetMu = Systematic("JET_Pileup_OffsetMu","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")               
JET_Pileup_OffsetNPV = Systematic("JET_Pileup_OffsetNPV","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")              
JET_Pileup_PtTerm = Systematic("JET_Pileup_PtTerm","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                 
JET_Pileup_RhoTopology = Systematic("JET_Pileup_RhoTopology","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")            
JET_PunchThrough_MC15 = Systematic("JET_PunchThrough_MC15","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")             
JET_SingleParticle_HighPt = Systematic("JET_SingleParticle_HighPt","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")         
JET_JER_CROSS_CALIB_FORWARD = Systematic("JET_JER_CROSS_CALIB_FORWARD","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")       
JET_JER_NOISE_FORWARD = Systematic("JET_JER_NOISE_FORWARD","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")             
JET_JER_NP0 = Systematic("JET_JER_NP0","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP1 = Systematic("JET_JER_NP1","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP2 = Systematic("JET_JER_NP2","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP3 = Systematic("JET_JER_NP3","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP4 = Systematic("JET_JER_NP4","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP5 = Systematic("JET_JER_NP5","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP6 = Systematic("JET_JER_NP6","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP7 = Systematic("JET_JER_NP7","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")                       
JET_JER_NP8 = Systematic("JET_JER_NP8","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
####   histoSys
nB_SYS                              = Systematic("B_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nC_SYS                              = Systematic("C_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nL_SYS                              = Systematic("L_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nE_SYS                              = Systematic("E_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nEFC_SYS                            = Systematic("EFC_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                           
nJVT_SYS                            = Systematic("JVT_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                           
nJET_BJES_Response                  = Systematic("JET_BJES_Response","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_1                  = Systematic("JET_EffectiveNP_1","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_2                  = Systematic("JET_EffectiveNP_2","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_3                  = Systematic("JET_EffectiveNP_3","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_4                  = Systematic("JET_EffectiveNP_4","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_5                  = Systematic("JET_EffectiveNP_5","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_6                  = Systematic("JET_EffectiveNP_6","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_7                  = Systematic("JET_EffectiveNP_7","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_8restTerm          = Systematic("JET_EffectiveNP_8restTerm","_NoSys","_elEffup","_ElEffdown","tree","histoSys")         
nJET_EtaIntercalibration_Modelling  = Systematic("JET_EtaIntercalibration_Modelling","_NoSys","_elEffup","_ElEffdown","tree","histoSys") 
nJET_EtaIntercalibration_NonClosure = Systematic("JET_EtaIntercalibration_NonClosure","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nJET_EtaIntercalibration_TotalStat  = Systematic("JET_EtaIntercalibration_TotalStat","_NoSys","_elEffup","_ElEffdown","tree","histoSys") 
nJET_Flavor_Composition             = Systematic("JET_Flavor_Composition","_NoSys","_elEffup","_ElEffdown","tree","histoSys")            
nJET_Flavor_Response                = Systematic("JET_Flavor_Response","_NoSys","_elEffup","_ElEffdown","tree","histoSys")               
nJET_Pileup_OffsetMu                = Systematic("JET_Pileup_OffsetMu","_NoSys","_elEffup","_ElEffdown","tree","histoSys")               
nJET_Pileup_OffsetNPV               = Systematic("JET_Pileup_OffsetNPV","_NoSys","_elEffup","_ElEffdown","tree","histoSys")              
nJET_Pileup_PtTerm                  = Systematic("JET_Pileup_PtTerm","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_Pileup_RhoTopology             = Systematic("JET_Pileup_RhoTopology","_NoSys","_elEffup","_ElEffdown","tree","histoSys")            
nJET_PunchThrough_MC15              = Systematic("JET_PunchThrough_MC15","_NoSys","_elEffup","_ElEffdown","tree","histoSys")             
nJET_SingleParticle_HighPt          = Systematic("JET_SingleParticle_HighPt","_NoSys","_elEffup","_ElEffdown","tree","histoSys")         
nJET_JER_CROSS_CALIB_FORWARD        = Systematic("JET_JER_CROSS_CALIB_FORWARD","_NoSys","_elEffup","_ElEffdown","tree","histoSys")       
nJET_JER_NOISE_FORWARD              = Systematic("JET_JER_NOISE_FORWARD","_NoSys","_elEffup","_ElEffdown","tree","histoSys")             
nJET_JER_NP0                        = Systematic("JET_JER_NP0","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP1                        = Systematic("JET_JER_NP1","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP2                        = Systematic("JET_JER_NP2","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP3                        = Systematic("JET_JER_NP3","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP4                        = Systematic("JET_JER_NP4","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP5                        = Systematic("JET_JER_NP5","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP6                        = Systematic("JET_JER_NP6","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP7                        = Systematic("JET_JER_NP7","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP8                        = Systematic("JET_JER_NP8","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                      

### theory systematics overallNormHistoSys
PDFCHOICE_SYS1                          = Systematic("PDFCHOICE_SYS1","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
PDFCHOICE_SYS2                          = Systematic("PDFCHOICE_SYS2","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
MUF_SYS                                 = Systematic("MUF_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
MUR_SYS                                 = Systematic("MUR_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
ALPHA_SYS                               = Systematic("ALPHA_SYS","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
PDF_SYS_ENVELOPE                        = Systematic("PDF_SYS_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
QCD_SCALE_ENVELOPE                      = Systematic("QCD_SCALE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
PDF_COICE_ENVELOPE                      = Systematic("PDF_COICE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","overallNormHistoSys")
# histoSys
nPDFCHOICE_SYS1                         = Systematic("PDFCHOICE_SYS1","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDFCHOICE_SYS2                         = Systematic("PDFCHOICE_SYS2","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nMUF_SYS                                = Systematic("MUF_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nMUR_SYS                                = Systematic("MUR_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDF_SYS_ENVELOPE                       = Systematic("PDF_SYS_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDF_SYS_ENVELOPE                       = Systematic("PDF_SYS_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nQCD_SCALE_ENVELOPE                     = Systematic("QCD_SCALE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDF_COICE_ENVELOPE                     = Systematic("PDF_COICE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")


#nominal name of the histograms with systematic variation
configMgr.nomName = "_NoSys"

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

#backgrounds
topSample = Sample("top_physics",kGreen-9)
topSample.legName = "t#bar{t}"
topSample.setNormByTheory() #scales with lumi
topSample.setStatConfig(useStat)
# topSample.setNormFactor("mu_top",1.,0.,5000.)
# topSample.setNormRegions([("OSTTCR","mee")])
topSample.xsecUp = 1.2
topSample.xsecDown = 0.8
topSample.addSystematic(nelchflip)
topSample.addSystematic(neleff)
topSample.addSystematic(neltrig)
topSample.addSystematic(nelreco)
topSample.addSystematic(neliso)
topSample.addSystematic(nelreso)
topSample.addSystematic(nelescale)
topSample.addSystematic(nelscintillator)
topSample.addSystematic(nB_SYS                             )
topSample.addSystematic(nC_SYS                             )
topSample.addSystematic(nL_SYS                             )
topSample.addSystematic(nE_SYS                             )
topSample.addSystematic(nEFC_SYS                           )
topSample.addSystematic(nJVT_SYS                           )
topSample.addSystematic(nJET_BJES_Response                 )
topSample.addSystematic(nJET_EffectiveNP_1                 )
topSample.addSystematic(nJET_EffectiveNP_2                 )
topSample.addSystematic(nJET_EffectiveNP_3                 )
topSample.addSystematic(nJET_EffectiveNP_4                 )
topSample.addSystematic(nJET_EffectiveNP_5                 )
topSample.addSystematic(nJET_EffectiveNP_6                 )
topSample.addSystematic(nJET_EffectiveNP_7                 )
topSample.addSystematic(nJET_EffectiveNP_8restTerm         )
topSample.addSystematic(nJET_EtaIntercalibration_Modelling )
topSample.addSystematic(nJET_EtaIntercalibration_NonClosure)
topSample.addSystematic(nJET_EtaIntercalibration_TotalStat )
topSample.addSystematic(nJET_Flavor_Composition            )
topSample.addSystematic(nJET_Flavor_Response               )
topSample.addSystematic(nJET_Pileup_OffsetMu               )
topSample.addSystematic(nJET_Pileup_OffsetNPV              )
topSample.addSystematic(nJET_Pileup_PtTerm                 )
topSample.addSystematic(nJET_Pileup_RhoTopology            )
topSample.addSystematic(nJET_PunchThrough_MC15             )
topSample.addSystematic(nJET_SingleParticle_HighPt         )
topSample.addSystematic(nJET_JER_CROSS_CALIB_FORWARD       )
topSample.addSystematic(nJET_JER_NOISE_FORWARD             )
topSample.addSystematic(nJET_JER_NP0                       )
topSample.addSystematic(nJET_JER_NP1                       )
topSample.addSystematic(nJET_JER_NP2                       )
topSample.addSystematic(nJET_JER_NP3                       )
topSample.addSystematic(nJET_JER_NP4                       )
topSample.addSystematic(nJET_JER_NP5                       )
topSample.addSystematic(nJET_JER_NP6                       )
topSample.addSystematic(nJET_JER_NP7                       )
topSample.addSystematic(nJET_JER_NP8                       )
#topSample.addSystematic(elttGen)
#topSample.addSystematic(elttRad)
#topSample.addSystematic(elttHadron)
#topSample.addSystematic(eltopmod)
topSample.addSystematic(nmueff1  )
topSample.addSystematic(nmueff2  )
topSample.addSystematic(nmutrig1 )
topSample.addSystematic(nmutrig2 )
topSample.addSystematic(nmuiso1  )
topSample.addSystematic(nmuiso2  )
topSample.addSystematic(nmuttva1 )
topSample.addSystematic(nmuttva2 )
topSample.addSystematic(nmuTreeID    )
topSample.addSystematic(nmuTreeMS    )
topSample.addSystematic(nmuTreeRES   )
topSample.addSystematic(nmuTreeRHO   )
topSample.addSystematic(nmuTreeSCALE )

zSample = Sample("SherpaDY221",kAzure+1)
zSample.legName = "Drell-Yan Z/#gamma"
zSample.setNormByTheory()
zSample.setStatConfig(useStat)
zSample.setNormFactor("mu_Z",1.,0.,5000.)
zSample.setNormRegions([("SSZCRee","Mjj")])
zSample.addSystematic(elchflip)
zSample.addSystematic(eleff)
zSample.addSystematic(eltrig)
zSample.addSystematic(elreco)
zSample.addSystematic(eliso)
zSample.addSystematic(elreso)
zSample.addSystematic(elescale)
zSample.addSystematic(elscintillator)
zSample.addSystematic(B_SYS                             )
zSample.addSystematic(C_SYS                             )
zSample.addSystematic(L_SYS                             )
zSample.addSystematic(E_SYS                             )
zSample.addSystematic(EFC_SYS                           )
zSample.addSystematic(JVT_SYS                           )
zSample.addSystematic(JET_BJES_Response                 )
zSample.addSystematic(JET_EffectiveNP_1                 )
zSample.addSystematic(JET_EffectiveNP_2                 )
zSample.addSystematic(JET_EffectiveNP_3                 )
zSample.addSystematic(JET_EffectiveNP_4                 )
zSample.addSystematic(JET_EffectiveNP_5                 )
zSample.addSystematic(JET_EffectiveNP_6                 )
zSample.addSystematic(JET_EffectiveNP_7                 )
zSample.addSystematic(JET_EffectiveNP_8restTerm         )
zSample.addSystematic(JET_EtaIntercalibration_Modelling )
zSample.addSystematic(JET_EtaIntercalibration_NonClosure)
zSample.addSystematic(JET_EtaIntercalibration_TotalStat )
zSample.addSystematic(JET_Flavor_Composition            )
zSample.addSystematic(JET_Flavor_Response               )
zSample.addSystematic(JET_Pileup_OffsetMu               )
zSample.addSystematic(JET_Pileup_OffsetNPV              )
zSample.addSystematic(JET_Pileup_PtTerm                 )
zSample.addSystematic(JET_Pileup_RhoTopology            )
zSample.addSystematic(JET_PunchThrough_MC15             )
zSample.addSystematic(JET_SingleParticle_HighPt         )
zSample.addSystematic(JET_JER_CROSS_CALIB_FORWARD       )
zSample.addSystematic(JET_JER_NOISE_FORWARD             )
zSample.addSystematic(JET_JER_NP0                       )
zSample.addSystematic(JET_JER_NP1                       )
zSample.addSystematic(JET_JER_NP2                       )
zSample.addSystematic(JET_JER_NP3                       )
zSample.addSystematic(JET_JER_NP4                       )
zSample.addSystematic(JET_JER_NP5                       )
zSample.addSystematic(JET_JER_NP6                       )
zSample.addSystematic(JET_JER_NP7                       )
zSample.addSystematic(JET_JER_NP8                       )
zSample.addSystematic(PDFCHOICE_SYS1                    )
zSample.addSystematic(PDFCHOICE_SYS2                    )
zSample.addSystematic(MUF_SYS                           )
zSample.addSystematic(MUR_SYS                           )
zSample.addSystematic(ALPHA_SYS                         )
zSample.addSystematic(PDF_SYS_ENVELOPE                  )
zSample.addSystematic(QCD_SCALE_ENVELOPE                )
zSample.addSystematic(PDF_COICE_ENVELOPE                )
# zSample.addSystematic(mueff1  )
# zSample.addSystematic(mueff2  )
# zSample.addSystematic(mutrig1 )
# zSample.addSystematic(mutrig2 )
# zSample.addSystematic(muiso1  )
# zSample.addSystematic(muiso2  )
# zSample.addSystematic(muttva1 )
# zSample.addSystematic(muttva2 )
# zSample.addSystematic(muTreeID    )
# zSample.addSystematic(muTreeMS    )
# zSample.addSystematic(muTreeRES   )
# zSample.addSystematic(muTreeRHO   )
# zSample.addSystematic(muTreeSCALE )


dbSample = Sample("dibosonSherpa",kYellow-3)
dbSample.legName = "DB ee"
dbSample.setNormByTheory()
dbSample.setStatConfig(useStat)
dbSample.setNormFactor("mu_DB",1.,0.,5000.)
dbSample.setNormRegions([("SSZCRmm","Mjj"),("SSZCRee","Mjj")])
dbSample.addSystematic(elchflip)
dbSample.addSystematic(eleff)
dbSample.addSystematic(eltrig)
dbSample.addSystematic(elreco)
dbSample.addSystematic(eliso)
dbSample.addSystematic(elreso)
dbSample.addSystematic(elescale)
dbSample.addSystematic(elscintillator)
dbSample.addSystematic(B_SYS                             )
dbSample.addSystematic(C_SYS                             )
dbSample.addSystematic(L_SYS                             )
dbSample.addSystematic(E_SYS                             )
dbSample.addSystematic(EFC_SYS                           )
dbSample.addSystematic(JVT_SYS                           )
dbSample.addSystematic(JET_BJES_Response                 )
dbSample.addSystematic(JET_EffectiveNP_1                 )
dbSample.addSystematic(JET_EffectiveNP_2                 )
dbSample.addSystematic(JET_EffectiveNP_3                 )
dbSample.addSystematic(JET_EffectiveNP_4                 )
dbSample.addSystematic(JET_EffectiveNP_5                 )
dbSample.addSystematic(JET_EffectiveNP_6                 )
dbSample.addSystematic(JET_EffectiveNP_7                 )
dbSample.addSystematic(JET_EffectiveNP_8restTerm         )
dbSample.addSystematic(JET_EtaIntercalibration_Modelling )
dbSample.addSystematic(JET_EtaIntercalibration_NonClosure)
dbSample.addSystematic(JET_EtaIntercalibration_TotalStat )
dbSample.addSystematic(JET_Flavor_Composition            )
dbSample.addSystematic(JET_Flavor_Response               )
dbSample.addSystematic(JET_Pileup_OffsetMu               )
dbSample.addSystematic(JET_Pileup_OffsetNPV              )
dbSample.addSystematic(JET_Pileup_PtTerm                 )
dbSample.addSystematic(JET_Pileup_RhoTopology            )
dbSample.addSystematic(JET_PunchThrough_MC15             )
dbSample.addSystematic(JET_SingleParticle_HighPt         )
dbSample.addSystematic(JET_JER_CROSS_CALIB_FORWARD       )
dbSample.addSystematic(JET_JER_NOISE_FORWARD             )
dbSample.addSystematic(JET_JER_NP0                       )
dbSample.addSystematic(JET_JER_NP1                       )
dbSample.addSystematic(JET_JER_NP2                       )
dbSample.addSystematic(JET_JER_NP3                       )
dbSample.addSystematic(JET_JER_NP4                       )
dbSample.addSystematic(JET_JER_NP5                       )
dbSample.addSystematic(JET_JER_NP6                       )
dbSample.addSystematic(JET_JER_NP7                       )
dbSample.addSystematic(JET_JER_NP8                       )
dbSample.addSystematic(mueff1  )
dbSample.addSystematic(mueff2  )
dbSample.addSystematic(mutrig1 )
dbSample.addSystematic(mutrig2 )
dbSample.addSystematic(muiso1  )
dbSample.addSystematic(muiso2  )
dbSample.addSystematic(muttva1 )
dbSample.addSystematic(muttva2 )
dbSample.addSystematic(muTreeID    )
dbSample.addSystematic(muTreeMS    )
dbSample.addSystematic(muTreeRES   )
dbSample.addSystematic(muTreeRHO   )
dbSample.addSystematic(muTreeSCALE )
dbSample.addSystematic(PDFCHOICE_SYS1                    )
dbSample.addSystematic(PDFCHOICE_SYS2                    )
dbSample.addSystematic(MUF_SYS                           )
dbSample.addSystematic(MUR_SYS                           )
dbSample.addSystematic(ALPHA_SYS                         )
dbSample.addSystematic(PDF_SYS_ENVELOPE                  )
dbSample.addSystematic(QCD_SCALE_ENVELOPE                )
dbSample.addSystematic(PDF_COICE_ENVELOPE                )

# dbSample.addSystematic(PDF_SYS_ENVELOPE                  )

# rareSample = Sample("rare",kRed-3)
# rareSample.legName = "Rare"
# rareSample.setNormByTheory()
# rareSample.setStatConfig(useStat)
# rareSample.addSystematic(nelchflip)
# rareSample.addSystematic(neleff)
# rareSample.addSystematic(neltrig)
# rareSample.addSystematic(nelreco)
# rareSample.addSystematic(neliso)
# rareSample.addSystematic(nelreso)
# rareSample.addSystematic(nelescale)
# rareSample.addSystematic(nelscintillator)
# rareSample.addSystematic(nB_SYS                             )
# rareSample.addSystematic(nC_SYS                             )
# rareSample.addSystematic(nL_SYS                             )
# rareSample.addSystematic(nE_SYS                             )
# rareSample.addSystematic(nEFC_SYS                           )
# rareSample.addSystematic(nJVT_SYS                           )
# rareSample.addSystematic(nJET_BJES_Response                 )
# rareSample.addSystematic(nJET_EffectiveNP_1                 )
# rareSample.addSystematic(nJET_EffectiveNP_2                 )
# rareSample.addSystematic(nJET_EffectiveNP_3                 )
# rareSample.addSystematic(nJET_EffectiveNP_4                 )
# rareSample.addSystematic(nJET_EffectiveNP_5                 )
# rareSample.addSystematic(nJET_EffectiveNP_6                 )
# rareSample.addSystematic(nJET_EffectiveNP_7                 )
# rareSample.addSystematic(nJET_EffectiveNP_8restTerm         )
# rareSample.addSystematic(nJET_EtaIntercalibration_Modelling )
# rareSample.addSystematic(nJET_EtaIntercalibration_NonClosure)
# rareSample.addSystematic(nJET_EtaIntercalibration_TotalStat )
# rareSample.addSystematic(nJET_Flavor_Composition            )
# rareSample.addSystematic(nJET_Flavor_Response               )
# rareSample.addSystematic(nJET_Pileup_OffsetMu               )
# rareSample.addSystematic(nJET_Pileup_OffsetNPV              )
# rareSample.addSystematic(nJET_Pileup_PtTerm                 )
# rareSample.addSystematic(nJET_Pileup_RhoTopology            )
# rareSample.addSystematic(nJET_PunchThrough_MC15             )
# rareSample.addSystematic(nJET_SingleParticle_HighPt         )
# rareSample.addSystematic(nJET_JER_CROSS_CALIB_FORWARD       )
# rareSample.addSystematic(nJET_JER_NOISE_FORWARD             )
# rareSample.addSystematic(nJET_JER_NP0                       )
# rareSample.addSystematic(nJET_JER_NP1                       )
# rareSample.addSystematic(nJET_JER_NP2                       )
# rareSample.addSystematic(nJET_JER_NP3                       )
# rareSample.addSystematic(nJET_JER_NP4                       )
# rareSample.addSystematic(nJET_JER_NP5                       )
# rareSample.addSystematic(nJET_JER_NP6                       )
# rareSample.addSystematic(nJET_JER_NP7                       )
# rareSample.addSystematic(nJET_JER_NP8                       )
# rareSample.addSystematic(nmueff1  )
# rareSample.addSystematic(nmueff2  )
# rareSample.addSystematic(nmutrig1 )
# rareSample.addSystematic(nmutrig2 )
# rareSample.addSystematic(nmuiso1  )
# rareSample.addSystematic(nmuiso2  )
# rareSample.addSystematic(nmuttva1 )
# rareSample.addSystematic(nmuttva2 )
# rareSample.addSystematic(nmuTreeID    )
# rareSample.addSystematic(nmuTreeMS    )
# rareSample.addSystematic(nmuTreeRES   )
# rareSample.addSystematic(nmuTreeRHO   )
# rareSample.addSystematic(nmuTreeSCALE )

fakesSample = Sample("fakes",kGray+1) 
fakesSample.setStatConfig(True) # problematic, statistics-driven is wrong here?
fakesSample.addSystematic(elfakesys)
fakesSample.addSystematic(mufakesys)

# Signal sample
sigSample = Sample(sigSamples[0],kViolet)
sigSample.legName = "HN signal"
sigSample.setNormByTheory()
sigSample.setStatConfig(useStat)       
sigSample.setNormFactor("mu_SIG",1.,0.,50.)
sigSample.addSystematic(eleff)
sigSample.addSystematic(eltrig)
sigSample.addSystematic(elreco)
sigSample.addSystematic(eliso)
sigSample.addSystematic(elreso)
sigSample.addSystematic(elescale)
sigSample.addSystematic(elscintillator)
sigSample.addSystematic(B_SYS                             )
sigSample.addSystematic(C_SYS                             )
sigSample.addSystematic(L_SYS                             )
sigSample.addSystematic(E_SYS                             )
sigSample.addSystematic(EFC_SYS                           )
sigSample.addSystematic(JVT_SYS                           )
sigSample.addSystematic(JET_BJES_Response                 )
sigSample.addSystematic(JET_EffectiveNP_1                 )
sigSample.addSystematic(JET_EffectiveNP_2                 )
sigSample.addSystematic(JET_EffectiveNP_3                 )
sigSample.addSystematic(JET_EffectiveNP_4                 )
sigSample.addSystematic(JET_EffectiveNP_5                 )
sigSample.addSystematic(JET_EffectiveNP_6                 )
sigSample.addSystematic(JET_EffectiveNP_7                 )
sigSample.addSystematic(JET_EffectiveNP_8restTerm         )
sigSample.addSystematic(JET_EtaIntercalibration_Modelling )
sigSample.addSystematic(JET_EtaIntercalibration_NonClosure)
sigSample.addSystematic(JET_EtaIntercalibration_TotalStat )
sigSample.addSystematic(JET_Flavor_Composition            )
sigSample.addSystematic(JET_Flavor_Response               )
sigSample.addSystematic(JET_Pileup_OffsetMu               )
sigSample.addSystematic(JET_Pileup_OffsetNPV              )
sigSample.addSystematic(JET_Pileup_PtTerm                 )
sigSample.addSystematic(JET_Pileup_RhoTopology            )
sigSample.addSystematic(JET_PunchThrough_MC15             )
sigSample.addSystematic(JET_SingleParticle_HighPt         )
sigSample.addSystematic(JET_JER_CROSS_CALIB_FORWARD       )
sigSample.addSystematic(JET_JER_NOISE_FORWARD             )
sigSample.addSystematic(JET_JER_NP0                       )
sigSample.addSystematic(JET_JER_NP1                       )
sigSample.addSystematic(JET_JER_NP2                       )
sigSample.addSystematic(JET_JER_NP3                       )
sigSample.addSystematic(JET_JER_NP4                       )
sigSample.addSystematic(JET_JER_NP5                       )
sigSample.addSystematic(JET_JER_NP6                       )
sigSample.addSystematic(JET_JER_NP7                       )
sigSample.addSystematic(JET_JER_NP8                       )
sigSample.addSystematic(mueff1  )
sigSample.addSystematic(mueff2  )
sigSample.addSystematic(mutrig1 )
sigSample.addSystematic(mutrig2 )
sigSample.addSystematic(muiso1  )
sigSample.addSystematic(muiso2  )
sigSample.addSystematic(muttva1 )
sigSample.addSystematic(muttva2 )
sigSample.addSystematic(muTreeID    )
sigSample.addSystematic(muTreeMS    )
sigSample.addSystematic(muTreeRES   )
sigSample.addSystematic(muTreeRHO   )
sigSample.addSystematic(muTreeSCALE )

#data
dataSample = Sample("dataNom",kBlack)
dataSample.setData()


#**************
# fit
#**************

## Bkg-only template
commonSamples = [dataSample,topSample,dbSample,fakesSample,zSample]

## Parameters of the Measurement
measName = "NormalMeasurement"
measLumi = 1.
measLumiError = 0.021 # 2015+16

bkgOnly = configMgr.addFitConfig("Template_BkgOnly")
if useStat:
    bkgOnly.statErrThreshold=0.05 #values above this will be considered in the fit
else:
    bkgOnly.statErrThreshold=None
bkgOnly.addSamples(commonSamples)
bkgOnly.addSamples([sigSample])
bkgOnly.setSignalSample(sigSample)
meas = bkgOnly.addMeasurement(measName,measLumi,measLumiError)
meas.addPOI("mu_SIG")

channelSSZCRee = bkgOnly.addChannel("Mjj",["SSZCRee"],12,0,12)
channelSSZCRee.hasB = False
channelSSZCRee.hasBQCD = False
channelSSZCRee.useOverflowBin = False
channelSSZCRee.removeSample(sigSamples[0])

# channelSSZSRee = bkgOnly.addChannel("HT",["SSZSRee"],5,0,5)
# channelSSZSRee.hasB = False
# channelSSZSRee.hasBQCD = False
# channelSSZSRee.useOverflowBin = False

channelSSZCRmm = bkgOnly.addChannel("Mjj",["SSZCRmm"],5,0,5)
channelSSZCRmm.hasB = False
channelSSZCRmm.hasBQCD = False
channelSSZCRmm.useOverflowBin = False
channelSSZCRmm.removeSample("SherpaDY221")
channelSSZCRmm.removeSample(sigSamples[0])

channelSSZSRmm = bkgOnly.addChannel("Mlljj",["SSZSRmm"],8,0,8)
channelSSZSRmm.hasB = False
channelSSZSRmm.hasBQCD = False
channelSSZSRmm.useOverflowBin = False
channelSSZSRmm.removeSample("SherpaDY221")

allChannels =  [channelSSZCRee,channelSSZCRmm]
allChannels+=  [channelSSZSRmm]

## CRs:
bkgOnly.setBkgConstrainChannels([channelSSZCRee,channelSSZCRmm])
## VRs:
# bkgOnly.addValidationChannels([channelSSZVRee])
## SRs:
bkgOnly.setSignalChannels(channelSSZSRmm)
# Set global plotting colors/styles
bkgOnly.dataColor = dataSample.color
bkgOnly.totalPdfColor = kBlack
bkgOnly.errorFillColor = kBlue-5
bkgOnly.errorFillStyle = 3004
bkgOnly.errorLineStyle = kDashed
bkgOnly.errorLineColor = kBlue-5

# Set Channel titleX, titleY, minY, maxY, logY
for channel in allChannels:
    channel.titleX = "m(jj) bins"
    channel.titleY = "Events"
    channel.ATLASLabelX = 0.25
    channel.ATLASLabelY = 0.85
    channel.ATLASLabelText = "internal"
    channel.showLumi = True


# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = TLegend(0.65,0.475,0.9,0.925,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = TLegendEntry()
entry = leg.AddEntry("","Data","p") 
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("","Total","lf") 
entry.SetLineColor(bkgOnly.totalPdfColor)
entry.SetLineWidth(2)
entry.SetFillColor(bkgOnly.errorFillColor)
entry.SetFillStyle(bkgOnly.errorFillStyle)
#
entry = leg.AddEntry("","Drell-Yan Z/#gamma*","f") 
entry.SetLineColor(zSample.color)
entry.SetFillColor(zSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","fakes","lf") 
entry.SetLineColor(fakesSample.color)
entry.SetFillColor(fakesSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","diboson","lf") 
entry.SetLineColor(dbSample.color)
entry.SetFillColor(dbSample.color)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("","top physics","lf") 
entry.SetLineColor(topSample.color)
entry.SetFillColor(topSample.color)
entry.SetFillStyle(compFillStyle)
#
# entry = leg.AddEntry("","rare","lf") 
# entry.SetLineColor(rareSample.color)
# entry.SetFillColor(rareSample.color)
# entry.SetFillStyle(compFillStyle)

bkgOnly.tLegend = leg
c.Close()