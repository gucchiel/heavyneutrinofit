massPoint="MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2"
echo $massPoint
mkdir -p logs
mkdir -p limits
cp data/templateHN_v2_004.root data/backupCacheFile_SameSignHN-LRSM_WR$1_NR$2-excl-mm.root
HistFitter.py -w -p -f -F excl -l -D before,after analysis/SameSignHN/SameSignHN_excl_mm.py\
 -c "analysisSuffix='LRSM_WR$1_NR$2-excl-mm';sigSamples=['MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2']"\
 -g $massPoint  2>&1 | tee logs/LRSM_WR$1_NR$2-HNexcl-mm.log