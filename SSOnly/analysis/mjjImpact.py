from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)


## modules
import os
import sys
import re

files = [ open('logs/LRSM_WR300_NR300-HNbkg%s.log' % mjj) for mjj in ["","-60","-70","-80","-90","-100"] ]

mu_Z  = ROOT.TGraphErrors()
mu_DB = ROOT.TGraphErrors()

for file,mjj in zip(files,[50,60,70,80,90,100]):
  text = file.read()

  regular = re.findall("mu_Z    1.0000e\+00[ \t]*([0-9\.\-\+e]*)[ \t/\-\+]*([0-9\.\-\+e]*).*",text)[-1]
  print "mu_Z ",float(regular[0])," ",float(regular[1])
  mu_Z.SetPoint(mu_Z.GetN(),mjj-1,float(regular[0]))
  mu_Z.SetPointError(mu_Z.GetN()-1,0,float(regular[1]))

  regular2 = re.findall("mu_DB    1.0000e\+00[ \t]*([0-9\.\-\+e]*)[ \t/\-\+]*([0-9\.\-\+e]*).*",text)[-1]
  print "mu_DB ",float(regular2[0])," ",float(regular2[1])
  mu_DB.SetPoint(mu_DB.GetN(),mjj+1,float(regular2[0]))
  mu_DB.SetPointError(mu_DB.GetN()-1,0,float(regular2[1]))

  file.close()


mu_Z.SetLineColor(ROOT.kBlue)
mu_Z.SetMarkerColor(ROOT.kBlue)
mu_DB.SetLineColor(ROOT.kRed)
mu_DB.SetMarkerColor(ROOT.kRed)

mg = ROOT.TMultiGraph()
mg.Add(mu_Z,"l p e")
mg.Add(mu_DB,"l p e")

leg = ROOT.TLegend(0.18,0.61,0.4,0.79);
leg.SetBorderSize(0);
leg.SetFillColor(0);
leg.SetFillStyle(0);
leg.SetTextSize(0.045);
leg.AddEntry(mu_Z,"#font[42]{#mu_{Z}}","l");
leg.AddEntry(mu_DB,"#font[42]{#mu_{DB}}","l");


canv = ROOT.TCanvas("excl","excl",800,600)
canv.cd()
mg.Draw("a")
mg.SetMinimum(0.5)
mg.SetMaximum(2.0)
mg.GetXaxis().SetTitle("p_{T}(jet) cut")
mg.GetYaxis().SetTitle("normalization factor")
leg.Draw()
ROOT.ATLASLabel(0.185,0.875,"Internal",1)
ROOT.myText(0.185,0.80,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}")

canv.Print("mjjImpact.eps")



