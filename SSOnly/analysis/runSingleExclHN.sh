massPoint="MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2"
echo $massPoint
mkdir -p logs
mkdir -p limits
cp data/templateHN_v2_002.root data/backupCacheFile_SameSignHN-LRSM_WR$1_NR$2-excl.root
HistFitter.py -w -p -f -F excl -l analysis/SameSignHN/SameSignHN_excl.py\
 -c "analysisSuffix='LRSM_WR$1_NR$2-excl';sigSamples=['MadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2']"\
 -g $massPoint  2>&1 | tee logs/LRSM_WR$1_NR$2-HNexcl.log