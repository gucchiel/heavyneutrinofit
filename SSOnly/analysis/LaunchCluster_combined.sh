# This is a sample PBS script. It will request 1 processor on 1 node
# for 4 hours.
#   
#   Request 1 processors on 1 node 
#   
#PBS -l nodes=1:ppn=1


#PBS -l walltime=10:00:00
#
#   Request 4 gigabyte of memory per process
#
#PBS -l pmem=1gb
#!/bin/bash
STARTTIME=`date +%s`
date

#-------------------------------- ENV VARS ------------------------------- 
echo " INTARBALL:     $INTARBALL"
echo " OUTPATH:      $OUTPATH"
echo " TEMPLATEFILE:   $TEMPLATEFILE"
echo " CONFIG:   $CONFIG"
echo " WRMASS:   $WRMASS"
echo " NRMASS:   $NRMASS"
echo " CHANNEL:  $CHANNEL"
echo " NEUTRINO:  $NEUTRINO"


echo
export 

MYDIR=Hist_${RANDOM}${RANDOM}

echo "umask 000"
umask 000

#-------------------------------- NODE CONFIG ------------------------------
echo "going to tmp node dir: $TMPDIR"
cd $TMPDIR

echo "ls ${TMPDIR} -la"
ls ${TMPDIR} -la

echo "mkdir ${MYDIR}"
mkdir ${MYDIR}

echo "ls ${TMPDIR} -la"
ls ${TMPDIR} -la

echo "cd ${MYDIR}"
cd ${MYDIR}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
. /ceph/grid/runtime/APPS/HEP/ATLAS-SITE
lsetup "root 6.06.06-x86_64-slc6-gcc49-opt"

## copy over working area
##echo "ls /data/fscutti"
##ls /data/fscutti

## copy over working area
echo "copying input tarball ${INTARBALL}..."
cp $INTARBALL .
date
ENDTIME=`date +%s`
TOTALTIME=$(($ENDTIME-$STARTTIME))
echo "Total Time: ${TOTALTIME}s"
echo "extracting input tarball..."
tar xzf *.tar.gz
date
ENDTIME=`date +%s`
TOTALTIME=$(($ENDTIME-$STARTTIME))
echo "Total Time: ${TOTALTIME}s"
echo "done setting working area"
ls -alh 

echo 
echo "setting up workarea..."
source ./setup_afs.sh
cd src/
make clean
make
cd ..

echo ln -s ${TEMPLATEFILE} data/mergedOSSS.root
ln -s ${TEMPLATEFILE} data/mergedOSSS.root

ls -alh


echo ""
echo "executing job..." 
# source analysis/SameSignHN/runSingleExclHN_combined.sh 1000 800 ee Majorana excl False histoSys
echo ./analysis/SameSignHN/runSingleExclHN_combined.sh ${WRMASS} ${NRMASS} ${CHANNEL} ${NEUTRINO} excl False overallNormHistoSys
./analysis/SameSignHN/runSingleExclHN_combined.sh ${WRMASS} ${NRMASS} ${CHANNEL} ${NEUTRINO} excl False overallNormHistoSys

ls -alh

echo "finished execution"

echo 
echo "preparing output dir..."

if [ ! -d ${OUTPATH} ]; 
  then mkdir -p ${OUTPATH};
  # chmod a+rxw `dirname $OUTPATH`;
  # chmod a+rxw ${OUTPATH};
fi

echo "copying output"
cp -r results/* ${OUTPATH}/
cp -r logs/* ${OUTPATH}/
# cp results/*.root ${OUTPATH}/
# chmod a+wrx ${OUTPATH}/*
# echo "chmod a+wrx ${OUTPATH}/SameSignHN-DCH${SIGMASS}-${CONFIG}/*"
# chmod a+wrx ${OUTPATH}/SameSignHN-DCH${SIGMASS}-${CONFIG}/*


echo "cd ${TMPDIR}"
cd ${TMPDIR}

echo "ls ${TMPDIR} -la"
ls ${TMPDIR} -la

echo "rm -rf ${MYDIR}"
rm -rf ${MYDIR}

echo "ls ${TMPDIR} -la"
ls ${TMPDIR} -la

echo "finished job"

date
ENDTIME=`date +%s`
TOTALTIME=$(($ENDTIME-$STARTTIME))
echo "Total Time: ${TOTALTIME}s"







