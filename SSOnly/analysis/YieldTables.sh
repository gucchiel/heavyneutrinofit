python scripts/YieldsTable.py -L "tab:controlRegions" \
-C "The number of expected (bottom part) and predicted (upper part) background events in control regions after the fit, compared to the data. Uncertainties correspond to the total uncertainties in the predicted event yields, and are smaller for the total than for the individual contributions because the latter are anti-correlated. Due to rounding the totals can differ from the sums of components. Diboson and Drell--Yan normalizations are floating in the fit. Just for the reference purpose, the expectation of their yields and its error are shown in the bottom part of the table." \
-c SSZCRee,SSZCRmm \
-w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
-s SherpaDY221,fakes,dibosonSherpa,top_physics,hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2 -b -o tables/HN/controlRegions.tex -y

python scripts/YieldsTable.py -L "tab:validationRegions" \
-C "The number of expected (bottom part) and predicted (upper part) background events in validation regions after the fit, compared to the data. Uncertainties correspond to the total uncertainties in the predicted event yields, and are smaller for the total than for the individual contributions because the latter are anti-correlated. Due to rounding the totals can differ from the sums of components. Diboson and Drell--Yan normalizations are floating in the fit. Just for the reference purpose, the expectation of their yields and its error are shown in the bottom part of the table." \
-c SSZVRee,SSZVRmm \
-w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
-s SherpaDY221,fakes,dibosonSherpa,top_physics,hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2 -b -o tables/HN/validationRegions.tex -y

python scripts/YieldsTable.py -L "tab:signalRegions" \
-C "The number of expected (bottom part) and predicted (upper part) background events in signal regions after the fit, compared to the data. Uncertainties correspond to the total uncertainties in the predicted event yields, and are smaller for the total than for the individual contributions because the latter are anti-correlated. Due to rounding the totals can differ from the sums of components. Diboson and Drell--Yan normalizations are floating in the fit. Just for the reference purpose, the expectation of their yields and its error are shown in the bottom part of the table." \
-c SSZSRee,SSZSRmm \
-w results/SameSignHN-WR$1_NR$2_Sig_$3-bkg/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root \
-s SherpaDY221,fakes,dibosonSherpa,top_physics,hMadGraphPythia8EvtGen_A14NNPDF23LO_LRSM_WR$1_NR$2 -b -o tables/HN/signalRegions.tex -y

