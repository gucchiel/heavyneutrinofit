# encoding: utf-8
'''
SubmitHist.py
'''
import ROOT

## modules
import os
import sys
import re
import subprocess
import time

## environment variables
MAIN   = os.getenv('MAIN') # upper folder
USER   = os.getenv('USER')

JOBDIR = "/ceph/grid/home/atlas/%s/jobdirHF" % USER # Alright this is twisted...
INTARBALL = os.path.join(JOBDIR,'histtarball_%s.tar.gz' % (time.strftime("d%d_m%m_y%Y_H%H_M%M_S%S")) )

TEMPLATEFILE = os.path.join(JOBDIR,'mergedOSSS.root')

RUN = "HistFitter_HN_combined_076_exclusion"

OUTPATH="/ceph/grid/home/atlas/%s/AnalysisCode/%s"%(USER,RUN) # 

BEXEC = "analysis/SameSignHN/LaunchCluster_combined.sh"                  # exec script (probably dont change) 


neutrino = "Majorana"
 
signalSamples = [
[600,50],
[600,150],
[600,300],
[600,450],
[600,500],
[600,900],
[600,1200],
[1200,50],
[1200,300],
[1200,600],
[1200,900],
[1200,1100],
[1200,1800],
[1200,2400],
[1800,50],
[1800,450],
[1800,900],
[1800,1350],
[1800,1700],
[1800,2700],
[1800,3600],
[2400,50],
[2400,600],
[2400,1200],
[2400,1800],
[2400,2300],
[2400,3600],
[2400,4800],
[3000,50],
[3000,750],
[3000,1500],
[3000,2250],
[3000,2900],
[3000,4500],
[3000,6000],
[3500,50],
[3500,875],
[3500,1750],
[3500,2625],
[3500,3400],
[3500,5250],
[3500,7000],
[3600,50],
[3600,900],
[3600,1800],
[3600,2700],
[3600,3500],
[3600,5400],
[3600,7200],
[4200,50],
[4200,1050],
[4200,2100],
[4200,3150],
[4200,4100],
[4200,6300],
[4200,8400],
[600,600],
[600,700],
[1000,700],
[1000,800],
[1000,1000],
[1000,1100],
[1200,1200],
[1200,1500],
[1500,1000],
[1500,1100],
[1500,1200],
[1800,1500],
[2400,1900],
[2400,2100],
[3000,1600],
[3000,1800],
[3000,30],
[3000,150],
[3000,300],
[4000,400],
[5000,500],
[4500,50],
[4500,1125],
[4500,2250],
[4500,3375],
[4500,4400],
[5000,50],
[5000,1250],
[5000,2500],
[5000,3750],
[5000,4900],

[2100,1500],
[3500,400],
[3800,3000],
[4200,200],
[4200,400],
[4500,800],
[4750,3000],
[5800,1000],
]



def main():
    """
    * configure the samples (input->output)
    * configure which samples to run for each systematic
    * prepare outdirs and build intarball
    * launch the jobs
    """
    global MAIN
    global USER
    global NTUP
    global INTARBALL
    global AUTOBUILD
    global RUN
    global OUTPATH
    global OUTFILE
    global QUEUE
    global SCRIPT
    global BEXEC
    global DO_NOM
    global DO_NTUP_SYS
    global DO_PLOT_SYS
    global TESTMODE

    p = subprocess.Popen(['./makeZip.sh '+INTARBALL],shell=True,stdout=subprocess.PIPE)
    output, errors = p.communicate()
    print output

    p = subprocess.Popen(['cp ./data/mergedOSSS.root '+TEMPLATEFILE],shell=True,stdout=subprocess.PIPE)
    output, errors = p.communicate()
    print output

    absintar   = os.path.abspath(INTARBALL)
    absoutpath = os.path.abspath(OUTPATH)
    templatefile = os.path.abspath(TEMPLATEFILE)


    ARCRUNNERDIR = "/afs/f9.ijs.si/home/miham/arcrunner"
    cmd = "mkdir -p %s" % os.path.join(ARCRUNNERDIR, RUN)
    print cmd
    m = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    print m.communicate()[0]

    for channel in ["ee","mm"]:
        for sample in signalSamples:
        
            CONFIG = "WR"+str(sample[0])+"HN"+str(sample[1])
            jobname = RUN+"_"+CONFIG+"_"+channel

            cmd = "mkdir -p %s" % os.path.join(ARCRUNNERDIR, RUN, jobname)
            print cmd
            m = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
            print m.communicate()[0]

            TEMPXRSL = os.path.join(ARCRUNNERDIR, RUN, jobname, 'run.xrsl' )
            # TEMPXRSL = os.path.join(JOBDIR,'temp_'+ str(time.strftime("d%d_m%m_y%Y_H%H_M%M_S%S")) + '.xrsl' )
            JOBLISTF = os.path.join(JOBDIR,'joblist_%s.xml' % (time.strftime("d%d_m%m_y%Y")) )
            cmd =  'printf "'
            cmd += '&\n'
            cmd += '(executable=\\"%s\\")\n' % BEXEC
            cmd += '(inputFiles=(\\"%s\\" \\"/afs/f9.ijs.si/home/miham/HistFitter/%s\\"))' % (BEXEC,BEXEC)
            cmd += '(jobName=\\"'+jobname+'\\")\n'
            cmd += '(memory=2000)\n'
            cmd += '(join=yes)\n'
            cmd += '(stdout="cp.out")\n'
            cmd += '(gmlog="gmlog")\n'
            cmd += '(cpuTime="1000")\n'
            cmd += '(environment=(\\"INTARBALL\\" \\"%s\\")\n' % absintar
            cmd += '             (\\"OUTPATH\\" \\"%s\\")\n'   % (absoutpath+"_"+channel)
            cmd += '             (\\"TEMPLATEFILE\\" \\"%s\\")\n'   % templatefile
            cmd += '             (\\"CONFIG\\" \\"%s\\")\n'    % CONFIG
            cmd += '             (\\"CHANNEL\\" \\"%s\\")\n'    % channel
            cmd += '             (\\"NEUTRINO\\" \\"%s\\")\n'    % neutrino
            cmd += '             (\\"WRMASS\\" \\"%s\\")\n'    % sample[0]
            cmd += '             (\\"NRMASS\\" \\"%s\\"))'    % sample[1]
            cmd += '">>' + TEMPXRSL
            # cmd += ';arcsub -c pikolit.ijs.si -S org.nordugrid.gridftpjob -o '+JOBLISTF+' '+TEMPXRSL
            # cmd += ';arcsub -c pikolit.ijs.si -S org.ogf.glue.emies.activitycreation -o '+JOBLISTF+' '+TEMPXRSL
            print cmd
            m = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
            # print m.communicate()[0]


if __name__=='__main__': main()

## EOF
