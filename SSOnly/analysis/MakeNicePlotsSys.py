from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
import ROOT
ROOT.SetAtlasStyle()

ROOT.gROOT.SetBatch(True)


## modules
import os
import sys
import re
import subprocess

def getSystematic(string,table):
  out = re.findall("\n"+string+" .*",sysTable)
  assert len(out) > 0 , "did not find " + string
  out = out[0].split("&")[1:]
  out[-1] = out[-1].replace("\\\\","")
  out = [float(x.replace(" ","").replace("\\pm","").replace("$","")) for x in out]
  print string
  print out
  return out

def getTotalSystematic(string,table):
  out = re.findall("\n"+string+" .*",sysTable)
  assert len(out) > 0 , "did not find " + string
  out = out[0].split("&")[1:]
  out[-1] = out[-1].replace("\\\\","")
  out = [float(x.replace(" ","").replace("\\pm","").replace("$","")) for x in out]
  return out

def sumSystematics(array):
  out = getSystematic(array[0],sysTable)
  for elem in array[1:]:
    temp = getSystematic(elem,sysTable)
    out = [(x**2+y**2)**(1/2.) for x,y in zip(out,temp) ]
  return out



def getPoissonInterval(n, alpha=1.- 0.682689492):
  return (n - ROOT.Math.gamma_quantile( alpha/2, n, 1.) if n!=0. else 0.,\
    ROOT.Math.gamma_quantile_c( alpha/2, n+1, 1) - n)

SSONLY = True

regDict = []

if not SSONLY:
  regDict += [
  ("ZCR\_eeos"    ,  "CR (e^{#pm}e^{#mp})"     ),
  ("SR\_emos"     ,  "CR (e^{#pm}#mu^{#mp})"   ),
  ("ZCR\_mmos"    ,  "CR (#mu^{#pm}#mu^{#mp})" ),
  ]
regDict += [
("SSZCRee"     ,  "CR (e^{#pm}e^{#pm})"     ),
("SSZCRmm"     ,  "CR (#mu^{#pm}#mu^{#pm})",),
]
if not SSONLY:
  regDict += [
  ("ZVR\_eeos"    ,  "VR (e^{#pm}e^{#mp})"     ),
  ("ZVR\_mmos"    ,  "VR (#mu^{#pm}#mu^{#mp})" ),
  ]
regDict += [
("SSZVRee"     ,  "VR (e^{#pm}e^{#pm})"     ),
("SSZVRmm"     ,  "VR (#mu^{#pm}#mu^{#pm})" ),
]
if not SSONLY:
  regDict += [
  ("SR\_eeos"     ,  "SR (e^{#pm}e^{#mp})"     ),
  ("SR\_mmos"     ,  "SR (#mu^{#pm}#mu^{#mp})" ),
  ]
regDict += [
("SSZSRee"     ,  "SR (e^{#pm}e^{#pm})"     ),
("SSZSRmm"     ,  "SR (#mu^{#pm}#mu^{#pm})" ),
]

for fit in [""]:

  sysTable = open('tables/allBins/AllSysHN'+fit+'.tex','r').read()


  allReg = re.findall("\{\\\\textbf\{Uncertainty of channel\}\}.*",sysTable)[0].split("&")[1:]
  allReg[-1] = allReg[-1].replace("\\\\","")
  allReg = [x.replace(" ","") for x in allReg]

  totSys = re.findall("Total background systematic.*",sysTable)[0].split("&")[1:]
  totSys[-1] = totSys[-1].replace("\\\\","")
  totSys = [float(re.findall("pm([0-9\.]*)",x.replace(" ",""))[0]) for x in totSys]

  totalBkg = getSystematic("Total background expectation",sysTable)

  statErr = getSystematic("total stat. error",sysTable)

  lumiErr = getSystematic("Lumi",sysTable)

  chargeFlipErr= getSystematic("CF",sysTable)

  # MJJRWErr= getSystematic("MJJRW\\\\_1",sysTable)

  if not SSONLY:
    theoryErr = sumSystematics(["MJJRW\\\\_1","Scale\\\\_MUR\\\\_MUF\\\\_PDF261000","AlphaS\\\\_PDF261000","PDF\\\\_PDF261000","PDFVary\\\\_PDF","PDF\\\\_COICE\\\\_ENVELOPE","PDF\\\\_SYS\\\\_ENVELOPE","QCD\\\\_SCALE\\\\_ENVELOPE","ALPHA\\\\_SYS"])
  else:
    theoryErr = sumSystematics(["PDF\\\\_COICE\\\\_ENVELOPE","PDF\\\\_SYS\\\\_ENVELOPE","QCD\\\\_SCALE\\\\_ENVELOPE","ALPHA\\\\_SYS","tt\\\\_THEORY"])    

  fakeFactorErr = sumSystematics(["FF","MUFF"])

  if not SSONLY:
    yieldFitErr = sumSystematics(["mu\\\\_top","mu\\\\_ZOS","mu\\\\_ZSS","mu\\\\_DB"])
  else:
    yieldFitErr = sumSystematics(["mu\\\\_ZSS","mu\\\\_DB"])

  if not SSONLY:
    experimentalErr = sumSystematics(["JET\\\\_SR1\\\\_JET\\\\_GroupedNP\\\\_1","JET\\\\_SR1\\\\_JET\\\\_GroupedNP\\\\_2","JET\\\\_JER\\\\_SINGLE\\\\_NP","JET\\\\_SR1\\\\_JET\\\\_EtaIntercalibration\\\\_NonClosure","JET\\\\_SR1\\\\_JET\\\\_GroupedNP\\\\_3","JET\\\\_Flavor\\\\_Response","JET\\\\_EffectiveNP\\\\_6","JET\\\\_EffectiveNP\\\\_7","JET\\\\_EffectiveNP\\\\_4","JET\\\\_EffectiveNP\\\\_5","JET\\\\_EffectiveNP\\\\_2","JET\\\\_EffectiveNP\\\\_3","JET\\\\_EffectiveNP\\\\_1","JET\\\\_JER\\\\_NP8","JET\\\\_SingleParticle\\\\_HighPt","JET\\\\_JER\\\\_NP0","JET\\\\_JER\\\\_NP1","JET\\\\_JER\\\\_NP2","JET\\\\_JER\\\\_NP3","JET\\\\_JER\\\\_NP4","JET\\\\_JER\\\\_NP5","JET\\\\_JER\\\\_NP6","JET\\\\_JER\\\\_NP7","JET\\\\_BJES\\\\_Response","JET\\\\_JER\\\\_NOISE\\\\_FORWARD","JET\\\\_Pileup\\\\_OffsetNPV","JET\\\\_EtaIntercalibration\\\\_TotalStat","JET\\\\_Pileup\\\\_RhoTopology","JET\\\\_Pileup\\\\_PtTerm","JET\\\\_EtaIntercalibration\\\\_Modelling","JET\\\\_EffectiveNP\\\\_8restTerm","JET\\\\_Flavor\\\\_Composition","JET\\\\_EtaIntercalibration\\\\_NonClosure","JET\\\\_JER\\\\_CROSS\\\\_CALIB\\\\_FORWARD","JET\\\\_Pileup\\\\_OffsetMu","JET\\\\_PunchThrough\\\\_MC15","MUON\\\\_EFF\\\\_SYS","MUON\\\\_EFF\\\\_TrigSystUncertainty","MUON\\\\_ISO\\\\_SYS","MUON\\\\_EFF\\\\_STAT","MUON\\\\_SCALE\\\\_OS","MUON\\\\_EFF\\\\_TrigStatUncertainty","MUON\\\\_ID\\\\_OS","MUON\\\\_TTVA\\\\_SYS","MUON\\\\_TTVA\\\\_STAT","MUON\\\\_ISO\\\\_STAT","RECOSYS","TTVASTAT","ISOSYS","TTVASYS","MUON\\\\_ID","TRIGSYS","MUON\\\\_SCALE","MUON\\\\_MS","MUON\\\\_RESBIAS","RECOSTAT","MUON\\\\_RHO","ISOSTAT","EG\\\\_SCALE\\\\_ALL","EL\\\\_EFF\\\\_ID\\\\_TOTAL\\\\_1NPCOR\\\\_PLUS\\\\_UNCOR","FT\\\\_EFF\\\\_extrapolation\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_B\\\\_1\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_B\\\\_0\\\\_AntiKt4EMTopoJets","EL\\\\_EFF\\\\_Reco\\\\_TOTAL\\\\_1NPCOR\\\\_PLUS\\\\_UNCOR","EL\\\\_EFF\\\\_Iso\\\\_TOTAL\\\\_1NPCOR\\\\_PLUS\\\\_UNCOR","EL\\\\_EFF\\\\_Trigger\\\\_TOTAL\\\\_1NPCOR\\\\_PLUS\\\\_UNCOR","EG\\\\_RESOLUTION\\\\_ALL\\\\_OS","FT\\\\_EFF\\\\_Eigen\\\\_B\\\\_2\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_C\\\\_1\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_Light\\\\_0\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_Light\\\\_3\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_C\\\\_2\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_Light\\\\_2\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_Light\\\\_4\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_Light\\\\_1\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_Eigen\\\\_C\\\\_0\\\\_AntiKt4EMTopoJets","FT\\\\_EFF\\\\_extrapolation\\\\_from\\\\_charm\\\\_AntiKt4EMTopoJets","TRIG","TRIGSTAT","ID","C\\\\_SYS","L\\\\_SYS","EG\\\\_RESOLUTION\\\\_ALL","B\\\\_SYS","EG\\\\_SCALE\\\\_E4SCINTILLATOR","ISO","E\\\\_SYS","EG\\\\_SCALE\\\\_ALLCORR","EFC\\\\_SYS","RECO","JVT\\\\_SYS"])
  else:
    experimentalErr = sumSystematics(["JET\\\\_EffectiveNP\\\\_6","JET\\\\_EffectiveNP\\\\_7","JET\\\\_EffectiveNP\\\\_4","JET\\\\_EffectiveNP\\\\_5","JET\\\\_EffectiveNP\\\\_2","JET\\\\_EffectiveNP\\\\_3","JET\\\\_EffectiveNP\\\\_1","JET\\\\_JER\\\\_NP8","JET\\\\_SingleParticle\\\\_HighPt","JET\\\\_JER\\\\_NP0","JET\\\\_JER\\\\_NP1","JET\\\\_JER\\\\_NP2","JET\\\\_JER\\\\_NP3","JET\\\\_JER\\\\_NP4","JET\\\\_JER\\\\_NP5","JET\\\\_JER\\\\_NP6","JET\\\\_JER\\\\_NP7","JET\\\\_BJES\\\\_Response","JET\\\\_JER\\\\_NOISE\\\\_FORWARD","JET\\\\_Pileup\\\\_OffsetNPV","JET\\\\_EtaIntercalibration\\\\_TotalStat","JET\\\\_Pileup\\\\_RhoTopology","JET\\\\_Pileup\\\\_PtTerm","JET\\\\_EtaIntercalibration\\\\_Modelling","JET\\\\_EffectiveNP\\\\_8restTerm","JET\\\\_Flavor\\\\_Composition","JET\\\\_EtaIntercalibration\\\\_NonClosure","JET\\\\_JER\\\\_CROSS\\\\_CALIB\\\\_FORWARD","JET\\\\_Pileup\\\\_OffsetMu","JET\\\\_PunchThrough\\\\_MC15","RECOSYS","TTVASTAT","ISOSYS","TTVASYS","MUON\\\\_ID","TRIGSYS","MUON\\\\_SCALE","MUON\\\\_MS","MUON\\\\_RESBIAS","RECOSTAT","MUON\\\\_RHO","TRIG","TRIGSTAT","ID","C\\\\_SYS","L\\\\_SYS","EG\\\\_RESOLUTION\\\\_ALL","B\\\\_SYS","EG\\\\_SCALE\\\\_E4SCINTILLATOR","ISO","E\\\\_SYS","EG\\\\_SCALE\\\\_ALLCORR","EFC\\\\_SYS","RECO","JVT\\\\_SYS"])

  hTotSys          = ROOT.TH1D(fit+"hTotSys"         ,fit+"hTotSys",len(regDict),-0.5,len(regDict)-0.5)
  hStatErr         = ROOT.TH1D(fit+"hStatErr"        ,fit+"hStatErr",len(regDict),-0.5,len(regDict)-0.5)
  hChargeFlipErr   = ROOT.TH1D(fit+"hChargeFlipErr"  ,fit+"hChargeFlipErr",len(regDict),-0.5,len(regDict)-0.5)
  # hMJJRWErr        = ROOT.TH1D(fit+"hMJJRWErr"       ,fit+"hMJJRWErr",len(regDict),-0.5,len(regDict)-0.5)
  hTheoryErr       = ROOT.TH1D(fit+"hTheoryErr"      ,fit+"hTheoryErr",len(regDict),-0.5,len(regDict)-0.5)
  hFakeFactorErr   = ROOT.TH1D(fit+"hFakeFactorErr"  ,fit+"hFakeFactorErr",len(regDict),-0.5,len(regDict)-0.5)
  hYieldFitErr     = ROOT.TH1D(fit+"hYieldFitErr"    ,fit+"hYieldFitErr",len(regDict),-0.5,len(regDict)-0.5)
  hExperimentalErr = ROOT.TH1D(fit+"hExperimentalErr",fit+"hExperimentalErr",len(regDict),-0.5,len(regDict)-0.5)
  hLumiErr         = ROOT.TH1D(fit+"hLumiErr"        ,fit+"hLumiErr",len(regDict),-0.5,len(regDict)-0.5)

  for i in range(len(regDict)):
    print regDict[i][0]
    index = allReg.index(regDict[i][0])
    hTotSys         .SetBinContent(i+1,totSys         [index]/totalBkg[index]*100.)
    hStatErr        .SetBinContent(i+1,statErr        [index]/totalBkg[index]*100.)
    hChargeFlipErr  .SetBinContent(i+1,chargeFlipErr  [index]/totalBkg[index]*100.)
    # hMJJRWErr       .SetBinContent(i+1,MJJRWErr       [index]/totalBkg[index]*100.)
    hTheoryErr      .SetBinContent(i+1,theoryErr      [index]/totalBkg[index]*100.)
    hFakeFactorErr  .SetBinContent(i+1,fakeFactorErr  [index]/totalBkg[index]*100.)
    hYieldFitErr    .SetBinContent(i+1,yieldFitErr    [index]/totalBkg[index]*100.)
    hExperimentalErr.SetBinContent(i+1,experimentalErr[index]/totalBkg[index]*100.)
    hLumiErr        .SetBinContent(i+1,lumiErr[index]/totalBkg[index]*100.)

    hTotSys.GetXaxis().SetBinLabel(i+1,regDict[i][1])

  rsplit = 0.45
  hTotSys.SetFillColor(ROOT.kBlue-10)
  hTotSys.SetLineColor(37)
  hTotSys.GetXaxis().LabelsOption("v")
  hTotSys.GetXaxis().SetLabelSize(0.17*rsplit)

  hStatErr.SetLineColor(ROOT.kBlack)
  hStatErr.SetLineStyle(1)

  hTheoryErr.SetLineColor(ROOT.kBlue)
  hTheoryErr.SetLineStyle(1)

  hFakeFactorErr.SetLineColor(ROOT.kYellow+3)
  hFakeFactorErr.SetLineStyle(2)

  hChargeFlipErr.SetLineColor(ROOT.kCyan-7)
  hChargeFlipErr.SetLineStyle(2)

  # hMJJRWErr.SetLineColor(ROOT.kOrange)
  # hMJJRWErr.SetLineStyle(2)

  hYieldFitErr.SetLineColor(ROOT.kGreen)
  hYieldFitErr.SetLineStyle(2)

  hExperimentalErr.SetLineColor(6)
  hExperimentalErr.SetLineStyle(2)

  hLumiErr.SetLineColor(ROOT.kRed)
  hLumiErr.SetLineStyle(1)
  hLumiErr.SetLineWidth(2)

  LeftMarin = 0.1
  BottomMargin = 0.23

  canv = ROOT.TCanvas(fit+"c1",fit+"c1",1400,600)
  canv.SetLogy()
  canv.SetLeftMargin(LeftMarin)
  canv.SetBottomMargin(BottomMargin)
  hTotSys.Draw()
  hTotSys.GetYaxis().SetRangeUser(0.3,200)
  hTotSys.GetYaxis().SetTitle("Relative Uncertainty [%]")
  hTotSys.GetYaxis().SetTitleOffset(0.8)
  hTotSys.GetYaxis().SetNoExponent()
  hTotSys.GetYaxis().SetMoreLogLabels()
  hStatErr.Draw("same")
  hLumiErr.Draw("same")
  hTheoryErr.Draw("same")
  hFakeFactorErr.Draw("same")
  hChargeFlipErr.Draw("same")
  # hMJJRWErr.Draw("same")
  hYieldFitErr.Draw("same")
  hExperimentalErr.Draw("same")


  # canv.Update();
  # print ROOT.gPad.GetUymax()
  # print ROOT.gPad.GetUymin()
  # lineY1 = ROOT.TLine(4.5,10**ROOT.gPad.GetUymin(),4.5,10**ROOT.gPad.GetUymax());
  # lineY1.Draw()
  # lineY2 = ROOT.TLine(12.5,10**ROOT.gPad.GetUymin(),12.5,10**ROOT.gPad.GetUymax());
  # lineY2.Draw()
  # latex = ROOT.TLatex()
  # latex.SetTextSize(0.11*rsplit)
  # latex.SetNDC()
  # latex.SetTextFont(42)
  # latex.SetTextColor(1)
  # latex.DrawLatex(3.5/19.,0.63,"CRs");
  # latex = ROOT.TLatex()
  # latex.SetTextSize(0.11*rsplit)
  # latex.SetNDC()
  # latex.SetTextFont(42)
  # latex.SetTextColor(1)
  # latex.DrawLatex(8./19.,0.63,"VRs");
  # latex = ROOT.TLatex()
  # latex.SetTextSize(0.11*rsplit)
  # latex.SetNDC()
  # latex.SetTextFont(42)
  # latex.SetTextColor(1)
  # latex.DrawLatex(14/19.,0.63,"SRs");

  leg = ROOT.TLegend(0.15,0.72,0.42,0.91)
  leg.SetBorderSize(0)
  leg.SetNColumns(2)
  leg.SetFillColor(0)
  leg.SetFillStyle(0)
  leg.SetTextSize(0.05)
  leg.AddEntry(hTotSys,"#font[42]{Total Unc.}","f")
  leg.AddEntry(hStatErr,"#font[42]{Stat. Unc.}","l")
  leg.AddEntry(hLumiErr,"#font[42]{Lumi}","l")
  leg.AddEntry(hTheoryErr,"#font[42]{Theory}","l")
  leg.AddEntry(hFakeFactorErr,"#font[42]{Fakes}","l")
  leg.AddEntry(hChargeFlipErr,"#font[42]{Charge-Flip}","l")
  # leg.AddEntry(hMJJRWErr,"#font[42]{m(jj) RW}","l")
  leg.AddEntry(hYieldFitErr,"#font[42]{Yield fit}","l")
  leg.AddEntry(hExperimentalErr,"#font[42]{Exp.}","l")
  leg.Draw()


  latex = ROOT.TLatex()
  latex.SetTextAlign(12)
  latex.SetTextSize(0.13*rsplit)
  latex.SetNDC()
  latex.SetTextFont(72)
  latex.SetTextColor(1)
  latex.DrawLatex(0.45,0.887,"ATLAS");
  latex = ROOT.TLatex()
  latex.SetTextSize(0.115*rsplit)
  latex.SetNDC()
  latex.SetTextFont(42)
  latex.SetTextColor(1)
  latex.DrawLatex(0.54,0.87,"Internal");
  latex = ROOT.TLatex()
  latex.SetTextSize(0.115*rsplit)
  latex.SetNDC()
  latex.SetTextFont(42)
  latex.SetTextColor(1)
  latex.DrawLatex(0.45,0.8,"#sqrt{s}=13 TeV, 36.1 fb^{-1}");

  ROOT.gPad.RedrawAxis("g")

  canv.Print("histos/allRegionsSys"+fit+".eps")
  canv.Print("histos/allRegionsSys"+fit+".pdf")
  canv.Print("histos/allRegionsSys"+fit+".png")



# "JET\_SR1\_JET\_GroupedNP\_1",

# "JET\_SR1\_JET\_GroupedNP\_2",

# "JET\_JER\_SINGLE\_NP",

# "JET\_SR1\_JET\_EtaIntercalibration\_NonClosure",

# "JET\_SR1\_JET\_GroupedNP\_3",

# "JET\_Flavor\_Response",

# "JET\_EffectiveNP\_6",

# "JET\_EffectiveNP\_7",

# "JET\_EffectiveNP\_4",

# "JET\_EffectiveNP\_5",

# "JET\_EffectiveNP\_2",

# "JET\_EffectiveNP\_3",

# "JET\_EffectiveNP\_1",

# "JET\_JER\_NP8",

# "JET\_SingleParticle\_HighPt",

# "JET\_JER\_NP0",

# "JET\_JER\_NP1",

# "JET\_JER\_NP2",

# "JET\_JER\_NP3",

# "JET\_JER\_NP4",

# "JET\_JER\_NP5",

# "JET\_JER\_NP6",

# "JET\_JER\_NP7",

# "JET\_BJES\_Response",

# "JET\_JER\_NOISE\_FORWARD",

# "JET\_Pileup\_OffsetNPV",

# "JET\_EtaIntercalibration\_TotalStat",

# "JET\_Pileup\_RhoTopology",

# "JET\_Pileup\_PtTerm",

# "JET\_EtaIntercalibration\_Modelling",

# "JET\_EffectiveNP\_8restTerm",

# "JET\_Flavor\_Composition",

# "JET\_EtaIntercalibration\_NonClosure",

# "JET\_JER\_CROSS\_CALIB\_FORWARD",

# "JET\_Pileup\_OffsetMu",

# "JET\_PunchThrough\_MC15",

# "MUON\_EFF\_SYS",

# "MUON\_EFF\_TrigSystUncertainty",

# "MUON\_ISO\_SYS",

# "MUON\_EFF\_STAT",

# "MUON\_SCALE\_OS",

# "MUON\_EFF\_TrigStatUncertainty",

# "MUON\_ID\_OS",

# "MUON\_TTVA\_SYS",

# "MUON\_TTVA\_STAT",

# "MUON\_ISO\_STAT",

# "RECOSYS",

# "TTVASTAT",

# "ISOSYS",

# "TTVASYS",

# "MUON\_ID",

# "TRIGSYS",

# "MUON\_SCALE",

# "MUON\_MS",

# "MUON\_RESBIAS",

# "RECOSTAT",

# "MUON\_RHO",

# "ISOSTAT",

# "EG\_SCALE\_ALL",

# "EL\_EFF\_ID\_TOTAL\_1NPCOR\_PLUS\_UNCOR",

# "FT\_EFF\_extrapolation\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_B\_1\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_B\_0\_AntiKt4EMTopoJets",

# "EL\_EFF\_Reco\_TOTAL\_1NPCOR\_PLUS\_UNCOR",

# "EL\_EFF\_Iso\_TOTAL\_1NPCOR\_PLUS\_UNCOR",

# "EL\_EFF\_Trigger\_TOTAL\_1NPCOR\_PLUS\_UNCOR",

# "EG\_RESOLUTION\_ALL\_OS",

# "FT\_EFF\_Eigen\_B\_2\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_C\_1\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_Light\_0\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_Light\_3\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_C\_2\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_Light\_2\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_Light\_4\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_Light\_1\_AntiKt4EMTopoJets",

# "FT\_EFF\_Eigen\_C\_0\_AntiKt4EMTopoJets",

# "FT\_EFF\_extrapolation\_from\_charm\_AntiKt4EMTopoJets",

# "TRIG",

# "TRIGSTAT",

# "ID",

# "C\_SYS",

# "L\_SYS",

# "EG\_RESOLUTION\_ALL",

# "B\_SYS",

# "EG\_SCALE\_E4SCINTILLATOR",

# "ISO",

# "E\_SYS",

# "EG\_SCALE\_ALLCORR",

# "EFC\_SYS",

# "RECO",

# "JVT\_SYS",




