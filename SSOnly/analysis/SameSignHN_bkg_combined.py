"""
SS Analysis
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from collections import defaultdict
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from copy import deepcopy

import re
import os 

import logger
from logger import Logger

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
ROOT.SetAtlasStyle()

def nested_dict(n, type):
    if n == 1:
        return defaultdict(type)
    else:
        return defaultdict(lambda: nested_dict(n-1, type))

#---------------------------------------
# Analysis name
#---------------------------------------

if not analysisSuffix:
    analysisSuffix = "01"

analysis_name="SameSignHN-" + str(analysisSuffix)

print analysis_name

#---------------------------------------
# only SS regions
#---------------------------------------
onlySS = False

#---------------------------------------
# Logger
#---------------------------------------

log = Logger(analysis_name)
log.setLevel(logger.INFO) #should have no effect if -L is used
log.warning("example warning from python")
log.error("example error from python")

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat=True # statistics variation of samples 

#---------------------------------------------------
# Specify the default signal point
# Others to be given via option -g via command line
#---------------------------------------------------

if not 'sigSamples' in dir():
    sigSamples=["Pythia8EvtGen_A14NNPDF23LO_DCH500"+DCHFilter]

# print "SIG SAMPLE ",sigSamples[0]


#-------------------------------
# Config manager basics
#-------------------------------

# Setting the parameters of the hypothesis test
configMgr.doExclusion=True # True=exclusion, False=discovery
#configMgr.doHypoTest=False
#configMgr.nTOYs=5000
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=20

configMgr.writeXML = True

configMgr.analysisName = analysis_name
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

#activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False # enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
configMgr.histBackupCacheFile =  "data/backupCacheFile_" + analysis_name + ".root" # histogram templates - the data file of your previous fit, backup cache

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 36.07456 # Luminosity of input TTree after weighting
configMgr.outputLumi = 36.07456 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")
configMgr.removeEmptyBins=True

#generate using Asimov mock data
# configMgr.useAsimovSet=False
configMgr.blindSR = False


temp = re.findall("WR([0-9]*)_NR([0-9]*)",sigSamples[0])[0]
OSvar = "Mlljj" if float(temp[0]) > float(temp[1]) else "Mjj"
print float(temp[0])," ",float(temp[1])," ",OSvar

# sys.exit()

#-------------------------------------
# Now we start to build the data model
#-------------------------------------


# Dictionnary of cuts defining channels/regions (for Tree->hist)
configMgr.cutsDict["SSZCRee"] = "1."
configMgr.cutsDict["SSZVRee"] = "1."
configMgr.cutsDict["SSZSRee"] = "1."
configMgr.cutsDict["SSZCRmm"] = "1."
configMgr.cutsDict["SSZVRmm"] = "1."
configMgr.cutsDict["SSZSRmm"] = "1."

if not onlySS:
  configMgr.cutsDict["SR_eeos"] = "1."
  configMgr.cutsDict["SR_mmos"] = "1."
  configMgr.cutsDict["SR_emos"] = "1."
  configMgr.cutsDict["ZCR_eeos"] = "1."
  configMgr.cutsDict["ZCR_mmos"] = "1."
  configMgr.cutsDict["ZVR_eeos"] = "1."
  configMgr.cutsDict["ZVR_mmos"] = "1."

if not onlySS:
  sysNames = [ 'EG_RESOLUTION_ALL_OS', 'EG_SCALE_ALL', 'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'MUON_EFF_STAT', 'MUON_EFF_SYS', 'MUON_TTVA_STAT', 'MUON_TTVA_SYS', 'MUON_ISO_STAT', 'MUON_ISO_SYS', 'JET_SR1_JET_GroupedNP_1', 'JET_SR1_JET_GroupedNP_2', 'JET_SR1_JET_GroupedNP_3', 'MUON_ID_OS', 'MUON_SCALE_OS', 'MUON_EFF_TrigSystUncertainty', 'MUON_EFF_TrigStatUncertainty', 'FT_EFF_Eigen_B_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_B_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_B_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_C_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets', 'FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets', 'FT_EFF_extrapolation_AntiKt4EMTopoJets', 'FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets' ]
  oneSidedSysNames = [ "JET_JER_SINGLE_NP" ]
  # oneSidedSysNames = [ ]
  ZjetsTheorySysNames = [ "Scale_MUR_MUF_PDF261000", "AlphaS_PDF261000", "JET_SR1_JET_EtaIntercalibration_NonClosure", "PDF_PDF261000", "PDFVary_PDF" ]
  # oneSidedZjetsTheorySysNames = [  ] 
  oneSidedZjetsTheorySysNames = [ "MJJRW_1" ] 


# Define weights - dummy here
configMgr.weights = "1."

#--------------------
# List of systematics
#--------------------

fittedBkgSys = "overallNormHistoSys"
# fittedBkgSys = "histoSys"


#Caution: Care must be taken when applying Norm type systematics, as only samples that carry a normalization factor mu_... in the fit can be used. Otherwise the uncertainty on the total event count or normalization is not taken into account for this sample, which can lead to underestimation of the sample uncertainty in the signal region.

#-->normalized sys for samples with mu_XX scaling (see above)
#electron ID eff systematics
eleff = Systematic("ID","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #electron Trig eff systematics
eltrig = Systematic("TRIG","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #electron reco eff systematics
elreco = Systematic("RECO","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #electron iso eff systematics
eliso = Systematic("ISO","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #electron resolution  systematics
elreso = Systematic("EG_RESOLUTION_ALL","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #electron energy scale systematics
elescale = Systematic("EG_SCALE_ALLCORR","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #electron energy scale systematics
elscintillator = Systematic("EG_SCALE_E4SCINTILLATOR","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#chflip systematics 
elchflip = Systematic("CF","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#BEAM error sys - DYZ
elDYbeam = Systematic("BEAM","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#PDF CHOICE error sys - DYZ
elDYchoice = Systematic("CHOICE","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#PDF error sys - DYZ
elDYpdf = Systematic("PDF","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#PI error sys - DYZ
elDYpi = Systematic("PI","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#SCALE error sys - DYZ
elDYscale = Systematic("SCALE_Z","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#model uncertainty for diboson
eldbmod   = Systematic("DBGen",  "_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
neldbmod  = Systematic("DBGen",  "_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
eldbmodEE = Systematic("DBGenEE","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
eldbmodEM = Systematic("DBGenEM","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
eldbmodMM = Systematic("DBGenMM","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
#model uncertainty for diboson
# elttGen = Systematic("TTGen","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #model uncertainty for diboson
# elttRad = Systematic("TTRad","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #model uncertainty for diboson
# elttCF = Systematic("TTCF","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# #model uncertainty for diboson
# elttHadron = Systematic("TTHadron","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)

#-->non-normalized sys for samples without mu_XX scaling (see above)
#electron ID eff systematics
neleff = Systematic("ID","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron Trig eff systematics
neltrig = Systematic("TRIG","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron reco eff systematics
nelreco = Systematic("RECO","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron iso eff systematics
neliso = Systematic("ISO","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron resolution  systematics
nelreso = Systematic("EG_RESOLUTION_ALL","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron energy scale systematics
nelescale = Systematic("EG_SCALE_ALLCORR","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
# #electron energy scale systematics
nelscintillator = Systematic("EG_SCALE_E4SCINTILLATOR","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
#chflip systematics 
nelchflip = Systematic("CF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")


### muon exp
mueff1  = Systematic("RECOSYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
mueff2  = Systematic("RECOSTAT","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
mutrig1 = Systematic("TRIGSYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
mutrig2 = Systematic("TRIGSTAT","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muiso1  = Systematic("ISOSYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muiso2  = Systematic("ISOSTAT","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muttva1 = Systematic("TTVASYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muttva2 = Systematic("TTVASTAT","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muTreeID = Systematic("MUON_ID","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muTreeMS = Systematic("MUON_MS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muTreeRES = Systematic("MUON_RESBIAS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muTreeRHO = Systematic("MUON_RHO","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
muTreeSCALE = Systematic("MUON_SCALE","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
### muon exp n
nmueff1  = Systematic("RECOSYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmueff2  = Systematic("RECOSTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmutrig1 = Systematic("TRIGSYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmutrig2 = Systematic("TRIGSTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuiso1  = Systematic("ISOSYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuiso2  = Systematic("ISOSTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuttva1 = Systematic("TTVASYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuttva2 = Systematic("TTVASTAT","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeID    = Systematic("MUON_ID","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeMS    = Systematic("MUON_MS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeRES   = Systematic("MUON_RESBIAS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeRHO   = Systematic("MUON_RHO","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nmuTreeSCALE = Systematic("MUON_SCALE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")



### jet systematics histoSys
B_SYS = Systematic("B_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                             
C_SYS = Systematic("C_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                             
L_SYS = Systematic("L_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                             
E_SYS = Systematic("E_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                             
EFC_SYS = Systematic("EFC_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                           
JVT_SYS = Systematic("JVT_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                           
JET_BJES_Response = Systematic("JET_BJES_Response","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_1 = Systematic("JET_EffectiveNP_1","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_2 = Systematic("JET_EffectiveNP_2","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_3 = Systematic("JET_EffectiveNP_3","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_4 = Systematic("JET_EffectiveNP_4","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_5 = Systematic("JET_EffectiveNP_5","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_6 = Systematic("JET_EffectiveNP_6","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_7 = Systematic("JET_EffectiveNP_7","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_EffectiveNP_8restTerm = Systematic("JET_EffectiveNP_8restTerm","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)         
JET_EtaIntercalibration_Modelling = Systematic("JET_EtaIntercalibration_Modelling","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys) 
JET_EtaIntercalibration_NonClosure = Systematic("JET_EtaIntercalibration_NonClosure","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
JET_EtaIntercalibration_TotalStat = Systematic("JET_EtaIntercalibration_TotalStat","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys) 
JET_Flavor_Composition = Systematic("JET_Flavor_Composition","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)            
JET_Flavor_Response = Systematic("JET_Flavor_Response","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)               
JET_Pileup_OffsetMu = Systematic("JET_Pileup_OffsetMu","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)               
JET_Pileup_OffsetNPV = Systematic("JET_Pileup_OffsetNPV","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)              
JET_Pileup_PtTerm = Systematic("JET_Pileup_PtTerm","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                 
JET_Pileup_RhoTopology = Systematic("JET_Pileup_RhoTopology","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)            
JET_PunchThrough_MC15 = Systematic("JET_PunchThrough_MC15","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)             
JET_SingleParticle_HighPt = Systematic("JET_SingleParticle_HighPt","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)         
JET_JER_CROSS_CALIB_FORWARD = Systematic("JET_JER_CROSS_CALIB_FORWARD","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)       
JET_JER_NOISE_FORWARD = Systematic("JET_JER_NOISE_FORWARD","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)             
JET_JER_NP0 = Systematic("JET_JER_NP0","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP1 = Systematic("JET_JER_NP1","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP2 = Systematic("JET_JER_NP2","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP3 = Systematic("JET_JER_NP3","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP4 = Systematic("JET_JER_NP4","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP5 = Systematic("JET_JER_NP5","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP6 = Systematic("JET_JER_NP6","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP7 = Systematic("JET_JER_NP7","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)                       
JET_JER_NP8 = Systematic("JET_JER_NP8","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
####   histoSys
nB_SYS                              = Systematic("B_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nC_SYS                              = Systematic("C_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nL_SYS                              = Systematic("L_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nE_SYS                              = Systematic("E_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                             
nEFC_SYS                            = Systematic("EFC_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                           
nJVT_SYS                            = Systematic("JVT_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                           
nJET_BJES_Response                  = Systematic("JET_BJES_Response","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_1                  = Systematic("JET_EffectiveNP_1","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_2                  = Systematic("JET_EffectiveNP_2","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_3                  = Systematic("JET_EffectiveNP_3","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_4                  = Systematic("JET_EffectiveNP_4","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_5                  = Systematic("JET_EffectiveNP_5","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_6                  = Systematic("JET_EffectiveNP_6","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_7                  = Systematic("JET_EffectiveNP_7","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_EffectiveNP_8restTerm          = Systematic("JET_EffectiveNP_8restTerm","_NoSys","_elEffup","_ElEffdown","tree","histoSys")         
nJET_EtaIntercalibration_Modelling  = Systematic("JET_EtaIntercalibration_Modelling","_NoSys","_elEffup","_ElEffdown","tree","histoSys") 
nJET_EtaIntercalibration_NonClosure = Systematic("JET_EtaIntercalibration_NonClosure","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nJET_EtaIntercalibration_TotalStat  = Systematic("JET_EtaIntercalibration_TotalStat","_NoSys","_elEffup","_ElEffdown","tree","histoSys") 
nJET_Flavor_Composition             = Systematic("JET_Flavor_Composition","_NoSys","_elEffup","_ElEffdown","tree","histoSys")            
nJET_Flavor_Response                = Systematic("JET_Flavor_Response","_NoSys","_elEffup","_ElEffdown","tree","histoSys")               
nJET_Pileup_OffsetMu                = Systematic("JET_Pileup_OffsetMu","_NoSys","_elEffup","_ElEffdown","tree","histoSys")               
nJET_Pileup_OffsetNPV               = Systematic("JET_Pileup_OffsetNPV","_NoSys","_elEffup","_ElEffdown","tree","histoSys")              
nJET_Pileup_PtTerm                  = Systematic("JET_Pileup_PtTerm","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                 
nJET_Pileup_RhoTopology             = Systematic("JET_Pileup_RhoTopology","_NoSys","_elEffup","_ElEffdown","tree","histoSys")            
nJET_PunchThrough_MC15              = Systematic("JET_PunchThrough_MC15","_NoSys","_elEffup","_ElEffdown","tree","histoSys")             
nJET_SingleParticle_HighPt          = Systematic("JET_SingleParticle_HighPt","_NoSys","_elEffup","_ElEffdown","tree","histoSys")         
nJET_JER_CROSS_CALIB_FORWARD        = Systematic("JET_JER_CROSS_CALIB_FORWARD","_NoSys","_elEffup","_ElEffdown","tree","histoSys")       
nJET_JER_NOISE_FORWARD              = Systematic("JET_JER_NOISE_FORWARD","_NoSys","_elEffup","_ElEffdown","tree","histoSys")             
nJET_JER_NP0                        = Systematic("JET_JER_NP0","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP1                        = Systematic("JET_JER_NP1","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP2                        = Systematic("JET_JER_NP2","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP3                        = Systematic("JET_JER_NP3","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP4                        = Systematic("JET_JER_NP4","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP5                        = Systematic("JET_JER_NP5","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP6                        = Systematic("JET_JER_NP6","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP7                        = Systematic("JET_JER_NP7","_NoSys","_elEffup","_ElEffdown","tree","histoSys")                       
nJET_JER_NP8                        = Systematic("JET_JER_NP8","_NoSys","_elEffup","_ElEffdown","tree","histoSys")

### theory systematics histoSys
PDFCHOICE_SYS1                          = Systematic("PDFCHOICE_SYS1","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
PDFCHOICE_SYS2                          = Systematic("PDFCHOICE_SYS2","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
MUF_SYS                                 = Systematic("MUF_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
MUR_SYS                                 = Systematic("MUR_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
ALPHA_SYS                               = Systematic("ALPHA_SYS","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
PDF_SYS_ENVELOPE                        = Systematic("PDF_SYS_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
QCD_SCALE_ENVELOPE                      = Systematic("QCD_SCALE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
PDF_COICE_ENVELOPE                      = Systematic("PDF_COICE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree",fittedBkgSys)
# histoSys
nPDFCHOICE_SYS1                         = Systematic("PDFCHOICE_SYS1","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDFCHOICE_SYS2                         = Systematic("PDFCHOICE_SYS2","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nMUF_SYS                                = Systematic("MUF_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nMUR_SYS                                = Systematic("MUR_SYS","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDF_SYS_ENVELOPE                       = Systematic("PDF_SYS_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDF_SYS_ENVELOPE                       = Systematic("PDF_SYS_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nQCD_SCALE_ENVELOPE                     = Systematic("QCD_SCALE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
nPDF_COICE_ENVELOPE                     = Systematic("PDF_COICE_ENVELOPE","_NoSys","_elEffup","_ElEffdown","tree","histoSys")

#fake correction systematics 
elfakesys = Systematic("FF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")
mufakesys = Systematic("MUFF","_NoSys","_elEffup","_ElEffdown","tree","histoSys")

#nominal name of the histograms with systematic variation
configMgr.nomName = "_NoSys"

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

#backgrounds
topSample = Sample("top_physics",kGreen-9)
topSample.setNormByTheory() #scales with lumi
topSample.setStatConfig(useStat)
topSample.xsecUp = 1.2
topSample.xsecDown = 0.8
# topSample.addSystematic(nelchflip)
topSample.addSystematic(neleff)
topSample.addSystematic(neltrig)
topSample.addSystematic(nelreco)
topSample.addSystematic(neliso)
topSample.addSystematic(nelreso)
topSample.addSystematic(nelescale)
topSample.addSystematic(nelscintillator)
topSample.addSystematic(nB_SYS                             )
topSample.addSystematic(nC_SYS                             )
topSample.addSystematic(nL_SYS                             )
topSample.addSystematic(nE_SYS                             )
topSample.addSystematic(nEFC_SYS                           )
topSample.addSystematic(nJVT_SYS                           )
topSample.addSystematic(nJET_BJES_Response                 )
topSample.addSystematic(nJET_EffectiveNP_1                 )
topSample.addSystematic(nJET_EffectiveNP_2                 )
topSample.addSystematic(nJET_EffectiveNP_3                 )
topSample.addSystematic(nJET_EffectiveNP_4                 )
topSample.addSystematic(nJET_EffectiveNP_5                 )
topSample.addSystematic(nJET_EffectiveNP_6                 )
topSample.addSystematic(nJET_EffectiveNP_7                 )
topSample.addSystematic(nJET_EffectiveNP_8restTerm         )
topSample.addSystematic(nJET_EtaIntercalibration_Modelling )
topSample.addSystematic(nJET_EtaIntercalibration_NonClosure)
topSample.addSystematic(nJET_EtaIntercalibration_TotalStat )
topSample.addSystematic(nJET_Flavor_Composition            )
topSample.addSystematic(nJET_Flavor_Response               )
topSample.addSystematic(nJET_Pileup_OffsetMu               )
topSample.addSystematic(nJET_Pileup_OffsetNPV              )
topSample.addSystematic(nJET_Pileup_PtTerm                 )
topSample.addSystematic(nJET_Pileup_RhoTopology            )
topSample.addSystematic(nJET_PunchThrough_MC15             )
topSample.addSystematic(nJET_SingleParticle_HighPt         )
topSample.addSystematic(nJET_JER_CROSS_CALIB_FORWARD       )
topSample.addSystematic(nJET_JER_NOISE_FORWARD             )
topSample.addSystematic(nJET_JER_NP0                       )
topSample.addSystematic(nJET_JER_NP1                       )
topSample.addSystematic(nJET_JER_NP2                       )
topSample.addSystematic(nJET_JER_NP3                       )
topSample.addSystematic(nJET_JER_NP4                       )
topSample.addSystematic(nJET_JER_NP5                       )
topSample.addSystematic(nJET_JER_NP6                       )
topSample.addSystematic(nJET_JER_NP7                       )
topSample.addSystematic(nJET_JER_NP8                       )
#topSample.addSystematic(elttGen)
#topSample.addSystematic(elttRad)
#topSample.addSystematic(elttHadron)
#topSample.addSystematic(eltopmod)
topSample.addSystematic(nmueff1  )
topSample.addSystematic(nmueff2  )
topSample.addSystematic(nmutrig1 )
topSample.addSystematic(nmutrig2 )
topSample.addSystematic(nmuiso1  )
topSample.addSystematic(nmuiso2  )
topSample.addSystematic(nmuttva1 )
topSample.addSystematic(nmuttva2 )
topSample.addSystematic(nmuTreeID    )
topSample.addSystematic(nmuTreeMS    )
topSample.addSystematic(nmuTreeRES   )
topSample.addSystematic(nmuTreeRHO   )
topSample.addSystematic(nmuTreeSCALE )

if not onlySS:
  topSampleOS = Sample("top_ttV",kGreen-9)
  topSampleOS.setNormByTheory() #scales with lumi
  topSampleOS.setStatConfig(useStat)
  topSampleOS.setNormFactor("mu_top",1.,0.,5000.)
  topSampleOS.setNormRegions([('SR_emos',OSvar)])
  for sysName in sysNames:
    topSampleOS.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
  for oneSidedSysName in oneSidedSysNames:
    topSampleOS.addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )


zSample = Sample("SherpaDY221",kAzure+1)
zSample.setNormByTheory()
zSample.setStatConfig(useStat)
zSample.setNormFactor("mu_ZSS",1.,0.,5000.)
zSample.setNormRegions([("SSZCRee","Mjj")])
# zSample.addSystematic(elchflip)
zSample.addSystematic(eleff)
zSample.addSystematic(eltrig)
zSample.addSystematic(elreco)
zSample.addSystematic(eliso)
zSample.addSystematic(elreso)
zSample.addSystematic(elescale)
zSample.addSystematic(elscintillator)
zSample.addSystematic(B_SYS                             )
zSample.addSystematic(C_SYS                             )
zSample.addSystematic(L_SYS                             )
zSample.addSystematic(E_SYS                             )
zSample.addSystematic(EFC_SYS                           )
zSample.addSystematic(JVT_SYS                           )
zSample.addSystematic(JET_BJES_Response                 )
zSample.addSystematic(JET_EffectiveNP_1                 )
zSample.addSystematic(JET_EffectiveNP_2                 )
zSample.addSystematic(JET_EffectiveNP_3                 )
zSample.addSystematic(JET_EffectiveNP_4                 )
zSample.addSystematic(JET_EffectiveNP_5                 )
zSample.addSystematic(JET_EffectiveNP_6                 )
zSample.addSystematic(JET_EffectiveNP_7                 )
zSample.addSystematic(JET_EffectiveNP_8restTerm         )
zSample.addSystematic(JET_EtaIntercalibration_Modelling )
zSample.addSystematic(JET_EtaIntercalibration_NonClosure)
zSample.addSystematic(JET_EtaIntercalibration_TotalStat )
zSample.addSystematic(JET_Flavor_Composition            )
zSample.addSystematic(JET_Flavor_Response               )
zSample.addSystematic(JET_Pileup_OffsetMu               )
zSample.addSystematic(JET_Pileup_OffsetNPV              )
zSample.addSystematic(JET_Pileup_PtTerm                 )
zSample.addSystematic(JET_Pileup_RhoTopology            )
zSample.addSystematic(JET_PunchThrough_MC15             )
zSample.addSystematic(JET_SingleParticle_HighPt         )
zSample.addSystematic(JET_JER_CROSS_CALIB_FORWARD       )
zSample.addSystematic(JET_JER_NOISE_FORWARD             )
zSample.addSystematic(JET_JER_NP0                       )
zSample.addSystematic(JET_JER_NP1                       )
zSample.addSystematic(JET_JER_NP2                       )
zSample.addSystematic(JET_JER_NP3                       )
zSample.addSystematic(JET_JER_NP4                       )
zSample.addSystematic(JET_JER_NP5                       )
zSample.addSystematic(JET_JER_NP6                       )
zSample.addSystematic(JET_JER_NP7                       )
zSample.addSystematic(JET_JER_NP8                       )
zSample.addSystematic(ALPHA_SYS                         )
zSample.addSystematic(PDF_SYS_ENVELOPE                  )
zSample.addSystematic(QCD_SCALE_ENVELOPE                )
zSample.addSystematic(PDF_COICE_ENVELOPE                )
# zSample.addSystematic(PDFCHOICE_SYS1                    )
# zSample.addSystematic(PDFCHOICE_SYS2                    )
# zSample.addSystematic(MUF_SYS                           )
# zSample.addSystematic(MUR_SYS                           )
# zSample.addSystematic(mueff1  )
# zSample.addSystematic(mueff2  )
# zSample.addSystematic(mutrig1 )
# zSample.addSystematic(mutrig2 )
# zSample.addSystematic(muiso1  )
# zSample.addSystematic(muiso2  )
# zSample.addSystematic(muttva1 )
# zSample.addSystematic(muttva2 )
# zSample.addSystematic(muTreeID    )
# zSample.addSystematic(muTreeMS    )
# zSample.addSystematic(muTreeRES   )
# zSample.addSystematic(muTreeRHO   )
# zSample.addSystematic(muTreeSCALE )

if not onlySS:
  zSampleOS = Sample("Zjets",kAzure+1)
  zSampleOS.setNormByTheory() #scales with lumi
  zSampleOS.setStatConfig(useStat)
  zSampleOS.setNormFactor("mu_ZOS",1.,0.,5000.)
  zSampleOS.setNormRegions([('ZCR_eeos','cuts'),('ZCR_mmos','cuts')])
  for sysName in sysNames:
    zSampleOS.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
  for oneSidedSysName in oneSidedSysNames:
    zSampleOS.addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )
  for ZjetsTheorySysName in ZjetsTheorySysNames:
    zSampleOS.addSystematic( Systematic( "%s" % ( ZjetsTheorySysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys ) )
  for oneSidedZjetsTheorySysName in oneSidedZjetsTheorySysNames:
    zSampleOS.addSystematic( Systematic( "%s" % ( oneSidedZjetsTheorySysName ), "Nom", "dummy", "dummy", "tree", fittedBkgSys+"OneSide" ) )

dbSample = Sample("dibosonSherpa",kYellow-3)
dbSample.setNormByTheory()
dbSample.setStatConfig(useStat)
dbSample.setNormFactor("mu_DB",1.,0.,5000.)
dbSample.setNormRegions([("SSZCRmm","Mjj"),("SSZCRee","Mjj")])
# dbSample.addSystematic(elchflip)
dbSample.addSystematic(eleff)
dbSample.addSystematic(eltrig)
dbSample.addSystematic(elreco)
dbSample.addSystematic(eliso)
dbSample.addSystematic(elreso)
dbSample.addSystematic(elescale)
dbSample.addSystematic(elscintillator)
dbSample.addSystematic(B_SYS                             )
dbSample.addSystematic(C_SYS                             )
dbSample.addSystematic(L_SYS                             )
dbSample.addSystematic(E_SYS                             )
dbSample.addSystematic(EFC_SYS                           )
dbSample.addSystematic(JVT_SYS                           )
dbSample.addSystematic(JET_BJES_Response                 )
dbSample.addSystematic(JET_EffectiveNP_1                 )
dbSample.addSystematic(JET_EffectiveNP_2                 )
dbSample.addSystematic(JET_EffectiveNP_3                 )
dbSample.addSystematic(JET_EffectiveNP_4                 )
dbSample.addSystematic(JET_EffectiveNP_5                 )
dbSample.addSystematic(JET_EffectiveNP_6                 )
dbSample.addSystematic(JET_EffectiveNP_7                 )
dbSample.addSystematic(JET_EffectiveNP_8restTerm         )
dbSample.addSystematic(JET_EtaIntercalibration_Modelling )
dbSample.addSystematic(JET_EtaIntercalibration_NonClosure)
dbSample.addSystematic(JET_EtaIntercalibration_TotalStat )
dbSample.addSystematic(JET_Flavor_Composition            )
dbSample.addSystematic(JET_Flavor_Response               )
dbSample.addSystematic(JET_Pileup_OffsetMu               )
dbSample.addSystematic(JET_Pileup_OffsetNPV              )
dbSample.addSystematic(JET_Pileup_PtTerm                 )
dbSample.addSystematic(JET_Pileup_RhoTopology            )
dbSample.addSystematic(JET_PunchThrough_MC15             )
dbSample.addSystematic(JET_SingleParticle_HighPt         )
dbSample.addSystematic(JET_JER_CROSS_CALIB_FORWARD       )
dbSample.addSystematic(JET_JER_NOISE_FORWARD             )
dbSample.addSystematic(JET_JER_NP0                       )
dbSample.addSystematic(JET_JER_NP1                       )
dbSample.addSystematic(JET_JER_NP2                       )
dbSample.addSystematic(JET_JER_NP3                       )
dbSample.addSystematic(JET_JER_NP4                       )
dbSample.addSystematic(JET_JER_NP5                       )
dbSample.addSystematic(JET_JER_NP6                       )
dbSample.addSystematic(JET_JER_NP7                       )
dbSample.addSystematic(JET_JER_NP8                       )
dbSample.addSystematic(mueff1  )
dbSample.addSystematic(mueff2  )
dbSample.addSystematic(mutrig1 )
dbSample.addSystematic(mutrig2 )
dbSample.addSystematic(muiso1  )
dbSample.addSystematic(muiso2  )
dbSample.addSystematic(muttva1 )
dbSample.addSystematic(muttva2 )
dbSample.addSystematic(muTreeID    )
dbSample.addSystematic(muTreeMS    )
dbSample.addSystematic(muTreeRES   )
dbSample.addSystematic(muTreeRHO   )
dbSample.addSystematic(muTreeSCALE )
dbSample.addSystematic(ALPHA_SYS                         )
dbSample.addSystematic(PDF_SYS_ENVELOPE                  )
dbSample.addSystematic(QCD_SCALE_ENVELOPE                )
dbSample.addSystematic(PDF_COICE_ENVELOPE                )
# dbSample.addSystematic(PDFCHOICE_SYS1                    )
# dbSample.addSystematic(PDFCHOICE_SYS2                    )
# dbSample.addSystematic(MUF_SYS                           )
# dbSample.addSystematic(MUR_SYS                           )

if not onlySS:
  dbSampleOS = Sample("Wjets_Diboson",kYellow-3)
  dbSampleOS.legName = "DB + Wjets"
  dbSampleOS.setNormByTheory() #scales with lumi
  dbSampleOS.setStatConfig(useStat)
  for sysName in sysNames:
    dbSampleOS.addSystematic( Systematic( "%s" % ( sysName ), "Nom", "dummy", "dummy", "tree", "histoSys" ) )
  for oneSidedSysName in oneSidedSysNames:
    dbSampleOS.addSystematic( Systematic( "%s" % ( oneSidedSysName ), "Nom", "dummy", "dummy", "tree", "histoSys"+"OneSide" ) )


fakesSample = Sample("fakes",kGray+1) 
fakesSample.setStatConfig(True) # problematic, statistics-driven is wrong here?

#data
dataSample = Sample("Data",kBlack)
dataSample.setData()


#**************
# fit
#**************

## Bkg-only template
commonSamples = [dataSample,fakesSample,topSample,dbSample,zSample]
if not onlySS:
  commonSamples += [zSampleOS,topSampleOS,dbSampleOS]

## Parameters of the Measurement
measName = "NormalMeasurement"
measLumi = 1.
measLumiError = 0.021 # 2015+16

bkgOnly = configMgr.addFitConfig("Template_BkgOnly")
if useStat:
    bkgOnly.statErrThreshold=0.05 #values above this will be considered in the fit
else:
    bkgOnly.statErrThreshold=None
bkgOnly.addSamples(commonSamples)
# bkgOnly.addSamples([sigSample])
# bkgOnly.setSignalSample(sigSample)
meas = bkgOnly.addMeasurement(measName,measLumi,measLumiError)
meas.addPOI("mu_SIG")

CRs = []
VRs = []
SRs = []

channelSSZCRee = bkgOnly.addChannel("Mjj",["SSZCRee"],12,0,12)
channelSSZCRee.hasB = False
channelSSZCRee.hasBQCD = False
channelSSZCRee.useOverflowBin = False
channelSSZCRee.getSample("SherpaDY221").addSystematic(elchflip)
channelSSZCRee.getSample("dibosonSherpa").addSystematic(elchflip)
channelSSZCRee.getSample("top_physics").addSystematic(nelchflip)
channelSSZCRee.getSample("fakes").addSystematic(elfakesys)
if not onlySS:
  channelSSZCRee.removeSample("top_ttV")
  channelSSZCRee.removeSample("Zjets")
  channelSSZCRee.removeSample("Wjets_Diboson")
CRs += [channelSSZCRee]

channelSSZVRee = bkgOnly.addChannel("HT",["SSZVRee"],8,0,8)
channelSSZVRee.hasB = False
channelSSZVRee.hasBQCD = False
channelSSZVRee.useOverflowBin = False
channelSSZVRee.getSample("SherpaDY221").addSystematic(elchflip)
channelSSZVRee.getSample("dibosonSherpa").addSystematic(elchflip)
channelSSZVRee.getSample("top_physics").addSystematic(nelchflip)
channelSSZVRee.getSample("fakes").addSystematic(elfakesys)
if not onlySS:
  channelSSZVRee.removeSample("top_ttV")
  channelSSZVRee.removeSample("Zjets")
  channelSSZVRee.removeSample("Wjets_Diboson")
# channelSSZVRee.removeSample(sigSamples[0])
VRs += [channelSSZVRee]

channelSSZSRee = bkgOnly.addChannel("HT",["SSZSRee"],8,0,8)
channelSSZSRee.hasB = False
channelSSZSRee.hasBQCD = False
channelSSZSRee.useOverflowBin = False
channelSSZSRee.getSample("SherpaDY221").addSystematic(elchflip)
channelSSZSRee.getSample("dibosonSherpa").addSystematic(elchflip)
channelSSZSRee.getSample("top_physics").addSystematic(nelchflip)
channelSSZSRee.getSample("fakes").addSystematic(elfakesys)
if not onlySS:
  channelSSZSRee.removeSample("top_ttV")
  channelSSZSRee.removeSample("Zjets")
  channelSSZSRee.removeSample("Wjets_Diboson")
# channelSSZSRee.removeSample(sigSamples[0])
SRs += [channelSSZSRee]

channelSSZCRmm = bkgOnly.addChannel("Mjj",["SSZCRmm"],5,0,5)
channelSSZCRmm.hasB = False
channelSSZCRmm.hasBQCD = False
channelSSZCRmm.useOverflowBin = False
channelSSZCRmm.getSample("fakes").addSystematic(mufakesys)
channelSSZCRmm.removeSample("SherpaDY221")
if not onlySS:
  channelSSZCRmm.removeSample("top_ttV")
  channelSSZCRmm.removeSample("Zjets")
  channelSSZCRmm.removeSample("Wjets_Diboson")
# channelSSZCRmm.removeSample(sigSamples[0])
CRs += [channelSSZCRmm]

channelSSZVRmm = bkgOnly.addChannel("HT",["SSZVRmm"],8,0,8)
channelSSZVRmm.hasB = False
channelSSZVRmm.hasBQCD = False
channelSSZVRmm.useOverflowBin = False
channelSSZVRmm.getSample("fakes").addSystematic(mufakesys)
channelSSZVRmm.removeSample("SherpaDY221")
if not onlySS:
  channelSSZVRmm.removeSample("top_ttV")
  channelSSZVRmm.removeSample("Zjets")
  channelSSZVRmm.removeSample("Wjets_Diboson")
# channelSSZVRmm.removeSample(sigSamples[0])
VRs += [channelSSZVRmm]

channelSSZSRmm = bkgOnly.addChannel("HT",["SSZSRmm"],8,0,8)
channelSSZSRmm.hasB = False
channelSSZSRmm.hasBQCD = False
channelSSZSRmm.useOverflowBin = False
channelSSZSRmm.getSample("fakes").addSystematic(mufakesys)
channelSSZSRmm.removeSample("SherpaDY221")
if not onlySS:
  channelSSZSRmm.removeSample("top_ttV")
  channelSSZSRmm.removeSample("Zjets")
  channelSSZSRmm.removeSample("Wjets_Diboson")
# channelSSZSRmm.removeSample(sigSamples[0])
SRs += [channelSSZSRmm]

if not onlySS:
  ZCRee = bkgOnly.addChannel( 'cuts', [ 'ZCR_eeos' ], 1, 0.5, 1.5 )
  ZCRee.hasB = False
  ZCRee.hasBQCD = False
  ZCRee.useOverflowBin = False
  ZCRee.removeSample("top_physics")
  ZCRee.removeSample("SherpaDY221")
  ZCRee.removeSample("dibosonSherpa")
  ZCRee.removeSample("fakes")
  CRs += [ZCRee]
  ZCRmm = bkgOnly.addChannel( 'cuts', [ 'ZCR_mmos' ], 1, 0.5, 1.5 )
  ZCRmm.hasB = False
  ZCRmm.hasBQCD = False
  ZCRmm.useOverflowBin = False
  ZCRmm.removeSample("top_physics")
  ZCRmm.removeSample("SherpaDY221")
  ZCRmm.removeSample("dibosonSherpa")
  ZCRmm.removeSample("fakes")
  CRs += [ZCRmm]
  mixedCR = bkgOnly.addChannel( OSvar, [ 'SR_emos' ], 18, 0, 9000 )
  mixedCR.hasB = False
  mixedCR.hasBQCD = False
  mixedCR.useOverflowBin = False
  mixedCR.removeSample("top_physics")
  mixedCR.removeSample("SherpaDY221")
  mixedCR.removeSample("dibosonSherpa")
  mixedCR.removeSample("fakes")
  CRs += [mixedCR]

  ZVRee = bkgOnly.addChannel( OSvar, [ 'ZVR_eeos' ], 18, 0, 9000 )
  ZVRee.hasB = False
  ZVRee.hasBQCD = False
  ZVRee.useOverflowBin = False
  ZVRee.removeSample("top_physics")
  ZVRee.removeSample("SherpaDY221")
  ZVRee.removeSample("dibosonSherpa")
  ZVRee.removeSample("fakes")
  VRs += [ZVRee]
  ZVRmm = bkgOnly.addChannel( OSvar, [ 'ZVR_mmos' ], 18, 0, 9000 )
  ZVRmm.hasB = False
  ZVRmm.hasBQCD = False
  ZVRmm.useOverflowBin = False
  ZVRmm.removeSample("top_physics")
  ZVRmm.removeSample("SherpaDY221")
  ZVRmm.removeSample("dibosonSherpa")
  ZVRmm.removeSample("fakes")
  VRs += [ZVRmm]

  SRee = bkgOnly.addChannel( OSvar, [ 'SR_eeos' ], 18, 0, 9000 )
  SRee.hasB = False
  SRee.hasBQCD = False
  SRee.useOverflowBin = False
  SRee.removeSample("top_physics")
  SRee.removeSample("SherpaDY221")
  SRee.removeSample("dibosonSherpa")
  SRee.removeSample("fakes")
  SRs += [SRee]
  SRmm = bkgOnly.addChannel( OSvar, [ 'SR_mmos' ], 18, 0, 9000 )
  SRmm.hasB = False
  SRmm.hasBQCD = False
  SRmm.useOverflowBin = False
  SRmm.removeSample("top_physics")
  SRmm.removeSample("SherpaDY221")
  SRmm.removeSample("dibosonSherpa")
  SRmm.removeSample("fakes")
  SRs += [SRmm]

## CRs:
bkgOnly.addBkgConstrainChannels(CRs)
# bkgOnly.addBkgConstrainChannels([channelSSZCRee,channelSSZCRmm,ZCRee,mixedCR])
# bkgOnly.addBkgConstrainChannels([channelSSZCRee,channelSSZCRmm])
## VRs:
bkgOnly.addValidationChannels(VRs)
# bkgOnly.addValidationChannels([channelSSZVRee,channelSSZVRmm])
## SRs:
bkgOnly.addSignalChannels(SRs)
# bkgOnly.addSignalChannels([channelSSZSRee,channelSSZSRmm])
# Set global plotting colors/styles
bkgOnly.dataColor = dataSample.color
bkgOnly.totalPdfColor = kBlack
bkgOnly.errorFillColor = kBlue-5
bkgOnly.errorFillStyle = 3004
bkgOnly.errorLineStyle = kDashed
bkgOnly.errorLineColor = kBlue-5

# Set Channel titleX, titleY, minY, maxY, logY
# for channel in CRVRs:
#     channel.titleX = "m(jj) bins"
#     channel.titleY = "Events"
#     channel.ATLASLabelX = 0.25
#     channel.ATLASLabelY = 0.85
#     channel.ATLASLabelText = "internal"
#     channel.showLumi = True
# for channel in SRs:
#     channel.titleX = "H_{T} bins"
#     channel.titleY = "Events"
#     channel.ATLASLabelX = 0.25
#     channel.ATLASLabelY = 0.85
#     channel.ATLASLabelText = "internal"
#     channel.showLumi = True

